<?php
    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */

    function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }

    // Check if the event is already in array
    function isInArray($haystack, $needle) {
        foreach ($haystack as $object) {
            if ($object->id == $needle) {
                return true;
            }
        }

        return false;
    }

    date_default_timezone_set('UTC');

    $cdn_url = "http://cdn.boogiecall.com";
    $web_url = "http://www.boogiecall.com";
    $maximumDistance = 40000;
    $limitNumberOfTags = 20;

    // Connect to Memcached
    if (!isset($m)) {
        $m = new Memcached();
        $m->addServer('127.0.0.1', 11211);
    }