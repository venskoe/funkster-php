<?php
    // This name is used by Android client
    $array['pages'] = array();

    function isInArray($haystack, $needle) {
        foreach ($haystack as $object) {
            if ($object->id == $needle) {
                return true;
            }
        }

        return false;
    }

    if (!empty($_REQUEST['latlng']) && $_REQUEST['latlng'] !== "null") {
        // Connect to Memcached
        if(!isset($m)) {
            $m = new Memcached();
            $m->addServer('127.0.0.1', 11211);
        }

        list($latitude, $longitude) = explode(',', addslashes($_REQUEST['latlng']));

        // Try in memcached first
        if (!($decodedDump = $m->get(round($latitude, 3) . '-' . round($longitude, 3)))) {
            $webApiUrl = "http://www.boogiecall.com/api/v1/events/search/position?latitude=" . $latitude . "&longitude=" . $longitude;
            $jsonObject = file_get_contents($webApiUrl);
            $decodedDump = json_decode($jsonObject);
            $m->set(round($latitude, 3) . '-' . round($longitude, 3), $decodedDump, time() + 300);
            $usingCache = false;
        } else { $usingCache = true; }

        if (!($events = $m->get("eventsCache"))) {
            $events = array();
        }

        // Initialize arrays
        $places = array();

        foreach($decodedDump->eventList as $singleEvent) {
            // Parse the event list first
            $variableName = sprintf("%s", $singleEvent->place->id);

            if (!$usingCache && !isInArray($events, $singleEvent->id)) {
                $events[] = $singleEvent;
            }

            if (!isset($places[$variableName])) {
                // First gig for this venue
                $places[$variableName] = array();
                $places[$variableName]['id'] = $singleEvent->place->id;
                $places[$variableName]['name'] = $singleEvent->place->title;
                $places[$variableName]['lat'] = $singleEvent->place->latitude;
                $places[$variableName]['long'] = $singleEvent->place->longitude;
                $places[$variableName]['eventsToday'] = 0;
                $places[$variableName]['eventsThisWeek'] = 0;
                $places[$variableName]['eventsThisMonth'] = 0;
            }

            // Let's use time difference instead
            if ($singleEvent->startsAt - time() < 72000 || ((time() > $singleEvent->startsAt) && time() < $singleEvent->endsAt)) {
                $places[$variableName]['eventsToday']++;
            }

            if ($singleEvent->startsAt - time() < 604800) {
                $places[$variableName]['eventsThisWeek']++;
            }

            if ($singleEvent->startsAt - time() < 18144000) {
                $places[$variableName]['eventsThisMonth']++;
            }

            // Update counters
            /*
            if (date("Ymd", $singleEvent->startsAt) == date("Ymd")) {
                $places[$variableName]['eventsToday']++;
            }

            if (date("YW", $singleEvent->startsAt) == date("YW")) {
                $places[$variableName]['eventsThisWeek']++;
            }

            if (date("Ym", $singleEvent->startsAt) == date("Ym")) {
                $places[$variableName]['eventsThisMonth']++;
            }
            */
        }

        $m->set("eventsCache", $events, 0);

        foreach($places as $place) {
            array_push($array['pages'], array(
                'id' => $place['id'],
                'pageid' => $place['name'],
                'name' => $place['name'],
                'lat' => $place['lat'],
                'lon' => $place['long'],
                'eventstoday' => $place['eventsToday'],
                'eventsweek' => $place['eventsThisWeek'],
                'eventsmonth' => $place['eventsThisMonth']));
        }
    }

    echo json_encode($array);

