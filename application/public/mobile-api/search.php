<?php
$cdn_url = "http://www.boogiecall.com";

function haversineGreatCircleDistance(
    $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return $angle * $earthRadius;
}

if (!empty($_REQUEST['query'])) {
    // Connect to Memcached
    if(!isset($m)) {
        $m = new Memcached();
        $m->addServer('127.0.0.1', 11211);
    }

    $array['events'] = array();
    $events = $m->get("eventsCache");

    foreach($events as $singleEvent) {
        // Todo - check the distance

        if ((stristr($singleEvent->title, $_REQUEST['query']) !== false || stristr($singleEvent->place->title, $_REQUEST['query'])) && $singleEvent->startsAt > time()) {
            $tagLine = "";
            foreach($singleEvent->tagList as $oneTag) {
                $tagLine .= "#" . $oneTag->title . " ";
            }

            array_push($array['events'], array(
                'id' => $singleEvent->id,
                'eventid' => $singleEvent->id,
                'pageid' => $singleEvent->place->title,
                'name' => $singleEvent->title,
                'tags' => $tagLine,
                'start' => date("Y-m-d H:i:s", $singleEvent->startsAt),
                'location' => $singleEvent->place->title,
                'address' => $singleEvent->place->address,
                'lat' => $singleEvent->place->latitude,
                'lon' => $singleEvent->place->longitude,
                'picture' => $cdn_url . stripslashes($singleEvent->cover->small)));
        }
    }

    echo json_encode($array);
}

