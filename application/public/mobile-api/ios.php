<?php
    require("ios_common.php");

    if (!empty($_REQUEST['latlng']) && $_REQUEST['latlng'] !== "null") {
        list($latitude, $longitude) = explode(',', addslashes($_REQUEST['latlng']));

        // Try in memcached first
        if (!($decodedDump = $m->get(round($latitude, 3) . '-' . round($longitude, 3)))) {
            $webApiUrl = $web_url . "/api/v1/events/search/position?latitude=" . $latitude . "&longitude=" . $longitude;
            $jsonObject = file_get_contents($webApiUrl);
            $decodedDump = json_decode($jsonObject);
            $m->set(round($latitude, 3) . '-' . round($longitude, 3), $decodedDump, time() + 300);
            $usingCache = false;
        } else { $usingCache = true; }

        if (!($events = $m->get("eventsCache"))) {
            $events = array();
        }

        $array['photos'] = array();
        $array['categories'] = array();
        $array['stores'] = array();
        $tags = array();
        $counters = array();

        // $events = $m->get("eventsCache");
        $photoNo = 0;

        foreach ($decodedDump->eventList as $singleEvent) {
            $tagLine = "";

            // Account for timezone
            //$singleEvent->startsAt -= ($singleEvent->tzOffset * 60);
            if (!$usingCache && !isInArray($events, $singleEvent->id)) {
                $events[] = $singleEvent;
            }

            // The event has already ended
            if (time() > $singleEvent->endsAt) {
            // if (time() - $singleEvent->startsAt > 10800) {
                continue;
            }

            // The event will start more than 3 months later
            if ($singleEvent->startsAt - time() > 7776000) {
                continue;
            }

            // If distance is more than 40km
            if (haversineGreatCircleDistance($latitude, $longitude, $singleEvent->place->latitude, $singleEvent->place->longitude) > $maximumDistance) {
                continue;
            }

            foreach ($singleEvent->tagList as $oneTag) {
                $tagLine .= "#" . $oneTag->title . " ";
            }

            $artistList = "";
            foreach ($singleEvent->artistList as $oneArtist) {
                $artistList .= $oneArtist->title . " (" . $oneArtist->hometown . ") ";
            }

            $photoNo++;
            array_push($array['photos'], array(
                'store_id' => $singleEvent->id,
                'photo_id' => strval($photoNo),
                'photo_url' => $cdn_url . stripslashes($singleEvent->cover->small),
                'thumb_url' => $cdn_url . stripslashes($singleEvent->cover->small),
                'created_at' => strval(time()),
                'updated_at' => strval(time()),
                'is_deleted' => "0"
            ));

            array_push($array['stores'], array(
                'store_id' => $singleEvent->id,
                'store_name' => $singleEvent->title,
                'category_id' => "1",
                // 'tags' => $tagLine,
                // 'start' => date("Y-m-d H:i:s", $singleEvent->startsAt),
                'store_desc' => $singleEvent->place->title,
                'store_address' => $singleEvent->place->address,
                'cover_small' => $cdn_url . stripslashes($singleEvent->cover->small),
                'sms_no' => "",
                'phone_no' => "",
                'email' => "",
                'website' => $web_url . "/en/events/" . $singleEvent->id,
                'artist_list' => $artistList,
                'tag_list' => $tagLine,
                'starts_at' => $singleEvent->startsAt,
                'created_at' => strval(time()),
                'updated_at' => strval(time()),
                'featured' => "1",
                'is_deleted' => "0",
                'rating_total' => "5",
                'rating_count' => "1",
                'lat' => $singleEvent->place->latitude,
                'lon' => $singleEvent->place->longitude));
            //'picture' => $cdn_url . stripslashes($singleEvent->cover->small)));

            foreach ($singleEvent->tagList as $oneTag) {
                if (!in_array($oneTag, $tags)) {
                    $tags[] = $oneTag;
                    $counters[$oneTag->id] = 1;
                } else {
                    $counters[$oneTag->id]++;
                }
            }
        }

        $m->set("eventsCache", $events, 0);

        $tagNo = 0;

        foreach ($tags as $singleTag) {
            $tagNo++;

            array_push($array['categories'], array(
                'category_id' => $singleTag->title,
                'category' => $singleTag->title,
                'category_icon' => "",
                'created_at' => strval(time()),
                'updated_at' => strval(time()),
                'is_deleted' => "0"
            ));
        }

        echo json_encode($array);
    } else {
        return null;
    }