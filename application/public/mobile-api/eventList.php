<?php
    date_default_timezone_set('UTC');

    $cdn_url = "http://www.boogiecall.com";

    if (!empty($_REQUEST['name'])) {
        // Connect to Memcached
        if(!isset($m)) {
            $m = new Memcached();
            $m->addServer('127.0.0.1', 11211);
        }

        $array['events'] = array();
        $events = $m->get("eventsCache");

        foreach($events as $singleEvent) {
            // Account for timezone
            $singleEvent->startsAt -= ($singleEvent->tzOffset * 60);

            if ($singleEvent->place->title == $_REQUEST['name'] && time() < $singleEvent->endsAt) {
                $tagLine = "";
                foreach($singleEvent->tagList as $oneTag) {
                    $tagLine .= "#" . $oneTag->title . " ";
                }

                array_push($array['events'], array(

                    'id' => $singleEvent->id,
                    'eventid' => $singleEvent->id,
                    'pageid' => $singleEvent->place->title,
                    'name' => $singleEvent->title,
                    'tags' => $tagLine,
                    'start_raw' => $singleEvent->startsAt,
                    'start' => date("Y-m-d H:i:s", $singleEvent->startsAt),
                    'location' => $singleEvent->place->title,
                    'address' => $singleEvent->place->address,
                    'lat' => $singleEvent->place->latitude,
                    'lon' => $singleEvent->place->longitude,
                    'picture' => $cdn_url . stripslashes($singleEvent->cover->small)));
            }
        }

        function compareEvents($eventA, $eventB)
        {
            if ($eventA['start_raw'] == $eventB['start_raw']) {
                return 0;
            }
            return ($eventA['start_raw'] < $eventB['start_raw']) ? -1 : 1;
        }

        usort($array['events'], "compareEvents");

        echo json_encode($array);
    }

