<?php
    date_default_timezone_set('UTC');

    $cdn_url = "http://www.boogiecall.com";

    if (!empty($_REQUEST['eventid'])) {
        // Connect to Memcached
        if(!isset($m)) {
            $m = new Memcached();
            $m->addServer('127.0.0.1', 11211);
        }

        $array['info'] = array();
        $events = $m->get("eventsCache");

        foreach($events as $singleEvent) {
            if ($singleEvent->id == $_REQUEST['eventid']) {
                $tagLine = "";
                foreach($singleEvent->tagList as $oneTag) {
                    $tagLine .= "#" . $oneTag->title . " ";
                }

                // Account for timezone
                // temporarily commented out - Android applies TZ too
                //$singleEvent->startsAt -= ($singleEvent->tzOffset * 60);

                $artistList = "";
                foreach($singleEvent->artistList as $oneArtist) {
                    $artistList .= $oneArtist->title . " (" . $oneArtist->hometown . ")\n";
                }

                array_push($array['info'], array(
                    'id' => $singleEvent->id,
                    'eventid' => $singleEvent->id,
                    'pageid' => $singleEvent->place->title,
                    'name' => $singleEvent->title,
                    'tags' => $tagLine,
                    'start' => date("Y-m-d H:i:s", $singleEvent->startsAt),
                    'location' => $singleEvent->place->title,
                    'description' => $artistList,
                    'address' => $singleEvent->place->address,
                    'lat' => $singleEvent->place->latitude,
                    'lon' => $singleEvent->place->longitude,
                    'picture' => $cdn_url . stripslashes($singleEvent->cover->small),
                    'attending' => 0));
            }
        }

        echo json_encode($array);
    }

