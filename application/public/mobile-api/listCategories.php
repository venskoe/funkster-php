<?php
    require("ios_common.php");

    if (!empty($_REQUEST['latlng']) && $_REQUEST['latlng'] !== "null") {
        list($latitude, $longitude) = explode(',', addslashes($_REQUEST['latlng']));

        $array['categories'] = array();
        $events = $m->get("eventsCache");
        $tags = array();
        $counters = array();

        foreach ($events as $singleEvent) {
            // The event started more than 3 hours ago
            if (time() - $singleEvent->startsAt > 10800) {
                continue;
            }

            // The event will start more than 3 months later
            if ($singleEvent->startsAt - time() > 7776000) {
                continue;
            }

            // If distance is more than 40km
            if (haversineGreatCircleDistance($latitude, $longitude, $singleEvent->place->latitude, $singleEvent->place->longitude) > $maximumDistance) {
                continue;
            }

            foreach ($singleEvent->tagList as $oneTag) {
                if (!in_array($oneTag, $tags)) {
                    $tags[] = $oneTag;
                    $counters[$oneTag->id] = 1;
                } else {
                    $counters[$oneTag->id]++;
                }
            }
        }

        $tagNo = 0;

        foreach ($tags as $singleTag) {
            $tagNo++;

            if ($tagNo > $limitNumberOfTags) break;

            array_push($array['categories'], array(
                'category_id' => $singleTag->title,
                'category' => $singleTag->title . " (" . $counters[$singleTag->id] . ")",
                'category_icon' => "",
                'created_at' => strval(time()),
                'updated_at' => strval(time()),
                'is_deleted' => "0"
            ));
        }

        echo json_encode($array);
    } else {
        return null;
    }
