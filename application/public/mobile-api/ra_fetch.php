<?php
    error_reporting(E_ALL & ~(E_DEPRECATED | E_NOTICE));

    require("simple_html_dom.php");
    require(dirname(dirname(dirname(dirname(__FILE__)))) . "/vendor/autoload.php");

    date_default_timezone_set('UTC');

    define('MAXIMUM_FETCH_ERRORS', 20);
    $ra_website = "http://www.residentadvisor.net";
    $artist = array();

    // Check if the event is already in cache
    function eventAlreadyInDatabase($placeId, $eventDate) {
        global $fullEventList;

        foreach ($fullEventList as $singleEvent) {
            if ($singleEvent['place']['id'] == $placeId && date("Y-m-d", ($singleEvent['startsAt'] - ($singleEvent['tzOffset'] * 60))) == $eventDate) {
                return true;
            }
        }

        return false;
    }

    // Check if the artist is in our RA match table
    function artistAlreadyInDatabase($artistRALink) {
        global $artists;

        $parsedRALink = parse_url($artistRALink);

        foreach ($artists as $singleArtist) {
            if ($singleArtist['ra_link'] == $parsedRALink['path']) {
                return $singleArtist;
            }
        }

        return false;
    }

    class DB_Functions
    {
        private $db;
        private $neoClient;

        function __construct()
        {
	    $this->db = new DB_Connect();
	    $this->db->connect();
            $this->neoClient = new Everyman\Neo4j\Client('vps1.menara.com.au', 7474);
            //$this->neoClient->getTransport()->useHttps();
        }

	public function closeConnection()
        {
            $this->db->close();
        }

        public function fetchBoogieArtists($artistList = null) {

            function isInArray($haystack, $needle) {
                foreach ($haystack as $oneItem) {
                    if ($oneItem['artist_id'] == $needle) return true;
                }

                return false;
            }

            $queryString = "MATCH (artist:Artist) RETURN artist.id as id, artist.title as title";
            $query = new Everyman\Neo4j\Cypher\Query($this->neoClient, $queryString);
            $resultSet =  $query->getResultSet();

            $artists = array();

            foreach($resultSet as $result) {
                array_push($artists, array(
                    'artist_name' => $result['title'],
                    'artist_id' => $result['id']
                ));

                if ($artistList !== null && !isInArray($artistList, $result['id'])) {
                    // Add new artist to RA matching table
                    mysql_query("INSERT INTO artists_ra (artist_name, artist_id) values ('".$result['title']."', '".$result['id']."')");
                }
            }

            return $artists;
        }

        public function fetchBoogieEvents() {
            $queryString = "MATCH (artist:Artist)-[:PERFORMS_AT]->(event:Event)-[:HOSTED_AT]->(place:Place) RETURN event, collect(artist) as artistList, place";
            $query = new Everyman\Neo4j\Cypher\Query($this->neoClient, $queryString);
            $resultSet =  $query->getResultSet();

            $events = array();

            foreach($resultSet as $result) {
                //var_dump($result);
                //var_dump($result[1]->getProperties());

                $artistList = array();
                foreach($result[1] as $singleArtist) {
                    array_push($artistList, array(
                       'title' => $singleArtist->getProperty('title'),
                        'id' => $singleArtist->getProperty('id')
                    ));
                }

                array_push($events, array(
                    'startsAt' => $result[0]->getProperty('startsAt'),
                    'endsAt' => $result[0]->getProperty('endsAt'),
                    'tzOffset' => $result[0]->getProperty('tzOffset'),
                    'artistList' => $artistList,
                    'place' => array('title' => $result[2]->getProperty('title'), 'id' => $result[2]->getProperty('id')),
                    'title' => $result[0]->getProperty('title')));
            }

            return $events;
        }

        public function fetchRAPlaces()
        {
            $res = mysql_query("SELECT * FROM venues_ra where status='1'") or die(mysql_error());
            $array['places'] = array();

            while ($row = mysql_fetch_array($res))
                array_push($array['places'], array(
                    'id' => $row['id'],
                    'place_id' => $row['place_id'],
                    'ra_link' => $row['ra_link'],
                    'place_title' => $row['place_title'],
                    'tzOffset' => $row['tzOffset'],
                    'default_cover_big' => $row['default_cover_big'],
                    'default_cover_small' => $row['default_cover_small']
                ));

            return $array;
        }

        public function fetchRAArtists()
        {
            $res = mysql_query("SELECT * FROM artists_ra") or die(mysql_error());
            $array = array();

            while ($row = mysql_fetch_array($res)) {
                $parsedLink = parse_url($row['ra_link']);
                array_push($array, array(
                    'id' => $row['id'],
                    'artist_id' => $row['artist_id'],
                    'ra_link' => $parsedLink['path'],
                    'artist_name' => $row['artist_name'],
                    'tag' => $row['tag']
                ));
            }

            return $array;
        }

        public function createEvent($placeTitle, $eventTitle, $startsAt, $duration, $coverImageBig, $coverImageSmall, $artistList, $tagList)
        {
            $newEvent = array();

            if (mb_strlen($placeTitle) > 36) {
                $placeTitle = substr($placeTitle, 0, 36) . "...";
            }

            $newEvent['place'] = $placeTitle;
            $newEvent['title'] = $eventTitle;
            $newEvent['startsAt'] = $startsAt;
            $newEvent['duration'] = intval($duration);
            $newEvent['cover'] = array(
                'large' => $coverImageBig,
                'small' => $coverImageSmall
            );
            $newEvent['artistList'] = array_unique($artistList);
            $newEvent['tagList'] = $tagList;

            return $newEvent;
        }
    }

	class DB_Connect {
        // Connecting to database
        public function connect() {
            define("DB_HOST", "localhost");
            define("DB_USER", "boogie_tools");
            define("DB_PASSWORD", "woogie");
            define("DB_DATABASE", "boogie_tools");
            // connecting to mysql
            $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
            // selecting database
            mysql_select_db(DB_DATABASE);
            mysql_query("SET names UTF8");
            mysql_query('SET CHARACTER SET utf8_unicode_ci');

            // return database handler
            return $con;
        }

        // Closing database connection
        public function close() {
            mysql_close();
        }
    }


    setlocale (LC_ALL, 'utf-8');

    $db = new DB_Functions();
    $places = $db->fetchRAPlaces();
    $artists = $db->fetchRAArtists();

    //$fullArtistList = $db->fetchBoogieArtists();
    $fullEventList = $db->fetchBoogieEvents();
    $fullArtistList = $db->fetchBoogieArtists($artists);


    //var_dump($artists);
    //var_dump($fullEventList);

    $today = date("Y-m-d");
    $UTC = new DateTimeZone("UTC");
    $errorCounter = 0;

    foreach ($places['places'] as $place) {
        set_time_limit(60);
        $file = fopen("/tmp/ra_fetch.log", "a");

        echo "=== working on " . $place['place_id'] . " ===<br />";

        if ($errorCounter >= MAXIMUM_FETCH_ERRORS) {
            die("Too many errors, sorry");
        }

        // Fetch the venue page first, containing links to events
        if ($raPlacePage = file_get_html($place['ra_link'])) {
        } else {
            echo "Error fetching " . $place['ra_link'];
            $errorCounter++;
        }

        foreach($raPlacePage->find('ul#items') as $ul) {
            foreach($ul->find('li') as $li) {
                // echo $li->innertext . '<br>';

                $eventDate = $li->find('time', 0)->innertext;
                list($eventDate, $eventEmptyTime) = explode("T", $eventDate);
                $eventLink = $li->find('a', 0)->href;
                $eventTitle = $li->find('a', 1)->innertext;
                $artistList = $li->find('span.grey', 0);
                if ($artistList !== null) $artistList = strip_tags($artistList->innertext);

                echo "parsing event " . $eventTitle . " (" . $eventDate . ") " . $eventLink . " feat " . $artistList . "<br />";

                // We know the place and the date, we need to check if we have an event
                // on this day already. Only if not, continue
                if (!eventAlreadyInDatabase($place['place_id'], $eventDate)) {
                    echo "event absent in our db. fetching<br />";
//                     if ($raEventPage = file_get_html("http://demo.boogiecall.com/mockups/ra_test_event.html")) {
                    if ($raEventPage = file_get_html($ra_website . $eventLink)) {
                            $eventTime = $raEventPage->find('aside#detail ul li', 0);
                            list($day, $link, $time, $extra) = explode("<br />", $eventTime->innertext);
                            // echo "time found = " . $time . "<br />";
                            list($timeStart, $timeEnd) = explode("-", $time);
                            //$startsAt = strtotime($eventDate . " " . trim($timeStart));
                            $startsAt = trim($eventDate) . "T" . trim($timeStart) . ".00" .  $place['tzOffset'];

                            if (!empty($timeEnd)) {
                                list($startHours, $startMinutes) = explode(":", trim($timeStart));
                                list($endHours, $endMinutes) = explode(":", trim($timeEnd));

                                if (intval($startHours) > intval($endHours)) {
                                    $duration = (24 - intval($startHours)) + intval($endHours);
                                    $duration = $duration * 60;
                                    $duration = $duration - $startMinutes + $endMinutes;
                                } else {
                                    $duration = intval($endHours) - intval($startHours);
                                    $duration = $duration * 60;
                                    $duration = $duration - $startMinutes + $endMinutes;
                                }
                            } else {
                                $duration = 360;
                            }

                            $eventLineup = $raEventPage->find('p.lineup a');
                            $artistList = array();
                            $tagList = array();
                            $artistId = array();
                            $artistPlainList = "";

                            if ($eventLineup !== null) {
                                foreach ($eventLineup as $eventArtist) {
                                    $artistPlainList = $artistPlainList . $eventArtist->innertext . ", ";
                                    //echo "Found artist " . $eventArtist->innertext . " (" . $eventArtist->href . ")<br />";
                                    if ($artistId = artistAlreadyInDatabase($eventArtist->href)) {
                                        echo "we have this artist, id = " . $artistId['artist_id'] . "<br />";
                                        $artistList[] = $artistId['artist_name'];

                                        if (!in_array($artistId['tag'], $tagList)) $tagList[] = $artistId['tag'];
                                    }
                                }

                                $artistPlainList = substr($artistPlainList, 0, strlen($artistPlainList) - 2);
                            }

                            if ($coverImageBig = $raEventPage->find('div.flyer a', 0)->href) {
                                //echo "flyer found = " . $coverImageBig . "<br />";
                            }

                            if (count($artistList) == 0) {
                                echo "We don't have a single artist from this event, cancelling la<br />";
                                //continue;
                            } else {
                                $result = $db->createEvent($place['place_title'], $eventTitle, $startsAt, $duration, $place['default_cover_big'], $place['default_cover_small'], $artistList, $tagList);
                                //var_dump(json_encode($result));
                                echo "\n<br />=====<br />\n" . json_encode($result) . "\n<br />=====<br />\n";
                                fputs($file, json_encode($result) . "\n\n");
                                //die ("enough");
                            }

                        /**
                            if ($eventDescription = $raEventPage->find('div.left p', 1)->innertext && !empty($eventDescription)) {
                                echo "description found = <br />" . $eventDescription;
                            }
                         * */

                        // Sleep for 1 second
                        sleep(2);
                    } else {
                        echo "Error fetching";
                        $errorCounter++;
                    }
                }
            }
        }

    }

    fclose($file);
    $db->closeConnection();
