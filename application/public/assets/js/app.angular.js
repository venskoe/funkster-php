'use strict';

/**
 * @ngdoc module
 * @name bc
 */
angular.module('bc', [
    'menara',
    'bc.config',
    'iso',
    'ezfb',
    'toaster',
    'ipCookie',
    'ngImgCrop',
    'ui.bootstrap'
]);

/**
 * @ngdoc config
 * @module bc
 */
angular.module('bc').config([
    '$interpolateProvider',
    function($interpolateProvider){
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    }]);

/**
 * @ngdoc config
 * @module bc
 */
angular.module('bc').config([
    '$locationProvider',
    function($locationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
    }]);

/**
 * @ngdoc config
 * @module bc
 */
angular.module('bc').config([
    'ezfbProvider', 'facebookAppId',
    function(provider, clientId) {
        provider.setLocale('en_US');
        provider.setInitParams({
            appId: clientId,
            xfbml: false,
            cookie: false,
            status: false,
            version: 'v2.2'
        });
    }]);

/**
 * @ngdoc config
 * @module bc
 */
angular.module('bc').config([
    '$instagramProvider', 'instagramAppId',
    function(provider, clientId) {
        provider.setClientId(clientId);
    }]);

/**
 * @ngdoc run
 * @module bc
 */
angular.module('bc').run([
    '$rootScope', '$location', '$secured',
    function($rootScope, $location, $secured) {
        $rootScope.parseFloat = parseFloat;
        $rootScope.$location = $location;
        $rootScope.$position = {current: null};
        $rootScope.$secured = $secured;
        $rootScope.now = function() {
            return moment().unix();
        };
        $rootScope.utc = function() {
            return moment().utc().unix();
        };
    }]);

/**
 * @ngdoc run
 * @module bc
 */
angular.module('bc').run([
    function() {
        moment.fn.nextDay = function(day) {
            if (day <= this.day()) {
                day += 7;
            }
            return this.day(day);
        };
    }]);

/**
 * @ngdoc filter
 * @module bc
 * @name unsafe
 */
angular.module('bc').filter('unsafe', [
    '$sce',
    function($sce) {
        return $sce.trustAsHtml;
    }]);

/**
 * @ngdoc constant
 * @module bc
 * @name $tzOffset
 */
angular.module('bc').constant('$tzOffset', (new Date()).getTimezoneOffset() * 60);

/**
 * @ngdoc filter
 * @module bc
 * @name localtimestamp
 */
angular.module('bc').filter('localtimestamp', [
    '$tzOffset',
    function($tzOffset) {
        return function(timestamp, offset) {
            offset = angular.isDefined(offset) ? offset * 60 : $tzOffset;
            return timestamp - offset;
        };
    }]);

/**
 * @ngdoc filter
 * @module bc
 * @name date
 */
angular.module('bc').filter('momentDate', [
    function() {
        return function(timestamp, format) {
            return moment.utc(timestamp * 1000).format(format);
        };
    }]);

/**
 * @ngdoc filter
 * @module bc
 * @name date
 */
angular.module('bc').filter('momentTime', [
    function() {
        return function(timestamp, format) {
            return moment.utc(timestamp * 1000).format(format);
        };
    }]);

/**
 * @ngdoc factory
 * @module bc
 * @name $secured
 */
angular.module('bc').factory('$secured', [
    '$q', 'ipCookie', 'ezfb', 'server', 'toaster',
    function($q, $cookies, $facebook, server, toaster) {
        var promise = null,
            $securedInstance = function(fn) {
                return function() {
                    if (promise === null) {
                        promise = createPromise();
                    }
                    var args = Array.prototype.slice.call(arguments);
                    return promise.then(function(token) {
                        if (token === null) {
                            promise = null;
                        }
                        return fn.apply(null, args.concat([token]));
                    });
                };
            },
            connectServer = function(authResponse) {
                return server.signinFacebook(authResponse)
                    .then(function(session) {
                        $cookies('token', session.token, {
                            path: '/',
                            expires: session.duration,
                            expirationUnit: 'seconds'
                        });
                        $securedInstance.token = session.token;
                        return $q.when(session.token);
                    })
                    .catch(function() {
                        toaster.pop('error', 'Authentication failed', 'BoogieCall login failed', 3000);
                        return $q.when(null);
                    });
            },
            createPromise = function() {
                if ($cookies('token')) {
                    return $q.when($cookies('token'));
                }
                return $facebook
                    .login()
                    .then(function(response) {
                        if (response.status === 'connected') {
                            return connectServer(response.authResponse);
                        }
                        toaster.pop('error', 'Authentication failed', 'Facebook login failed', 3000);
                        return $q.when(null);
                    });
            };
        $securedInstance.token = $cookies('token');
        return $securedInstance;
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name ToastController
 */
angular.module('bc').controller('ToastController', [
    '$scope', 'toaster',
    function($scope, toaster) {
        $scope.toastEventSaved = function() {
            toaster.pop('success', 'Event saved', null, 3000);
        };

        $scope.toastEventError = function(message) {
            toaster.pop('error', 'Event not saved', message, 3000);
        };

        $scope.toastArtistSaved = function() {
            toaster.pop('success', 'Artist saved', null, 3000);
        };

        $scope.toastArtistError = function(message) {
            toaster.pop('error', 'Artist not saved', message, 3000);
        };

        $scope.toastPlaceSaved = function() {
            toaster.pop('success', 'Place saved', null, 3000);
        };

        $scope.toastPlaceError = function(message) {
            toaster.pop('error', 'Place not saved', message, 3000);
        };

        $scope.toastEntityPostError = function(message) {
            toaster.pop('error', 'Post not published', message, 3000);
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name LayoutController
 */
angular.module('bc').controller('LayoutController', [
    '$scope', '$modal', '$window', '$secured',
    function($scope, $modal, $window, $secured) {
        var dialog = null;

        $scope.signinFacebook = $secured(function($event, token) {
            if (token !== null) {
                $window.location.reload(true);
            }
        });

        $scope.toggleCreateEventDialog = function($event) {
            if (dialog === null) {
                dialog = $modal
                    .open({
                        targetEvent: $event,
                        templateUrl: '/templates/events/create.html',
                        controller: 'CreateEventController'
                    });
                dialog
                    .result
                    .then(function(event) {
                        /**
                         * @TODO: fix static url building
                         */
                        $window.location.href = '/en/' + event.id;
                    })
                    .catch(function() {
                        dialog = null;
                    });
            }
            else if (dialog !== null) {
                dialog.dismiss();
                dialog = null;
            }
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name CreateEventController
 */
angular.module('bc').controller('CreateEventController', [
    '$controller', '$scope', '$modalInstance', '$tzOffset', '$secured', 'server',
    function($controller, $scope, $modalInstance, $tzOffset, $secured, server) {
        $controller('ToastController', {$scope: $scope});

        var setEntityTimestamps = function(newValue) {
            $scope.editor.entity.startsAt = moment(newValue).hours(22).minutes(0).seconds(0).utc().unix();
            $scope.editor.entity.endsAt = $scope.editor.entity.startsAt + 6 * 3600;
        };

        $scope.editor = {
            ngForm: null,
            entity: {
                tzOffset: $tzOffset / 60
            },
            minDate: moment().toDate(),
            date: moment().day(12).toDate()
        };

        setEntityTimestamps($scope.editor.date);

        $scope.$watch('editor.date', function(newValue, oldValue) {
            if (!newValue || newValue === oldValue) { return; }
            setEntityTimestamps(newValue);
        });

        $scope.submit = $secured(function() {
            server
                .createEvent($scope.editor.entity)
                .then(function(event) {
                    $scope.toastEventSaved();
                    $scope.editor.entity = {};
                    $modalInstance.close(event);
                })
                .catch($scope.toastEventError);
        });

        $scope.cancel = function() {
            $modalInstance.dismiss();
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityController
 */
angular.module('bc').controller('EntityController', [
    '$scope',
    function($scope) {
        $scope.entity = {};
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityTitleController
 */
angular.module('bc').controller('EntityTitleController', [
    '$scope',
    function($scope) {
        $scope.entity.title = null;

        $scope.$watch('entity.title', function(newTitle, oldTitle) {
            if (!newTitle || newTitle === oldTitle) { return; }
            $scope.page.title = newTitle;
        });
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name SearchController
 */
angular.module('bc').controller('SearchController', [
    '$controller', '$scope', '$filter', '$functools', '$geolocation', 'server', 'getWhenFilterConfig', '$geocoder', '$loading',
    function($controller, $scope, $filter, $functools, $geolocation, server, getWhenFilterConfig, $geocoder, $loading) {
        $controller('LayoutController', {$scope: $scope});
        $controller('EntityController', {$scope: $scope});
        $controller('EntityTitleController', {$scope: $scope});

        $scope.search = {};
        $scope.position = null;

        var __init__ = function() {
            $loading('eventList', function() {
                return getEventList()
                    .then(setEventList)
                    .then(getLocation)
                    .then(setLocation);
            })();
        };

        var getEventList = function() {
            return $geolocation
                .getLocation()
                .then(function(data) {
                    $scope.position = data.coords;
                    return server.getEventListPageByPosition({
                        latitude: data.coords.latitude.toFixed(3),
                        longitude: data.coords.longitude.toFixed(3)
                    });
                })
                .catch(function() {
                    return server.getEventListPageByIp();
                });
        };

        var setEventList = function(bundle) {
            $scope.entity = bundle.location;
            $scope.search.eventList = bundle.eventList.map(setOrderId).map(setEventFilterList);
            $scope.search.filterList = createSearchFilterList($scope.search.eventList);
        };

        var filterDistance = $filter('distance'),
            filterDate = $filter('date'),
            setOrderId = function(event) {
                event.orderId = formatDate(event) + formatDistance(event);
                return event;
            },
            formatDate = function(event) {
                return filterDate(event.startsAt * 1000, 'yyyyMMdd');
            },
            formatDistance = function(event) {
                var distance = filterDistance(event.place, $scope.position);
                if (distance < 10) {
                    return '0' + distance;
                }
                return distance.toString();
            };

        var setEventFilterList = function(event) {
            event.filterList = [];
            event.filterList = event.filterList.concat(createEventWhatFilterList(event));
            event.filterList = event.filterList.concat(createEventWhenFilterList(event));
            event.filterClassList = event.filterList.map($functools.attrgetter('id'));
            return event;
        };

        var createEventWhenFilterList = function(event) {
            var ret = [];
            angular.forEach(getWhenFilterConfig(event.tzOffset * 60), function(config) {
                if (event.startsAt >= config.range[0] && event.startsAt <= config.range[1]) {
                    ret.push(config.filter);
                }
            });
            return ret;
        };

        var createEventWhatFilterList = function(event) {
            return event.tagList.map(function(tag) {
                return angular.extend({}, tag, {
                    id: 'filter-what-' + tag.id,
                    group: 'what'
                });
            });
        };

        var createSearchFilterList = function(eventList) {
            var filterList = eventList
                .map($functools.attrgetter('filterList'))
                .flatten()
                .partition(function(filter) { return filter.group === 'when'; });
            var whenFilterList = filterList[0].unique($functools.attrgetter('id')),
                whatFilterList = filterList[1].uniqueAndSort($functools.attrgetter('id'));
            return whenFilterList.concat(whatFilterList);
        };

        var getLocation = function() {
            return $geocoder
                .getArea($scope.entity.latitude, $scope.entity.longitude)
                .catch(function() { return null; });
        };

        var setLocation = function(location) {
            $scope.entity.title = location;
        };

        $scope.changeLocation = function($event) {
            //$event.stopPropagation();
            //$event.preventDefault();
        };

        __init__();
    }]);

/**
 * @ngdoc factory
 * @module bc
 * @name getWhenFilterConfig
 */
angular.module('bc').factory('getWhenFilterConfig', [
    function() {
        var cache = {};

        var createWhenFilterList = function(tzOffset) {
            var nowTimestamp = moment.utc().unix() - tzOffset,
                nowMoment = moment.utc(nowTimestamp * 1000);
            return [
                {
                    range: [
                        nowMoment.clone().startOf('day').unix() + tzOffset,
                        nowMoment.clone().endOf('day').unix() + tzOffset
                    ],
                    filter: {
                        id: 'filter-when-today',
                        title: 'Today',
                        group: 'when'
                    }
                },
                {
                    range: [
                        nowMoment.clone().nextDay(5).startOf('day').unix() + tzOffset,
                        nowMoment.clone().nextDay(7).endOf('day').unix() + tzOffset
                    ],
                    filter: {
                        id: 'filter-when-weekend',
                        title: 'Weekend',
                        group: 'when'
                    }
                },
                {
                    range: [
                        nowMoment.nextDay(1).startOf('day').unix() + tzOffset,
                        nowMoment.nextDay(7).endOf('day').unix() + tzOffset
                    ],
                    filter: {
                        id: 'filter-when-next-week',
                        title: 'Next week',
                        group: 'when'
                    }
                }
            ];
        };

        return function(tzOffset) {
            var ret = cache[tzOffset];
            if (angular.isUndefined(ret)) {
                ret = cache[tzOffset] = createWhenFilterList(tzOffset);
            }
            return ret;
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityEditorController
 */
angular.module('bc').controller('EntityEditorController', [
    '$controller', '$scope', '$once', '$location',
    function($controller, $scope, $once, $location) {
        $controller('EntityController', {$scope: $scope});

        $scope.editor = {
            ngForm: null,
            entity: null
        };

        $scope.$watch('entity', function(entity) {
            if ($scope.editor.entity === null && entity) {
                $scope.editor.entity = cleanCopy(angular.copy(entity));
            }
        });

        $scope.updateEntity = $once(3850, function() {
            var cleanEntity = cleanCopy($scope.entity);
            if (angular.equals($scope.editor.entity, cleanEntity)) {
                return;
            }
            else {
                $scope.editor.entity = cleanEntity;
            }
            if (!$scope.isEntityValid()) {
                return;
            }
            $scope.updateServerEntity($scope.entity)
                .then(function(entity) {
                    $scope.entity = angular.extend($scope.entity, entity);
                    $scope.editor.entity = cleanCopy($scope.entity);
                    $location.path('/' + $scope.entity.id).replace();
                    $scope.toastEntitySaved();
                })
                .catch(function(error) {
                    $scope.toastEntityError(error.statusText);
                });
        });

        var cleanCopy = function(entity) {
            entity = angular.copy(entity);
            delete entity['cover'];
            delete entity['place'];
            delete entity['tagList'];
            delete entity['linkList'];
            delete entity['newsList'];
            delete entity['artistList'];
            return entity;
        };

        $scope.isEntityValid = function() {
            return true;
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityEditorCoverController
 */
angular.module('bc').controller('EntityEditorCoverController', [
    '$scope', '$image',
    function($scope, $image) {
        $scope.editor.cover = {
            source: null,
            target: null,
            filters: {
                large: [],
                small: []
            }
        };

        $scope.updateEntityCover = function(image) {
            var large = $image.transform(image, $scope.editor.cover.filters.large),
                small = $image.transform(large, $scope.editor.cover.filters.small);
            $scope.updateServerEntityCover($scope.entity.id, small, large)
                .then(function(cover) {
                    $scope.entity.cover = cover;
                    $scope.editor.cover.source = null;
                    $scope.editor.cover.target = null;
                    $scope.toastEntitySaved();
                })
                .catch(function(error) {
                    $scope.editor.cover.source = null;
                    $scope.editor.cover.target = null;
                    $scope.toastEntityError(error.statusText);
                });
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityEditorTagListController
 */
angular.module('bc').controller('EntityEditorTagListController', [
    '$scope',
    function($scope) {
        $scope.entity.tagList = [];
        $scope.editor.tagList = {
            ngForm: null,
            limit: 8
        };

        $scope.$watch('entity.tagList', function() {
            if ($scope.entity.tagList.length >= $scope.editor.tagList.limit) {
                return;
            }
            if ($scope.entity.tagList.length === 0 || $scope.entity.tagList.last().id) {
                $scope.entity.tagList.push({title: '', id: null});
            }
        }, true);

        $scope.updateEntityTagList = function() {
            if (!$scope.editor.tagList.ngForm.$valid) {
                return;
            }
            $scope.updateServerEntityTagList($scope.entity.id, $scope.entity.tagList)
                .then(function(tagList) {
                    $scope.entity.tagList = tagList;
                    $scope.toastEntitySaved();
                })
                .catch(function(error) {
                    $scope.toastEntityError(error.statusText);
                });
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityEditorLinkListController
 */
angular.module('bc').controller('EntityEditorLinkListController', [
    '$scope', 'getLinkSite',
    function($scope, getLinkSite) {
        $scope.entity.linkList = [];
        $scope.editor.linkList = {
            active: false,
            limit: 6,
            href: ''
        };

        $scope.$watch('entity.linkList', function() {
            for (var i = 0, link; link = $scope.entity.linkList[i++]; ) {
                link.site = getLinkSite(link.href);
            }
            if ($scope.entity.linkList.length >= $scope.editor.linkList.limit) {
                return;
            }
            if ($scope.entity.linkList.length === 0 || $scope.entity.linkList.last().href) {
                $scope.entity.linkList.push({href: '', site: null});
            }
        }, true);

        $scope.clickLink = function($event, link) {
            if (link !== $scope.entity.linkList.last()) {
                return true;
            }
            $event.preventDefault();
            $scope.editor.linkList.active = true;
            return false;
        };

        $scope.getLinkClass = function(link, classPrefix, gearPostfix, plusPostfix, linkPostfix) {
            var classPostfix = null;
            if (link === $scope.entity.linkList.last()) {
                classPostfix = $scope.editor.linkList.active ? plusPostfix : gearPostfix;
            }
            else {
                classPostfix = link.site || linkPostfix;
            }
            return classPrefix + classPostfix;
        };

        $scope.updateEntityLinkListIfActive = function() {
            if (!$scope.editor.linkList.active) {
                return;
            }
            $scope.editor.linkList.active = false;
            $scope.updateServerEntityLinkList($scope.entity.id, $scope.filterHidden($scope.entity.linkList))
                .then(function(linkList) {
                    $scope.entity.linkList = linkList;
                    $scope.toastEntitySaved();
                })
                .catch(function(error) {
                    $scope.toastEntityError(error.statusText);
                });
        };

        $scope.filterHidden = function(linkList) {
            return linkList.filter(function(link) { return !link.hide; })
        }
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityEditorArtistListController
 */
angular.module('bc').controller('EntityEditorArtistListController', [
    '$scope', '$once', '$filter', '$functools', 'server',
    function($scope, $once, $filter, $functools, server) {
        var filter = $filter('filter');

        $scope.entity.artistList = [];
        $scope.editor.artistList = {
            active: false,
            artist: { title: '', hometown: '', id: null },
            foundList: [],
            limit: 8,
            cache: {}
        };

        $scope.$watch('entity.artistList', function() {
            if ($scope.entity.tagList.length >= $scope.editor.tagList.limit) {
                return;
            }
            if ($scope.entity.artistList.length === 0 || $scope.entity.artistList.last().id) {
                $scope.entity.artistList.push({id: null, title: '', hometown: ''});
            }
        }, true);

        $scope.$watch('editor.artistList.artist.title', function(/* string */newSearch, oldSearch) {
            if (newSearch === oldSearch) { return; }
            if (newSearch) {
                newSearch = newSearch.trim().replace('&nbsp;', '');
            }
            if (!newSearch) {
                $scope.editor.artistList.foundList = [];
                return;
            }
            var subSearch = newSearch.substr(0, 4);
            if (angular.isUndefined($scope.editor.artistList.cache[subSearch])) {
                $scope.editor.artistList.cache[subSearch] = server.searchArtistList(subSearch);
            }
            $scope.editor.artistList.cache[subSearch].then(excludeExisted).then(function(artistList) {
                $scope.editor.artistList.foundList = filter(artistList, newSearch);
            });
        });

        var excludeExisted = function(artistList) {
            return artistList.filter(function(artist) {
                return !$scope.entity.artistList.has(artist, $functools.attrgetter('id'));
            });
        };

        $scope.createArtist = function(artist) {
            if (!artist.title || !artist.hometown) { return; }
            server
                .createArtist(artist)
                .then(function(artist) {
                    $scope.toastArtistSaved();
                    $scope.appendArtist(artist);
                })
                .catch($scope.toastArtistError);
        };

        $scope.appendArtist = function(artist) {
            $scope.editor.artistList.artist = { title: '', hometown: '', id: null };
            $scope.entity.artistList.splice(-1, 0, artist);
            $scope.updateEntityArtistList();
        };

        $scope.removeArtist = function(artist) {
            var index = calculateOldIndex(artist);
            $scope.entity.artistList.splice(index, 1);
            $scope.updateEntityArtistList();
        };

        $scope.moveArtistLeft = function(artist) {
            moveArtist(artist, -1);
            $scope.updateEntityArtistList();
        };

        $scope.moveArtistRight = function(artist) {
            moveArtist(artist, 1);
            $scope.updateEntityArtistList();
        };

        $scope.updateEntityArtistList = $once(3850, function() {
            $scope
                .updateServerEntityArtistList($scope.entity.id, $scope.entity.artistList)
                .then(function(artistList) {
                    $scope.entity.artistList = artistList;
                    $scope.toastEntitySaved();
                })
                .catch($scope.toastEntityError);
        });

        var moveArtist = function(artist, shift) {
            var oldIndex = calculateOldIndex(artist),
                newIndex = calculateNewIndex(oldIndex, shift);
            $scope.entity.artistList[oldIndex] = $scope.entity.artistList[newIndex];
            $scope.entity.artistList[newIndex] = artist;
        };

        var calculateOldIndex = function(artist) {
            return $scope.entity.artistList.getIndexOf(artist, $functools.attrgetter('id'));
        };

        var calculateNewIndex = function(oldIndex, shift) {
            return (oldIndex + shift).mod($scope.entity.artistList.length - 1);
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityEditorNewsListController
 */
angular.module('bc').controller('EntityEditorNewsListController', [
    '$scope', '$once', '$filter', '$functools',
    function($scope, $once, $filter, $functools) {
        var filter = $filter('filter');

        $scope.entity.newsList = [];
        $scope.editor.newsList = {
            ngForm: null
        };

        $scope.$watch('entity.newsList', function() {
            if ($scope.entity.newsList.length === 0 || $scope.entity.newsList[0].createdAt) {
                $scope.entity.newsList.unshift({createdAt: null, title: '', hometown: ''});
            }
        }, true);

        $scope.updateEntityNews = function(news) {
            if (!$scope.editor.newsList.ngForm.$valid) {
                return;
            }
            var index = $scope.entity.newsList.getIndexOf(news, $functools.attrgetter('createdAt'));
            if (news.createdAt && !news.title && !news.content) {
                $scope
                    .deleteServerEntityNews($scope.entity.id, news)
                    .then(function() {
                        $scope.entity.newsList.splice(index, 1);
                        $scope.toastEntitySaved();
                    })
                    .catch($scope.toastEntityError);
            }
            else {
                $scope
                    .updateServerEntityNews($scope.entity.id, news)
                    .then(function(news) {
                        $scope.entity.newsList.splice(index, 1, news);
                        $scope.toastEntitySaved();
                    })
                    .catch($scope.toastEntityError);
            }
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityEditorPlaceController
 */
angular.module('bc').controller('EntityEditorPlaceController', [
    '$scope', '$filter', '$once', 'server',
    function($scope, $filter, $once, server) {
        var filter = $filter('filter');

        $scope.entity.place = {};
        $scope.editor.place = {
            active: false,
            foundList: [],
            cache: {}
        };

        $scope.$watch('entity.place.title', function(/* string */newSearch, oldSearch) {
            if ($scope.entity.place.id) { return; }
            if (newSearch === oldSearch) { return; }
            if (newSearch) {
                newSearch = newSearch.trim().replace('&nbsp;', '');
            }
            if (!newSearch) {
                $scope.editor.place.foundList = [];
                return;
            }
            var subSearch = newSearch.substr(0, 4);
            if (angular.isUndefined($scope.editor.place.cache[subSearch])) {
                $scope.editor.place.cache[subSearch] = server.searchPlaceList(subSearch);
            }
            $scope.editor.place.cache[subSearch].then(function(placeList) {
                $scope.editor.place.foundList = filter(placeList, newSearch);
            });
        });

        $scope.createPlace = function(place) {
            if (!place.title || !place.address || !place.latitude || !place.longitude) { return; }
            server
                .createPlace(place)
                .then(function(place) {
                    $scope.toastPlaceSaved();
                    $scope.appendPlace(place);
                })
                .catch($scope.toastPlaceError);
        };

        $scope.appendPlace = function(place) {
            $scope.entity.place = place;
            $scope.updateEntityPlace();
        };

        $scope.removePlace = function() {
            $scope.entity.place = {};
        };

        $scope.updateEntityPlace = $once(3850, function() {
            $scope
                .updateServerEntityPlace($scope.entity.id, $scope.entity.place)
                .then(function(place) {
                    $scope.entity.place = place;
                })
                .catch($scope.toastEntityError);
        });
    }]);

/**
 * @ngdoc factory
 * @module bc
 * @name getLinkSite
 */
angular.module('bc').factory('getLinkSite', [
    '$parseHref',
    function($parseHref) {
        var knownSiteDict = {
            'twitter.com': 'twitter',
            'dribble.com': 'dribble',
            'facebook.com': 'facebook',
            'linkedin.com': 'linkedin',
            'pinterest.com': 'pinterest',
            'instagram.com': 'instagram',
            'thumbtack.com': 'thumb-tack',
            'plus.google.com': 'google-plus'
        };

        return function(href) {
            var host = $parseHref(href).hostname.replace('www.', ''),
                site = knownSiteDict[host];
            if (angular.isDefined(site)) {
                return site;
            }
            return null;
        };
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name EntityPostListController
 */
angular.module('bc').controller('EntityPostListController', [
    '$scope', '$rootScope', '$modal', '$window', '$timeout',
    function($scope, $rootScope, $modal, $window, $timeout) {
        var dialog = null;

        $scope.entity.postList = [];

        $scope.$watch('entity.postList.length', function(newValue, oldValue) {
            if (!newValue || newValue == oldValue) { return; }
            $timeout(function() {
                $window.$('.isotope').isotope('destroy').isotope().isotope('reLayout');
            }, 300);
        });

        $scope.toggleCreateEntityPostDialog = function($event, entityId) {
            if (dialog === null) {
                dialog = $modal
                    .open({
                        targetEvent: $event,
                        templateUrl: '/templates/posts/create.html',
                        controller: 'CreateEntityPostController',
                        scope: angular.extend($rootScope.$new(), {
                            entity: {
                                id: entityId
                            },
                            createServerEntityPost: $scope.createServerEntityPost
                        })
                    });
                dialog
                    .result
                    .then(function(post) {
                        $scope.entity.postList = [post].concat($scope.entity.postList || []);
                    })
                    .finally(function() {
                        dialog = null;
                    });
            }
            else if (dialog !== null) {
                dialog.dismiss();
                dialog = null;
            }
        };

    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name CreateEventController
 */
angular.module('bc').controller('CreateEntityPostController', [
    '$controller', '$scope', '$modalInstance', '$tzOffset', '$secured',
    function($controller, $scope, $modalInstance, $tzOffset, $secured) {
        $controller('ToastController', {$scope: $scope});

        $scope.editor = {
            ngForm: {},
            entity: {
                href: null,
                title: ''
            }
        };

        $scope.submit = $secured(function() {
            if (!$scope.editor.ngForm.$valid) {
                return;
            }
            $scope
                .createServerEntityPost($scope.entity.id, $scope.editor.entity)
                .then(function(post) {
                    $scope.toastEventSaved();
                    $scope.editor.entity = {};
                    $modalInstance.close(post);
                })
                .catch($scope.toastEntityPostError);
        });

        $scope.cancel = function() {
            $modalInstance.dismiss();
        };
    }]);


/**
 * @ngdoc controller
 * @module bc
 * @name EventController
 */
angular.module('bc').controller('EventController', [
    '$controller', '$scope', '$filter', '$google', '$instagram', 'server',
    function($controller, $scope, $filter, $google, $instagram, server) {
        $controller('ToastController', {$scope: $scope});
        $controller('LayoutController', {$scope: $scope});
        $controller('EntityController', {$scope: $scope});
        $controller('EntityTitleController', {$scope: $scope});
        $controller('EntityEditorController', {$scope: $scope});
        $controller('EntityEditorCoverController', {$scope: $scope});
        $controller('EntityEditorPlaceController', {$scope: $scope});
        $controller('EntityEditorTagListController', {$scope: $scope});
        $controller('EntityEditorNewsListController', {$scope: $scope});
        $controller('EntityEditorArtistListController', {$scope: $scope});

        $scope.toastEntitySaved = $scope.toastEventSaved;
        $scope.toastEntityError = $scope.toastEventError;
        $scope.updateServerEntity = server.updateEvent;
        $scope.updateServerEntityCover = server.updateEventCover;
        $scope.updateServerEntityPlace = server.updateEventPlace;
        $scope.updateServerEntityNews = server.updateEventNews;
        $scope.deleteServerEntityNews = server.deleteEventNews;
        $scope.updateServerEntityTagList = server.updateEventTagList;
        $scope.updateServerEntityArtistList = server.updateEventArtistList;

        $scope.editor.cover.filters = {
            large: [['scale', 1280, 700], ['grayscale']],
            small: [['scale', 600, 430]]
        };

        $scope.editDateTime = function(timestamp, offset) {
            return moment.utc((timestamp - offset * 60) * 1000);
        };

        $scope.$watch('editor.startsAt', function(value) {
            if (!value) { return; }
            $scope.entity.startsAt = value.unix() + $scope.entity.tzOffset * 60;
            $scope.entity.endsAt = $scope.entity.startsAt + getDuration();
        });

        $scope.$watch('editor.endsAt', function(value) {
            if (!value) { return; }
            $scope.entity.endsAt = value.unix() + $scope.entity.tzOffset * 60;
            $scope.entity.endsAt = $scope.entity.startsAt + getDuration();
        });

        var getDuration = function() {
            if ($scope.entity.startsAt && $scope.entity.endsAt) {
                return ($scope.entity.endsAt - $scope.entity.startsAt).mod(24 * 3600);
            }
            return 7 * 3600;
        };

        $scope.$watch('editor.cover.source', function(newSource, oldSource) {
            if (!newSource || newSource === oldSource) { return; }
            $scope.updateEntityCover(newSource);
        });

        $scope.$watch('entity.place.latitude + entity.place.longitude + entity.startsAt / 24 / 3600', function(newValue, oldValue) {
            if (!newValue || Math.abs(newValue - oldValue) < 1.0) { return; }
            $google
                .getTimezoneOffset($scope.entity.place.latitude, $scope.entity.place.longitude, $scope.entity.startsAt)
                .then(function(tzOffset) {
                    $scope.entity.tzOffset = tzOffset / 60;
                });
        });

        $scope.$watch('entity.tzOffset', function(newValue, oldValue) {
            if (!newValue || !oldValue || newValue === oldValue) { return; }
            $scope.entity.startsAt += (newValue - oldValue) * 60;
            $scope.entity.endsAt += (newValue - oldValue) * 60;
        });

        angular.forEach(['entity.tzOffset', 'entity.startsAt', 'entity.endsAt'], function(expression) {
            $scope.$watch(expression, function(newValue, oldValue) {
                if (!newValue || newValue == oldValue) { return; }
                $scope.updateEntity();
            });
        });

        var artistClassCache = {};
        $scope.getArtistClassList = function(index, count, rowMax) {
            var artistClassCountCache = artistClassCache[count];
            if (angular.isUndefined(artistClassCountCache)) {
                artistClassCountCache = artistClassCache[count] = createArtistClassCountConfig(count, rowMax);
            }
            var artistClassIndexCache = artistClassCountCache.cache[index];
            if (angular.isUndefined(artistClassIndexCache)) {
                artistClassIndexCache = artistClassCountCache.cache[index] = createArtistClassIndexConfig(index, artistClassCountCache);
            }
            return artistClassIndexCache;
        };

        var createArtistClassCountConfig = function(count, rowMax) {
            rowMax = rowMax || 4;
            var partCount = Math.ceil(count / rowMax),
                division = count / partCount,
                rows = [];
            for (var i = 0; i < partCount; i++) {
                var min = Math.ceil(division * i),
                    max = Math.ceil(division * (i + 1));
                rows.push({
                    range: [min, max],
                    count: max - min
                });
            }
            return {
                rows: rows,
                cache: {}
            };
        };

        var createArtistClassIndexConfig = function(index, artistClassCountCache) {
            for (var i = 0, row; row = artistClassCountCache.rows[i++]; ) {
                if (index >= row.range[1]) {
                    continue;
                }
                var ret = [];
                if ([1, 3].has(row.count)) {
                    ret = ret.concat(['col-md-4']);
                }
                else {
                    ret = ret.concat(['col-md-3']);
                }
                if (index === row.range[0]) {
                    if (row.count === 1) {
                        ret = ret.concat(['col-md-offset-4']);
                    }
                    else if (row.count === 2) {
                        ret = ret.concat(['col-md-offset-3']);
                    }
                    ret = ret.concat(['first-item']);
                }
                return ret;
            }
            return ['col-md-3'];
        };

        $scope.instagram = {
            itemList: null
        };

        $scope.$watch('entity.place.instagram', function(newValue, oldValue) {
            if ($scope.now() < $scope.entity.startsAt) return;
            if (!newValue) return;
            if ($scope.instagram.itemList != null && newValue == oldValue) return;
            var locationList = newValue.split(':'),
                minTimestamp = $scope.entity.startsAt,
                maxTimestamp = $scope.entity.endsAt;
            $instagram
                .getRecentMediaByLocationList(locationList, minTimestamp, maxTimestamp)
                .then(function(itemList) {
                    $scope.instagram.itemList = itemList;
                });
        });
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name PlaceController
 */
angular.module('bc').controller('PlaceController', [
    '$controller', '$scope', 'server',
    function($controller, $scope, server) {
        $controller('ToastController', {$scope: $scope});
        $controller('LayoutController', {$scope: $scope});
        $controller('EntityController', {$scope: $scope});
        $controller('EntityTitleController', {$scope: $scope});
        $controller('EntityEditorController', {$scope: $scope});
        $controller('EntityEditorLinkListController', {$scope: $scope});

        $scope.toastEntitySaved = $scope.toastPlaceSaved;
        $scope.toastEntityError = $scope.toastPlaceError;
        $scope.updateServerEntity = server.updatePlace;
        $scope.updateServerEntityLinkList = server.updatePlaceLinkList;
    }]);

/**
 * @ngdoc controller
 * @module bc
 * @name ArtistController
 */
angular.module('bc').controller('ArtistController', [
    '$controller', '$scope', 'server',
    function($controller, $scope, server) {
        $controller('ToastController', {$scope: $scope});
        $controller('LayoutController', {$scope: $scope});
        $controller('EntityController', {$scope: $scope});
        $controller('EntityPostListController', {$scope: $scope});
        $controller('EntityTitleController', {$scope: $scope});
        $controller('EntityEditorController', {$scope: $scope});
        $controller('EntityEditorCoverController', {$scope: $scope});
        $controller('EntityEditorLinkListController', {$scope: $scope});

        $scope.toastEntitySaved = $scope.toastArtistSaved;
        $scope.toastEntityError = $scope.toastArtistError;
        $scope.updateServerEntity = server.updateArtist;
        $scope.updateServerEntityCover = server.updateArtistCover;
        $scope.updateServerEntityLinkList = server.updateArtistLinkList;
        $scope.createServerEntityPost = server.createArtistPost;

        $scope.editor.cover.filters = {
            large: [['scale', 400, 345]],
            small: [['scale', 120, 120]]
        };
    }]);

/**
 * @ngdoc service
 * @module bc
 * @name server
 */
angular.module('bc').service('server', [
    '$http', '$q', '$functools',
    function($http, $q, $functools) {
        this.signinFacebook = function(authResponse) {
            return $http
                .post('/api/v1/users/signin/facebook', authResponse)
                .then(returnResponseData);
        };

        this.getEventListPageByPosition = function(position) {
            return $http
                .get('/api/v1/events/search/position', { params: position })
                .then(returnResponseData);
        };

        this.getEventListPageByIp = function() {
            return $http
                .get('/api/v1/events/search/ip')
                .then(returnResponseData);
        };

        this.createEvent = function(event) {
            return createEntity('events', event);
        };

        this.updateEvent = function(event) {
            return updateEntity('events', event);
        };

        this.updateEventCover = function(eventId, small, large) {
            return updateEntityCover('events', eventId, small, large);
        };

        this.updateEventNews = function(eventId, news) {
            return $http
                .post('/api/v1/events/' + eventId + '/news', news)
                .then(returnResponseData);
        };

        this.deleteEventNews = function(eventId, news) {
            return $http
                .post('/api/v1/events/' + eventId + '/news/' + news.createdAt + '/delete', {})
                .then(returnResponseData);
        };

        this.updateEventTagList = function(eventId, tagList) {
            return $http
                .post('/api/v1/events/' + eventId + '/tags', {
                    tagList: tagList.map($functools.attrgetter('title')).filter($functools.bool)
                })
                .then(returnResponseData);
        };

        this.updateEventArtistList = function(eventId, artistList) {
            return $http
                .post('/api/v1/events/' + eventId + '/artists', {
                    artistList: artistList.map($functools.attrgetter('id')).filter($functools.bool)
                })
                .then(returnResponseData);
        };

        this.updateEventPlace = function(eventId, place) {
            return $http
                .post('/api/v1/events/' + eventId + '/place', {
                    place: place.id
                })
                .then(returnResponseData);
        };

        this.searchPlaceList = function(search) {
            return searchEntityList('places', search);
        };

        this.createPlace = function(place) {
            return createEntity('places', place);
        };

        this.updatePlace = function(place) {
            return updateEntity('places', place);
        };

        this.updatePlaceLinkList = function(placeId, linkList) {
            return updateEntityLinkList('places', placeId, linkList);
        };

        this.searchArtistList = function(search) {
            return searchEntityList('artists', search)
        };

        this.createArtist = function(artist) {
            return createEntity('artists', artist);
        };

        this.updateArtist = function(artist) {
            return updateEntity('artists', artist);
        };

        this.updateArtistCover = function(artistId, small, large) {
            return updateEntityCover('artists', artistId, small, large);
        };

        this.updateArtistLinkList = function(artistId, linkList) {
            return updateEntityLinkList('artists', artistId, linkList);
        };

        this.createArtistPost = function(artistId, post) {
            return createEntityPost('artists', artistId, post);
        };

        var searchEntityList = function(entityScope, search) {
            return $http
                .get('/api/v1/' + entityScope + '/search/' + search)
                .then(returnResponseData);
        };

        var createEntity = function(entityScope, entity) {
            return $http
                .post('/api/v1/' + entityScope + '/', angular.extend({}, entity, {
                    place: null,
                    cover: null,
                    tagList: null,
                    linkList: null,
                    newsList: null,
                    artistList: null
                }))
                .then(returnResponseData);
        };

        var updateEntity = function(entityScope, entity) {
            return $http
                .post('/api/v1/' + entityScope + '/' + entity.id, angular.extend({}, entity, {
                    place: null,
                    cover: null,
                    tagList: null,
                    linkList: null,
                    newsList: null,
                    artistList: null
                }))
                .then(returnResponseData);
        };

        var updateEntityLinkList = function(entityScope, entityId, linkList) {
            return $http
                .post('/api/v1/' + entityScope + '/' + entityId + '/links', {
                    linkList: linkList.map($functools.attrgetter('href')).filter($functools.bool)
                })
                .then(returnResponseData);
        };

        var updateEntityCover = function(entityScope, entityId, small, large) {
            return $http
                .post('/api/v1/' + entityScope + '/' + entityId + '/cover', {
                    small: small,
                    large:large
                })
                .then(returnResponseData);
        };

        var createEntityPost = function(entityScope, entityId, post) {
            return $http
                .post('/api/v1/' + entityScope + '/' + entityId + '/posts', post)
                .then(returnResponseData);
        };

        var returnResponseData = function(response) {
            return response.data;
        };
    }]);

/**
 * @ngdoc service
 * @module bc
 * @name $google
 */
angular.module('bc').service('$google', [
    '$q', '$http', '$tzOffset',
    function($q, $http, $tzOffset) {
        var accessKey = 'AIzaSyCnolWYalBMhDOZ7GFaQmuRTCpY18VSewU';

        this.getTimezoneOffset = function(latitude, longitude, timestamp) {
            if (!longitude || !latitude) {
                return $q.when($tzOffset);
            }
            return $http
                .get('https://maps.googleapis.com/maps/api/timezone/json', { params: {
                    location: [latitude, longitude].join(','),
                    timestamp: timestamp,
                    key: accessKey
                }})
                .then(function(response) {
                    if (response.data.status === 'OK') {
                        return - (response.data.rawOffset + response.data.dstOffset);
                    }
                    return $tzOffset;
                });
        };
    }]);

/**
 * @ngdoc factory
 * @module bc
 * @name $parseHref
 */
angular.module('bc').factory('$parseHref', [
    function() {
        var parser = document.createElement('a');
        return function(href) {
            parser.href = href;
            return {
                hostname: parser.hostname
            };
        };
    }]);

/**
 * @ngdoc factory
 * @module bc
 * @name $hasInput
 */
angular.module('bc').factory('$hasInput', [
    function() {
        var supported = {date: false, number: false, time: false, month: false, week: false},
            tester = document.createElement('input');

        angular.forEach(supported, function(_, type) {
            tester.type = type;
            supported[type] = (tester.type === type);
        });

        /**
         * @ngdoc function
         * @module bc
         * @name $hasInput
         * @param {String} type
         * @return {Boolean}
         */
        return function(type) {
            return supported[type];
        };
    }]);

angular.module('bc').directive('soundcloudSrc', [
    '$sce',
    function($sce) {
        var template = 'https://w.soundcloud.com/player/?url=$$$&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true';

        return {
            restrict: 'A',
            scope: {
                href: '@soundcloudSrc'
            },
            link: function($scope, $element) {
                $element.attr('src', template.replace('$$$', $scope.href));
            }
        };
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name pickDate
 * @restrict A
 */
angular.module('bc').directive('pickDate', [
    '$hasInput',
    function($hasInput) {
        return {
            restrict: 'A',
            scope: {
                pickDate: '=',
                pickDateContainer: '@'
            },
            link: function($scope, element) {
                if (angular.isDefined($scope.pickDate)) {
                    element.attr('data-value', $scope.pickDate.format('YYYY-MM-DD'));
                }
                $(element[0]).pickadate({
                    format: 'yyyy-mm-dd',
                    container: $scope.pickDateContainer,
                    min: new Date(),
                    clear: '',
                    onOpen: function() {
                    },
                    onClose: function() {
                        element[0].blur();
                    },
                    onSet: function(context) {
                        if (angular.isUndefined(context.select)) { return; }
                        $scope.$apply(function($scope) {
                            var selected = moment(context.select);
                            $scope.pickDate = $scope.pickDate.clone();
                            $scope.pickDate.year(selected.year());
                            $scope.pickDate.month(selected.month());
                            $scope.pickDate.date(selected.date());
                        });
                    }
                });
            }
        };
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name pickTime
 * @restrict A
 */
angular.module('bc').directive('pickTime', [
    '$hasInput',
    function($hasInput) {
        return {
            restrict: 'A',
            scope: {
                pickTime: '=',
                pickTimeContainer: '@',
                min: '@',
                max: '@'
            },
            link: function($scope, element) {
                if (angular.isDefined($scope.pickTime)) {
                    element.attr('data-value', $scope.pickTime.format('HH:mm'));
                }
                $(element[0]).pickatime({
                    format: 'H:i',
                    container: $scope.pickTimeContainer,
                    clear: '',
                    onOpen: function() {
                    },
                    onClose: function() {
                        element[0].blur();
                    },
                    onSet: function(context) {
                        if (angular.isUndefined(context.select)) { return; }
                        $scope.$apply(function($scope) {
                            var selected = moment.utc(context.select * 60000);
                            $scope.pickTime = $scope.pickTime.clone();
                            $scope.pickTime.hours(selected.hours());
                            $scope.pickTime.minutes(selected.minutes());
                            $scope.pickTime.seconds(0);
                        });
                    }
                });
            }
        };
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name bcHref
 * @restrict A
 */
angular.module('bc').directive('bcHref', [
    function() {
        return {
            restrict: 'A',
            scope: {
                bcHref: '@'
            },
            link: function($scope, element, attrs) {
                var href = $scope.bcHref;
                angular.forEach(attrs, function(value, name) {
                    if (name[0] !== '$') {
                        href = href.replace('{' + name + '}', value);
                    }
                });
                element.attr('href', href);
            }
        };
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name ngClickOutside
 * @restrict A
 */
angular.module('bc').directive('ngClickOutside', [
    '$parse', '$document',
    function($parse, $document) {
        return {
            restrict: 'A',
            compile: function($element, attr) {
                var fn = $parse(attr['ngClickOutside']);
                return function ngEventHandler($scope, element) {
                    element.on('click', function(e) {
                        e.stopPropagation();
                    });
                    $document.on('click', function(event) {
                        var callback = function() {
                            fn($scope, {$event:event});
                        };
                        $scope.$apply(callback);
                    });
                };
            }
        }
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name title
 * @restrict E
 */
angular.module('bc').directive('title', [
    '$rootScope',
    function($rootScope) {
        return {
            restrict: 'E',
            link: function(_, element, attrs) {
                if (angular.isUndefined(element.text())) {
                    return;
                }
                $rootScope.page = angular.extend($rootScope || {}, {
                    postfix: attrs.postfix,
                    prefix: attrs.prefix,
                    title: element.text().replace(attrs.prefix, '').replace(attrs.postfix, '')
                });
            }
        };
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name contenteditable
 * @restrict A
 */
angular.module('bc').directive('contenteditable', [
    function() {
        var stripBrPattern = /(<br>)+$/,
            stripNbspPattern = /(&nbsp;)+?/g,
            defaultOptions = {
                multiline: 'false'
            };

        return {
            require: 'ngModel',
            restrict: 'A',
            priority: 1,
            scope: {
                onBlur: '&',
                ngModel: '='
            },
            link: function($scope, element, attrs, ngModel) {
                var options = {};
                angular.forEach(defaultOptions, function(defaultValue, name) {
                    var value = attrs[name] || defaultOptions[name];
                    options[name] = value !== 'false';
                });

                $scope.$pristine = true;

                ngModel.$render = function() {
                    element.html(ngModel.$viewValue || '');
                };

                element.bind('keypress keydown keyup', function(e) {
                    if (options.multiline || e.which !== 13) {
                        return true;
                    }
                    e.preventDefault();
                    return false;
                });

                element.bind('keyup change', function() {
                    $scope.$pristine = false;
                    $scope.$apply(read);
                });

                element.bind('blur', function() {
                    $scope.$apply(read);
                    !$scope.$pristine && $scope.onBlur();
                });

                element.bind('paste', function(event) {
                    if (event && event.clipboardData && event.clipboardData.getData) {
                        event.preventDefault();
                        window.document.execCommand('insertText', false, event.clipboardData.getData('text/plain'));
                    }
                });

                function read() {
                    var value;
                    if (options.multiline) {
                        value = element.html().replace(stripNbspPattern, ' ').replace(stripBrPattern, '');
                    }
                    else {
                        value = element.text();
                    }
                    ngModel.$setViewValue(value);
                }

                if ($scope.ngModel) {
                    element.html($scope.ngModel);
                    read();
                }
            }
        };
    }]);

/**
 * @ngdoc service
 * @module bc
 * @name $geocoder
 */
angular.module('bc').service('$geocoder', [
    '$q',
    function($q) {
        var client = new google.maps.Geocoder(),
            createLatLng = function(latitude, longitude) {
                return new google.maps.LatLng(latitude, longitude);
            };

        this.getAddress = function(latitude, longitude) {
            return request({location: createLatLng(latitude, longitude)}).then(function(response) {
                return response[0].formatted_address;
            });
        };

        this.getArea = function(latitude, longitude) {
            return request({location: createLatLng(latitude, longitude)}).then(function(response) {
                for (var i = 0, component; component = response[0].address_components[i++];) {
                    if (!component.types.has('political')) {
                        continue;
                    }
                    for (var j = 2; j > 0; j--) {
                        if (component.types.has('administrative_area_level_' + j)) {
                            return component.short_name;
                        }
                    }
                    if (component.types.has('country')) {
                        return component.long_name;
                    }
                }
                return null;
            });
        };

        this.getPosition = function(address) {
            return request({address: address}).then(function(response) {
                var location = response[0].geometry.location;
                return {
                    latitude: location.lat(),
                    longitude: location.lng()
                };
            });
        };

        var request = function(param) {
            var defer = $q.defer();
            client.geocode(param, function(response, status) {
                if(status === google.maps.GeocoderStatus.OK) {
                    defer.resolve(response);
                }
                else {
                    defer.reject();
                }
            });
            return defer.promise;
        };
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name eventGallery
 * @restrict C
 */
angular.module('bc').directive('eventGallery', [
    function() {
        return {
            restrict: 'C',
            link: function($scope, $element) {
                //console.debug($element[0].innerHTML);
                //console.debug($element[0].querySelector('.event-gallery-item'));
            }
        };
    }]);

/**
 * @ngdoc directive
 * @module bc
 * @name entityMap
 * @restrict E
 */
angular.module('bc').directive('entityMap', [
    '$compile', '$q', '$timeout', '$document', '$geocoder', '$geolocation', 'toaster',
    function($compile, $q, $timeout, $document, $geocoder, $geolocation, toaster) {
        var defaults = {
            options: {
                type: 'ROADMAP',
                zoom: 15,
                scroll: false,
                marker: 'show',
                markerImage: '/assets/images/marker.png',
                draggable: false
            },
            styles: [{
                'stylers': [{
                    'visibility': 'off'
                }]
            },{
                'featureType': 'road',
                'stylers': [{
                    'visibility': 'on'
                },{
                    'color': '#ffffff'
                }]
            },{
                'featureType': 'road.arterial',
                'stylers': [{
                    'visibility': 'on'
                },{
                    'color': '#f1c40f'
                }]
            },{
                'featureType': 'road.highway',
                'stylers': [{
                    'visibility': 'on'
                },{
                    'color': '#f1c40f'
                }]
            },{
                'featureType': 'landscape',
                'stylers': [{
                    'visibility': 'on'
                },{
                    'color': '#ecf0f1'
                }]
            },{
                'featureType': 'water',
                'stylers': [{
                    'visibility': 'on'
                },{
                    'color': '#73bfc1'
                }]
            },{},{
                'featureType': 'road',
                'elementType': 'labels',
                'stylers': [{
                    'visibility': 'off'
                }]
            },{
                'featureType': 'poi.park',
                'elementType': 'geometry.fill',
                'stylers': [{
                    'visibility': 'on'
                },{
                    'color': '#2ecc71'
                }]
            },{
                'elementType': 'labels',
                'stylers': [{
                    'visibility': 'off'
                }]
            },{
                'featureType': 'landscape.man_made',
                'elementType': 'geometry',
                'stylers': [{
                    'weight': 0.9
                },{
                    'visibility': 'off'
                }]
            }]
        };

        // Our custom marker label overlay
        var MarkerLabel = function(options) {
            this.setValues(options);
            this.div = $compile(
                '<div class="map-marker-label fadeIn animated" ng-show="address && editable" ng-bind="address" ng-click="setEntityAddress(address)"></div>'
            )(options.$scope);
        };

        MarkerLabel.prototype = angular.extend(new google.maps.OverlayView(), {
            onAdd: function() {
                this.getPanes().overlayImage.appendChild(this.div[0]);

                // Ensures the label is redrawn if the text or position is changed.
                var self = this;
                this.listeners = [
                    google.maps.event.addListener(this, 'position_changed', function() { self.draw(); }),
                    google.maps.event.addListener(this, 'text_changed', function() { self.draw(); }),
                    google.maps.event.addListener(this, 'zindex_changed', function() { self.draw(); }),
                    google.maps.event.addListener(this.marker, 'dragend', function() { self.draw() })
                ];
            },
            onRemove: function() {
                this.div[0].parentNode.removeChild(this.div[0]);
                // Label is removed from the map, stop updating its position/text
                for (var i = 0, l = this.listeners.length; i < l; ++i) {
                    google.maps.event.removeListener(this.listeners[i]);
                }
            },
            draw: function() {
                var markerSize = this.marker.icon.anchor,
                    position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
                // dynamically grab the label height/width in order to properly position it vertically/horizontally.
                var labelHeight = this.div.prop('offsetHeight');
                var labelWidth = this.div.prop('offsetWidth');
                this.div[0].style.position = 'relative';
                this.div[0].style.left = (position.x - (labelWidth / 2) + 5) + 'px';
                this.div[0].style.top = (position.y - markerSize.y - labelHeight -10) + 'px';
            }
        });

        var Marker = function(options) {
            this.MarkerLabel = new MarkerLabel({
                map: this.map,
                marker: this,
                $scope: options.$scope
            });
            this.MarkerLabel.bindTo('position', this, 'position');
            google.maps.Marker.apply(this, arguments);
        };

        Marker.prototype = angular.extend(new google.maps.Marker(), {
            // If we're adding/removing the marker from the map, we need to do the same for the marker label overlay
            setMap: function(){
                google.maps.Marker.prototype.setMap.apply(this, arguments);
                this.MarkerLabel.setMap.apply(this.MarkerLabel, arguments);
            }
        });

        return {
            restrict: 'E',
            scope: {
                entity: '=',
                styles: '@',
                options: '@',
                editable: '@',
                onPositionChanged: '&'
            },
            link: function($scope, element) {
                var timeout = null;
                $scope.$watch('entity.address', function(newAddress, oldAddress) {
                    if (!newAddress) { $scope.auto = null; return; }
                    newAddress = newAddress.replace(/<\/?[^>]+(>|$)/g, '');
                    if (newAddress === oldAddress) { return; }
                    if (!newAddress) { $scope.auto = null; return; }
                    if (false === $scope.auto) { return; }
                    getLocation(newAddress)
                        .then(setPosition)
                        .catch(function() {
                            if (!timeout) {
                                timeout = $timeout(function() { timeout = null; }, 2850);
                                toaster.clear();
                                toaster.pop('error', 'Address not found', null, 2000);
                            }
                        });
                });

                var map, marker,
                    init = function() {
                        $scope.auto = null;
                        $scope.styles = angular.extend({}, defaults.styles, $scope.styles);
                        $scope.options = angular.extend({}, defaults.options, $scope.options);
                        $scope.editable = ($scope.editable === 'true');
                        $scope.address = '';
                        $scope.setEntityAddress = function(address) {
                            $scope.auto = false;
                            $scope.entity.address = address;
                            $scope.onPositionChanged();
                        };

                        getPosition().then(getAddress).then(displayMap);
                    };

                var getLocation = function(address) {
                    return $geocoder
                        .getPosition(address)
                        .then(function(position) {
                            $scope.auto = true;
                            $scope.entity.latitude = position.latitude;
                            $scope.entity.longitude = position.longitude;
                        });
                };

                var getAddress = function() {
                    return $geocoder
                        .getAddress($scope.entity.latitude, $scope.entity.longitude)
                        .then(function(address) {
                            $scope.address = address;
                        })
                        .catch(angular.noop);
                };

                var getPosition = function() {
                    if ($scope.entity.latitude && $scope.entity.longitude) {
                        $scope.auto = false;
                        return $q.when();
                    }
                    return $geolocation.getLocation().then(function(data) {
                        $scope.auto = true;
                        $scope.entity.latitude = data.coords.latitude;
                        $scope.entity.longitude = data.coords.longitude;
                        return $q.when();
                    });
                };

                var displayMap = function() {
                    var position = createLatLng();
                        map = new google.maps.Map(element[0], {
                            zoom: parseInt($scope.options.zoom, 10),
                            center: position,
                            scrollwheel: $scope.options.scroll,
                            draggable: $scope.options.draggable,
                            mapTypeId: google.maps.MapTypeId[$scope.options.type],
                            styles: $scope.styles
                        });

                    if($scope.options.marker === 'show') {
                        var image = {
                            url: $scope.options.markerImage,
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(12, 50)
                        };

                        marker = new Marker({
                            position: position,
                            icon:image,
                            map: map,
                            draggable: $scope.editable,
                            $scope: $scope
                        });

                        google.maps.event.addListener(marker, 'click', function() {
                            if (marker.getAnimation() !== null) {
                                marker.setAnimation(null);
                            } else {
                                marker.setAnimation(google.maps.Animation.BOUNCE);
                            }
                        });

                        google.maps.event.addListener(marker, 'dragend', function() {
                            var position = marker.getPosition();
                            $scope.$apply(function() {
                                $scope.auto = false;
                                $scope.entity.latitude = position.lat();
                                $scope.entity.longitude = position.lng();
                                $scope.onPositionChanged();
                                setPosition();
                                getAddress();
                            });
                        });
                    }
                };

                var setPosition = function() {
                    var position = createLatLng();
                    map.setCenter(position);
                    marker.setPosition(position);
                };

                var createLatLng = function() {
                    return new google.maps.LatLng($scope.entity.latitude, $scope.entity.longitude);
                };

                $scope.$watch('entity', init);
            }
        };
    }]);

angular.module('bc').directive('flexslider', [
    '$window',
    function($window) {
        var $ = $window.$,
            flexInit = function(element) {
            // is our slider inside a masonry container?
            var $isotopeContainer = $(element).closest('div.isotope');

            // We use data atributes on the flexslider items to control the behaviour of the slideshow
            var slider = $(element),

            //data-slideshow: defines if the slider will start automatically (true) or not
                sliderShow = slider.attr('data-flex-slideshow') === 'false' ? false : true,

            //data-flex-animation: defines the animation type, slide (default) or fade
                sliderAnimation = !slider.attr('data-flex-animation') ? 'slide' : slider.attr('data-flex-animation'),

            //data-flex-speed: defines the animation speed, 7000 (default) or any number
                sliderSpeed = !slider.attr('data-flex-speed') ? 7000 : slider.attr('data-flex-speed'),

            //data-flex-sliderdirection: defines the slide direction
                direction = !slider.attr('data-flex-sliderdirection') ? 'horizontal' : slider.attr('data-flex-sliderdirection'),

            //data-flex-duration: defines the transition speed in milliseconds
                sliderDuration = !slider.attr('data-flex-duration') ? 600 : slider.attr('data-flex-duration'),

            //data-flex-directions: defines the visibillity of the nanigation arrows, hide (default) or show
                sliderDirections = slider.attr('data-flex-directions') === 'hide' ? false : true,

            //data-flex-directions-type: defines the type of the direction arrows, fancy (with bg) or simple
                sliderDirectionsType = slider.attr('data-flex-directions-type') === 'fancy' ? 'flex-directions-fancy' : '',

            //data-flex-directions-position: defines the positioning of the direction arrows, default (inside the slider) or outside the slider
                sliderDirectionsPosition = slider.attr('data-flex-directions-position') === 'outside' ? 'flex-directions-outside' : '',

            //data-flex-controls: defines the visibillity of the nanigation controls, hide (default) or show
                sliderControls = slider.attr('data-flex-controls') === 'thumbnails' ? 'thumbnails' : slider.attr('data-flex-controls') === 'hide' ? false : true,

            //data-flex-controlsposition: defines the positioning of the controls, inside (default) absolute positioning on the slideshow, or outside
                sliderControlsPosition = slider.attr('data-flex-controlsposition') === 'inside' ? 'flex-controls-inside' : 'flex-controls-outside',

            //data-flex-controlsalign: defines the alignment of the controls, center (default) left or right
                sliderControlsAlign = !slider.attr('data-flex-controlsalign') ? 'flex-controls-center' : 'flex-controls-' + slider.attr('data-flex-controlsalign'),

            //data-flex-itemwidth: the width of each item in case of a multiitem carousel, 0 (default for 100%) or a nymber representing pixels
                sliderItemWidth = !slider.attr('data-flex-itemwidth') ? 0 : parseInt(slider.attr('data-flex-itemwidth'), 10),

            //data-flex-itemmax: the max number of items in a carousel
                sliderItemMax = !slider.attr('data-flex-itemmax') ? 0 : parseInt(slider.attr('data-flex-itemmax'), 0),

            //data-flex-itemmin: the max number of items in a carousel
                sliderItemMin = !slider.attr('data-flex-itemmin') ? 0 : parseInt(slider.attr('data-flex-itemmin'), 0),

            //data-flex-captionvertical: defines the vertical positioning of the captions, top or bottom
                sliderCaptionsVertical = slider.attr('data-flex-captionvertical') === 'top' ? 'flex-caption-top' : '',

            //data-flex-captionvertical: defines the horizontal positioning of the captions, left or right or alternate
                sliderCaptionsHorizontal = slider.attr('data-flex-captionhorizontal') === 'alternate' ? 'flex-caption-alternate' : 'flex-caption-'+ slider.attr('data-flex-captionhorizontal');

            //assign the positioning classes to the navigation
            slider.addClass(sliderControlsPosition).addClass(sliderControlsAlign).addClass(sliderDirectionsType).addClass(sliderDirectionsPosition).addClass(sliderCaptionsHorizontal).addClass(sliderCaptionsVertical);

            slider.flexslider({
                slideshow: sliderShow,
                animation: sliderAnimation,
                direction: direction,
                slideshowSpeed: parseInt(sliderSpeed),
                animationSpeed: parseInt(sliderDuration),
                itemWidth: sliderItemWidth,
                minItems: sliderItemMin,
                maxItems: sliderItemMax,
                controlNav: sliderControls,
                directionNav: sliderDirections,
                prevText: '',
                nextText: '',
                smoothHeight: true,
                useCSS : false,
                after: function(slider) {
                    if($isotopeContainer.length > 0 ){
                        // if flexslider is inside masonry container, trigger relayout in case of uneven sized images
                        $isotopeContainer.isotope( 'reLayout' );
                    }
                }
                });
        };

        return {
            restrict: 'C',
            link: function($_, element) {
                var $element = $(element[0]);
                if ($element.parents('.carousel').length > 0) {
                    return;
                }
                $element.imagesLoaded().done(function() {
                    flexInit(element[0]);
                });
            }
        };
    }]);
