'use strict';

$(document).ready(function () {
    $('#add-event-button').click(function() {
        $('.navbar-toggle').addClass('collapsed');
    });

    $('.event-gallery').each(function(index , value) {
        var items = [];

        $('.event-gallery-item').each(function () {
            items.push({
                src: $(this).attr('href'),
                title: $(this).attr('title'),
                type: $(this).attr('data-gallery-type')
            });
        });

        $(this).magnificPopup({
            mainClass: 'mfp-fade',
            items:items,
            gallery:{
                enabled: true,
                tPrev: $(this).data('prev-text'),
                tNext: $(this).data('next-text'),
                navigateByImgClick: true,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>' // markup of an arrow button
            },
            type: 'image'
        });
    });

    var filters = $('.isotope-filters-multiple');
    filters.on('click', 'a', function(e) {
        var current = $(this),
            currentGroup = current.attr('data-filter-group'),
            currentFilter = current.attr('data-filter'),
            parentFilters = current.closest('.isotope-filters-multiple');

        if (currentFilter === '*') {
            if(current.hasClass('active')) {
                // don't do anything if clicked already selected filter
                return false;
            }
            // clear all active filters
            parentFilters.find('a').removeClass('active');
            // make clicked filter active
            current.addClass('active');
        }
        else {
            if (current.hasClass('active')) {
                current.removeClass('active');
            }
            else {
                parentFilters.find('a[data-filter="*"]').removeClass('active');
                parentFilters.find('a[data-filter-group="' + currentGroup + '"]').removeClass('active');
                current.addClass('active');
            }
            currentFilter = '';
            parentFilters.find('a.active').each(function(_, a) {
                currentFilter += $(a).attr('data-filter');
            });
            if (!currentFilter) {
                currentFilter = '*';
                parentFilters.find('a[data-filter="*"]').addClass('active');
            }
        }

        // filter the collection below the isotope-filters (find for recent posts , addback for port)
        parentFilters.next().find('.isotope').addBack('.isotope').isotope({ filter: currentFilter });

        // return false to prevent click
        return false;
    });

    // Super dirty hack to fix the bug with image displaying
    setTimeout(function() { $(window).trigger('resize'); }, 5000);
    setTimeout(function() { $(window).trigger('resize'); }, 10000);
});
