'use strict';

/**
 * @ngdoc module
 * @name menara
 */
angular.module('menara', []);

/**
 * @ngdoc run
 * @module menara
 */
angular.module('menara').run([
    function() {
        if (angular.isUndefined(String.prototype.contains)) {
            String.prototype.contains = function(searchString, position) {
                return (String(this).indexOf(searchString, position) > -1);
            };
        }
        if (angular.isUndefined(String.prototype.trim)) {
            String.prototype.trim = function() {
                return String(this).replace(/^\s+|\s+$/g, '');
            };
        }
        if (typeof String.prototype.startsWith != 'function') {
            String.prototype.startsWith = function (str){
                return this.slice(0, str.length) == str;
            };
        }
        if (typeof String.prototype.endsWith != 'function') {
            String.prototype.endsWith = function (str){
                return this.slice(-str.length) == str;
            };
        }
        if (typeof Number.prototype.toRadians != 'function') {
            Number.prototype.toRadians = function() {
                return this * Math.PI / 180;
            }
        }
        if (angular.isUndefined(Number.prototype.mod)) {
            Number.prototype.mod = function(n) {
                return ((this % n) + n) % n;
            };
        }
        Array.prototype.has = function(value, valueGetter) {
            return this.getIndexOf(value, valueGetter) >= 0;
        };
        Array.prototype.getIndexOf = function(value, valueGetter) {
            if (angular.isUndefined(valueGetter)) {
                return this.indexOf(value);
            }
            return this.map(valueGetter).indexOf(valueGetter(value));
        };
        Array.prototype.last = function() {
            return this[this.length - 1];
        };
        Array.prototype.flatten = function() {
            return [].concat.apply([], this);
        };
        Array.prototype.unique = function(func) {
            return this
                .uniqueAndCount(func)
                .map(function(counter) { return counter.item });
        };
        Array.prototype.uniqueAndSort = function(func) {
            return this
                .uniqueAndCount(func)
                .sort(function(a, b) { return b.count - a.count; })
                .map(function(counter) { return counter.item });
        };
        Array.prototype.uniqueAndCount = function(func) {
            var dict = {};
            for (var i = 0, item; item = this[i++];) {
                var uniqueId = func(item),
                    counter = dict[uniqueId];
                if (angular.isUndefined(counter)) {
                    dict[uniqueId] = {
                        item: item,
                        count: 1
                    };
                }
                else {
                    counter.count += 1;
                }
            }
            var ret = [];
            angular.forEach(dict, function(value) {
                ret.push(value);
            });
            return ret;
        };
        Array.prototype.partition = function(func) {
            var ret = [[], []];
            for (var i = 0, item; item = this[i++];) {
                if (func(item)) {
                    ret[0].push(item);
                }
                else {
                    ret[1].push(item);
                }
            }
            return ret;
        };
    }]);

/**
 * @ngdoc filter
 * @module menara
 * @name default
 */
angular.module('menara').filter('default', [
    function() {
        return function(value, defaultValue) {
            if (!value) {
                return defaultValue;
            }
            return value;
        }
    }]);

/**
 * @ngdoc service
 * @module menara
 * @name $functools
 */
angular.module('menara').service('$functools', [
    function() {
        var that = this;

        this.attrgetter = function(path, _default) {
            path = path.split('.');
            return function(object) {
                angular.forEach(path, function(attr) {
                    if (!object) {
                        object = _default;
                    }
                    else {
                        object = object[attr];
                    }
                });
                return object;
            };
        };

        this.attrsetter = function(path, func) {
            path = path.split('.');
            var last = path.last();
            path.splice(-1, 1);
            return function(object) {
                angular.forEach(path, function(attr) {
                    if (!object) {
                        object = {};
                    }
                    else {
                        object = object[attr];
                    }
                });
                object[last] = func(object);
                return object;
            }
        };

        this.methodcaller = function(path, _default) {
            return function(object) {
                return that.attrgetter(path, function() { return _default; })(object)();
            };
        };

        this.noop = angular.noop;

        this.arg = function(value) {
            return function() {
                return value;
            }
        };

        this.bool = function(value) {
            return !!value;
        };
    }]);

/**
 * @ngdoc factory
 * @module menara
 * @name $loading
 */
angular.module('menara').factory('$loading', [
    '$rootScope',
    function($rootScope) {
        $rootScope.$loading = {};

        return function(name, func) {
            return function() {
                $rootScope.$loading[name] = true;
                return func().finally(function() {
                    $rootScope.$loading[name] = false;
                });
            };
        };
    }]);

/**
 * @ngdoc filter
 * @module menara
 * @name escape
 */
angular.module('menara').filter('escape', [
    '$sce',
    function($sce) {
        return function(value) {
            return $sce.trustAsUrl(encodeURIComponent($sce.trustAsUrl(value)));
        };
    }]);

/**
 * @ngdoc filter
 * @module menara
 * @name replace
 */
angular.module('menara').filter('replace', [
    function() {
        return function(subject, search, replace) {
            if (typeof(subject) !== 'string') {
                return subject;
            }
            return subject.replace(search, replace);
        };
    }]);

/**
 * @ngdoc factory
 * @module menara
 * @name $once
 */
angular.module('menara').factory('$once', [
    '$timeout',
    function($timeout) {
        return function(delay, fn) {
            var handler = null, last = null;
            return function() {
                var args = arguments;
                if (handler === null) {
                    last = function() {};
                    handler = $timeout(function() { handler = null; }, delay).then(function() { last(); });
                    fn.apply(null, args);
                }
                else {
                    last = function() {
                        fn.apply(null, args);
                    };
                }
            };
        };
    }]);

/**
 * @ngdoc directive
 * @module menara
 * @name fileModel
 * @restrict A
 */
angular.module('menara').directive('fileModel', [
    function() {
        return {
            restrict: 'A',
            scope: {
                fileModel: '='
            },
            link: function($scope, element) {
                var tagName = element[0].tagName.toLowerCase();
                if (tagName === 'input') {
                    element.on('change', function(e) {
                        var file = e.currentTarget.files[0];
                        if (angular.isUndefined(file)) {
                            return;
                        }
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $scope.$apply(function($scope) {
                                $scope.fileModel = e.target.result;
                            });
                        };
                        reader.readAsDataURL(file);
                    });

                    $scope.$watch('fileModel', function(newModel, oldModel) {
                        if (newModel || !oldModel) { return; }
                        element.val('');
                    });
                }
                else if (tagName === 'img') {
                    var src = element.attr('src'),
                        contentType = null;
                    if (src.contains('png')) {
                        contentType = 'image/png'
                    }
                    else {
                        contentType = 'image/jpg';
                    }

                    var canvas = document.createElement('canvas');
                    canvas.width = element[0].width;
                    canvas.height = element[0].height;

                    var context = canvas.getContext('2d');
                    context.drawImage(element[0], 0, 0);

                    // Get the data-URL formatted image
                    // Firefox supports PNG and JPEG. You could check img.src to guess the
                    // original format, but be aware the using "image/jpg" will re-encode the image.
                    $scope.fileModel = canvas.toDataURL(contentType);
                }
            }
        };
    }]);

/**
 * @ngdoc filter
 * @module menara
 * @name ago
 * @TODO: implement
 */
angular.module('menara').filter('ago', [
    '$filter',
    function($filter) {
        var filterDate = $filter('date');

        return function(timestamp) {
            return filterDate(timestamp * 1000, 'yyyy-MM-dd hh:mm');
        };
    }]);

/**
 * @ngdoc filter
 * @module menara
 * @name distance
 */
angular.module('menara').filter('distance', [
    '$geo',
    function($geo) {
        return function(target_position, source_position) {
            if (!target_position || !source_position) {
                return 'N/A';
            }
            if (angular.isUndefined(target_position.__distances__)) {
                target_position.__distances__ = {};
            }
            var key = Math.floor(source_position.latitude) + '_' + Math.floor(source_position.longitude);
            if (angular.isUndefined(target_position.__distances__[key])) {
                target_position.__distances__[key] = $geo.calculate_distance(source_position, target_position);
            }
            return target_position.__distances__[key];
        };
    }]);

/**
 * @ngdoc filter
 * @module menara
 * @name distance
 */
angular.module('menara').filter('formatDistance', [
    '$geo',
    function($geo) {
        return function(distance) {
            return $geo.format_distance(distance);
        };
    }]);

/**
 * @ngdoc service
 * @module menara
 * @name $geo
 */
angular.module('menara').service('$geo', [
    '$i18n',
    function($i18n) {
        this.calculate_distance = function(source_position, target_position) {
            var R = 6371, // km
                lat1 = target_position.latitude,
                lon1 = target_position.longitude,
                lat2 = source_position.latitude,
                lon2 = source_position.longitude,
                fi1 = lat1.toRadians(),
                fi2 = lat2.toRadians(),
                d_fi = (lat2-lat1).toRadians(),
                d_lambda = (lon2-lon1).toRadians(),
                a = Math.sin(d_fi/2) * Math.sin(d_fi/2) +
                    Math.cos(fi1) * Math.cos(fi2) *
                        Math.sin(d_lambda/2) * Math.sin(d_lambda/2),
                c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

            return R * c;
        };

        this.format_distance = function(distance) {
            if (distance < 0.001) {
                return $i18n._('units/distance_close');
            }
            if (distance < 1) {
                return $i18n._('units/distance_small', {distance: Math.floor(distance * 1000)});
            }
            if (distance < 5) {
                return $i18n._('units/distance_large', {distance: Math.floor(distance * 10) / 10});
            }
            return $i18n._('units/distance_large', {distance: Math.floor(distance)});
        };
    }]);

/**
 * @ngdoc service
 * @module menara
 * @name $i18n
 */
angular.module('menara').service('$i18n', [
    function() {
        var map = {
            'units/distance_close': function() { return 'Right here'; },
            'units/distance_small': function(params) { return params.distance + 'm'; },
            'units/distance_large': function(params) { return params.distance + 'km'; }
        };

        this._ = function(scope_label, params) {
            return map[scope_label](params);
        };
    }]);


/**
 * @ngdoc provider
 * @module menara
 * @name $instagram
 */
angular.module('menara').provider('$instagram', [
    function() {
        var endpoint = 'https://api.instagram.com/v1',
            clientId = null;

        var $instagram = function($q, $http) {
            var self = this;

            this.getRecentMediaByLocationList = function(locationList, minTimestamp, maxTimestamp) {
                return $q
                    .all(locationList.map(function(location) {
                        return self.getRecentMediaByLocation(location, minTimestamp, maxTimestamp);
                    }))
                    .then(function(dataList) {
                        return dataList.flatten().sort(function(a, b) {
                            return b.created_time - a.created_time;
                        });
                    });
            };

            this.getRecentMediaByLocation = function(location, minTimestamp, maxTimestamp) {
                return $http
                    .jsonp(endpoint + '/locations/' + location + '/media/recent', {
                        params: {
                            callback: 'JSON_CALLBACK',
                            client_id: clientId,
                            min_timestamp: minTimestamp,
                            max_timestamp: maxTimestamp,
                            count: 30
                        }
                    })
                    .then(returnResponseData);
            };

            var returnResponseData = function(response) {
                if (response.data.meta.code !== 200) {
                    console.debug(response.data.meta);
                    return [];
                }
                return response.data.data;
            };
        };

        this.setClientId = function(argClientId) {
            clientId = argClientId;
        };

        this.$get = [
            '$q', '$http',
            function($q, $http) {
                return new $instagram($q, $http);
            }];
    }]);

/**
 * @ngdoc constant
 * @module menara
 * @name geolocation_messages
 */
angular.module('menara').constant('geolocation_messages', {
    'errors.location.unsupportedBrowser':'Browser does not support location services',
    'errors.location.permissionDenied':'You have rejected access to your location',
    'errors.location.positionUnavailable':'Unable to determine your location',
    'errors.location.timeout':'Service timeout has been reached'
});

/**
 * @ngdoc run
 * @module menara
 */
angular.module('menara').run([
    '$rootScope',
    function($rootScope) {
        $rootScope.$position = {
            current: null
        };
    }]);

/**
 * @ngdoc service
 * @module menara
 * @name $geolocation
 */
angular.module('menara').service('$geolocation', [
    '$q', '$rootScope', '$window', 'geolocation_messages',
    function ($q, $rootScope, $window, geolocation_messages) {
        this.getLocation = function(opts) {
            var deferred = $q.defer();
            if ($window.navigator && $window.navigator.geolocation) {
                $window.navigator.geolocation.getCurrentPosition(function(position){
                    $rootScope.$apply(function(){deferred.resolve(position);});
                    $rootScope.$position.current = position;
                }, function(error) {
                    switch (error.code) {
                        case 1:
                            $rootScope.$broadcast('error',geolocation_messages['errors.location.permissionDenied']);
                            $rootScope.$apply(function() {
                                deferred.reject(geolocation_messages['errors.location.permissionDenied']);
                            });
                            break;
                        case 2:
                            $rootScope.$broadcast('error',geolocation_messages['errors.location.positionUnavailable']);
                            $rootScope.$apply(function() {
                                deferred.reject(geolocation_messages['errors.location.positionUnavailable']);
                            });
                            break;
                        case 3:
                            $rootScope.$broadcast('error',geolocation_messages['errors.location.timeout']);
                            $rootScope.$apply(function() {
                                deferred.reject(geolocation_messages['errors.location.timeout']);
                            });
                            break;
                    }
                }, opts);
            }
            else
            {
                $rootScope.$broadcast('error',geolocation_messages['errors.location.unsupportedBrowser']);
                $rootScope.$apply(function(){deferred.reject(geolocation_messages['errors.location.unsupportedBrowser']);});
            }
            return deferred.promise;
        };
    }]);

/**
 * @ngdoc service
 * @module menara
 * @name $image
 */
angular.module('menara').service('$image', [
    function() {
        this.transform = function(source, filterList) {
            var target = createTarget(source);
            angular.forEach(filterList, function(filter) {
                filter = angular.copy(filter);
                target = filters[filter.shift()].apply(null, [target].concat(filter));
            });
            return target.toDataURL();
        };

        var createTarget = function(source) {
            var imgTag = createImgTag(source),
                canvas = createCanvas(imgTag.width, imgTag.height),
                context = canvas.getContext('2d');
                context.drawImage(imgTag, 0, 0);
            return canvas;
        };

        var createImgTag = function(source) {
            var img = document.createElement('img');
            img.src = source;
            return img;
        };

        var createCanvas = function(width, height) {
            var canvas = document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;
            return canvas;
        };

        var getPixels = function(canvas) {
            return canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height);
        };

        var filters = {
            grayscale: function(sourceCanvas) {
                var pixels = getPixels(sourceCanvas),
                    data = pixels.data;
                for (var i = 0; i < data.length; i+=4) {
                    var r = data[i];
                    var g = data[i+1];
                    var b = data[i+2];
                    // CIE luminance for the RGB
                    // The human eye is bad at seeing red and blue, so we de-emphasize them.
                    var v = 0.2126 * r + 0.7152 * g + 0.0722 * b;
                    data[i] = data[i+1] = data[i+2] = v;
                }
                var targetCanvas = createCanvas(sourceCanvas.width, sourceCanvas.height);
                    targetCanvas.getContext('2d').putImageData(pixels, 0, 0);
                return targetCanvas;
            },
            scale: function(sourceCanvas, targetWidth, targetHeight) {
                var widthRatio =  targetWidth / sourceCanvas.width;
                var heightRatio = targetHeight/ sourceCanvas.height;

                if (widthRatio >= 1 || heightRatio >= 1) return sourceCanvas;

                if (widthRatio > heightRatio) {
                    return cropCanvas(downscaleCanvas(sourceCanvas, widthRatio), targetWidth, targetHeight);
                } else {
                    return cropCanvas(downscaleCanvas(sourceCanvas, heightRatio), targetWidth, targetHeight);
                }
            }
        };

        var cropCanvas = function(sourceCanvas, targetWidth, targetHeight) {
            var sw = sourceCanvas.width;
            var sh = sourceCanvas.height;
            var sBuffer = sourceCanvas.getContext('2d');

            // create result canvas
            var targetCanvas = document.createElement('canvas');
            targetCanvas.width = targetWidth;
            targetCanvas.height = targetHeight;
            var targetCanvasContext = targetCanvas.getContext('2d');
            var imgRes = sBuffer.getImageData((sw - targetWidth) / 2, (sh - targetHeight) / 2, targetWidth, targetHeight);
            // writing result to canvas.
            targetCanvasContext.putImageData(imgRes, 0, 0);
            return targetCanvas;
        };

        var downscaleCanvas = function(sourceCanvas, scale) {
            // if (!(scale < 1) || !(scale > 0)) throw ('scale must be a positive number <1 ');
            var sqScale = scale * scale; // square scale = area of source pixel within target
            var sw = sourceCanvas.width; // source image width
            var sh = sourceCanvas.height; // source image height
            var tw = Math.floor(sw * scale); // target image width
            var th = Math.floor(sh * scale); // target image height
            // EDIT (credits to @Enric ) : was ceil before, and creating artifacts :
            //                           var tw = Math.ceil(sw * scale); // target image width
            //                           var th = Math.ceil(sh * scale); // target image height
            var sx = 0, sy = 0, sIndex = 0; // source x,y, index within source array
            var tx = 0, ty = 0, yIndex = 0, tIndex = 0; // target x,y, x,y index within target array
            var tX = 0, tY = 0; // rounded tx, ty
            var w = 0, nw = 0, wx = 0, nwx = 0, wy = 0, nwy = 0; // weight / next weight x / y
            // weight is weight of current source point within target.
            // next weight is weight of current source point within next target's point.
            var crossX = false; // does scaled px cross its current px right border ?
            var crossY = false; // does scaled px cross its current px bottom border ?
            var sBuffer = sourceCanvas.getContext('2d').
                getImageData(0, 0, sw, sh).data; // source buffer 8 bit rgba
            var tBuffer = new Float32Array(3 * sw * sh); // target buffer Float32 rgb
            var sR = 0, sG = 0,  sB = 0; // source's current point r,g,b
            /* untested !
             var sA = 0;  //source alpha  */

            for (sy = 0; sy < sh; sy++) {
                ty = sy * scale; // y src position within target
                tY = 0 | ty;     // rounded : target pixel's y
                yIndex = 3 * tY * tw;  // line index within target array
                crossY = (tY != (0 | ty + scale));
                if (crossY) { // if pixel is crossing botton target pixel
                    wy = (tY + 1 - ty); // weight of point within target pixel
                    nwy = (ty + scale - tY - 1); // ... within y+1 target pixel
                }
                for (sx = 0; sx < sw; sx++, sIndex += 4) {
                    tx = sx * scale; // x src position within target
                    tX = 0 |  tx;    // rounded : target pixel's x
                    tIndex = yIndex + tX * 3; // target pixel index within target array
                    crossX = (tX != (0 | tx + scale));
                    if (crossX) { // if pixel is crossing target pixel's right
                        wx = (tX + 1 - tx); // weight of point within target pixel
                        nwx = (tx + scale - tX - 1); // ... within x+1 target pixel
                    }
                    sR = sBuffer[sIndex    ];   // retrieving r,g,b for curr src px.
                    sG = sBuffer[sIndex + 1];
                    sB = sBuffer[sIndex + 2];

                    /* !! untested : handling alpha !!
                     sA = sBuffer[sIndex + 3];
                     if (!sA) continue;
                     if (sA != 0xFF) {
                     sR = (sR * sA) >> 8;  // or use /256 instead ??
                     sG = (sG * sA) >> 8;
                     sB = (sB * sA) >> 8;
                     }
                     */
                    if (!crossX && !crossY) { // pixel does not cross
                        // just add components weighted by squared scale.
                        tBuffer[tIndex    ] += sR * sqScale;
                        tBuffer[tIndex + 1] += sG * sqScale;
                        tBuffer[tIndex + 2] += sB * sqScale;
                    } else if (crossX && !crossY) { // cross on X only
                        w = wx * scale;
                        // add weighted component for current px
                        tBuffer[tIndex    ] += sR * w;
                        tBuffer[tIndex + 1] += sG * w;
                        tBuffer[tIndex + 2] += sB * w;
                        // add weighted component for next (tX+1) px
                        nw = nwx * scale;
                        tBuffer[tIndex + 3] += sR * nw;
                        tBuffer[tIndex + 4] += sG * nw;
                        tBuffer[tIndex + 5] += sB * nw;
                    } else if (crossY && !crossX) { // cross on Y only
                        w = wy * scale;
                        // add weighted component for current px
                        tBuffer[tIndex    ] += sR * w;
                        tBuffer[tIndex + 1] += sG * w;
                        tBuffer[tIndex + 2] += sB * w;
                        // add weighted component for next (tY+1) px
                        nw = nwy * scale;
                        tBuffer[tIndex + 3 * tw    ] += sR * nw;
                        tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                        tBuffer[tIndex + 3 * tw + 2] += sB * nw;
                    } else { // crosses both x and y : four target points involved
                        // add weighted component for current px
                        w = wx * wy;
                        tBuffer[tIndex    ] += sR * w;
                        tBuffer[tIndex + 1] += sG * w;
                        tBuffer[tIndex + 2] += sB * w;
                        // for tX + 1; tY px
                        nw = nwx * wy;
                        tBuffer[tIndex + 3] += sR * nw;
                        tBuffer[tIndex + 4] += sG * nw;
                        tBuffer[tIndex + 5] += sB * nw;
                        // for tX ; tY + 1 px
                        nw = wx * nwy;
                        tBuffer[tIndex + 3 * tw    ] += sR * nw;
                        tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                        tBuffer[tIndex + 3 * tw + 2] += sB * nw;
                        // for tX + 1 ; tY +1 px
                        nw = nwx * nwy;
                        tBuffer[tIndex + 3 * tw + 3] += sR * nw;
                        tBuffer[tIndex + 3 * tw + 4] += sG * nw;
                        tBuffer[tIndex + 3 * tw + 5] += sB * nw;
                    }
                } // end for sx
            } // end for sy

            // create result canvas
            var targetCanvas = document.createElement('canvas');
            targetCanvas.width = tw;
            targetCanvas.height = th;
            var targetCanvasContext = targetCanvas.getContext('2d');
            var imgRes = targetCanvasContext.getImageData(0, 0, tw, th);
            var tByteBuffer = imgRes.data;
            // convert float32 array into a UInt8Clamped Array
            var pxIndex = 0; //
            for (sIndex = 0, tIndex = 0; pxIndex < tw * th; sIndex += 3, tIndex += 4, pxIndex++) {
                tByteBuffer[tIndex] = Math.ceil(tBuffer[sIndex]);
                tByteBuffer[tIndex + 1] = Math.ceil(tBuffer[sIndex + 1]);
                tByteBuffer[tIndex + 2] = Math.ceil(tBuffer[sIndex + 2]);
                tByteBuffer[tIndex + 3] = 255;
            }
            // writing result to canvas.
            targetCanvasContext.putImageData(imgRes, 0, 0);
            return targetCanvas;
        };

    }]);
