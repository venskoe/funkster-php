// Flexslider Init
// ======================

function flexInit(element) {
    // is our slider inside a masonry container?
    var $isotopeContainer = $(element).closest('div.isotope');

    // We use data atributes on the flexslider items to control the behaviour of the slideshow
    var slider = $(element),

    //data-slideshow: defines if the slider will start automatically (true) or not
        sliderShow = slider.attr('data-flex-slideshow') === 'false' ? false : true,

    //data-flex-animation: defines the animation type, slide (default) or fade
        sliderAnimation = !slider.attr('data-flex-animation') ? 'slide' : slider.attr('data-flex-animation'),

    //data-flex-speed: defines the animation speed, 7000 (default) or any number
        sliderSpeed = !slider.attr('data-flex-speed') ? 7000 : slider.attr('data-flex-speed'),

    //data-flex-sliderdirection: defines the slide direction
        direction = !slider.attr('data-flex-sliderdirection') ? 'horizontal' : slider.attr('data-flex-sliderdirection'),

    //data-flex-duration: defines the transition speed in milliseconds
        sliderDuration = !slider.attr('data-flex-duration') ? 600 : slider.attr('data-flex-duration'),

    //data-flex-directions: defines the visibillity of the nanigation arrows, hide (default) or show
        sliderDirections = slider.attr('data-flex-directions') === 'hide' ? false : true,

    //data-flex-directions-type: defines the type of the direction arrows, fancy (with bg) or simple
        sliderDirectionsType = slider.attr('data-flex-directions-type') === 'fancy' ? 'flex-directions-fancy' : '',

    //data-flex-directions-position: defines the positioning of the direction arrows, default (inside the slider) or outside the slider
        sliderDirectionsPosition = slider.attr('data-flex-directions-position') === 'outside' ? 'flex-directions-outside' : '',

    //data-flex-controls: defines the visibillity of the nanigation controls, hide (default) or show
        sliderControls = slider.attr('data-flex-controls') === 'thumbnails' ? 'thumbnails' : slider.attr('data-flex-controls') === 'hide' ? false : true,

    //data-flex-controlsposition: defines the positioning of the controls, inside (default) absolute positioning on the slideshow, or outside
        sliderControlsPosition = slider.attr('data-flex-controlsposition') === 'inside' ? 'flex-controls-inside' : 'flex-controls-outside',

    //data-flex-controlsalign: defines the alignment of the controls, center (default) left or right
        sliderControlsAlign = !slider.attr('data-flex-controlsalign') ? 'flex-controls-center' : 'flex-controls-' + slider.attr('data-flex-controlsalign'),

    //data-flex-itemwidth: the width of each item in case of a multiitem carousel, 0 (default for 100%) or a nymber representing pixels
        sliderItemWidth = !slider.attr('data-flex-itemwidth') ? 0 : parseInt(slider.attr('data-flex-itemwidth'), 10),

    //data-flex-itemmax: the max number of items in a carousel
        sliderItemMax = !slider.attr('data-flex-itemmax') ? 0 : parseInt(slider.attr('data-flex-itemmax'), 0),

    //data-flex-itemmin: the max number of items in a carousel
        sliderItemMin = !slider.attr('data-flex-itemmin') ? 0 : parseInt(slider.attr('data-flex-itemmin'), 0),

    //data-flex-captionvertical: defines the vertical positioning of the captions, top or bottom
        sliderCaptionsVertical = slider.attr('data-flex-captionvertical') === 'top' ? 'flex-caption-top' : '',

    //data-flex-captionvertical: defines the horizontal positioning of the captions, left or right or alternate
        sliderCaptionsHorizontal = slider.attr('data-flex-captionhorizontal') === 'alternate' ? 'flex-caption-alternate' : 'flex-caption-'+ slider.attr('data-flex-captionhorizontal');

    //assign the positioning classes to the navigation
    slider.addClass(sliderControlsPosition).addClass(sliderControlsAlign).addClass(sliderDirectionsType).addClass(sliderDirectionsPosition).addClass(sliderCaptionsHorizontal).addClass(sliderCaptionsVertical);

    slider.flexslider({
        slideshow: sliderShow,
        animation: sliderAnimation,
        direction: direction,
        slideshowSpeed: parseInt(sliderSpeed),
        animationSpeed: parseInt(sliderDuration),
        itemWidth: sliderItemWidth,
        minItems: sliderItemMin,
        maxItems: sliderItemMax,
        controlNav: sliderControls,
        directionNav: sliderDirections,
        prevText: '',
        nextText: '',
        smoothHeight: true,
        useCSS : false,
        after: function(slider) {
            if($isotopeContainer.length > 0 ){
                // if flexslider is inside masonry container, trigger relayout in case of uneven sized images
                $isotopeContainer.isotope( 'reLayout' );
            }
        }
    });
}


$('.flexslider[id]').filter(function(){
    return $(this).parents('.carousel').length < 1;
}).each(function(){
    var that = this;
    $(this).imagesLoaded().done( function(instance){
        flexInit(that);
    });
});

// Scroll to hide nav in iOS < 7
// ======================

if( ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) ) && !navigator.userAgent.match(/(iPad|iPhone);.*CPU.*OS 7_\d/i) ) {
    if(window.addEventListener){
        window.addEventListener('load',function() {
            // Set a timeout...
            setTimeout(function(){
                // Hide the address bar!
                window.scrollTo(0, 1);
            }, 0);
        });
    }
}

// Function to init bootstrap's tooltip
$('[data-toggle="tooltip"]').tooltip();

// Function to init bootstrap's carousel
$('.carousel').carousel({
    interval: 7000
});

// init nested flexsliders inside each slide when shown
$('.carousel').on('slid',function(event){
    setTimeout(function(){
        initNestedSliders();
    },0);
});

function initNestedSliders(){
    $('.carousel').find('.active .flexslider[id]').each(function(){
        if(!$(this).hasClass('triggered')){
            $(this).addClass('triggered');
            flexInit(this);
        }
    });
}

// for initial slide there is no slid event
initNestedSliders();
