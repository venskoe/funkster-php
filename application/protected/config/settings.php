<?php

return array(
    'storage' => array(
        'redis' => array(
            'host' => '127.0.0.1',
            'port' => 6379,
        ),
        'neo4j' => array(
            'host' => '127.0.0.1',
            'port' => 7474,
        ),
        'mongo' => array(
            'host' => '127.0.0.1',
            'port' => 27017,
        ),
    ),
    'files' => array(
        'roots' => array(
            'public' => __DIR__ . '/../../public/uploads',
            'http' => '/uploads',
        ),
        'cdn' => '',
        'web' => '',
    ),
    'socials' => array(
        'facebook' => array(
            'client_id' => null,
            'client_secret' => null,
        ),
    ),
    'security' => array(
        'tokens' => array(
            'cipher' => MCRYPT_3DES,
            'mode' => MCRYPT_MODE_CFB,
            'key' => substr(md5('staging-server-key'), 0, 24),
            'iv' => 'krZaW1ly968=',
        ),
    ),
    'logging' => array(
        'targets' => array(
            'runtime' => __DIR__ . '/../var/log/runtime.log',
            'console' => __DIR__ . '/../var/log/console.log',
        ),
    ),
    'locales' => array(
        'scheme' => ['en', 'ru'],
        'labels' => array(
            'events' => array(
                'comment_count' => [
                    '{count, plural, one{1 comment} other{# comments}}',
                    '{count, plural, one{1 комментарий} few{# комментария} other{# комментариев}}',
                ],
            ),
        ),
    ),
    'release' => array(
        'mocks' => false,
    ),
);
