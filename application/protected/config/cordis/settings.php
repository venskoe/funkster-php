<?php

return array(
    'socials' => array(
        'facebook' => array(
            'client_id' => '1499260270357472',
            'client_secret' => '16ca82f4fb3993173c645ebad7824c27',
        ),
        'instagram' => array(
            'client_id' => '23c1b4a6b6b64578a91595407de94d35',
        ),
    ),
    'release' => array(
        'mocks' => true,
    ),
    'files' => array(
        'web' => 'http://php.funkster.local',
    ),
);
