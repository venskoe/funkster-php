<?php

return function(Phalcon\DI $di)
{
    $di->setShared('\Menara\Generic\Interfaces\ICipher', new \Menara\Generic\Cipher\MCrypt());
    $di->setShared('\Menara\Generic\Interfaces\IFormat::json', new \Menara\Generic\Implementations\Format\PhpJson());
    $di->set('\Menara\Generic\Interfaces\ILogger', '\Generic\Implementations\Logger\PhalconLogger');
    $di->set('\Menara\Generic\Interfaces\IRequest', function() use ($di) {
        $adapter = $di->get('\Menara\Generic\Interfaces\IRequestAdapter', func_get_args());
        return new \Menara\Generic\Request\Request($adapter);
    });
    $di->set('\Menara\Generic\Interfaces\IRequestAdapter', function($arguments, \Phalcon\Http\Request $impl) use ($di) {
        $instance = new \Menara\Generic\Request\Adapters\StubAdapter();
        $instance = new \Menara\Generic\Request\Adapters\DictAdapter($instance, (array)$impl->getJsonRawBody());
        $instance = new \Menara\Generic\Request\Adapters\PhalconRequestAdapter($instance, $impl);
        $instance = new \Menara\Generic\Request\Adapters\PhalconCookiesAdapter($instance, $di->get('cookies'));
        $instance = new \Menara\Generic\Request\Adapters\DictAdapter($instance, $arguments);
        $instance = new \Menara\Generic\Request\Adapters\CallAdapter($instance, $impl);
        return $instance;
    });
    $di->setShared('\Menara\Generic\Storage\Interfaces\IRedisClient', function() use ($di) {
        $config = $di->getShared('config')->storage->redis;
        $redis = new \Redis();
        $redis->connect($config->host, $config->port);
        $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
        return new Menara\Generic\Storage\Redis\Client($redis);
    });
    $di->setShared('\Menara\Generic\Storage\Interfaces\IGraphClient', function() use ($di) {
        $config = $di->getShared('config')->storage->neo4j;
        $neo4j = new \Everyman\Neo4j\Client($config->host, $config->port);
        $client = new Menara\Generic\Storage\Neo4j\Client($neo4j);
        return $client;
    });
    $di->setShared('\Menara\Generic\Storage\Interfaces\IMongoClient', function() use ($di) {
        $config = $di->getShared('config')->storage->mongo;
        $mongo = new \MongoClient(strtr('mongodb://host:port', (array)$config));
        return new Menara\Generic\Storage\Mongo\Client($mongo);
    });
    $di->setShared('\Menara\Generic\Phalcon\Listener\Request', new \Menara\Generic\Phalcon\Listener\Request());
    $di->setShared('\Menara\Generic\Phalcon\Listener\Protocol', function() use ($di) {
        $instance = new \Menara\Generic\Phalcon\Listener\Protocol();
        $instance->protocol = $di->getShared('\Menara\Web\Interfaces\IProtocol');
        return $instance;
    });
    $di->setShared('\Menara\Generic\Phalcon\Listener\Injector', new \Menara\Generic\Phalcon\Listener\Injector());
    $di->setShared('\Menara\Generic\Phalcon\Listener\Response', function() use ($di) {
        $instance = new \Menara\Generic\Phalcon\Listener\Response();
        $instance->format = $di->getShared('\Menara\Generic\Interfaces\IFormat::json');
        $instance->protocol = $di->getShared('\Menara\Web\Interfaces\IProtocol');
        return $instance;
    });
    $di->setShared('\Menara\Generic\Console\Interfaces\IRunner', array(
        'className' => '\Menara\Generic\Console\Runner\Reactive',
        'properties' => [
            array(
                'name' => 'loop',
                'value' => array('type' => 'service', 'name' => '\Menara\Generic\Console\Runner\Reactive::loop'),
            ),
        ],
    ));
    //$di->setShared('\Menara\Generic\Console\Runner\Reactive::loop', function() {
    //    return React\EventLoop\Factory::create();
    //});
    $di->setShared('\Menara\Generic\Console\Interfaces\IWatchdog', array(
        'className' => '\Menara\Generic\Console\Watchdog',
        'properties' => [
            array(
                'name' => 'logger',
                'value' => array('type' => 'service', 'name' => '\Menara\Generic\Interfaces\ILogger::watchdog'),
            ),
        ],
    ));
    return $di;
};
