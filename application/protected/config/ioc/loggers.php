<?php

use \Phalcon\Logger;

return function(Phalcon\DI $di)
{
    $di->setShared('\Logger\Adapter::runtime', function() use ($di) {
        return new Logger\Adapter\File($di->getShared('config')->logging->targets->runtime);
    });
    $di->setShared('\Logger\Adapter::console', function() use ($di) {
        return new Logger\Adapter\File($di->getShared('config')->logging->targets->console);
    });
    $di->setShared('\Logger\Adapter::watchdog', function() use ($di) {
        return new Logger\Adapter\File($di->getShared('config')->logging->targets->console);
    });
    $di->setShared('\Menara\Generic\Interfaces\ILogger::runtime', function() use ($di) {
        return $di->get('\Menara\Generic\Interfaces\ILogger', [$di->getShared('\Logger\Adapter::runtime')]);
    });
    $di->setShared('\Menara\Generic\Interfaces\ILogger::console', function() use ($di) {
        return $di->get('\Menara\Generic\Interfaces\ILogger', [$di->getShared('\Logger\Adapter::console')]);
    });
    $di->setShared('\Menara\Generic\Interfaces\ILogger::watchdog', function() use ($di) {
        return $di->get('\Menara\Generic\Interfaces\ILogger', [$di->getShared('\Logger\Adapter::watchdog')]);
    });
    return $di;
};
