<?php

return function(Phalcon\DI $di)
{
    $di->setShared('\Menara\Auth\Interfaces\IService', array(
        'className' => '\Menara\Auth\Service',
        'properties' => [
            array(
                'name' => 'dao',
                'value' => array('type' => 'service', 'name' => '\Menara\Auth\Interfaces\IDao'),
            ),
        ],
    ));
    $di->setShared('\Menara\Auth\Interfaces\IDao', array(
        'className' => '\Menara\Auth\Implementations\Dao\Encrypted',
        'properties' => [
            array(
                'name' => 'config',
                'value' => array('type' => 'parameter', 'value' => $di->getShared('config')->security->tokens),
            ),
            array(
                'name' => 'cipher',
                'value' => array('type' => 'service', 'name' => '\Menara\Generic\Interfaces\ICipher'),
            ),
        ],
    ));
    return $di;
};
