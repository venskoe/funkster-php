<?php

return function(Phalcon\DI $di) {
    $di->setShared('\Graph\Interfaces\IConsts', array(
        'className' => '\Graph\Consts',
    ));
    $di->setShared('\Graph\Interfaces\IService', array(
        'className' => '\Graph\Service',
        'properties' => [
            array(
                'name' => 'client',
                'value' => array('type' => 'service', 'name' => '\Menara\Generic\Storage\Interfaces\IGraphClient'),
            ),
        ],
    ));
    return $di;
};
