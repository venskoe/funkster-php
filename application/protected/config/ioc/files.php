<?php

return function(Phalcon\DI $di) {
    $di->setShared('\Files\Interfaces\IService', array(
        'className' => '\Files\Service',
        'properties' => [
            array(
                'name' => 'config',
                'value' => array('type' => 'parameter', 'value' => $di->getShared('config')->files),
            ),
        ],
    ));
    return $di;
};
