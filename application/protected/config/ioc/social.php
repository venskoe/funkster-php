<?php

return function(Phalcon\DI $di) {
    $di->setShared('\Menara\Social\Interfaces\IService', array(
        'className' => '\Menara\Social\Service',
        'properties' => [
            array(
                'name' => 'facebook',
                'value' => array('type' => 'service', 'name' => '\Menara\Social\Platforms\Facebook\Client'),
            ),
        ],
    ));
    $di->setShared('\Menara\Social\Platforms\Facebook\Client', array(
        'className' => '\Menara\Social\Platforms\Facebook\Client',
        'arguments' => [
            array(
                'type' => 'parameter',
                'value' =>  $di->getShared('config')->socials->facebook->client_id,
            ),
            array(
                'type' => 'parameter',
                'value' =>  $di->getShared('config')->socials->facebook->client_secret,
            ),
        ],
    ));
    return $di;
};
