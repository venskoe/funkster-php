<?php

return function(Phalcon\DI $di) {
    $di->setShared('\Media\Interfaces\IService', array(
        'className' => '\Media\Service',
    ));
    return $di;
};
