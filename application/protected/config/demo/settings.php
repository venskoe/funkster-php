<?php

return array(
    'security' => array(
        'secrets' => array(),
    ),
    'socials' => array(
        'facebook' => array(
            'client_id' => '786000781471920',
            'client_secret' => 'a472a1aae2abb339d71c3c96b712147f',
        ),
    ),
    'release' => array(
        'mocks' => true,
    ),
    'files' => array(
        'cdn' => '//demo.boogiecall.com',
        'web' => 'http://demo.boogiecall.com'
    )
);
