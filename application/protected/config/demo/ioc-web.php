<?php

return function(Phalcon\DI $di)
{
    $di->setShared('annotations', new \Phalcon\Annotations\Adapter\Apc());
    return $di;
};
