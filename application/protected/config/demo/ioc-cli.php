<?php

return function(Phalcon\DI $di)
{
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . '/ioc/',
        FilesystemIterator::CURRENT_AS_PATHNAME | FilesystemIterator::SKIP_DOTS
    ));
    foreach ($iterator as $pathname) {
        $callable = include($pathname);
        $di = call_user_func_array($callable, [$di]);
    }
    return $di;
};
