<?php

return function(Phalcon\DI $di)
{
    $di->setShared('router', function() use ($di) {
        $router = new \Cli\Implementations\Phalcon\Router(false);
        $router->setActionSuffix('');
        $router->setControllerSuffix('');
        $router->addResource('\Cli\Controllers\Graph');
        return $router;
    });
    $di->setShared('\Cli\Controllers\Manager', array(
        'className' => '\Cli\Controllers\Manager',
    ));
    $di->setShared('\Cli\Controllers\Graph', array(
        'className' => '\Cli\Controllers\Graph',
        'properties' => [
            array(
                'name' => 'release',
                'value' => array('type' => 'parameter', 'value' => $di->getShared('config')->release),
            ),
            array(
                'name' => 'socials',
                'value' => array('type' => 'parameter', 'value' => $di->getShared('config')->socials),
            ),
            array(
                'name' => 'graphClient',
                'value' => array('type' => 'service', 'name' => '\Menara\Generic\Storage\Interfaces\IGraphClient'),
            ),
        ],
    ));
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . '/ioc/',
        FilesystemIterator::CURRENT_AS_PATHNAME | FilesystemIterator::SKIP_DOTS
    ));
    foreach ($iterator as $pathname) {
        $callable = include($pathname);
        $di = call_user_func_array($callable, [$di]);
    }
    return $di;
};
