<?php

return array(
    'security' => array(
        'secrets' => array(),
    ),
    'socials' => array(
        'facebook' => array(
            'client_id' => '783926641679334',
            'client_secret' => 'f6f44f8c4acfca3fc310cd914d79791d',
        ),
        'instagram' => array(
            'client_id' => '23c1b4a6b6b64578a91595407de94d35',
        ),
    ),
    'release' => array(
        'mocks' => true,
    ),
);
