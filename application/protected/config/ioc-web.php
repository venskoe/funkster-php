<?php

return function(Phalcon\DI $di)
{
    $di->setShared('annotations', new \Phalcon\Annotations\Adapter\Apc());
    $di->setShared('url', function(){
        $url = new Phalcon\Mvc\Url();
        $url->setBaseUri('/');
        return $url;
    });
    $di->set('volt', function($view, Phalcon\DI $di) {
        $engine = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
        $engine->setOptions(array(
            'compiledPath' => '../protected/var/cache/views/',
            'compiledExtension' => '.php',
        ));
        $engine->getCompiler()->addFilter('string', function($arguments, $_) {
            return $arguments . ' ? "true" : "false"';
        });
        $engine->getCompiler()->addFunction('_', function($arguments, $_) {
            return strtr('print_r([0], true)', [$arguments]);
        });
        $engine->getCompiler()->addFilter('localtimestamp', function($arguments, $_) {
            return strtr('[$args][0] - [$args][1] * 60', ['$args' => $arguments]);
        });
        $engine->getCompiler()->addFilter('ago', function($arguments, $_) {
            return strtr('date("Y-m-d H:i", 0)', [$arguments]);
        });
        $engine->getCompiler()->addFilter('date', function($arguments, $_) {
            return strtr('date("d F Y", 0)', [$arguments]);
        });
        $engine->getCompiler()->addFilter('dateFormat', function($arguments, $_) {
            return strtr('call_user_func_array("date", array_reverse([0]))', [$arguments]);
        });
        $engine->getCompiler()->addFilter('dateShort', function($arguments, $_) {
            return strtr('date("d F", 0)', [$arguments]);
        });
        $engine->getCompiler()->addFilter('time', function($arguments, $_) {
            return strtr('date("H:i", 0)', [$arguments]);
        });
        $engine->getCompiler()->addFilter('humanify', function($arguments, $_) {
            return strtr('ucfirst(str_replace("_", " ", 0))', [$arguments]);
        });
        $engine->getCompiler()->addFilter('cdn', function($arguments, $_) {
            return strtr('implode("", [$this->config->files->cdn, 0])', [$arguments]);
        });
        $engine->getCompiler()->addFilter('web', function($arguments, $_) {
            return strtr('implode("", [$this->config->files->web, 0])', [$arguments]);
        });
        $engine->getCompiler()->addFunction('js', function($arguments, $_) {
            return strtr('call_user_func_array($this->voltJsHandler, [[0]])', [$arguments]);
        });
        $engine->getCompiler()->addFunction('css', function($arguments, $_) {
            return strtr('call_user_func_array($this->voltCssHandler, [[0]])', [$arguments]);
        });
        return $engine;
    });
    $di->set('voltJsHandler', function() use ($di) {
        /**
         * @var \Phalcon\Assets\Manager $assets
         */
        $assets = $di->getShared('assets');
        $cdn = $di->getShared('config')->files->cdn;

        return function($jsList) use ($assets, $cdn) {
            $collection = $assets->collection('js')
                ->setPrefix($cdn ? substr($cdn, 1) . '/' : '')
                ->addFilter(new Phalcon\Assets\Filters\Jsmin())
                ->join(true)
                ->setTargetPath(__DIR__ . '/../../public/assets/generated/bundle.js')
                ->setTargetUri('assets/generated/bundle.js');
            foreach ($jsList as $js) {
                $collection->addJs(__DIR__ . '/../../public' . $js);
            }
            $assets->outputJs('js');
        };
    });
    $di->set('voltCssHandler', function() use ($di) {
        /**
         * @var \Phalcon\Assets\Manager $assets
         */
        $assets = $di->getShared('assets');
        $cdn = $di->getShared('config')->files->cdn;

        return function($cssList) use ($assets, $cdn) {
            $collection = $assets->collection('css')
                ->setPrefix($cdn ? substr($cdn, 1) . '/' : '')
                ->addFilter(new Phalcon\Assets\Filters\Cssmin())
                ->join(true)
                ->setTargetPath(__DIR__ . '/../../public/assets/generated/bundle.css')
                ->setTargetUri('assets/generated/bundle.css');
            foreach ($cssList as $css) {
                $collection->addCss(__DIR__ . '/../../public' . $css);
            }
            $assets->outputCss('css');
        };
    });
    $di->set('view', function() {
        $view = new Phalcon\Mvc\View\Simple();
        $view->setViewsDir('../protected/views/');
        $view->registerEngines(array(
            '.twig' => 'volt'
        ));
        return $view;
    });
    $di->set('cookies', function() {
        $cookies = new Phalcon\Http\Response\Cookies();
        $cookies->useEncryption(false);
        return $cookies;
    });
    $di->setShared('router', function() use ($di) {
        $router = new \Phalcon\Mvc\Router\Annotations(false);
        $router->setActionSuffix('');
        $router->setControllerSuffix('');
        $router->addResource('\Web\Controllers\Rest', '/api/v1');
        $router->addResource('\Web\Controllers\Site', '/');
        return $router;
    });

    $di->setShared('\Menara\Web\Interfaces\IProtocol', new \Web\Protocol());

    $di->setShared('\Web\Controllers\Rest', array(
        'className' => '\Web\Controllers\Rest',
        'properties' => [
            array(
                'name' => 'privacy',
                'value' => array('type' => 'service', 'name' => '\Menara\Web\Interfaces\IPrivacy'),
            ),
            array(
                'name' => 'socialService',
                'value' => array('type' => 'service', 'name' => '\Menara\Social\Interfaces\IService'),
            ),
            array(
                'name' => 'graphService',
                'value' => array('type' => 'service', 'name' => '\Graph\Interfaces\IService'),
            ),
            array(
                'name' => 'filesService',
                'value' => array('type' => 'service', 'name' => '\Files\Interfaces\IService'),
            ),
            array(
                'name' => 'mediaService',
                'value' => array('type' => 'service', 'name' => '\Media\Interfaces\IService'),
            ),
        ],
    ));
    $di->setShared('\Web\Controllers\Site', array(
        'className' => '\Web\Controllers\Site',
        'properties' => [
            array(
                'name' => 'url',
                'value' => array('type' => 'service', 'name' => 'url'),
            ),
            array(
                'name' => 'config',
                'value' => array('type' => 'service', 'name' => 'config'),
            ),
            array(
                'name' => 'privacy',
                'value' => array('type' => 'service', 'name' => '\Menara\Web\Interfaces\IPrivacy'),
            ),
            array(
                'name' => 'graphConsts',
                'value' => array('type' => 'service', 'name' => '\Graph\Interfaces\IConsts'),
            ),
            array(
                'name' => 'graphService',
                'value' => array('type' => 'service', 'name' => '\Graph\Interfaces\IService'),
            ),
        ],
    ));
    $di->setShared('\Menara\Web\Interfaces\IPrivacy', array(
        'className' => '\Menara\Web\Privacy',
        'properties' => [
            array(
                'name' => 'authService',
                'value' => array('type' => 'service', 'name' => '\Menara\Auth\Interfaces\IService'),
            ),
        ],
    ));

    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . '/ioc/',
        FilesystemIterator::CURRENT_AS_PATHNAME | FilesystemIterator::SKIP_DOTS
    ));
    foreach ($iterator as $pathname) {
        $callable = include($pathname);
        $di = call_user_func_array($callable, [$di]);
    }
    return $di;
};
