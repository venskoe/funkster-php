<?php

return array(
    'security' => array(
        'secrets' => array(),
    ),
    'socials' => array(
        'facebook' => array(
            'client_id' => '783926271679371',
            'client_secret' => 'ba669d1d298c23b03e6f16a5d3e46a3d',
        ),
        'instagram' => array(
            'client_id' => '2b3403b2bbcb4fd7bfb3f22f8b5a022b',
        )
    ),
    'files' => array(
        'cdn' => '//cdn.boogiecall.com',
        'web' => 'http://www.boogiecall.com',
    ),
);
