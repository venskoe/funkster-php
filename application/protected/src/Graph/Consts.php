<?php

namespace Graph;

class Consts
{
    public $TZ_OFFSET_MIN = -780;
    public $TZ_OFFSET_MAX = +900;

    public $TAG_MIN_TITLE_LENGTH = 2;
    public $TAG_MAX_TITLE_LENGTH = 16;

    public $EVENT_TITLE_MIN_LENGTH = 2;
    public $EVENT_TITLE_MAX_LENGTH = 128;
    public $EVENT_ARTIST_LIST_LIMIT = 8;
    public $EVENT_TAG_LIST_LIMIT = 8;

    public $PLACE_TITLE_MAX_LENGTH = 128;
    public $PLACE_TITLE_MIN_LENGTH = 2;
    public $PLACE_INTRO_MAX_LENGTH = 512;
    public $PLACE_ADDRESS_MAX_LENGTH = 256;
    public $PLACE_LINK_LIST_LIMIT = 6;
    public $PLACE_SEARCH_MIN_LENGTH = 1;
    public $PLACE_SEARCH_MAX_LENGTH = 4;

    public $ARTIST_TITLE_MIN_LENGTH = 2;
    public $ARTIST_TITLE_MAX_LENGTH = 64;
    public $ARTIST_INTRO_MAX_LENGTH = 512;
    public $ARTIST_ABOUT_MAX_LENGTH = 4096;
    public $ARTIST_HOMETOWN_MIN_LENGTH = 4;
    public $ARTIST_HOMETOWN_MAX_LENGTH = 64;
    public $ARTIST_LINK_LIST_LIMIT = 6;
    public $ARTIST_SEARCH_MIN_LENGTH = 1;
    public $ARTIST_SEARCH_MAX_LENGTH = 4;

    public $NEWS_TITLE_MIN_LENGTH = 8;
    public $NEWS_TITLE_MAX_LENGTH = 64;
    public $NEWS_CONTENT_MIN_LENGTH = 8;
    public $NEWS_CONTENT_MAX_LENGTH = 1024;

    public $LINK_HREF_MAX_LENGTH = 2000;
    public $LINK_SITE_MAP = [
        'twitter.com' => 'twitter',
        'dribble.com' => 'dribble',
        'facebook.com' => 'facebook',
        'linkedin.com' => 'linkedin',
        'pinterest.com' => 'pinterest',
        'instagram.com' => 'instagram',
        'thumbtack.com' => 'thumb-tack',
        'plus.google.com' => 'google-plus',
        'residentadvisor.net' => 'residentadvisor',
    ];

    public $POST_HREF_MIN_LENGTH = 8;
    public $POST_HREF_MAX_LENGTH = 2000;

}
