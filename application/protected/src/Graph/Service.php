<?php

namespace Graph;

use Graph\Queries\GetArtistList;
use Graph\Queries\GetEventListStartsAfter;
use Graph\Queries\GetPlaceList;
use Menara\Generic\Storage\Interfaces\IGraphClient;
use Menara\Generic\Tools;

use Graph\Beans\ArtistPage;
use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Link;
use Graph\Beans\Entities\News;
use Graph\Beans\Entities\Photo;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Post;
use Graph\Beans\Entities\Profile;
use Graph\Beans\Entities\Tag;
use Graph\Beans\Entities\User;
use Graph\Beans\EventListPage;
use Graph\Beans\EventPage;
use Graph\Beans\GeoIndexPage;
use Graph\Beans\PlacePage;
use Graph\Beans\EmptyPage;
use Graph\Beans\SitemapPage;
use Graph\Beans\UserPage;
use Graph\Exceptions\ArtistAlreadyExistsException;
use Graph\Exceptions\ArtistNotFoundException;
use Graph\Exceptions\EventNotFoundException;
use Graph\Exceptions\InvalidArtistDataException;
use Graph\Exceptions\InvalidDataException;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\InvalidPlaceDataException;
use Graph\Exceptions\LocationNotFoundException;
use Graph\Exceptions\PlaceNotFoundException;
use Graph\Exceptions\UserNotFoundException;
use Graph\Exceptions\WriteAccessException;
use Graph\Interfaces\IService;
use Graph\Queries\CreateArtist;
use Graph\Queries\CreateArtistPost;
use Graph\Queries\CreateEvent;
use Graph\Queries\CreatePlace;
use Graph\Queries\DeleteEventNews;
use Graph\Queries\GetArtistForUpdate;
use Graph\Queries\GetArtistIdByLegacy;
use Graph\Queries\GetArtistPage;
use Graph\Queries\GetEventForUpdate;
use Graph\Queries\GetEventIdByLegacy;
use Graph\Queries\GetEventListPageByPosition;
use Graph\Queries\GetEventPage;
use Graph\Queries\GetGeoIndexPage;
use Graph\Queries\GetLocation;
use Graph\Queries\GetOrCreateSocialUser;
use Graph\Queries\GetPlaceForUpdate;
use Graph\Queries\GetPlaceIdByLegacy;
use Graph\Queries\GetPlacePage;
use Graph\Queries\GetSitemapPage;
use Graph\Queries\GetUserPage;
use Graph\Queries\SearchArtistList;
use Graph\Queries\SearchPlaceList;
use Graph\Queries\UpdateArtist;
use Graph\Queries\UpdateArtistCover;
use Graph\Queries\UpdateArtistLinkList;
use Graph\Queries\UpdateEvent;
use Graph\Queries\UpdateEventArtistList;
use Graph\Queries\UpdateEventCover;
use Graph\Queries\UpdateEventNews;
use Graph\Queries\UpdateEventPlace;
use Graph\Queries\UpdateEventTagList;
use Graph\Queries\UpdatePlace;
use Graph\Queries\UpdatePlaceLinkList;

class Service implements IService
{
    /**
     * @var IGraphClient
     */
    public $client;

    /**
     * @var Consts
     */
    private $consts;

    public function __construct()
    {
        $this->consts = new Consts();
    }

    /**
     * @return EmptyPage
     */
    public function getEmptyPage()
    {
        return new EmptyPage();
    }

    /**
     * @return SitemapPage
     */
    public function getSitemapPage()
    {
        return (new GetSitemapPage($this->client))->get();
    }

    /**
     * @return GeoIndexPage
     */
    public function getGeoIndexPage()
    {
        return (new GetGeoIndexPage($this->client))->get();
    }

    /**
     * @param \Menara\Social\Beans\Profile $profile
     * @return User
     */
    public function getOrCreateSocialUser(\Menara\Social\Beans\Profile $profile)
    {
        return (new GetOrCreateSocialUser($this->client, $profile))->get();
    }

    /**
     * @param string $eventId
     * @return string
     * @throws EventNotFoundException
     */
    public function getEventIdByLegacy($eventId)
    {
        return (new GetEventIdByLegacy($this->client, $eventId))->get();
    }

    /**
     * @param string $placeId
     * @return string
     * @throws PlaceNotFoundException
     */
    public function getPlaceIdByLegacy($placeId)
    {
        return (new GetPlaceIdByLegacy($this->client, $placeId))->get();
    }

    /**
     * @param string $artistId
     * @return string
     * @throws ArtistNotFoundException
     */
    public function getArtistIdByLegacy($artistId)
    {
        return (new GetArtistIdByLegacy($this->client, $artistId))->get();
    }

    /**
     * @param string $userId
     * @param int $eventLimit
     * @param int $placeLimit
     * @param int $artistLimit
     * @return UserPage
     * @throws UserNotFoundException
     */
    public function getUserPage($userId, $eventLimit, $placeLimit, $artistLimit)
    {
        return (new GetUserPage($this->client, $userId, $eventLimit, $placeLimit, $artistLimit))->get();
    }

    /**
     * @param String $userId
     * @param string $eventId
     * @param int $relatedEventLimit
     * @param int $commentLimit
     * @return EventPage
     * @throws EventNotFoundException
     */
    public function getEventPage($userId, $eventId, $relatedEventLimit, $commentLimit)
    {
        return (new GetEventPage($this->client, $userId, $eventId, $relatedEventLimit, $commentLimit))->get();
    }

    /**
     * @param String $userId
     * @param string $placeId
     * @param int $pastEventLimit
     * @return PlacePage
     * @throws PlaceNotFoundException
     */
    public function getPlacePage($userId, $placeId, $pastEventLimit)
    {
        return (new GetPlacePage($this->client, $userId, $placeId, $pastEventLimit))->get();
    }

    /**
     * @param String $userId
     * @param string $artistId
     * @param int $pastEventLimit
     * @return ArtistPage
     * @throws ArtistNotFoundException
     */
    public function getArtistPage($userId, $artistId, $pastEventLimit)
    {
        return (new GetArtistPage($this->client, $userId, $artistId, $pastEventLimit))->get();
    }

    /**
     * @param string $userId
     * @param string $title
     * @param int $tzOffset
     * @param int $startsAt
     * @param int $endsAt
     * @return Event
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function createEvent($userId, $title, $tzOffset, $startsAt, $endsAt)
    {
        list($title, $tzOffset, $startsAt, $endsAt) = $this->validateEvent($title, $tzOffset, $startsAt, $endsAt);
        $query = new CreateEvent(
            $this->client,
            $userId,
            $this->createEventId($title, $startsAt - $tzOffset),
            $title,
            $tzOffset,
            $startsAt,
            $endsAt
        );
        return $query->get();
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @param string $title
     * @param int $tzOffset
     * @param int $startsAt
     * @param int $endsAt
     * @return Event
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEvent($userId, $eventId, $title, $tzOffset, $startsAt, $endsAt)
    {
        list($title, $tzOffset, $startsAt, $endsAt) = $this->validateEvent($title, $tzOffset, $startsAt, $endsAt);
        $event = $this->getEventForUpdate($userId, $eventId);
        $query = new UpdateEvent(
            $this->client,
            $event->id,
            $this->createEventId($title, $startsAt - $tzOffset),
            $title,
            $tzOffset,
            $startsAt,
            $endsAt
        );
        return $query->get();
    }

    /**
     * @param string $title
     * @param int $tzOffset
     * @param int $startsAt
     * @param int $endsAt
     * @return array
     * @throws InvalidEventDataException
     */
    private function validateEvent($title, $tzOffset, $startsAt, $endsAt)
    {
        if (strlen($title) < $this->consts->EVENT_TITLE_MIN_LENGTH) {
            throw new InvalidEventDataException('invalid_title_length');
        }
        if (strlen($title) > $this->consts->EVENT_TITLE_MAX_LENGTH) {
            throw new InvalidEventDataException('invalid_title_length');
        }
        if ($tzOffset < $this->consts->TZ_OFFSET_MIN) {
            throw new InvalidEventDataException('invalid_tz_offset');
        }
        if ($tzOffset > $this->consts->TZ_OFFSET_MAX) {
            throw new InvalidEventDataException('invalid_tz_offset');
        }
        if ($startsAt < time()) {
            throw new InvalidEventDataException('invalid_starts_at');
        }
        if ($endsAt - $startsAt < 1800) {
            throw new InvalidEventDataException('invalid_duration');
        }
        return [$title, $tzOffset, $startsAt, $endsAt];
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @param string $largeHref
     * @param string $smallHref
     * @return Photo
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventCover($userId, $eventId, $largeHref, $smallHref)
    {
        $event = $this->getEventForUpdate($userId, $eventId);
        return (new UpdateEventCover($this->client, $event->id, $largeHref, $smallHref))->get();
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @param string $placeId
     * @return Place
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventPlace($userId, $eventId, $placeId)
    {
        $event = $this->getEventForUpdate($userId, $eventId);
        return (new UpdateEventPlace($this->client, $event->id, $placeId))->get();
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @param int $createdAt
     * @return News
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function deleteEventNews($userId, $eventId, $createdAt)
    {
        $event = $this->getEventForUpdate($userId, $eventId);
        return (new DeleteEventNews($this->client, $event->id, $createdAt))->get();
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @param int $createdAt
     * @param string $title
     * @param string $content
     * @return News
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventNews($userId, $eventId, $createdAt, $title, $content)
    {
        if ($createdAt > time()) {
            throw new InvalidEventDataException();
        }
        if (strlen($title) < $this->consts->NEWS_TITLE_MIN_LENGTH) {
            throw new InvalidEventDataException();
        }
        if (strlen($title) > $this->consts->NEWS_TITLE_MAX_LENGTH) {
            throw new InvalidEventDataException();
        }
        if (strlen($content) < $this->consts->NEWS_CONTENT_MIN_LENGTH) {
            throw new InvalidEventDataException();
        }
        if (strlen($content) > $this->consts->NEWS_CONTENT_MAX_LENGTH) {
            throw new InvalidEventDataException();
        }
        $event = $this->getEventForUpdate($userId, $eventId);
        return (new UpdateEventNews($this->client, $event->id, $createdAt, $title, $content))->get();
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @param string[] $titleList
     * @return Tag[]
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updateEventTagList($userId, $eventId, array $titleList)
    {
        array_walk($titleList, [$this, 'validateTagTitle']);
        $tagList = array_map([$this, 'createTagFromTitle'], $titleList);
        $tagList = $this->filterTagList($tagList, $this->consts->EVENT_TAG_LIST_LIMIT);
        $artist = $this->getEventForUpdate($userId, $eventId);
        return (new UpdateEventTagList($this->client, $artist->id, $tagList))->get();
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @param string[] $artistList
     * @return Profile[]
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventArtistList($userId, $eventId, array $artistList)
    {
        $event = $this->getEventForUpdate($userId, $eventId);
        $artistList = $this->filterUniqueList($artistList, $this->consts->EVENT_ARTIST_LIST_LIMIT);
        return (new UpdateEventArtistList($this->client, $event->id, $artistList))->get();
    }

    /**
     * @param string $userId
     * @param string $eventId
     * @return Event
     * @throws EventNotFoundException
     * @throws WriteAccessException
     */
    private function getEventForUpdate($userId, $eventId)
    {
        return (new GetEventForUpdate($this->client, $userId, $eventId))->get();
    }

    /**
     * @param int $since
     * @return Event[]
     */
    public function getEventListStartsAfter($since)
    {
        return (new GetEventListStartsAfter($this->client, $since))->get();
    }

    /**
     * @param string $locationId
     * @param int $distance
     * @param int $eventLimit
     * @return EventListPage
     * @throws LocationNotFoundException
     */
    public function getEventListPageByLocation($locationId, $distance, $eventLimit)
    {
        $location = (new GetLocation($this->client, $locationId))->get();
        $page = $this->getEventListPageByPosition($location->latitude, $location->longitude, $distance, $eventLimit);
        $page->location = $location;
        return $page;
    }

    /**
     * @param string $ip
     * @param int $distance
     * @param int $eventLimit
     * @return EventListPage
     * @throws LocationNotFoundException
     */
    public function getEventListPageByIp($ip, $distance, $eventLimit)
    {
        $record = @geoip_record_by_name($ip);
        if (!$record) {
            throw new LocationNotFoundException();
        }
        return $this->getEventListPageByPosition($record['latitude'], $record['longitude'], $distance, $eventLimit);
    }

    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $distance
     * @param int $eventLimit
     * @return EventListPage
     * @throws LocationNotFoundException
     */
    public function getEventListPageByPosition($latitude, $longitude, $distance, $eventLimit)
    {
        $page = (new GetEventListPageByPosition($this->client, $latitude, $longitude, $distance, $eventLimit))->get();
        $page->location->latitude = $latitude;
        $page->location->longitude = $longitude;
        return $page;
    }

    /**
     * @param string $userId
     * @param string $title
     * @param string $intro
     * @param string $address
     * @param float $latitude
     * @param float $longitude
     * @return Place
     * @throws WriteAccessException
     * @throws InvalidPlaceDataException
     */
    public function createPlace($userId, $title, $intro, $address, $latitude, $longitude)
    {
        list($title, $intro, $address, $latitude, $longitude) = $this->validatePlace(
            $title,
            $intro,
            $address,
            $latitude,
            $longitude
        );
        $query = new CreatePlace(
            $this->client,
            $userId,
            $this->createId($title),
            $title,
            $intro,
            $address,
            $latitude,
            $longitude
        );
        return $query->get();
    }

    /**
     * @param string $userId
     * @param string $placeId
     * @param string $title
     * @param string $intro
     * @param string $address
     * @param float $latitude
     * @param float $longitude
     * @return Place
     * @throws PlaceNotFoundException
     * @throws WriteAccessException
     * @throws InvalidPlaceDataException
     */
    public function updatePlace($userId, $placeId, $title, $intro, $address, $latitude, $longitude)
    {
        list($title, $intro, $address, $latitude, $longitude) = $this->validatePlace(
            $title,
            $intro,
            $address,
            $latitude,
            $longitude
        );
        $place = $this->getPlaceForUpdate($userId, $placeId);
        $query = new UpdatePlace(
            $this->client,
            $place->id,
            $this->createId($title),
            $title,
            $intro,
            $address,
            $latitude,
            $longitude
        );
        return $query->get();
    }

    /**
     * @param string $title
     * @param string $intro
     * @param string $address
     * @param float $latitude
     * @param float $longitude
     * @return array
     * @throws InvalidPlaceDataException
     */
    private function validatePlace($title, $intro, $address, $latitude, $longitude)
    {
        if (strlen($title) < $this->consts->PLACE_TITLE_MIN_LENGTH) {
            throw new InvalidPlaceDataException();
        }
        if (strlen($title) > $this->consts->PLACE_TITLE_MAX_LENGTH) {
            throw new InvalidPlaceDataException();
        }
        if (strlen($intro) > $this->consts->PLACE_INTRO_MAX_LENGTH) {
            throw new InvalidPlaceDataException();
        }
        if (strlen($address) > $this->consts->PLACE_ADDRESS_MAX_LENGTH) {
            throw new InvalidPlaceDataException();
        }
        if (!$latitude || !$longitude) {
            throw new InvalidPlaceDataException();
        }
        return [$title, $intro, $address, $latitude, $longitude];
    }

    /**
     * @param string $userId
     * @param string $placeId
     * @param string[] $hrefList
     * @return Link[]
     * @throws PlaceNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updatePlaceLinkList($userId, $placeId, array $hrefList)
    {
        $hrefList = array_map([$this, 'validateLinkHref'], $hrefList);
        $hrefList = $this->filterUniqueList($hrefList, $this->consts->PLACE_LINK_LIST_LIMIT);
        $linkList = array_map([$this, 'createLinkFromHref'], $hrefList);
        $place = $this->getPlaceForUpdate($userId, $placeId);
        return (new UpdatePlaceLinkList($this->client, $place->id, $linkList))->get();
    }

    /**
     * @param string $userId
     * @param string $placeId
     * @return Place
     */
    private function getPlaceForUpdate($userId, $placeId)
    {
        return (new GetPlaceForUpdate($this->client, $userId, $placeId))->get();
    }

    /**
     * @param string $search
     * @param int $limit
     * @return Place[]
     * @throws InvalidPlaceDataException
     */
    public function searchPlaceList($search, $limit)
    {
        if (strlen($search) < $this->consts->PLACE_SEARCH_MIN_LENGTH) {
            throw new InvalidPlaceDataException();
        }
        $search = $this->createId($search);
        $search = substr($search, 0, $this->consts->PLACE_SEARCH_MAX_LENGTH);
        return (new SearchPlaceList($this->client, $search, $limit))->get();
    }

    /**
     * @return Place[]
     */
    public function getPlaceList()
    {
        return (new GetPlaceList($this->client))->get();
    }

    /**
     * @param string $userId
     * @param string $title
     * @param string $intro
     * @param string $about
     * @param string $hometown
     * @return Profile
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     * @throws ArtistAlreadyExistsException
     */
    public function createArtist($userId, $title, $intro, $about, $hometown)
    {
        list($title, $intro, $about, $hometown) = $this->validateArtist($title, $intro, $about, $hometown);
        $query = new CreateArtist(
            $this->client,
            $userId,
            $this->createId($title),
            $title,
            $intro,
            $about,
            $hometown
        );
        return $query->get();
    }

    /**
     * @param string $userId
     * @param string $artistId
     * @param string $title
     * @param string $intro
     * @param string $about
     * @param string $hometown
     * @return Profile
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     */
    public function updateArtist($userId, $artistId, $title, $intro, $about, $hometown)
    {
        list($title, $intro, $about, $hometown) = $this->validateArtist($title, $intro, $about, $hometown);
        $artist = $this->getArtistForUpdate($userId, $artistId);
        $query = new UpdateArtist(
            $this->client,
            $artist->id,
            $this->createId($title),
            $title,
            $intro,
            $about,
            $hometown
        );
        return $query->get();
    }

    /**
     * @param string $title
     * @param string $intro
     * @param string $about
     * @param string $hometown
     * @return array
     * @throws InvalidArtistDataException
     */
    private function validateArtist($title, $intro, $about, $hometown)
    {
        if (strlen($title) < $this->consts->ARTIST_TITLE_MIN_LENGTH) {
            throw new InvalidArtistDataException();
        }
        if (strlen($title) > $this->consts->ARTIST_TITLE_MAX_LENGTH) {
            throw new InvalidArtistDataException();
        }
        if (strlen($intro) > $this->consts->ARTIST_INTRO_MAX_LENGTH + 16) {
            throw new InvalidArtistDataException();
        }
        if (strlen($about) > $this->consts->ARTIST_ABOUT_MAX_LENGTH + 64) {
            throw new InvalidArtistDataException();
        }
        if (strlen($hometown) < $this->consts->ARTIST_HOMETOWN_MIN_LENGTH) {
            throw new InvalidArtistDataException();
        }
        if (strlen($hometown) > $this->consts->ARTIST_HOMETOWN_MAX_LENGTH) {
            throw new InvalidArtistDataException();
        }
        return [$title, $intro, $about, $hometown];
    }

    /**
     * @param string $userId
     * @param string $artistId
     * @param string $largeHref
     * @param string $smallHref
     * @return Photo
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     */
    public function updateArtistCover($userId, $artistId, $largeHref, $smallHref)
    {
        $artist = $this->getArtistForUpdate($userId, $artistId);
        return (new UpdateArtistCover($this->client, $artist->id, $largeHref, $smallHref))->get();
    }

    /**
     * @param string $userId
     * @param string $artistId
     * @param string[] $hrefList
     * @return Link[]
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updateArtistLinkList($userId, $artistId, array $hrefList)
    {
        $hrefList = array_map([$this, 'validateLinkHref'], $hrefList);
        $hrefList = $this->filterUniqueList($hrefList, $this->consts->ARTIST_LINK_LIST_LIMIT);
        $linkList = array_map([$this, 'createLinkFromHref'], $hrefList);
        $artist = $this->getArtistForUpdate($userId, $artistId);
        return (new UpdateArtistLinkList($this->client, $artist->id, $linkList))->get();
    }

    /**
     * @param string $userId
     * @param string $artistId
     * @return Profile
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     */
    private function getArtistForUpdate($userId, $artistId)
    {
        return (new GetArtistForUpdate($this->client, $userId, $artistId))->get();
    }

    /**
     * @param string $userId
     * @param string $artistId
     * @param string $guid
     * @param string $source
     * @param string $type
     * @param string $href
     * @param string $cover
     * @param String $title
     * @return Post
     * @throws ArtistNotFoundException
     */
    public function createArtistPost($userId, $artistId, $guid, $source, $type, $href, $cover, $title)
    {
        $postId = $this->createId($guid);
        $query = new CreateArtistPost(
            $this->client,
            $userId,
            $artistId,
            $postId,
            $source,
            $type,
            $href,
            $cover,
            $title
        );
        return $query->get();
    }

    /**
     * @param string $search
     * @param int $limit
     * @return Profile[]
     * @throws InvalidArtistDataException
     */
    public function searchArtistList($search, $limit)
    {
        if (strlen($search) < $this->consts->ARTIST_SEARCH_MIN_LENGTH) {
            throw new InvalidArtistDataException();
        }
        $search = $this->createId($search);
        $search = substr($search, 0, $this->consts->ARTIST_SEARCH_MAX_LENGTH);
        return (new SearchArtistList($this->client, $search, $limit))->get();
    }

    /**
     * @return Profile[]
     */
    public function getArtistList()
    {
        return (new GetArtistList($this->client))->get();
    }

    /**
     * @param string $title
     * @return string
     * @throws InvalidDataException
     */
    private function validateTagTitle($title)
    {
        if (strlen($title) < $this->consts->TAG_MIN_TITLE_LENGTH) {
            throw new InvalidDataException();
        }
        if (strlen($title) > $this->consts->TAG_MAX_TITLE_LENGTH) {
            throw new InvalidDataException();
        }
        return $title;
    }

    /**
     * @param string $title
     * @return Tag
     */
    private function createTagFromTitle($title)
    {
        $bean = new Tag();
        $bean->id = $this->createId($title);
        $bean->title = $title;
        return $bean;
    }

    /**
     * @param Tag[] $tagList
     * @param int $limit
     * @return Tag[]
     */
    private function filterTagList(array $tagList, $limit)
    {
        $map = array();
        foreach ($tagList as $tag) {
            $map[$tag->id] = $tag;
        }
        return array_slice(array_values($map), 0, $limit);
    }

    /**
     * @param string[] $artistList
     * @param int $limit
     * @return string[]
     */
    private function filterUniqueList(array $artistList, $limit)
    {
        return array_slice(array_values(array_unique($artistList)), 0, $limit);
    }

    /**
     * @param string $href
     * @return mixed
     * @throws InvalidDataException
     */
    private function validateLinkHref($href)
    {
        if (strlen($href) > $this->consts->LINK_HREF_MAX_LENGTH) {
            throw new InvalidDataException();
        };
        $site = parse_url($href, PHP_URL_HOST);
        if (!$site) {
            throw new InvalidDataException();
        }
        return $href;
    }

    /**
     * @param string $href
     * @return Link
     */
    private function createLinkFromHref($href)
    {
        $bean = new Link();
        $bean->href = $href;
        $bean->site = $this->getLinkSiteFromHref($href);
        $bean->hide = false;
        $bean->used = 0;
        return $bean;
    }

    /**
     * @param string $href
     * @return String
     */
    private function getLinkSiteFromHref($href)
    {
        foreach ($this->consts->LINK_SITE_MAP as $domain => $site) {
            if (false !== stripos($href, $domain)) {
                return $site;
            }
        }
        return null;
    }

    /**
     * @param string $title
     * @param int $utcStartsAt
     * @return string
     */
    private function createEventId($title, $utcStartsAt)
    {
        return gmdate('Y/m/d/', $utcStartsAt) . $this->createId($title);
    }

    /**
     * @param string $title
     * @return string
     */
    private function createId($title)
    {
        return Tools::slugify($title);
    }

}
