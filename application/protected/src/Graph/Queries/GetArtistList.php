<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Profile;
use Graph\Beans\Factory;

class GetArtistList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist)
OPTIONAL MATCH
    (_artist)-
    [:HAS_LINK]->
    (_link:Link)
WITH
    _artist,
    collect({
        site: _link.site,
        href: _link.href,
        hide: _link.hide
    }) as linkList
OPTIONAL MATCH
    (_artist)-
    [:TAGGED_BY]->
    (_tag:Tag)
WITH
    _artist, linkList,
    collect({
        id: _tag.id,
        title: _tag.title
    }) as tagList
WITH
    collect({
        id: _artist.id,
        title: _artist.title,
        tagList: tagList,
        linkList: linkList,
        cover: null
    }) as artistList
RETURN
    {
        artistList: artistList
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @param IGraphClient $client
     */
    public function __construct(IGraphClient $client)
    {
        $this->client = $client;
        $this->beans = new Factory();
    }

    /**
     * @return Profile[]
     */
    public function get()
    {
        $data = $this->client->cypherJson(self::STATEMENT);
        return $this->beans->listMap([$this->beans, 'createProfile'], @$data['artistList']);
    }

}
