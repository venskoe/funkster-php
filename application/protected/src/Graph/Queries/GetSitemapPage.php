<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Factory;
use Graph\Beans\SitemapPage;

class GetSitemapPage
{
    const EVENT_HISTORY_LIMIT = 5184000; // 60 days

    const STATEMENT = <<<CYPHER
OPTIONAL MATCH
    (_event:Event)-
    [_managedBy:MANAGED_BY]->
    ()
WHERE
    _event.startsAt > {minEventStartsAt}
WITH
    _event, MAX(_managedBy.timestamp) as _eventModifiedAt
WITH
    collect({
        id: _event.id,
        modifiedAt: _eventModifiedAt,
        cover: null,
        place: null,
        tagList: [],
        linkList: [],
        artistList: []
    }) as eventList
OPTIONAL MATCH
    (_place:Place)-
    [_managedBy:MANAGED_BY]->
    ()
WITH
    eventList, _place, MAX(_managedBy.timestamp) as _placeModifiedAt
WITH
    eventList,
    collect({
        id: _place.id,
        modifiedAt: _placeModifiedAt,
        cover: null,
        linkList: []
    }) as placeList
OPTIONAL MATCH
    (_artist:Artist)-
    [_managedBy:MANAGED_BY]->
    ()
WITH
    eventList, placeList, _artist, MAX(_managedBy.timestamp) as _artistModifiedAt
WITH
    eventList, placeList,
    collect({
        id: _artist.id,
        modifiedAt: _artistModifiedAt,
        cover: null,
        tagList: [],
        linkList: []
    }) as artistList
OPTIONAL MATCH
    (_location:City)
WITH
    eventList, placeList, artistList,
    collect({
        id: _location.id,
        modifiedAt: {now},
        childList: []
    }) as locationList
RETURN
    {
        eventList: eventList,
        placeList: placeList,
        artistList: artistList,
        locationList: locationList
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var Factory
     */
    private $beans;

    /**
     * @param IGraphClient $client
     */
    public function __construct(IGraphClient $client)
    {
        $this->client = $client;
        $this->beans = new Factory();
    }

    /**
     * @return SitemapPage
     */
    public function get()
    {
        $args = array(
            'now' => time(),
            'minEventStartsAt' => time() - self::EVENT_HISTORY_LIMIT,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            $data = array();
        }
        return $this->beans->createSitemapPage($data);
    }

}
