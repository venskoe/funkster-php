<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\ArtistPage;
use Graph\Beans\Factory;
use Graph\Exceptions\ArtistNotFoundException;

class GetArtistPage
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist {id: {artistId}})
WITH
    _artist
OPTIONAL MATCH
    (_artist)-
    [:HAS_COVER]->
    (_artistCover:Photo)
WITH
    _artist, _artistCover
OPTIONAL MATCH
    (_artist)-
    [:HAS_LINK]->
    (_link:Link)
WITH
    _artist, _artistCover,
    collect({
        site: _link.site,
        href: _link.href,
        hide: _link.hide
    }) as linkList
WITH
    _artist,
    {
        id: _artist.id,
        title: _artist.title,
        intro: _artist.intro,
        about: _artist.about,
        hometown: _artist.hometown,
        tagList: [],
        linkList: linkList,
        cover: {
            large: _artistCover.large,
            small: null
        }
    } as artist
OPTIONAL MATCH
    (_artist)<-
    [_postedAt:POSTED_AT]-
    (_post:Post)
OPTIONAL MATCH
    (:User {id: _postedAt.userId})-
    [:HAS_PROFILE]->
    (_postAuthor:Profile)-
    [:HAS_COVER]->
    (_postAuthorCover:Photo)
WITH
    _artist, artist, _post, _postedAt, _postAuthor, _postAuthorCover
ORDER BY
    _postedAt.timestamp DESC
WITH
    _artist, artist,
    collect({
        id: _post.id,
        type: _post.type,
        href: _post.href,
        title: _post.title,
        cover: _post.cover,
        source: _post.source,
        postedAt: _postedAt.timestamp,
        author: {
            id: _postAuthor.id,
            title: _postAuthor.title,
            cover: {
                small: _postAuthorCover.small,
                large: null
            }
        }
    }) as postList
OPTIONAL MATCH
    (_artist)-
    [:PERFORMS_AT]->
    (_event:Event)
WHERE
    _event.startsAt < {now}
WITH
    _artist, artist, postList,
    count(_event) as pastEventCount
OPTIONAL MATCH
    (_artist)-
    [:PERFORMS_AT]->
    (_event:Event)-
    [:HOSTED_AT]->
    (_place:Place)
WHERE
    _event.startsAt < {now}
WITH
    _artist, artist, postList, pastEventCount, _event, _place
ORDER BY
    _event.startsAt DESC
LIMIT
    {pastEventLimit}
WITH
    _artist, artist, postList, pastEventCount,
    collect({
        id: _event.id,
        title: _event.title,
        tzOffset: _event.tzOffset,
        startsAt: _event.startsAt,
        endsAt: null,
        cover: null,
        place: {
            title: _place.title,
            address: _place.address,
            latitude: _place.latitude,
            longitude: _place.longitude,
            linkList: [],
            cover: null
        },
        tagList: [],
        linkList: [],
        artistList: []
    }) as pastEventList
OPTIONAL MATCH
    (_artist)-
    [:PERFORMS_AT]->
    (_event:Event)
WHERE
    _event.startsAt >= {now}
WITH
    _artist, artist, postList, pastEventCount, pastEventList,
    count(_event) as upcomingEventCount
OPTIONAL MATCH
    (_artist)-
    [:PERFORMS_AT]->
    (_event:Event)-
    [:HOSTED_AT]->
    (_place:Place)
WHERE
    _event.startsAt >= {now}
WITH
    _artist, artist, postList, pastEventCount, pastEventList, upcomingEventCount, _event, _place
ORDER BY
    _event.startsAt ASC
WITH
    _artist, artist, postList, pastEventCount, pastEventList, upcomingEventCount,
    collect({
        id: _event.id,
        title: _event.title,
        tzOffset: _event.tzOffset,
        startsAt: _event.startsAt,
        endsAt: null,
        cover: null,
        place: {
            title: _place.title,
            address: _place.address,
            latitude: _place.latitude,
            longitude: _place.longitude,
            linkList: [],
            cover: null
        },
        tagList: [],
        linkList: [],
        artistList: []
    }) as upcomingEventList
OPTIONAL MATCH
    (_user:User {id: {userId}})
WITH
    _artist, artist, postList, pastEventCount, pastEventList, upcomingEventCount, upcomingEventList,
    _user
OPTIONAL MATCH
    (_artist)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    artist, postList, pastEventCount, pastEventList, upcomingEventCount, upcomingEventList,
    sum(COALESCE(_user.isAdmin, 0)) + count(_managedBy) as canUpdate
WITH
    artist, postList, pastEventCount, pastEventList, upcomingEventCount, upcomingEventList,
    {
        canUpdate: canUpdate,
        canCreate: 0,
        canDelete: 0
    } as access
WITH
    {
        artist: artist,
        access: access,
        postList: postList,
        pastEventList: pastEventList,
        pastEventCount: pastEventCount,
        upcomingEventList: upcomingEventList,
        upcomingEventCount: upcomingEventCount
    } as json
RETURN
    json
CYPHER;

    /**
     * @var \Graph\Beans\Factory
     */
    private $beans;

    /**
     * @var \Menara\Generic\Storage\Interfaces\IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $artistId;

    /**
     * @var int
     */
    private $pastEventLimit;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $artistId
     * @param int $pastEventLimit
     */
    public function __construct(IGraphClient $client, $userId, $artistId, $pastEventLimit)
    {
        $this->beans = new Factory();
        $this->client = $client;
        $this->userId = $userId;
        $this->artistId = $artistId;
        $this->pastEventLimit = $pastEventLimit;
    }

    /**
     * @return ArtistPage
     * @throws ArtistNotFoundException
     */
    public function get()
    {
        $args = array(
            'userId' => $this->userId,
            'artistId' => $this->artistId,
            'pastEventLimit' => $this->pastEventLimit,
            'now' => time(),
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new ArtistNotFoundException();
        }
        return $this->beans->createArtistPage($data);
    }

}
