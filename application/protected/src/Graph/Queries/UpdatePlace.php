<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Place;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidPlaceDataException;
use Graph\Exceptions\PlaceNotFoundException;

class UpdatePlace
{
    const STATEMENT = <<<CYPHER
MATCH
    (_place:Place {id: {oldPlaceId}})
SET
    _place.id = {newPlaceId},
    _place.title = {title},
    _place.intro = {intro},
    _place.address = {address},
    _place.latitude = {latitude},
    _place.longitude = {longitude}
WITH
    _place
OPTIONAL MATCH
    (:Place)-
    [_was:WAS]->
    (:Legacy {id: {oldPlaceId}})
WITH
    _place, _was
DELETE
    _was
WITH
    _place
MERGE
    (_legacy:Legacy {id: {oldPlaceId}})
WITH
    _place, _legacy
MERGE
    (_place)-
    [:WAS]->
    (_legacy)
RETURN
    {
        nodeId: id(_place),
        place: {
            id: _place.id,
            title: _place.title,
            intro: _place.intro,
            address: _place.address,
            latitude: _place.latitude,
            longitude: _place.longitude,
            linkList: [],
            cover: null
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $oldPlaceId;

    /**
     * @var string
     */
    private $newPlaceId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $intro;

    /**
     * @var string
     */
    private $address;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @param IGraphClient $client
     * @param string $oldPlaceId
     * @param string $newPlaceId
     * @param string $title
     * @param string $intro
     * @param string $address
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(IGraphClient $client, $oldPlaceId, $newPlaceId, $title, $intro, $address, $latitude, $longitude)
    {
        $this->client = $client;
        $this->oldPlaceId = $oldPlaceId;
        $this->newPlaceId = $newPlaceId;
        $this->title = $title;
        $this->intro = $intro;
        $this->address = $address;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->beans = new Factory();
    }

    /**
     * @return Place
     * @throws InvalidPlaceDataException
     * @throws PlaceNotFoundException
     */
    public function get()
    {
        $args = array(
            'oldPlaceId' => $this->oldPlaceId,
            'newPlaceId' => $this->newPlaceId,
            'title' => $this->title,
            'intro' => $this->intro,
            'address' => $this->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
            $this->client->upsertSpatialPoint('Place', (int)$data['nodeId']);
        }
        catch (ItemNotFoundException $e) {
            throw new PlaceNotFoundException();
        }
        catch (ItemAlreadyExistsException $e) {
            throw new InvalidPlaceDataException();
        }
        return $this->beans->createPlace($data['place']);
    }

}
