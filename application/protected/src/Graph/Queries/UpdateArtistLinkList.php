<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Link;
use Graph\Beans\Factory;
use Graph\Exceptions\ArtistNotFoundException;

class UpdateArtistLinkList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist {id: {artistId}})
OPTIONAL MATCH
    (_artist)-
    [_hasLink:HAS_LINK]->
    (_linkToDelete:Link)
WHERE
    _linkToDelete.hide <> true
DELETE
    _hasLink, _linkToDelete
WITH
    distinct(_artist) as _artist
UNWIND
    {linkList} as __link
CREATE
    (_artist)-
    [:HAS_LINK]->
    (_link:Link {
        href: __link.href,
        site: __link.site,
        hide: false
    })
RETURN
    {
        linkList: collect({
            href: _link.href,
            site: _link.site,
            hide: _link.hide
        })
    } as json
CYPHER;


    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $artistId;

    /**
     * @var Link[]
     */
    private $linkList;

    /**
     * @param IGraphClient $client
     * @param string $artistId
     * @param Link[] $linkList
     */
    public function __construct(IGraphClient $client, $artistId, array $linkList)
    {
        $this->client = $client;
        $this->artistId = $artistId;
        $this->linkList = $linkList;
        $this->beans = new Factory();
    }

    /**
     * @return Link[]
     * @throws ArtistNotFoundException
     */
    public function get()
    {
        $args = array(
            'artistId' => $this->artistId,
            'linkList' => $this->linkList,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new ArtistNotFoundException();
        }
        return $this->beans->listMap([$this->beans, 'createLink'], $data['linkList']);
    }

}
