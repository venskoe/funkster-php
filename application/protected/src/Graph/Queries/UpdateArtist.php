<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Profile;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidArtistDataException;
use Graph\Exceptions\ArtistNotFoundException;

class UpdateArtist
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist {id: {oldArtistId}})
SET
    _artist.id = {newArtistId},
    _artist.title = {title},
    _artist.intro = {intro},
    _artist.about = {about},
    _artist.hometown = {hometown}
WITH
    _artist
OPTIONAL MATCH
    (:Artist)-
    [_was:WAS]->
    (:Legacy {id: {oldArtistId}})
WITH
    _artist, _was
DELETE
    _was
WITH
    _artist
MERGE
    (_legacy:Legacy {id: {oldArtistId}})
WITH
    _artist, _legacy
MERGE
    (_artist)-
    [:WAS]->
    (_legacy)
RETURN
    {
        artist: {
            id: _artist.id,
            title: _artist.title,
            intro: _artist.intro,
            about: _artist.about,
            hometown: _artist.hometown,
            tagList: [],
            linkList: [],
            cover: null
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $oldArtistId;

    /**
     * @var string
     */
    private $newArtistId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $intro;

    /**
     * @var string
     */
    private $about;

    /**
     * @var string
     */
    private $hometown;

    /**
     * @param IGraphClient $client
     * @param string $oldArtistId
     * @param string $newArtistId
     * @param string $title
     * @param string $intro
     * @param string $about
     * @param string $hometown
     */
    public function __construct(IGraphClient $client, $oldArtistId, $newArtistId, $title, $intro, $about, $hometown)
    {
        $this->client = $client;
        $this->oldArtistId = $oldArtistId;
        $this->newArtistId = $newArtistId;
        $this->title = $title;
        $this->intro = $intro;
        $this->about = $about;
        $this->hometown = $hometown;
        $this->beans = new Factory();
    }

    /**
     * @return Profile
     * @throws InvalidArtistDataException
     * @throws ArtistNotFoundException
     */
    public function get()
    {
        $args = array(
            'oldArtistId' => $this->oldArtistId,
            'newArtistId' => $this->newArtistId,
            'title' => $this->title,
            'intro' => $this->intro,
            'about' => $this->about,
            'hometown' => $this->hometown,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new ArtistNotFoundException();
        }
        catch (ItemAlreadyExistsException $e) {
            throw new InvalidArtistDataException();
        }
        return $this->beans->createProfile($data['artist']);
    }

}
