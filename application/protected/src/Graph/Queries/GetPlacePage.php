<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Factory;
use Graph\Beans\PlacePage;
use Graph\Exceptions\PlaceNotFoundException;

class GetPlacePage 
{
    const STATEMENT = <<<CYPHER
MATCH
    (_place:Place {id: {placeId}})
OPTIONAL MATCH
    (_place)-
    [:HAS_COVER]->
    (_placeCover:Photo)
OPTIONAL MATCH
    (_place)-
    [:HAS_LINK]->
    (_link:Link)
WITH
    _place, _placeCover,
    collect({
        site: _link.site,
        href: _link.href,
        hide: _link.hide
    }) as linkList
WITH
    _place,
    {
        id: _place.id,
        title: _place.title,
        intro: _place.intro,
        address: _place.address,
        latitude: _place.latitude,
        longitude: _place.longitude,
        instagram: _place.instagram,
        linkList: linkList,
        cover: {
            large: _placeCover.large,
            small: null
        }
    } as place
OPTIONAL MATCH
    (_place)<-
    [_postedAt:POSTED_AT]-
    (_post:Post)
OPTIONAL MATCH
    (:User {id: _postedAt.userId})-
    [:HAS_PROFILE]->
    (_postAuthor:Profile)-
    [:HAS_COVER]->
    (_postAuthorCover:Photo)
WITH
    _place, place, _post, _postedAt, _postAuthor, _postAuthorCover
ORDER BY
    _postedAt.timestamp DESC
WITH
    _place, place,
    collect({
        id: _post.id,
        type: _post.type,
        href: _post.href,
        title: _post.title,
        cover: _post.cover,
        source: _post.source,
        postedAt: _postedAt.timestamp,
        author: {
            id: _postAuthor.id,
            title: _postAuthor.title,
            cover: {
                small: _postAuthorCover.small,
                large: null
            }
        }
    }) as postList
OPTIONAL MATCH
    (_place)<-
    [:HOSTED_AT]->
    (_event:Event)
WHERE
    _event.startsAt < {now}
WITH
    _place, place, postList,
    count(_event) as pastEventCount
OPTIONAL MATCH
    (_place)<-
    [:HOSTED_AT]-
    (_event:Event)
WHERE
    _event.startsAt < {now}
WITH
    _place, place, postList, pastEventCount,
    _event
ORDER BY
    _event.startsAt DESC
LIMIT
    {pastEventLimit}
WITH
    _place, place, postList, pastEventCount,
    collect({
        id: _event.id,
        title: _event.title,
        tzOffset: _event.tzOffset,
        startsAt: _event.startsAt,
        endsAt: null,
        cover: null,
        place: null,
        tagList: [],
        linkList: [],
        artistList: []
    }) as pastEventList
OPTIONAL MATCH
    (_place)<-
    [:HOSTED_AT]-
    (_event:Event)
WHERE
    _event.startsAt >= {now}
WITH
    _place, place, postList, pastEventCount, pastEventList,
    count(_event) as upcomingEventCount
OPTIONAL MATCH
    (_place)<-
    [:HOSTED_AT]-
    (_event:Event)
WHERE
    _event.startsAt >= {now}
WITH
    _place, place, postList, pastEventCount, pastEventList, upcomingEventCount,
    _event
ORDER BY
    _event.startsAt ASC
WITH
    _place, place, postList, pastEventCount, pastEventList, upcomingEventCount,
    collect({
        id: _event.id,
        title: _event.title,
        tzOffset: _event.tzOffset,
        startsAt: _event.startsAt,
        endsAt: null,
        cover: null,
        place: null,
        tagList: [],
        linkList: [],
        artistList: []
    }) as upcomingEventList
OPTIONAL MATCH
    (_user:User {id: {userId}})
WITH
    _place, place, postList, pastEventCount, pastEventList, upcomingEventCount, upcomingEventList,
    _user
OPTIONAL MATCH
    (_artist)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    place, postList, pastEventCount, pastEventList, upcomingEventCount, upcomingEventList,
    sum(COALESCE(_user.isAdmin, 0)) + count(_managedBy) as canUpdate
WITH
    place, postList, pastEventCount, pastEventList, upcomingEventCount, upcomingEventList,
    {
        canUpdate: canUpdate,
        canCreate: 0,
        canDelete: 0
    } as access
WITH
    {
        place: place,
        access: access,
        postList: postList,
        pastEventList: pastEventList,
        pastEventCount: pastEventCount,
        upcomingEventList: upcomingEventList,
        upcomingEventCount: upcomingEventCount
    } as json
RETURN
    json
CYPHER;

    /**
     * @var \Graph\Beans\Factory
     */
    private $beans;

    /**
     * @var \Menara\Generic\Storage\Interfaces\IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $placeId;

    /**
     * @var int
     */
    private $pastEventLimit;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $placeId
     * @param int $pastEventLimit
     */
    public function __construct(IGraphClient $client, $userId, $placeId, $pastEventLimit)
    {
        $this->beans = new Factory();
        $this->client = $client;
        $this->userId = $userId;
        $this->placeId = $placeId;
        $this->pastEventLimit = $pastEventLimit;
    }

    /**
     * @return PlacePage
     * @throws PlaceNotFoundException
     */
    public function get()
    {
        $args = array(
            'userId' => $this->userId,
            'placeId' => $this->placeId,
            'pastEventLimit' => $this->pastEventLimit,
            'now' => time(),
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new PlaceNotFoundException();
        }
        return $this->beans->createPlacePage($data);
    }

}
