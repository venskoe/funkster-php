<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Photo;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\EventNotFoundException;

class UpdateEventCover
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})
WITH
    _event
MERGE
    (_event)-
    [:HAS_COVER]->
    (_photo:Photo)
WITH
    _photo
SET
    _photo.large = {largeHref},
    _photo.small = {smallHref}
WITH
    _photo
RETURN
    {
        cover: {
            large: _photo.large,
            small: _photo.small
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var string
     */
    private $largeHref;

    /**
     * @var string
     */
    private $smallHref;

    /**
     * @param IGraphClient $client
     * @param string $eventId
     * @param string $largeHref
     * @param string $smallHref
     */
    public function __construct(IGraphClient $client, $eventId, $largeHref, $smallHref)
    {
        $this->client = $client;
        $this->eventId = $eventId;
        $this->largeHref = $largeHref;
        $this->smallHref = $smallHref;
        $this->beans = new Factory();
    }

    /**
     * @return Photo
     * @throws InvalidEventDataException
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'eventId' => $this->eventId,
            'largeHref' => $this->largeHref,
            'smallHref' => $this->smallHref,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $this->beans->createPhoto($data['cover']);
    }

}
