<?php

namespace Graph\Queries;

use Menara\Generic\Guid\Generator;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Factory;

class GetOrCreateSocialUser
{
    const STATEMENT = <<<CYPHER
MERGE
    (_credentials:Credentials {id: {credentialsId}})<-
    [:HAS_CREDENTIALS]-
    (_user:User)-
    [:HAS_PROFILE]->
    (_profile:Profile)-
    [:HAS_COVER]->
    (_photo:Photo)
ON CREATE SET
    _user.id = {userId},
    _user.isAdmin = 0
SET
    _profile.title = {title},
    _photo.small = {coverSmallHref},
    _photo.large = {coverLargeHref}
WITH
    _user, _profile, _photo
MERGE
    (_profile)-
    [:HAS_LINK]->
    (_link:Link {site: {linkSite}})
SET
    _link.href = {linkHref},
    _link.hide = false
RETURN
    {
        user: {
            id: _user.id,
            isAdmin: _user.isAdmin,
            profile: {
                id: _profile.id,
                title: _profile.title,
                intro: _profile.intro,
                about: _profile.about,
                hometown: _profile.hometown,
                tagList: [],
                linkList: [],
                cover: {
                    small: _photo.small,
                    large: _photo.large
                }
            }
        }
    }
CYPHER;

    /**
     * @var \Menara\Generic\Storage\Interfaces\IGraphClient
     */
    private $client;

    /**
     * @var \Menara\Social\Beans\Profile
     */
    private $profile;

    /**
     * @param IGraphClient $client
     * @param \Menara\Social\Beans\Profile $profile
     */
    public function __construct(IGraphClient $client, \Menara\Social\Beans\Profile $profile)
    {
        $this->client = $client;
        $this->profile = $profile;
        $this->beans = new Factory();
    }

    /**
     * @return \Graph\Beans\Entities\User
     */
    public function get()
    {
        $args = array(
            'userId' => $this->generateUserId(),
            'credentialsId' => implode(':', [$this->profile->platform, $this->profile->userId]),
            'title' => $this->profile->title,
            'coverSmallHref' => $this->profile->smallCoverHref,
            'coverLargeHref' => $this->profile->largeCoverHref,
            'linkSite' => strtolower($this->profile->platform),
            'linkHref' => strtolower($this->profile->link),
        );
        $data = $this->client->cypherJson(self::STATEMENT, $args);
        return $this->beans->createUser($data['user']);
    }

    /**
     * @return string
     */
    private function generateUserId()
    {
        return Generator::getInstance()->generate();
    }

}
