<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Profile;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\EventNotFoundException;

class UpdateEventArtistList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})
OPTIONAL MATCH
    (:Artist)-
    [_performsAtToDelete:PERFORMS_AT]->
    (_event)
DELETE
    _performsAtToDelete
WITH
    _event
UNWIND
    {performsAtList} as _performsAtData
MATCH
    (_artistUpdate:Artist {id: _performsAtData.artistId})
MERGE
    (_artistUpdate)-
    [:PERFORMS_AT {index: _performsAtData.index}]->
    (_event)
WITH
    distinct(_event) as _event
WITH
    _event
OPTIONAL MATCH
    (_event)<-
    [_performsAt:PERFORMS_AT]-
    (_artist:Artist)
OPTIONAL MATCH
    (_artist)-
    [:HAS_COVER]->
    (_artistCover:Photo)
OPTIONAL MATCH
    (_artist)-
    [:HAS_LINK]->
    (_link:Link)
WITH
    _artist, _artistCover, _performsAt,
    collect({
        href: _link.href,
        site: _link.site,
        hide: _link.hide
    }) as linkList
ORDER BY
    _performsAt.index
RETURN
    {
        artistList: collect({
            id: _artist.id,
            title: _artist.title,
            intro: _artist.intro,
            about: null,
            hometown: _artist.hometown,
            tagList: [],
            linkList: linkList,
            cover: {
                large: _artistCover.large,
                small: null
            }
        })
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var string[]
     */
    private $artistList;

    /**
     * @param IGraphClient $client
     * @param string $eventId
     * @param string[] $artistList
     */
    public function __construct(IGraphClient $client, $eventId, array $artistList)
    {
        $this->client = $client;
        $this->eventId = $eventId;
        $this->artistList = $artistList;
        $this->beans = new Factory();
    }

    /**
     * @return Profile[]
     * @throws InvalidEventDataException
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'eventId' => $this->eventId,
            'performsAtList' => $this->formatPerformsAtList(),
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $this->beans->listMap([$this->beans, 'createProfile'], $data['artistList']);
    }

    /**
     * @return array
     */
    private function formatPerformsAtList()
    {
        return array_map(function($artistId, $index) {
            return array(
                'artistId' => $artistId,
                'index' => $index,
            );
        }, $this->artistList, array_keys($this->artistList));
    }

}
