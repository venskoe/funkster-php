<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Event;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\WriteAccessException;

class CreateEvent
{
    const STATEMENT = <<<CYPHER
MATCH
    (_user:User {id: {userId}})
CREATE
    (_event:Event {id: {eventId}})-
    [:MANAGED_BY {timestamp: {now}}]->
    (_user)
SET
    _event.title = {title},
    _event.tzOffset = {tzOffset},
    _event.startsAt = {startsAt},
    _event.endsAt = {endsAt}
RETURN
    {
        event: {
            id: _event.id,
            title: _event.title,
            tzOffset: _event.tzOffset,
            startsAt: _event.startsAt,
            endsAt: _event.endsAt,
            cover: null,
            place: null,
            tagList: [],
            linkList: [],
            artistList: []
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $tzOffset;

    /**
     * @var int
     */
    private $startsAt;

    /**
     * @var int
     */
    private $endsAt;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $eventId
     * @param string $title
     * @param int $tzOffset
     * @param int $startsAt
     * @param int $endsAt
     */
    public function __construct(IGraphClient $client, $userId, $eventId, $title, $tzOffset, $startsAt, $endsAt)
    {
        $this->client = $client;
        $this->userId = $userId;
        $this->eventId = $eventId;
        $this->title = $title;
        $this->tzOffset = $tzOffset;
        $this->startsAt = $startsAt;
        $this->endsAt = $endsAt;
        $this->beans = new Factory();
    }

    /**
     * @return Event
     * @throws InvalidEventDataException
     * @throws WriteAccessException
     */
    public function get()
    {
        $args = array(
            'now' => time(),
            'userId' => $this->userId,
            'eventId' => $this->eventId,
            'title' => $this->title,
            'tzOffset' => $this->tzOffset,
            'startsAt' => $this->startsAt,
            'endsAt' => $this->endsAt,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new WriteAccessException('write_access_required');
        }
        catch (ItemAlreadyExistsException $e) {
            throw new InvalidEventDataException('event_already_exists');
        }
        return $this->beans->createEvent($data['event']);
    }

}
