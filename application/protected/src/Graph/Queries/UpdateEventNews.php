<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\News;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\EventNotFoundException;

class UpdateEventNews
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})
WITH
    _event
MERGE
    (_event)<-
    [:POSTED_AT]-
    (_news:News {createdAt: {createdAt}})
ON CREATE SET
    _news.title = {title},
    _news.content = {content}
ON MATCH SET
    _news.title = {title},
    _news.content = {content}
WITH
    _news
RETURN
    {
        news: {
            createdAt: _news.createdAt,
            title: _news.title,
            content: _news.content
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var int
     */
    private $createdAt;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @param IGraphClient $client
     * @param string $eventId
     * @param int $createdAt
     * @param string $title
     * @param string $content
     */
    public function __construct(IGraphClient $client, $eventId, $createdAt, $title, $content)
    {
        $this->client = $client;
        $this->eventId = $eventId;
        $this->createdAt = $createdAt;
        $this->title = $title;
        $this->content = $content;
        $this->beans = new Factory();
    }

    /**
     * @return News
     * @throws InvalidEventDataException
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'eventId' => $this->eventId,
            'createdAt' => $this->createdAt,
            'title' => $this->title,
            'content' => $this->content,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $this->beans->createNews($data['news']);
    }

}
