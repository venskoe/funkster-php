<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Place;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\EventNotFoundException;

class UpdateEventPlace
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})
WITH
    _event
OPTIONAL MATCH
    (_event)-
    [_hostedAtToDelete:HOSTED_AT]->
    (:Place)
DELETE
    _hostedAtToDelete
WITH
    _event
MATCH
    (_place:Place {id: {placeId}})
CREATE
    (_event)-
    [:HOSTED_AT]->
    (_place)
RETURN
    {
        place: {
            id: _place.id,
            title: _place.title,
            intro: _place.intro,
            address: _place.address,
            latitude: _place.latitude,
            longitude: _place.longitude,
            linkList: [],
            cover: null
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var string
     */
    private $placeId;

    /**
     * @param IGraphClient $client
     * @param string $eventId
     * @param string $placeId
     */
    public function __construct(IGraphClient $client, $eventId, $placeId)
    {
        $this->client = $client;
        $this->eventId = $eventId;
        $this->placeId = $placeId;
        $this->beans = new Factory();
    }

    /**
     * @return Place
     * @throws InvalidEventDataException
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'eventId' => $this->eventId,
            'placeId' => $this->placeId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $this->beans->createPlace($data['place']);
    }

}
