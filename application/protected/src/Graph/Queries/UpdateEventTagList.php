<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Tag;
use Graph\Beans\Factory;
use Graph\Exceptions\EventNotFoundException;

class UpdateEventTagList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})
OPTIONAL MATCH
    (_event)-
    [_taggedBy:TAGGED_BY]->
    (:Tag)
DELETE
    _taggedBy
WITH
    distinct(_event) as _event
UNWIND
    {tagList} as __tag
MERGE
    (_tag:Tag {id: __tag.id})
ON CREATE SET
    _tag.title = __tag.title
WITH
    _event, _tag, __tag
CREATE
    (_event)-
    [:TAGGED_BY {index: __tag.index}]->
    (_tag)
RETURN
    {
        tagList: collect({
            id: _tag.id,
            title: _tag.title
        })
    } as json
CYPHER;


    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var Tag[]
     */
    private $tagList;

    /**
     * @param IGraphClient $client
     * @param string $eventId
     * @param Tag[] $tagList
     */
    public function __construct(IGraphClient $client, $eventId, array $tagList)
    {
        $this->client = $client;
        $this->eventId = $eventId;
        $this->tagList = $tagList;
        $this->beans = new Factory();
    }

    /**
     * @return Tag[]
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'eventId' => $this->eventId,
            'tagList' => $this->formatTagList(),
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $this->beans->listMap([$this->beans, 'createTag'], $data['tagList']);
    }

    /**
     * @return array
     */
    private function formatTagList()
    {
        return array_map(function(Tag $tag, $index) {
            return array(
                'id' => $tag->id,
                'title' => $tag->title,
                'index' => $index,
            );
        }, $this->tagList, array_keys($this->tagList));
    }

}
