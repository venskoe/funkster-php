<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Profile;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidArtistDataException;
use Graph\Exceptions\WriteAccessException;

class CreateArtist
{
    const STATEMENT = <<<CYPHER
MATCH
    (_user:User {id: {userId}})
CREATE
    (_artist:Profile:Artist {id: {artistId}})-
    [:MANAGED_BY {timestamp: {now}}]->
    (_user)
SET
    _artist.title = {title},
    _artist.intro = {intro},
    _artist.about = {about},
    _artist.hometown = {hometown}
RETURN
    {
        artist: {
            id: _artist.id,
            title: _artist.title,
            intro: _artist.intro,
            about: _artist.about,
            hometown: _artist.hometown,
            tagList: [],
            linkList: [],
            cover: null
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $artistId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $intro;

    /**
     * @var string
     */
    private $about;

    /**
     * @var string
     */
    private $hometown;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $artistId
     * @param string $title
     * @param string $intro
     * @param string $about
     * @param string $hometown
     */
    public function __construct(IGraphClient $client, $userId, $artistId, $title, $intro, $about, $hometown)
    {
        $this->client = $client;
        $this->userId = $userId;
        $this->artistId = $artistId;
        $this->title = $title;
        $this->intro = $intro;
        $this->about = $about;
        $this->hometown = $hometown;
        $this->beans = new Factory();
    }

    /**
     * @return Profile
     * @throws InvalidArtistDataException
     * @throws WriteAccessException
     */
    public function get()
    {
        $args = array(
            'now' => time(),
            'userId' => $this->userId,
            'artistId' => $this->artistId,
            'title' => $this->title,
            'intro' => $this->intro,
            'about' => $this->about,
            'hometown' => $this->hometown,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new WriteAccessException();
        }
        catch (ItemAlreadyExistsException $e) {
            throw new InvalidArtistDataException();
        }
        return $this->beans->createProfile($data['artist']);
    }

}
