<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Exceptions\EventNotFoundException;

class GetEventIdByLegacy
{
    const STATEMENT = <<<CYPHER
MATCH
    (:Legacy {id: {eventId}})<-
    [:WAS]-
    (_event:Event)
RETURN
    {
        eventId: _event.id
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @param IGraphClient $client
     * @param string $eventId
     */
    public function __construct(IGraphClient $client, $eventId)
    {
        $this->client = $client;
        $this->eventId = $eventId;
    }

    /**
     * @return string
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'eventId' => $this->eventId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $data['eventId'];
    }

}
