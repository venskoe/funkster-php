<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Exceptions\ArtistNotFoundException;

class GetArtistIdByLegacy
{
    const STATEMENT = <<<CYPHER
MATCH
    (:Legacy {id: {artistId}})<-
    [:WAS]-
    (_artist:Artist)
RETURN
    {
        artistId: _artist.id
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $artistId;

    /**
     * @param IGraphClient $client
     * @param string $artistId
     */
    public function __construct(IGraphClient $client, $artistId)
    {
        $this->client = $client;
        $this->artistId = $artistId;
    }

    /**
     * @return string
     * @throws ArtistNotFoundException
     */
    public function get()
    {
        $args = array(
            'artistId' => $this->artistId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new ArtistNotFoundException();
        }
        return $data['artistId'];
    }

}
