<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\News;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\EventNotFoundException;

class DeleteEventNews
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})<-
    [_postedAt:POSTED_AT]-
    (_news:News {createdAt: {createdAt}})
WITH
    _postedAt, _news,
    {
        createdAt: _news.createdAt,
        title: _news.title,
        content: _news.content
    } as news
DELETE
    _postedAt, _news
WITH
    news
RETURN
    {
        news: news
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var int
     */
    private $createdAt;

    /**
     * @param IGraphClient $client
     * @param string $eventId
     * @param int $createdAt
     */
    public function __construct(IGraphClient $client, $eventId, $createdAt)
    {
        $this->client = $client;
        $this->eventId = $eventId;
        $this->createdAt = $createdAt;
        $this->beans = new Factory();
    }

    /**
     * @return News
     * @throws InvalidEventDataException
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'eventId' => $this->eventId,
            'createdAt' => $this->createdAt,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $this->beans->createNews($data['news']);
    }

}
