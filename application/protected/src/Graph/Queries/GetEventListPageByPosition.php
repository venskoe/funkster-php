<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Location;
use Graph\Beans\EventListPage;
use Graph\Beans\Factory;

class GetEventListPageByPosition
{
    const STATEMENT = <<<CYPHER
START
    _place = node:Place({index})
MATCH
    (_place)<-
    [:HOSTED_AT]-
    (_event:Event)
WHERE
    _event.endsAt >= {now}
WITH
    _event, _place
ORDER BY
    _event.startsAt ASC
LIMIT
    {eventLimit}
WITH
    _event,
    {
        id: _place.id,
        title: _place.title,
        intro: null,
        address: _place.address,
        latitude: _place.latitude,
        longitude: _place.longitude,
        linkList: [],
        cover: null
    } as place
OPTIONAL MATCH
    (_event)-
    [:HAS_COVER]->
    (_eventCover:Photo)
WITH
    _event, place,
    {
        large: null,
        small: _eventCover.small
    } as eventCover
MATCH
    (_event)<-
    [_performsAt:PERFORMS_AT]-
    (_artist:Artist)
OPTIONAL MATCH
    (_artist)-
    [:HAS_COVER]->
    (_artistCover:Photo)
WITH
    _event, place, eventCover, _artist, _artistCover, _performsAt
ORDER BY
    _performsAt.index
WITH
    _event, place, eventCover,
    collect({
        id: _artist.id,
        title: _artist.title,
        intro: null,
        about: null,
        hometown: _artist.hometown,
        tagList: [],
        linkList: [],
        cover: {
            large: null,
            small: _artistCover.small
        }
    }) as artistList
OPTIONAL MATCH
    (_event)-
    [_taggedBy:TAGGED_BY]->
    (_tag:Tag)
WITH
    _event, place, eventCover, artistList, _tag, _taggedBy
ORDER BY
    _taggedBy.index
WITH
    _event, place, eventCover, artistList,
    collect({
        id: _tag.id,
        title: _tag.title
    }) as tagList
ORDER BY
    _event.startsAt ASC
WITH
    _event, place, eventCover, artistList, tagList
RETURN
    {
        location: null,
        eventList: collect({
            id: _event.id,
            title: _event.title,
            tzOffset: _event.tzOffset,
            startsAt: _event.startsAt,
            endsAt: _event.endsAt,
            cover: eventCover,
            place: place,
            tagList: tagList,
            linkList: [],
            artistList: artistList
        })
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @var int
     */
    private $distance;

    /**
     * @var int
     */
    private $eventLimit;

    /**
     * @param IGraphClient $client
     * @param float $latitude
     * @param float $longitude
     * @param int $distance
     * @param int $eventLimit
     */
    public function __construct(IGraphClient $client, $latitude, $longitude, $distance, $eventLimit)
    {
        $this->client = $client;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->distance = $distance;
        $this->eventLimit = $eventLimit;
        $this->beans = new Factory();
    }

    /**
     * @return EventListPage
     */
    public function get()
    {
        $args = array(
            'now' => time(),
            'eventLimit' => $this->eventLimit,
            'index' => strtr('withinDistance:[latitude, longitude, distance]', array(
                'latitude' => sprintf('%f', $this->latitude),
                'longitude' => sprintf('%f', $this->longitude),
                'distance' => sprintf('%f', $this->distance),
            )),
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
            $page = $this->beans->createEventListPage($data);
        }
        catch (ItemNotFoundException $e) {
            $page = new EventListPage();
            $page->location = new Location();
            $page->eventList = [];
        }
        return $page;
    }

}
