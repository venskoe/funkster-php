<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\UserPage;
use Graph\Beans\Factory;
use Graph\Exceptions\UserNotFoundException;

class GetUserPage
{
    const STATEMENT = <<<CYPHER
MATCH
    (_user:User {id: {userId}})-
    [:HAS_PROFILE]->
    (_profile:Profile)
WITH
    _user, _profile
OPTIONAL MATCH
    (_profile)-
    [:HAS_COVER]->
    (_profileCover:Photo)
WITH
    _user,
    {
        id: _profile.id,
        title: _profile.title,
        intro: null,
        about: null,
        hometown: null,
        tagList: [],
        linkList: [],
        cover: {
            large: _profileCover.large,
            small: _profileCover.small
        }
    } as profile
OPTIONAL MATCH
    (_event:Event)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    _user, profile,
    count(_event) as eventCount
OPTIONAL MATCH
    (_event:Event)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    _user, profile, eventCount, _event
ORDER BY
    _managedBy.timestamp DESC
LIMIT
    {eventLimit}
WITH
    _user, profile, eventCount,
    collect({
        id: _event.id,
        title: _event.title,
        tzOffset: null,
        startsAt: null,
        endsAt: null,
        cover: null,
        place: null,
        tagList: [],
        linkList: [],
        artistList: []
    }) as eventList
OPTIONAL MATCH
    (_place:Place)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    _user, profile, eventCount, eventList,
    count(_place) as placeCount
OPTIONAL MATCH
    (_place:Place)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    _user, profile, eventCount, eventList, placeCount, _place, _managedBy
ORDER BY
    _managedBy.timestamp DESC
LIMIT
    {placeLimit}
WITH
    _user, profile, eventCount, eventList, placeCount,
    collect({
        id: _place.id,
        title: _place.title,
        intro: null,
        address: null,
        latitude: null,
        longitude: null,
        linkList: [],
        cover: null
    }) as placeList
OPTIONAL MATCH
    (_artist:Artist)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    _user, profile, eventCount, eventList, placeCount, placeList,
    count(_artist) as artistCount
OPTIONAL MATCH
    (_artist:Artist)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    _user, profile, eventCount, eventList, placeCount, placeList, artistCount, _artist, _managedBy
ORDER BY
    _managedBy.timestamp DESC
LIMIT
    {artistLimit}
WITH
    _user, profile, eventCount, eventList, placeCount, placeList, artistCount,
    collect({
        id: _artist.id,
        title: _artist.title,
        intro: null,
        about: null,
        hometown: null,
        tagList: [],
        linkList: [],
        cover: null
    }) as artistList
WITH
    {
        userId: _user.id,
        profile: profile,
        eventList: eventList,
        eventCount: eventCount,
        placeList: placeList,
        placeCount: placeCount,
        artistList: artistList,
        artistCount: artistCount
    } as json
RETURN
    json
CYPHER;

    /**
     * @var \Graph\Beans\Factory
     */
    private $beans;

    /**
     * @var \Menara\Generic\Storage\Interfaces\IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var int
     */
    private $eventLimit;

    /**
     * @var int
     */
    private $placeLimit;

    /**
     * @var int
     */
    private $artistLimit;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param int $eventLimit
     * @param int $placeLimit
     * @param int $artistLimit
     */
    public function __construct(IGraphClient $client, $userId, $eventLimit, $placeLimit, $artistLimit)
    {
        $this->beans = new Factory();
        $this->client = $client;
        $this->userId = $userId;
        $this->eventLimit = $eventLimit;
        $this->placeLimit = $placeLimit;
        $this->artistLimit = $artistLimit;
    }

    /**
     * @return UserPage
     * @throws UserNotFoundException
     */
    public function get()
    {
        $args = array(
            'userId' => $this->userId,
            'eventLimit' => $this->eventLimit,
            'placeLimit' => $this->placeLimit,
            'artistLimit' => $this->artistLimit,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new UserNotFoundException();
        }
        return $this->beans->createUserPage($data);
    }

}
