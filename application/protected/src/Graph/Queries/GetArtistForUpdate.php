<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Profile;
use Graph\Beans\Factory;
use Graph\Exceptions\ArtistNotFoundException;
use Graph\Exceptions\WriteAccessException;

class GetArtistForUpdate
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist {id: {artistId}})
WITH
    _artist,
    {
        id: _artist.id,
        title: _artist.title,
        intro: _artist.intro,
        about: _artist.about,
        hometown: _artist.hometown,
        tagList: [],
        linkList: [],
        cover: null
    } as artist
OPTIONAL MATCH
    (_user:User {id: {userId}})
WITH
    _artist, artist, _user
OPTIONAL MATCH
    (_artist)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    artist,
    sum(COALESCE(_user.isAdmin, 0)) + count(_managedBy) as canUpdate
RETURN
    {
        artist: artist,
        canUpdate: canUpdate
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $artistId;

    /**
     * @var Factory
     */
    private $beans;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $artistId
     */
    public function __construct(IGraphClient $client, $userId, $artistId)
    {
        $this->client = $client;
        $this->userId = $userId;
        $this->artistId = $artistId;
        $this->beans = new Factory();
    }

    /**
     * @return Profile
     * @throws WriteAccessException
     * @throws ArtistNotFoundException
     */
    public function get()
    {
        $args = array(
            'userId' => $this->userId,
            'artistId' => $this->artistId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new ArtistNotFoundException();
        }
        if (!@$data['canUpdate']) {
            throw new WriteAccessException();
        }
        return $this->beans->createProfile($data['artist']);
    }

}
