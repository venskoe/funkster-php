<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Profile;
use Graph\Beans\Factory;

class SearchArtistList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist)
WHERE
    _artist.id =~ {search}
WITH
    _artist
LIMIT
    {limit}
WITH
    _artist
OPTIONAL MATCH
    (_artist)-
    [:HAS_COVER]->
    (_artistCover:Photo)
WITH
    collect({
        id: _artist.id,
        title: _artist.title,
        intro: _artist.intro,
        about: null,
        hometown: _artist.hometown,
        tagList: [],
        linkList: [],
        cover: {
            large: _artistCover.large,
            small: _artistCover.small
        }
    }) as artistList
RETURN
    {
        artistList: artistList
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $search;

    /**
     * @var int
     */
    private $limit;

    /**
     * @param IGraphClient $client
     * @param string $search
     * @param int $limit
     */
    public function __construct(IGraphClient $client, $search, $limit)
    {
        $this->client = $client;
        $this->search = $search;
        $this->limit = $limit;
        $this->beans = new Factory();
    }

    /**
     * @return Profile[]
     */
    public function get()
    {
        $args = array(
            'search' => '.*?' . $this->search . '.*',
            'limit' => $this->limit,
        );
        $data = $this->client->cypherJson(self::STATEMENT, $args);
        return $this->beans->listMap([$this->beans, 'createProfile'], @$data['artistList']);
    }

}
