<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Event;
use Graph\Beans\Factory;

class GetEventListStartsAfter
{
    const STATEMENT = <<<CYPHER
MATCH
    (_place)<-
    [:HOSTED_AT]-
    (_event:Event)
WHERE
    _event.endsAt >= {now}
WITH
    _event,
    {
        id: _place.id,
        linkList: [],
        cover: null
    } as place
RETURN
    {
        eventList: collect({
            id: _event.id,
            tzOffset: _event.tzOffset,
            startsAt: _event.startsAt,
            cover: null,
            place: place,
            tagList: [],
            linkList: [],
            artistList: []
        })
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var int
     */
    private $since;

    /**
     * @param IGraphClient $client
     * @param int $since
     */
    public function __construct(IGraphClient $client, $since)
    {
        $this->client = $client;
        $this->since = $since;
        $this->beans = new Factory();
    }

    /**
     * @return Event[]
     */
    public function get()
    {
        $args = array(
            'now' => $this->since,
        );
        $data = $this->client->cypherJson(self::STATEMENT, $args);
        return $this->beans->listMap([$this->beans, 'createEvent'], @$data['eventList']);
    }

}
