<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Place;
use Graph\Beans\Factory;

class SearchPlaceList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_place:Place)
WHERE
    _place.id =~ {search}
WITH
    _place
LIMIT
    {limit}
WITH
    collect({
        id: _place.id,
        title: _place.title,
        intro: _place.intro,
        address: _place.address,
        tzOffset: _place.tzOffset,
        latitude: _place.latitude,
        longitude: _place.longitude,
        linkList: [],
        cover: null
    }) as placeList
RETURN
    {
        placeList: placeList
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $search;

    /**
     * @var int
     */
    private $limit;

    /**
     * @param IGraphClient $client
     * @param string $search
     * @param int $limit
     */
    public function __construct(IGraphClient $client, $search, $limit)
    {
        $this->client = $client;
        $this->search = $search;
        $this->limit = $limit;
        $this->beans = new Factory();
    }

    /**
     * @return Place[]
     */
    public function get()
    {
        $args = array(
            'search' => '.*?' . $this->search . '.*',
            'limit' => $this->limit,
        );
        $data = $this->client->cypherJson(self::STATEMENT, $args);
        return $this->beans->listMap([$this->beans, 'createPlace'], @$data['placeList']);
    }

}
