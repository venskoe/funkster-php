<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Photo;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidArtistDataException;
use Graph\Exceptions\ArtistNotFoundException;

class UpdateArtistCover
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist {id: {artistId}})
WITH
    _artist
MERGE
    (_artist)-
    [:HAS_COVER]->
    (_photo:Photo)
WITH
    _photo
SET
    _photo.large = {largeHref},
    _photo.small = {smallHref}
WITH
    _photo
RETURN
    {
        cover: {
            large: _photo.large,
            small: _photo.small
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $artistId;

    /**
     * @var string
     */
    private $largeHref;

    /**
     * @var string
     */
    private $smallHref;

    /**
     * @param IGraphClient $client
     * @param string $artistId
     * @param string $largeHref
     * @param string $smallHref
     */
    public function __construct(IGraphClient $client, $artistId, $largeHref, $smallHref)
    {
        $this->client = $client;
        $this->artistId = $artistId;
        $this->largeHref = $largeHref;
        $this->smallHref = $smallHref;
        $this->beans = new Factory();
    }

    /**
     * @return Photo
     * @throws InvalidArtistDataException
     * @throws ArtistNotFoundException
     */
    public function get()
    {
        $args = array(
            'artistId' => $this->artistId,
            'largeHref' => $this->largeHref,
            'smallHref' => $this->smallHref,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new ArtistNotFoundException();
        }
        return $this->beans->createPhoto($data['cover']);
    }

}
