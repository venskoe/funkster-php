<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Post;
use Graph\Beans\Factory;
use Graph\Exceptions\ArtistNotFoundException;

class CreateArtistPost
{
    const STATEMENT = <<<CYPHER
MATCH
    (_artist:Artist {id: {artistId}})
WITH
    _artist
MATCH
    (_user:User {id: {userId}})-
    [:HAS_PROFILE]->
    (_profile:Profile)
WITH
    _artist, _user, _profile
OPTIONAL MATCH
    (_profile)-
    [:HAS_COVER]->
    (_profileCover:Photo)
WITH
    _artist, _user, _profile, _profileCover
MERGE
    (_post:Post {id: {postId}})
ON CREATE SET
    _post.type = {type},
    _post.href = {href},
    _post.cover = {cover},
    _post.title = {title},
    _post.source = {source},
    _post.createdAt = {now}
WITH
    _artist, _user, _profile, _profileCover, _post
MERGE
    (_post)-
    [_postedAt:POSTED_AT]->
    (_artist)
ON CREATE SET
    _postedAt.timestamp = {now},
    _postedAt.userId = {userId}
WITH
    _artist, _user, _profile, _profileCover, _post, _postedAt
RETURN
    {
        post: {
            id: _post.id,
            type: _post.type,
            href: _post.href,
            title: _post.title,
            cover: _post.cover,
            source: _post.source,
            postedAt: _postedAt.timestamp,
            author: {
                id: _profile.id,
                title: _profile.title,
                cover: {
                    small: _profileCover.small,
                    large: null
                },
                tagList: [],
                linkList: []
            }
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $artistId;

    /**
     * @var string
     */
    private $postId;

    /**
     * @var string
     */
    private $source;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $href;

    /**
     * @var string
     */
    private $cover;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Factory
     */
    private $beans;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $artistId
     * @param string $postId
     * @param string $source
     * @param string $type
     * @param string $href
     * @param string $cover
     * @param string $title
     */
    public function __construct(IGraphClient $client, $userId, $artistId, $postId, $source, $type, $href, $cover, $title)
    {
        $this->client = $client;
        $this->userId = $userId;
        $this->artistId = $artistId;
        $this->postId = $postId;
        $this->source = $source;
        $this->type = $type;
        $this->href = $href;
        $this->cover = $cover;
        $this->title = $title;
        $this->beans = new Factory();
    }

    /**
     * @return Post
     * @throws ArtistNotFoundException
     */
    public function get()
    {
        $args = array(
            'now' => time(),
            'userId' => $this->userId,
            'artistId' => $this->artistId,
            'postId' => $this->postId,
            'source' => $this->source,
            'type' => $this->type,
            'href' => $this->href,
            'cover' => $this->cover,
            'title' => $this->title
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new ArtistNotFoundException();
        }
        return $this->beans->createPost($data['post']);
    }

}
