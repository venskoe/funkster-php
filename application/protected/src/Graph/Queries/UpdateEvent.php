<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Event;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\EventNotFoundException;

class UpdateEvent
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {oldEventId}})
SET
    _event.id = {newEventId},
    _event.title = {title},
    _event.tzOffset = {tzOffset},
    _event.startsAt = {startsAt},
    _event.endsAt = {endsAt}
WITH
    _event
OPTIONAL MATCH
    (:Event)-
    [_was:WAS]->
    (:Legacy {id: {oldEventId}})
WITH
    _event, _was
DELETE
    _was
WITH
    _event
MERGE
    (_legacy:Legacy {id: {oldEventId}})
WITH
    _event, _legacy
MERGE
    (_event)-
    [:WAS]->
    (_legacy)
RETURN
    {
        event: {
            id: _event.id,
            title: _event.title,
            tzOffset: _event.tzOffset,
            startsAt: _event.startsAt,
            endsAt: _event.endsAt,
            cover: null,
            place: null,
            tagList: [],
            linkList: [],
            artistList: []
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $oldEventId;

    /**
     * @var string
     */
    private $newEventId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $tzOffset;

    /**
     * @var int
     */
    private $startsAt;

    /**
     * @var int
     */
    private $endsAt;

    /**
     * @param IGraphClient $client
     * @param string $oldEventId
     * @param string $newEventId
     * @param string $title
     * @param int $tzOffset
     * @param int $startsAt
     * @param int $endsAt
     */
    public function __construct(IGraphClient $client, $oldEventId, $newEventId, $title, $tzOffset, $startsAt, $endsAt)
    {
        $this->client = $client;
        $this->oldEventId = $oldEventId;
        $this->newEventId = $newEventId;
        $this->title = $title;
        $this->tzOffset = $tzOffset;
        $this->startsAt = $startsAt;
        $this->endsAt = $endsAt;
        $this->beans = new Factory();
    }

    /**
     * @return Event
     * @throws InvalidEventDataException
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'oldEventId' => $this->oldEventId,
            'newEventId' => $this->newEventId,
            'title' => $this->title,
            'tzOffset' => $this->tzOffset,
            'startsAt' => $this->startsAt,
            'endsAt' => $this->endsAt,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        catch (ItemAlreadyExistsException $e) {
            throw new InvalidEventDataException();
        }
        return $this->beans->createEvent($data['event']);
    }

}
