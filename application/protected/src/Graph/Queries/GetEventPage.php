<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\EventPage;
use Graph\Beans\Factory;
use Graph\Exceptions\EventNotFoundException;

class GetEventPage
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})
WITH
    _event
OPTIONAL MATCH
    (_event)-
    [:HAS_COVER]->
    (_eventCover:Photo)
WITH
    _event,
    {
        large: _eventCover.large,
        small: _eventCover.small
    } as eventCover
OPTIONAL MATCH
    (_event)-
    [:HOSTED_AT]->
    (_place:Place)
WITH
    _event, _place, eventCover,
    {
        id: _place.id,
        title: _place.title,
        intro: _place.intro,
        address: _place.address,
        latitude: _place.latitude,
        longitude: _place.longitude,
        instagram: _place.instagram,
        linkList: [],
        cover: null
    } as place
OPTIONAL MATCH
    (_event)-
    [:MANAGED_BY]->
    (:User)-
    [:HAS_PROFILE]->
    (_promoter:Profile)
WITH
    _event, _place, _promoter, place, eventCover
OPTIONAL MATCH
    (_promoter)-
    [:HAS_COVER]->
    (_promoterCover:Photo)
WITH
    _event, _place, place, eventCover,
    {
        id: _promoter.id,
        title: _promoter.title,
        intro: _promoter.intro,
        cover: {
            small: _promoterCover.small,
            large: null
        }
    } as promoter
OPTIONAL MATCH
    (_place)<-
    [:HOSTED_AT]-
    (_relatedEvent:Event)
WHERE
    _relatedEvent.startsAt > {now} AND
    _relatedEvent.id <> _event.id
WITH
    _event, place, eventCover, promoter, _relatedEvent
ORDER BY
    _relatedEvent.startsAt
LIMIT
    {relatedEventLimit}
WITH
    _event, place, eventCover, promoter,
    collect({
        id: _relatedEvent.id,
        title: _relatedEvent.title,
        tzOffset: _relatedEvent.tzOffset,
        startsAt: _relatedEvent.startsAt,
        endsAt: null,
        cover: null,
        place: null,
        tagList: [],
        linkList: [],
        artistList: []
    }) as relatedEventList
OPTIONAL MATCH
    (_event)<-
    [:POSTED_AT]-
    (_comment:Message)
WITH
    _event, place, eventCover, promoter, relatedEventList,
    count(_comment) as commentCount
OPTIONAL MATCH
    (_event)<-
    [:POSTED_AT]-
    (_comment:Message)-
    [:MANAGED_BY]->
    (:User)-
    [:HAS_PROFILE]->
    (_commentAuthor:Profile)-
    [:HAS_COVER]->
    (_commentAuthorCover:Photo)
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount,
    _comment, _commentAuthor, _commentAuthorCover
ORDER BY
    _comment.createdAt DESC
LIMIT
    {commentLimit}
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount,
    collect({
        content: _comment.content,
        createdAt: _comment.createdAt,
        author: {
            id: _commentAuthor.id,
            title: _commentAuthor.title,
            cover: {
                small: _commentAuthorCover.small,
                large: null
            }
        }
    }) as commentList
OPTIONAL MATCH
    (_event)-
    [_taggedBy:TAGGED_BY]->
    (_tag:Tag)
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, _tag, _taggedBy
ORDER BY
    _taggedBy.index
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList,
    collect({
        id: _tag.id,
        title: _tag.title
    }) as tagList
OPTIONAL MATCH
    (_event)<-
    [_performsAt:PERFORMS_AT]-
    (_artist:Artist)
OPTIONAL MATCH
    (_artist)-
    [:HAS_COVER]->
    (_artistCover:Photo)
OPTIONAL MATCH
    (_artist)-
    [:HAS_LINK]->
    (_link:Link)
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, tagList,
    _artist, _artistCover, _performsAt,
    collect({
        site: _link.site,
        href: _link.href,
        hide: _link.hide
    }) as artistLinkList
ORDER BY
    _performsAt.index
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, tagList,
    collect({
        id: _artist.id,
        title: _artist.title,
        intro: _artist.intro,
        about: null,
        hometown: _artist.hometown,
        tagList: [],
        linkList: artistLinkList,
        cover: {
            large: _artistCover.large,
            small: _artistCover.small
        }
    }) as artistList
OPTIONAL MATCH
    (_event)<-
    [:POSTED_AT]-
    (_news:News)
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, tagList, artistList,
    _news
ORDER BY
    _news.createdAt DESC
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, tagList, artistList,
    collect({
        title: _news.title,
        content: _news.content,
        createdAt: _news.createdAt,
        author: null
    }) as newsList
OPTIONAL MATCH
    (_event)<-
    [:PERFORMS_AT*0..1]-
    ()<-
    [_postedAt:POSTED_AT]-
    (_post:Post)
OPTIONAL MATCH
    (:User {id: _postedAt.userId})-
    [:HAS_PROFILE]->
    (_postAuthor:Profile)-
    [:HAS_COVER]->
    (_postAuthorCover:Photo)
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, tagList, artistList, newsList,
    _post, _postedAt, _postAuthor, _postAuthorCover
ORDER BY
    _postedAt.timestamp DESC
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, tagList, artistList, newsList,
    collect({
        id: _post.id,
        type: _post.type,
        href: _post.href,
        title: _post.title,
        cover: _post.cover,
        source: _post.source,
        postedAt: _postedAt.timestamp,
        author: {
            id: _postAuthor.id,
            title: _postAuthor.title,
            cover: {
                small: _postAuthorCover.small,
                large: null
            }
        }
    }) as postList
OPTIONAL MATCH
    (_user:User {id: {userId}})
WITH
    _event, place, eventCover, promoter, relatedEventList, commentCount, commentList, tagList, artistList, newsList,
    postList,
    _user
OPTIONAL MATCH
    (_event)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    promoter, relatedEventList, commentCount, commentList, newsList, postList,
    sum(COALESCE(_user.isAdmin, 0)) + count(_managedBy) as canUpdate,
    {
        id: _event.id,
        title: _event.title,
        tzOffset: _event.tzOffset,
        startsAt: _event.startsAt,
        endsAt: _event.endsAt,
        cover: eventCover,
        place: place,
        tagList: tagList,
        linkList: [],
        artistList: artistList
    } as event
WITH
    event, promoter, relatedEventList, commentCount, commentList, newsList, postList,
    {
        canUpdate: canUpdate,
        canCreate: 0,
        canDelete: 0
    } as access
WITH
    {
        event: event,
        access: access,
        promoter: promoter,
        newsList: newsList,
        postList: postList,
        commentList: commentList,
        commentCount: commentCount,
        relatedEventList: relatedEventList
    } as json
RETURN
    json
CYPHER;

    /**
     * @var \Graph\Beans\Factory
     */
    private $beans;

    /**
     * @var \Menara\Generic\Storage\Interfaces\IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var int
     */
    private $relatedEventLimit;

    /**
     * @var int
     */
    private $commentLimit;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $eventId
     * @param int $relatedEventLimit
     * @param int $commentLimit
     */
    public function __construct(IGraphClient $client, $userId, $eventId, $relatedEventLimit, $commentLimit)
    {
        $this->beans = new Factory();
        $this->client = $client;
        $this->userId = $userId;
        $this->eventId = $eventId;
        $this->relatedEventLimit = $relatedEventLimit;
        $this->commentLimit = $commentLimit;
    }

    /**
     * @return EventPage
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'userId' => $this->userId,
            'eventId' => $this->eventId,
            'relatedEventLimit' => $this->relatedEventLimit,
            'commentLimit' => $this->commentLimit,
            'now' => time(),
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        return $this->beans->createEventPage($data);
    }

}
