<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Event;
use Graph\Beans\Factory;
use Graph\Exceptions\EventNotFoundException;
use Graph\Exceptions\WriteAccessException;

class GetEventForUpdate
{
    const STATEMENT = <<<CYPHER
MATCH
    (_event:Event {id: {eventId}})
WITH
    _event,
    {
        id: _event.id,
        title: _event.title,
        tzOffset: _event.tzOffset,
        startsAt: _event.startsAt,
        endsAt: _event.endsAt,
        cover: null,
        place: null,
        tagList: [],
        linkList: [],
        artistList: []
    } as event
OPTIONAL MATCH
    (_user:User {id: {userId}})
WITH
    _event, event, _user
OPTIONAL MATCH
    (_event)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    event,
    sum(COALESCE(_user.isAdmin, 0)) + count(_managedBy) as canUpdate
RETURN
    {
        event: event,
        canUpdate: canUpdate
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $eventId;

    /**
     * @var Factory
     */
    private $beans;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $eventId
     */
    public function __construct(IGraphClient $client, $userId, $eventId)
    {
        $this->client = $client;
        $this->userId = $userId;
        $this->eventId = $eventId;
        $this->beans = new Factory();
    }

    /**
     * @return Event
     * @throws WriteAccessException
     * @throws EventNotFoundException
     */
    public function get()
    {
        $args = array(
            'userId' => $this->userId,
            'eventId' => $this->eventId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new EventNotFoundException();
        }
        if (!@$data['canUpdate']) {
            throw new WriteAccessException();
        }
        return $this->beans->createEvent($data['event']);
    }

}
