<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Link;
use Graph\Beans\Factory;
use Graph\Exceptions\PlaceNotFoundException;

class UpdatePlaceLinkList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_place:Place {id: {placeId}})
OPTIONAL MATCH
    (_place)-
    [_hasLink:HAS_LINK]->
    (_linkToDelete:Link)
WHERE
    _linkToDelete.hide <> true
DELETE
    _hasLink, _linkToDelete
WITH
    distinct(_place) as _place
UNWIND
    {linkList} as __link
CREATE
    (_place)-
    [:HAS_LINK]->
    (_link:Link {
        href: __link.href,
        site: __link.site,
        hide: false
    })
RETURN
    {
        linkList: collect({
            href: _link.href,
            site: _link.site,
            hide: _link.hide
        })
    } as json
CYPHER;


    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $placeId;

    /**
     * @var Link[]
     */
    private $linkList;

    /**
     * @param IGraphClient $client
     * @param string $placeId
     * @param Link[] $linkList
     */
    public function __construct(IGraphClient $client, $placeId, array $linkList)
    {
        $this->client = $client;
        $this->placeId = $placeId;
        $this->linkList = $linkList;
        $this->beans = new Factory();
    }

    /**
     * @return Link[]
     * @throws PlaceNotFoundException
     */
    public function get()
    {
        $args = array(
            'placeId' => $this->placeId,
            'linkList' => $this->linkList,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new PlaceNotFoundException();
        }
        return $this->beans->listMap([$this->beans, 'createLink'], $data['linkList']);
    }

}
