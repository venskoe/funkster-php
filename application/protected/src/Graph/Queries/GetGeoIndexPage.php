<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Factory;

class GetGeoIndexPage
{
    const STATEMENT = <<<CYPHER
MATCH
    (_city:City)-
    [:IS_IN_COUNTRY]->
    (_country:Country)
WITH
    _city, _country
ORDER BY
    _city.title
WITH
    _country,
    collect({
        id: _city.id,
        title: _city.title,
        latitude: _city.latitude,
        longitude: _city.longitude,
        childList: []
    }) as cityList
ORDER BY
    _country.title
WITH
    collect({
        id: _country.id,
        title: _country.title,
        latitude: _country.latitude,
        longitude: _country.longitude,
        childList: cityList
    }) as countryList
RETURN
    {
        countryList: countryList
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var Factory
     */
    private $beans;

    /**
     * @param IGraphClient $client
     */
    public function __construct(IGraphClient $client)
    {
        $this->client = $client;
        $this->beans = new Factory();
    }

    public function get()
    {
        $data = $this->client->cypherJson(self::STATEMENT);
        return $this->beans->createGeoIndexPage($data);
    }

}
