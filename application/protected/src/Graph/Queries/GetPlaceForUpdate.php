<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Place;
use Graph\Beans\Factory;
use Graph\Exceptions\PlaceNotFoundException;
use Graph\Exceptions\WriteAccessException;

class GetPlaceForUpdate
{
    const STATEMENT = <<<CYPHER
MATCH
    (_place:Place {id: {placeId}})
WITH
    _place,
    {
        id: _place.id,
        title: _place.title,
        intro: _place.intro,
        address: _place.address,
        latitude: _place.latitude,
        longitude: _place.longitude,
        linkList: [],
        cover: null
    } as place
OPTIONAL MATCH
    (_user:User {id: {userId}})
WITH
    _place, place, _user
OPTIONAL MATCH
    (_place)-
    [_managedBy:MANAGED_BY]->
    (_user)
WITH
    place,
    sum(COALESCE(_user.isAdmin, 0)) + count(_managedBy) as canUpdate
RETURN
    {
        place: place,
        canUpdate: canUpdate
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $placeId;

    /**
     * @var Factory
     */
    private $beans;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $placeId
     */
    public function __construct(IGraphClient $client, $userId, $placeId)
    {
        $this->client = $client;
        $this->userId = $userId;
        $this->placeId = $placeId;
        $this->beans = new Factory();
    }

    /**
     * @return Place
     * @throws WriteAccessException
     * @throws PlaceNotFoundException
     */
    public function get()
    {
        $args = array(
            'userId' => $this->userId,
            'placeId' => $this->placeId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new PlaceNotFoundException();
        }
        if (!@$data['canUpdate']) {
            throw new WriteAccessException();
        }
        return $this->beans->createPlace($data['place']);
    }

}
