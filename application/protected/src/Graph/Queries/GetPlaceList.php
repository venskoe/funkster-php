<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Place;
use Graph\Beans\Factory;

class GetPlaceList
{
    const STATEMENT = <<<CYPHER
MATCH
    (_place:Place)
OPTIONAL MATCH
    (_place)-
    [:HAS_LINK]->
    (_link:Link)
WITH
    _place,
    collect({
        site: _link.site,
        href: _link.href,
        hide: _link.hide
    }) as linkList
OPTIONAL MATCH
    (_place)-
    [:HAS_COVER]->
    (_cover:Photo)
WITH
    collect({
        id: _place.id,
        title: _place.title,
        tzOffset: _place.tzOffset,
        linkList: linkList,
        cover: {
            small: _cover.small,
            large: _cover.large
        }
    }) as placeList
RETURN
    {
        placeList: placeList
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @param IGraphClient $client
     */
    public function __construct(IGraphClient $client)
    {
        $this->client = $client;
        $this->beans = new Factory();
    }

    /**
     * @return Place[]
     */
    public function get()
    {
        $data = $this->client->cypherJson(self::STATEMENT);
        return $this->beans->listMap([$this->beans, 'createPlace'], @$data['placeList']);
    }

}
