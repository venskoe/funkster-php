<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Location;
use Graph\Beans\Factory;
use Graph\Exceptions\LocationNotFoundException;

class GetLocation
{
    const STATEMENT = <<<CYPHER
MATCH
    (_location:City {id: {locationId}})
RETURN
    {
        location: {
            id: _location.id,
            title: _location.title,
            latitude: _location.latitude,
            longitude: _location.longitude,
            childList: []
        }
    } as json
CYPHER;

    /**
     * @var Factory
     */
    private $beans;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $locationId;

    /**
     * @param IGraphClient $client
     * @param string $locationId
     */
    public function __construct(IGraphClient $client, $locationId)
    {
        $this->beans = new Factory();
        $this->client = $client;
        $this->locationId = $locationId;
    }

    /**
     * @return Location
     * @throws LocationNotFoundException
     */
    public function get()
    {
        $args = array(
            'locationId' => $this->locationId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new LocationNotFoundException();
        }
        return $this->beans->createLocation($data['location']);
    }

}
