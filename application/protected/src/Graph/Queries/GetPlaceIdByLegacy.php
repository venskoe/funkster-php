<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Exceptions\PlaceNotFoundException;

class GetPlaceIdByLegacy
{
    const STATEMENT = <<<CYPHER
MATCH
    (:Legacy {id: {placeId}})<-
    [:WAS]-
    (_place:Place)
RETURN
    {
        placeId: _place.id
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $placeId;

    /**
     * @param IGraphClient $client
     * @param string $placeId
     */
    public function __construct(IGraphClient $client, $placeId)
    {
        $this->client = $client;
        $this->placeId = $placeId;
    }

    /**
     * @return string
     * @throws PlaceNotFoundException
     */
    public function get()
    {
        $args = array(
            'placeId' => $this->placeId,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
        }
        catch (ItemNotFoundException $e) {
            throw new PlaceNotFoundException();
        }
        return $data['placeId'];
    }

}
