<?php

namespace Graph\Queries;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

use Graph\Beans\Entities\Place;
use Graph\Beans\Factory;
use Graph\Exceptions\InvalidPlaceDataException;
use Graph\Exceptions\WriteAccessException;

class CreatePlace
{
    const STATEMENT = <<<CYPHER
MATCH
    (_user:User {id: {userId}})
CREATE
    (_place:Place {id: {placeId}})-
    [:MANAGED_BY {timestamp: {now}}]->
    (_user)
SET
    _place.title = {title},
    _place.intro = {intro},
    _place.address = {address},
    _place.latitude = {latitude},
    _place.longitude = {longitude}
RETURN
    {
        nodeId: id(_place),
        place: {
            id: _place.id,
            title: _place.title,
            intro: _place.intro,
            address: _place.address,
            tzOffset: _place.tzOffset,
            latitude: _place.latitude,
            longitude: _place.longitude,
            linkList: [],
            cover: null
        }
    } as json
CYPHER;

    /**
     * @var IGraphClient
     */
    private $client;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $placeId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $intro;

    /**
     * @var string
     */
    private $address;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @param IGraphClient $client
     * @param string $userId
     * @param string $placeId
     * @param string $title
     * @param string $intro
     * @param string $address
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(IGraphClient $client, $userId, $placeId, $title, $intro, $address, $latitude, $longitude)
    {
        $this->client = $client;
        $this->userId = $userId;
        $this->placeId = $placeId;
        $this->title = $title;
        $this->intro = $intro;
        $this->address = $address;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->beans = new Factory();
    }

    /**
     * @return Place
     * @throws InvalidPlaceDataException
     * @throws WriteAccessException
     */
    public function get()
    {
        $args = array(
            'now' => time(),
            'userId' => $this->userId,
            'placeId' => $this->placeId,
            'title' => $this->title,
            'intro' => $this->intro,
            'address' => $this->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        );
        try {
            $data = $this->client->cypherJson(self::STATEMENT, $args);
            $this->client->upsertSpatialPoint('Place', (int)$data['nodeId']);
        }
        catch (ItemNotFoundException $e) {
            throw new WriteAccessException();
        }
        catch (ItemAlreadyExistsException $e) {
            throw new InvalidPlaceDataException();
        }
        return $this->beans->createPlace($data['place']);
    }

}
