<?php

namespace Graph;

use Menara\Generic\Operator\AttrGetter;
use Menara\Generic\Tools;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Profile;
use Graph\Beans\Entities\Tag;
use Graph\Interfaces\IDataSource;
use Graph\Interfaces\IService;

class Crawler
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var IService
     */
    private $service;

    /**
     * @var IDataSource[]
     */
    private $sourceList;

    /**
     * @param string $userId
     * @param IService $service
     */
    public function __construct($userId, IService $service)
    {
        $this->service = $service;
        $this->userId = $userId;
        $this->sourceList = [];
    }

    /**
     * @param IDataSource $source
     * @return $this
     */
    public function addSource(IDataSource $source)
    {
        $this->sourceList[] = $source;
        return $this;
    }

    /**
     * @param int|null $since
     */
    public function update($since = null)
    {
        if ($since === null) {
            $since = time();
        }
        array_walk($this->sourceList, function(IDataSource $source) use ($since) {
            $this->updateFromSource($source, $since);
        });
    }

    /**
     * @param IDataSource $source
     * @param int $since
     */
    private function updateFromSource(IDataSource $source, $since)
    {
        $artistList = $this->service->getArtistList();
        $artistDict = $this->indexArtistList($source, $artistList);
        $artistListMapper = $this->makeArtistListMapper($source, $artistDict);

        $eventList = $this->service->getEventListStartsAfter($since);
        $eventDict = $this->indexEventList($eventList);

        $placeList = $this->service->getPlaceList();
        foreach ($placeList as $place) {
            $eventList = $source->getEventList($place, $since);
            foreach ($eventList as $event) {
                $eventHash = $this->makeEventHash($event);
                if (isset($eventDict[$eventHash])) {
                    continue;
                }
                $event = $source->getEvent($event, $artistList);
                $event->artistList = Tools::filter(array_map($artistListMapper, $event->artistList));
                if (count($event->artistList) === 0) {
                    continue;
                }
                $event->tagList = $this->extractTagList($event->artistList);
                $event = $this->createEvent($event);
                $eventDict[$eventHash] = true;
            }
        }
    }

    /**
     * @param IDataSource $source
     * @param Profile[] $artistList
     * @return array
     */
    private function indexArtistList(IDataSource $source, array $artistList)
    {
        $ret = Tools::transformListToDict($artistList, [$source, 'makeArtistHash']);
        unset($ret[null]);
        return $ret;
    }

    /**
     * @param Event[] $eventList
     * @return array
     */
    private function indexEventList(array $eventList)
    {
        return Tools::transformListToDict($eventList, [$this, 'makeEventHash'], Tools::bool());
    }

    /**
     * @param Event $event
     * @return string
     */
    public function makeEventHash(Event $event)
    {
        return strtr('{placeId}--{date}', array(
            '{placeId}' => $event->place->id,
            '{date}' => gmdate('Y-m-d', $event->startsAt + $event->tzOffset * 60),
        ));
    }

    /**
     * @param IDataSource $source
     * @param array $artistDict
     * @return callable
     */
    private function makeArtistListMapper(IDataSource $source, array $artistDict)
    {
        return function(Profile $artist) use ($source, $artistDict) {
            $artistHash = $source->makeArtistHash($artist);
            if (null === $artistHash) {
                throw new \RuntimeException('Just imported artist hash is NULL as not unexpected');
            }
            if (isset($artistDict[$artistHash])) {
                $linkList = $artist->linkList;
                $artist = $artistDict[$artistHash];
                $artist->linkList = Tools::unique(array_merge($artist->linkList, $linkList), AttrGetter::create('site'));
                return $artist;
            }
            return null;
        };
    }

    /**
     * @param Profile[] $artistList
     * @return Tag[]
     */
    private function extractTagList(array $artistList)
    {
        $tagList = array_map(AttrGetter::create('tagList'), $artistList);
        $tagList = call_user_func_array('array_merge', $tagList);
        return Tools::unique($tagList, AttrGetter::create('id'));
    }

    /**
     * @param Event $event
     * @return Event
     */
    private function createEvent(Event $event)
    {
        $event->id = $this->service->createEvent(
            $this->userId,
            $event->title,
            $event->tzOffset,
            $event->startsAt,
            $event->endsAt
        )->id;
        $event->place = $this->service->updateEventPlace(
            $this->userId,
            $event->id,
            $event->place->id
        );
        $event->cover = $this->service->updateEventCover(
            $this->userId,
            $event->id,
            $event->cover->small,
            $event->cover->large
        );
        $event->tagList = $this->service->updateEventTagList(
            $this->userId,
            $event->id,
            array_map(AttrGetter::create('title'), $event->tagList)
        );
        $event->artistList = $this->service->updateEventArtistList(
            $this->userId,
            $event->id,
            array_map(AttrGetter::create('id'), $event->artistList)
        );
        return $event;
    }

}
