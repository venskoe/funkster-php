<?php

namespace Graph\DataSources;

use Menara\Generic\Dom;
use Menara\Generic\Tools;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Profile;
use Graph\Interfaces\IDataSource;

class ResidentAdvisor implements IDataSource
{
    const SITE = 'residentadvisor';
    const HOST = 'http://www.residentadvisor.net';
    const FREQUENCY = 86400; // 1 day
    const TBA = 'TBA';
    const FETCH_ATTEMPT_COUNT = 5;
    const FETCH_VERBOSE = true;

    /**
     * @var Helper
     */
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper(self::SITE, self::HOST);
    }

    /**
     * @param Profile $artist
     * @return string
     */
    public function makeArtistHash(Profile $artist)
    {
        try {
            return $this->helper->findHref($artist->linkList);
        }
        catch (\UnexpectedValueException $e) {
            return null;
        }
    }

    /**
     * @param Place $place
     * @param int $since
     * @return Event[]
     */
    public function getEventList(Place $place, $since)
    {
        try {
            $href = $this->helper->findHref($place->linkList);
            return $this->getEventListByHref($href, $place, $since);
        }
        catch (\UnexpectedValueException $e) {
            return [];
        }
    }

    /**
     * @param string $href
     * @param Place $place
     * @param int $since
     * @return Event[]
     * @throws \RuntimeException
     */
    private function getEventListByHref($href, Place $place, $since)
    {
        $root = $this->fetch($href);
        $nodeList = Dom::selectList($root, 'ul#items li');
        return Tools::filter(array_map($this->makeGetEventByNode($place, $since), $nodeList));
    }

    /**
     * @param Place $place
     * @param int $since
     * @return callable
     */
    private function makeGetEventByNode(Place $place, $since)
    {
        return function(\DOMNode $root) use ($place, $since) {
            $link = Dom::selectItem($root, 'h1.title > a');
            $href = Dom::makeAbsoluteHref(self::HOST, $link);
            if (null === $href) {
                return null;
            }
            $event = new Event();
            $event->title = trim($link->textContent);
            if ($event->title === self::TBA) {
                return null;
            }
            $event->startsAt = $this->parseDate(Dom::selectItem($root, 'time')->textContent, $place->tzOffset);
            if ($event->startsAt < $since) {
                return null;
            }
            $event->place = $place;
            $event->cover = $place->cover;
            $event->tzOffset = $place->tzOffset;
            $event->endsAt = $event->startsAt;
            $event->linkList = [$this->helper->makeLink($href)];
            return $event;
        };
    }

    /**
     * @param string $string
     * @param int $tzOffset
     * @return int
     */
    private function parseDate($string, $tzOffset)
    {
        $datetime = \DateTime::createFromFormat('Y-m-d\TH:i', $string, new \DateTimeZone('GMT'));
        return $datetime->getTimestamp() - $tzOffset * 60;
    }

    /**
     * @param Event $event
     * @param Profile[] $artistList
     * @return Event
     * @throws \RuntimeException
     */
    public function getEvent(Event $event, array $artistList)
    {
        $href = $this->helper->findHref($event->linkList);
        $root = $this->fetch($href);
        $root = Dom::selectItem($root, '#main');
        list($startsAt, $endsAt) = $this->parseTimeRange(Dom::selectItem($root, 'li li')->textContent);
        $event->startsAt += $startsAt;
        $event->endsAt += $endsAt;
        $event->artistList = Tools::filter(array_map([$this, 'getArtistByNode'], Dom::selectList($root, '.lineup a')));
        return $event;
    }

    /**
     * @param $string
     * @return array
     */
    private function parseTimeRange($string)
    {
        $matches = [];
        $matched = preg_match('/(\d{2}):(\d{2})\s*-\s*(\d{2}):(\d{2})$/', trim($string), $matches);
        if (!$matched) {
            return [22 * 3600, 30 * 3600];
        }
        $startsAt = $matches[1] * 3600 + $matches[2] * 60;
        $endsAt = $matches[3] * 3600 + $matches[4] * 60;
        if ($endsAt < $startsAt) {
            $endsAt += 24 * 3600;
        }
        return [$startsAt, $endsAt];
    }

    /**
     * @param \DOMNode $node
     * @return Profile
     */
    private function getArtistByNode(\DOMNode $node)
    {
        $href = Dom::makeAbsoluteHref(self::HOST, $node);
        if (null === $href) {
            return null;
        }
        $artist = new Profile();
        $artist->title = trim($node->textContent);
        $artist->linkList = [$this->helper->makeLink($href)];
        return $artist;
    }

    /**
     * @param string $href
     * @return \DOMDocument
     * @throws \RuntimeException
     */
    private function fetch($href)
    {
        return Dom::fetch($href, self::FETCH_ATTEMPT_COUNT, self::FETCH_VERBOSE);
    }

}
