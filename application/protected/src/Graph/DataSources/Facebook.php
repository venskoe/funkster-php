<?php

namespace Graph\DataSources;

use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookSession;
use Facebook\GraphObject;

use Menara\Generic\Retry;
use Menara\Generic\Tools;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Profile;
use Graph\Interfaces\IDataSource;

class Facebook implements IDataSource
{
    const SITE = 'facebook';
    const HOST = 'https://www.facebook.com';
    const DEFAULT_DURATION = 21600;
    const ATTEMPT_COUNT = 5;

    /**
     * @var FacebookSession
     */
    private $session;

    /**
     * @var Helper
     */
    private $helper;

    /**
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct($clientId, $clientSecret)
    {
        FacebookSession::setDefaultApplication($clientId, $clientSecret);
        $this->session = FacebookSession::newAppSession($clientId, $clientSecret);
        $this->helper = new Helper(self::SITE, self::HOST);
        $this->retry = new Retry(self::ATTEMPT_COUNT, '\Facebook\FacebookSDKException');
    }


    /**
     * @param Profile $artist
     * @return string
     */
    public function makeArtistHash(Profile $artist)
    {
        return $artist->id;
    }

    /**
     * @param Place $place
     * @param int $since
     * @return Event[]
     */
    public function getEventList(Place $place, $since)
    {
        try {
            $href = $this->helper->findHref($place->linkList);
            return $this->getEventListByHref($href, $place, $since);
        }
        catch (\UnexpectedValueException $e) {
            return [];
        }
    }

    /**
     * @param string $href
     * @param Place $place
     * @param int $since
     * @return Event[]
     */
    private function getEventListByHref($href, Place $place, $since)
    {
        $objectId = trim(str_replace(self::HOST, '', $href), '/');
        $response = $this->executeFacebookRequest(strtr('/0/events', [$objectId]));
        $objectList = $response->getGraphObjectList();
        return Tools::filter(array_map($this->makeGetEventByObject($place, $since), $objectList));
    }

    /**
     * @param Place $place
     * @param int $since
     * @return callable
     */
    private function makeGetEventByObject(Place $place, $since)
    {
        return function(GraphObject $object) use ($place, $since) {
            $href = strtr('0/events/1', [self::HOST, $object->getProperty('id')]);
            $event = new Event();
            $event->title = $object->getProperty('name');
            list($event->startsAt, $place->tzOffset) = $this->parseTimestampAndZone($object->getProperty('start_time'));
            if ($event->startsAt < $since) {
                return null;
            }
            $event->place = $place;
            $event->cover = $place->cover;
            $event->tzOffset = $place->tzOffset;
            $event->endsAt = $event->startsAt + self::DEFAULT_DURATION;
            $event->linkList = [$this->helper->makeLink($href, false)];
            return $event;
        };
    }

    /**
     * @param string $datetimeString
     * @return array
     */
    private function parseTimestampAndZone($datetimeString)
    {
        $p = date_parse($datetimeString);
        return [
            gmmktime($p['hour'], $p['minute'], 0, $p['month'], $p['day'], $p['year']) + $p['zone'] * 60,
            $p['zone']
        ];
    }

    /**
     * @param Event $event
     * @param Profile[] $artistList
     * @return Event
     */
    public function getEvent(Event $event, array $artistList)
    {
        $href = $this->helper->findHref($event->linkList);
        $objectId = trim(str_replace('/events', '', str_replace(self::HOST, '', $href)), '/');
        $response = $this->executeFacebookRequest(strtr('/0', [$objectId]));
        /**
         * @var GraphObject $object
         */
        $object = $response->getGraphObject();
        if ($object->getProperty('is_date_only')) {
            $event->startsAt += 22 * 3600;
            $event->endsAt += 22 * 3600;
        }
        $filterFunc = $this->makeIsArtistInDescriptionFilter($object->getProperty('description'));
        $event->artistList = Tools::filter($artistList, $filterFunc);
        return $event;
    }

    /**
     * @param string $description
     * @return callable
     */
    private function makeIsArtistInDescriptionFilter($description)
    {
        return function(Profile $artist) use ($description) {
            if (false === stripos($description, $artist->title)) {
                return false;
            }
            return true;
        };
    }

    /**
     * @param string $path
     * @return FacebookResponse
     * @throws FacebookSDKException
     */
    private function executeFacebookRequest($path)
    {
        return $this->retry->run(function() use ($path) {
            print(strtr('Fetching [X] 0:1 ...', [self::SITE, $path]));
            try {
                $response = (new FacebookRequest($this->session, 'GET', $path))->execute();
                print("OK\n");
                return $response;
            }
            catch (\Exception $e) {
                print("Fail\n");
                throw $e;
            }
        });
    }

}
