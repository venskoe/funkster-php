<?php

namespace Graph\DataSources;

use Graph\Beans\Entities\Link;

class Helper
{
    /**
     * @var string
     */
    private $site;

    /**
     * @var string
     */
    private $host;

    /**
     * @param string $site
     * @param string $host
     */
    public function __construct($site, $host)
    {
        $this->site = $site;
        $this->host = $host;
    }

    /**
     * @param string $href
     * @param bool $hide
     * @return Link
     */
    public function makeLink($href, $hide = true)
    {
        $link = new Link();
        $link->site = $this->site;
        $link->href = $href;
        $link->hide = $hide;
        return $link;
    }

    /**
     * @param Link[] $linkList
     * @return string
     * @throws \UnexpectedValueException
     */
    public function findHref(array $linkList)
    {
        return $this->findLink($linkList)->href;
    }

    /**
     * @param Link[] $linkList
     * @return Link
     * @throws \UnexpectedValueException
     */
    public function findLink(array $linkList)
    {
        foreach ($linkList as $link) {
            if ($this->site === $link->site) {
                return $link;
            }
        }
        throw new \UnexpectedValueException();
    }

}
