<?php

namespace Graph\Beans;

use Graph\Beans\Entities\Access;
use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Link;
use Graph\Beans\Entities\Location;
use Graph\Beans\Entities\Message;
use Graph\Beans\Entities\News;
use Graph\Beans\Entities\Photo;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Post;
use Graph\Beans\Entities\Profile;
use Graph\Beans\Entities\Tag;
use Graph\Beans\Entities\User;

class Factory
{
    /**
     * @param \ArrayAccess $data
     * @return SitemapPage
     */
    public function createSitemapPage($data)
    {
        $bean = new SitemapPage();
        $bean->eventList = $this->listMap([$this, 'createEvent'], @$data['eventList']);
        $bean->placeList = $this->listMap([$this, 'createPlace'], @$data['placeList']);
        $bean->artistList = $this->listMap([$this, 'createProfile'], @$data['artistList']);
        $bean->locationList = $this->listMap([$this, 'createLocation'], @$data['locationList']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return GeoIndexPage
     */
    public function createGeoIndexPage($data)
    {
        $bean = new GeoIndexPage();
        $bean->countryList = $this->listMap([$this, 'createLocation'], @$data['countryList']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return UserPage
     */
    public function createUserPage(\ArrayAccess $data)
    {
        $bean = new UserPage();
        $bean->id = $this->attrMap($data, 'userId');
        $bean->profile = $this->itemMap([$this, 'createProfile'], @$data['profile']);
        $bean->eventList = $this->listMap([$this, 'createEvent'], @$data['eventList']);
        $bean->eventCount = $data['eventCount'];
        $bean->placeList = $this->listMap([$this, 'createPlace'], @$data['placeList']);
        $bean->placeCount = $data['placeCount'];
        $bean->artistList = $this->listMap([$this, 'createProfile'], @$data['artistList']);
        $bean->artistCount = $data['artistCount'];
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return EventListPage
     */
    public function createEventListPage(\ArrayAccess $data)
    {
        $bean = new EventListPage();
        $bean->location = $this->itemMap([$this, 'createLocation'], @$data['location'], new Location());
        $bean->eventList = $this->listMap([$this, 'createEvent'], @$data['eventList']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return EventPage
     */
    public function createEventPage(\ArrayAccess $data)
    {
        $bean = new EventPage();
        $bean->access = $this->itemMap([$this, 'createAccess'], @$data['access']);
        $bean->event = $this->createEvent($data['event']);
        $bean->promoter = $this->createProfile($data['promoter']);
        $bean->newsList = $this->listMap([$this, 'createNews'], $data['newsList']);
        $bean->postList = $this->listMap([$this, 'createPost'], $data['postList']);
        $bean->relatedEventList = $this->listMap([$this, 'createEvent'], $data['relatedEventList']);
        $bean->commentList = $this->listMap([$this, 'createMessage'], $data['commentList']);
        $bean->commentCount = $data['commentCount'];
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return PlacePage
     */
    public function createPlacePage(\ArrayAccess $data)
    {
        $bean = new PlacePage();
        $bean->access = $this->itemMap([$this, 'createAccess'], @$data['access']);
        $bean->place = $this->createPlace($data['place']);
        $bean->postList = $this->listMap([$this, 'createPost'], $data['postList']);
        $bean->pastEventList = $this->listMap([$this, 'createEvent'], @$data['pastEventList']);
        $bean->pastEventCount = @$data['pastEventCount'];
        $bean->upcomingEventList = $this->listMap([$this, 'createEvent'], @$data['upcomingEventList']);
        $bean->upcomingEventCount = @$data['upcomingEventCount'];
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return ArtistPage
     */
    public function createArtistPage($data)
    {
        $bean = new ArtistPage();
        $bean->access = $this->itemMap([$this, 'createAccess'], @$data['access']);
        $bean->artist = $this->createProfile($data['artist']);
        $bean->postList = $this->listMap([$this, 'createPost'], $data['postList']);
        $bean->pastEventList = $this->listMap([$this, 'createEvent'], $data['pastEventList']);
        $bean->pastEventCount = @$data['pastEventCount'];
        $bean->upcomingEventList = $this->listMap([$this, 'createEvent'], $data['upcomingEventList']);
        $bean->upcomingEventCount = @$data['upcomingEventCount'];
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Location
     */
    public function createLocation($data)
    {
        $bean = new Location();
        $bean->id = $this->attrMap($data, 'id');
        $bean->title = $this->attrMap($data, 'title');
        $bean->latitude = $this->attrMap($data, 'latitude');
        $bean->longitude = $this->attrMap($data, 'longitude');
        $bean->modifiedAt = $this->attrMap($data, 'modifiedAt');
        $bean->childList = $this->listMap([$this, 'createLocation'], @$data['childList']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return User
     */
    public function createUser($data)
    {
        $bean = new User();
        $bean->id = $this->attrMap($data, 'id');
        $bean->isAdmin = boolval(@$data['isAdmin']);
        $bean->profile = $this->itemMap([$this, 'createProfile'], @$data['profile']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Access
     */
    public function createAccess($data)
    {
        $bean = new Access();
        $bean->canCreate = boolval(@$data['canCreate']);
        $bean->canUpdate = boolval(@$data['canUpdate']);
        $bean->canDelete = boolval(@$data['canDelete']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Event
     */
    public function createEvent($data)
    {
        $bean = new Event();
        $bean->id = $this->attrMap($data, 'id');
        $bean->title = $this->attrMap($data, 'title');
        $bean->tzOffset = $this->attrMap($data, 'tzOffset');
        $bean->startsAt = $this->attrMap($data, 'startsAt');
        $bean->endsAt = $this->attrMap($data, 'endsAt');
        $bean->modifiedAt = $this->attrMap($data, 'modifiedAt');
        $bean->cover = $this->itemMap([$this, 'createPhoto'], @$data['cover']);
        $bean->place = $this->itemMap([$this, 'createPlace'], @$data['place']);
        $bean->tagList = $this->listMap([$this, 'createTag'], @$data['tagList']);
        $bean->linkList = $this->listMap([$this, 'createLink'], @$data['linkList']);
        $bean->artistList = $this->listMap([$this, 'createProfile'], $data['artistList']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Place
     */
    public function createPlace($data)
    {
        $bean = new Place();
        $bean->id = $this->attrMap($data, 'id');
        $bean->title = $this->attrMap($data, 'title');
        $bean->intro = $this->attrMap($data, 'intro');
        $bean->address = $this->attrMap($data, 'address');
        $bean->tzOffset = $this->attrMap($data, 'tzOffset');
        $bean->latitude = $this->attrMap($data, 'latitude');
        $bean->longitude = $this->attrMap($data, 'longitude');
        $bean->instagram = $this->attrMap($data, 'instagram');
        $bean->modifiedAt = $this->attrMap($data, 'modifiedAt');
        $bean->linkList = $this->listMap([$this, 'createLink'], @$data['linkList']);
        $bean->cover = $this->itemMap([$this, 'createPhoto'], @$data['cover']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Message
     */
    public function createMessage($data)
    {
        $bean = new Message();
        $bean->content = $this->attrMap($data, 'content');
        $bean->createdAt = $this->attrMap($data, 'createdAt');
        $bean->author = $this->createProfile(@$data['author']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return News
     */
    public function createNews($data)
    {
        $bean = new News();
        $bean->title = $this->attrMap($data, 'title');
        $bean->content = $this->attrMap($data, 'content');
        $bean->createdAt = $this->attrMap($data, 'createdAt');
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Post
     */
    public function createPost($data)
    {
        $bean = new Post();
        $bean->id = $this->attrMap($data, 'id');
        $bean->type = @$data['type'];
        $bean->href = @$data['href'];
        $bean->title = @$data['title'];
        $bean->cover = @$data['cover'];
        $bean->source = @$data['source'];
        $bean->postedAt = @$data['postedAt'];
        $bean->author = $this->itemMap([$this, 'createProfile'], @$data['author']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Tag
     */
    public function createTag($data)
    {
        $bean = new Tag();
        $bean->id = $this->attrMap($data, 'id');
        $bean->title = $this->attrMap($data, 'title');
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Profile
     */
    public function createProfile($data)
    {
        $bean = new Profile();
        $bean->id = $this->attrMap($data, 'id');
        $bean->title = $this->attrMap($data, 'title');
        $bean->intro = $this->attrMap($data, 'intro');
        $bean->about = $this->attrMap($data, 'about');
        $bean->hometown = $this->attrMap($data, 'hometown');
        $bean->modifiedAt = $this->attrMap($data, 'modifiedAt');
        $bean->cover = $this->createPhoto(@$data['cover']);
        $bean->tagList = $this->listMap([$this, 'createTag'], @$data['tagList']);
        $bean->linkList = $this->listMap([$this, 'createLink'], @$data['linkList']);
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Photo
     */
    public function createPhoto($data)
    {
        $bean = new Photo();
        $bean->small = @$data['small'];
        $bean->large = @$data['large'];
        return $bean;
    }

    /**
     * @param \ArrayAccess $data
     * @return Link
     */
    public function createLink($data)
    {
        $bean = new Link();
        $bean->site = $data['site'];
        $bean->href = $data['href'];
        $bean->hide = boolval($this->attrMap($data, 'hide'));
        $bean->used = intval($this->attrMap($data, 'used'));
        return $bean;
    }

    /**
     * @param callable $callable
     * @param \Iterator $list
     * @param array $default
     * @return array
     */
    public function listMap($callable, $list, $default = [])
    {
        if (null === $list) {
            return $default;
        }
        $list = iterator_to_array($list);
        if (count($list) === 1) {
            $values = array_filter(iterator_to_array($list[0]), function($item) {
                return null !== $item && is_scalar($item);
            });
            if (empty($values)) {
                return $default;
            }
        }
        return array_map($callable, $list);
    }

    /**
     * @param callable $callable
     * @param Array $item
     * @param null $default
     * @return mixed|null
     */
    private function itemMap($callable, $item, $default = null)
    {
        if (null === $item) {
            return $default;
        }
        return call_user_func_array($callable, [$item]);
    }

    /**
     * @param \ArrayAccess $data
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    private function attrMap($data, $name, $default = null)
    {
        if (null === $data) {
            return $default;
        }
        if (false === $data[$name]) {
            return false;
        }
        if (!isset($data[$name])) {
            return $default;
        }
        return $data[$name];
    }

}
