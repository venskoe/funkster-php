<?php

namespace Graph\Beans;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Location;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Profile;

class SitemapPage
{
    /**
     * @var Event[]
     */
    public $eventList;

    /**
     * @var Place[]
     */
    public $placeList;

    /**
     * @var Profile[]
     */
    public $artistList;

    /**
     * @var Location[]
     */
    public $locationList;

}
