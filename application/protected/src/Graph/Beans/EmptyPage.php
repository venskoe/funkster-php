<?php

namespace Graph\Beans;

class EmptyPage
{
    /**
     * @var UserPage
     */
    public $user;

    /**
     * @var object
     */
    public $consts;

}
