<?php

namespace Graph\Beans;

use Graph\Beans\Entities\Location;

class GeoIndexPage
{
    /**
     * @var Location[]
     */
    public $countryList;

    /**
     * @var UserPage
     */
    public $user;

    /**
     * @var object
     */
    public $consts;

}
