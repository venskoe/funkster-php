<?php

namespace Graph\Beans;

use Graph\Beans\Entities\Access;
use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Post;

class PlacePage
{
    /**
     * @var Place
     */
    public $place;

    /**
     * @var Post[]
     */
    public $postList;

    /**
     * @var Event[]
     */
    public $pastEventList;

    /**
     * @var int
     */
    public $pastEventCount;

    /**
     * @var Event[]
     */
    public $upcomingEventList;

    /**
     * @var int
     */
    public $upcomingEventCount;

    /**
     * @var UserPage
     */
    public $user;

    /**
     * @var Access
     */
    public $access;

    /**
     * @var object
     */
    public $consts;

}
