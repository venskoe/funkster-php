<?php

namespace Graph\Beans;

use Graph\Beans\Entities\Access;
use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Message;
use Graph\Beans\Entities\News;
use Graph\Beans\Entities\Post;
use Graph\Beans\Entities\Profile;
use Graph\Beans\Entities\Tag;

class EventPage
{
    /**
     * @var Event
     */
    public $event;

    /**
     * @var Profile
     */
    public $promoter;

    /**
     * @var News[]
     */
    public $newsList;

    /**
     * @var Post[]
     */
    public $postList;

    /**
     * @var int
     */
    public $commentCount;

    /**
     * @var Message[]
     */
    public $commentList;

    /**
     * @var Event[]
     */
    public $relatedEventList;

    /**
     * @var UserPage
     */
    public $user;

    /**
     * @var Access
     */
    public $access;

    /**
     * @var object
     */
    public $consts;

}
