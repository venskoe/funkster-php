<?php

namespace Graph\Beans;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Location;

class EventListPage
{
    /**
     * @var Location
     */
    public $location;

    /**
     * @var Event[]
     */
    public $eventList;

    /**
     * @var UserPage
     */
    public $user;

    /**
     * @var object
     */
    public $consts;

}
