<?php

namespace Graph\Beans\Entities;

class Link 
{
    /**
     * @var string
     */
    public $site;

    /**
     * @var string
     */
    public $href;

    /**
     * @var boolean
     */
    public $hide;

    /**
     * @var int
     */
    public $used;

}
