<?php

namespace Graph\Beans\Entities;

class Place
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $intro;

    /**
     * @var string
     */
    public $address;

    /**
     * @var int
     */
    public $tzOffset;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    /**
     * @var Link[]
     */
    public $linkList;

    /**
     * @var Photo
     */
    public $cover;

    /**
     * @var int
     */
    public $modifiedAt;

    /**
     * @var string
     */
    public $instagram;

}
