<?php

namespace Graph\Beans\Entities;

class Profile
{
    /**
     * @var String
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var String
     */
    public $intro;

    /**
     * @var String
     */
    public $about;

    /**
     * @var String
     */
    public $hometown;

    /**
     * @var Photo
     */
    public $cover;

    /**
     * @var Tag[]
     */
    public $tagList;

    /**
     * @var Link[]
     */
    public $linkList;

    /**
     * @var int
     */
    public $modifiedAt;

}
