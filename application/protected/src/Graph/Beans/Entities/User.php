<?php

namespace Graph\Beans\Entities;

class User 
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var Profile
     */
    public $profile;

    /**
     * @var boolean
     */
    public $isAdmin;

}
