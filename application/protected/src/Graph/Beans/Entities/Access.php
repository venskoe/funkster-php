<?php

namespace Graph\Beans\Entities;

class Access 
{
    /**
     * @var boolean
     */
    public $canCreate;

    /**
     * @var boolean
     */
    public $canUpdate;

    /**
     * @var boolean
     */
    public $canDelete;

}
