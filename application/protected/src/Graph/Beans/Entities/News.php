<?php

namespace Graph\Beans\Entities;

class News
{
    /**
     * @var String
     */
    public $title;

    /**
     * @var String
     */
    public $content;

    /**
     * @var int
     */
    public $createdAt;

}
