<?php

namespace Graph\Beans\Entities;

class Message 
{
    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $createdAt;

    /**
     * @var Profile
     */
    public $author;

}
