<?php

namespace Graph\Beans\Entities;

class Photo 
{
    /**
     * @var String
     */
    public $large = null;

    /**
     * @var String
     */
    public $small = null;

}
