<?php

namespace Graph\Beans\Entities;

class Tag 
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

}
