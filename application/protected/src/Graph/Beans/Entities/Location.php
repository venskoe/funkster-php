<?php

namespace Graph\Beans\Entities;

class Location 
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    /**
     * @var Location[]
     */
    public $childList;

    /**
     * @var int
     */
    public $modifiedAt;

}
