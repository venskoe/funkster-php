<?php

namespace Graph\Beans\Entities;

class Post
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $href;

    /**
     * @var String
     */
    public $title;

    /**
     * @var String
     */
    public $cover;

    /**
     * @var string
     */
    public $source;

    /**
     * @var int
     */
    public $postedAt;

    /**
     * @var Profile
     */
    public $author;

}
