<?php

namespace Graph\Beans\Entities;

class Event
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var int
     */
    public $tzOffset;

    /**
     * @var int
     */
    public $startsAt;

    /**
     * @var Int
     */
    public $endsAt;

    /**
     * @var Photo
     */
    public $cover;

    /**
     * @var Place
     */
    public $place;

    /**
     * @var Tag[]
     */
    public $tagList;

    /**
     * @var Link[]
     */
    public $linkList;

    /**
     * @var Profile[]
     */
    public $artistList;

    /**
     * @var int
     */
    public $modifiedAt;

}
