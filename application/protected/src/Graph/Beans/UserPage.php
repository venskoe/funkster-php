<?php

namespace Graph\Beans;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Profile;

class UserPage
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var Profile
     */
    public $profile;

    /**
     * @var Event[]
     */
    public $eventList;

    /**
     * @var int
     */
    public $eventCount;

    /**
     * @var Place[]
     */
    public $placeList;

    /**
     * @var int
     */
    public $placeCount;

    /**
     * @var Profile[]
     */
    public $artistList;

    /**
     * @var int
     */
    public $artistCount;

}
