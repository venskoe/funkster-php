<?php

namespace Graph\Interfaces;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Profile;

interface IDataSource
{
    /**
     * @param Profile $artist
     * @return string
     */
    public function makeArtistHash(Profile $artist);

    /**
     * @param Place $place
     * @param int $since
     * @return Event[]
     */
    public function getEventList(Place $place, $since);

    /**
     * @param Event $event
     * @param Profile[] $artistList
     * @return Event
     */
    public function getEvent(Event $event, array $artistList);

}
