<?php

namespace Graph\Interfaces;

use Graph\Beans\ArtistPage;
use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Link;
use Graph\Beans\Entities\News;
use Graph\Beans\Entities\Photo;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Post;
use Graph\Beans\Entities\Profile;
use Graph\Beans\Entities\Tag;
use Graph\Beans\Entities\User;
use Graph\Beans\EventListPage;
use Graph\Beans\EventPage;
use Graph\Beans\GeoIndexPage;
use Graph\Beans\PlacePage;
use Graph\Beans\EmptyPage;
use Graph\Beans\SitemapPage;
use Graph\Beans\UserPage;
use Graph\Exceptions\ArtistAlreadyExistsException;
use Graph\Exceptions\ArtistNotFoundException;
use Graph\Exceptions\EventNotFoundException;
use Graph\Exceptions\InvalidArtistDataException;
use Graph\Exceptions\InvalidDataException;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\InvalidPlaceDataException;
use Graph\Exceptions\LocationNotFoundException;
use Graph\Exceptions\PlaceNotFoundException;
use Graph\Exceptions\UserNotFoundException;
use Graph\Exceptions\WriteAccessException;

interface IService
{
    /**
     * @return EmptyPage
     */
    public function getEmptyPage();

    /**
     * @return SitemapPage
     */
    public function getSitemapPage();

    /**
     * @return GeoIndexPage
     */
    public function getGeoIndexPage();

    /**
     * @param string $eventId
     * @return string
     * @throws EventNotFoundException
     */
    public function getEventIdByLegacy($eventId);

    /**
     * @param string $placeId
     * @return string
     * @throws PlaceNotFoundException
     */
    public function getPlaceIdByLegacy($placeId);

    /**
     * @param string $artistId
     * @return string
     * @throws ArtistNotFoundException
     */
    public function getArtistIdByLegacy($artistId);

    /**
     * @param \Menara\Social\Beans\Profile $profile
     * @return User
     */
    public function getOrCreateSocialUser(\Menara\Social\Beans\Profile $profile);

    /**
     * @param string $userId
     * @param int $eventLimit
     * @param int $placeLimit
     * @param int $artistLimit
     * @return UserPage
     * @throws UserNotFoundException
     */
    public function getUserPage($userId, $eventLimit, $placeLimit, $artistLimit);

    /**
     * @param String $userId
     * @param string $eventId
     * @param int $relatedEventLimit
     * @param int $commentLimit
     * @return EventPage
     */
    public function getEventPage($userId, $eventId, $relatedEventLimit, $commentLimit);

    /**
     * @param String $userId
     * @param string $placeId
     * @param int $pastEventLimit
     * @return PlacePage
     */
    public function getPlacePage($userId, $placeId, $pastEventLimit);

    /**
     * @param String $userId
     * @param string $artistId
     * @param int $pastEventLimit
     * @return ArtistPage
     */
    public function getArtistPage($userId, $artistId, $pastEventLimit);

    /**
     * @param string $userId
     * @param string $title
     * @param int $tzOffset
     * @param int $startsAt
     * @param int $endsAt
     * @return Event
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function createEvent($userId, $title, $tzOffset, $startsAt, $endsAt);

    /**
     * @param string $userId
     * @param string $eventId
     * @param string $title
     * @param int $tzOffset
     * @param int $startsAt
     * @param int $endsAt
     * @return Event
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEvent($userId, $eventId, $title, $tzOffset, $startsAt, $endsAt);

    /**
     * @param string $userId
     * @param string $eventId
     * @param string $largeHref
     * @param string $smallHref
     * @return Photo
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventCover($userId, $eventId, $largeHref, $smallHref);

    /**
     * @param string $userId
     * @param string $eventId
     * @param string $placeId
     * @return Place
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventPlace($userId, $eventId, $placeId);

    /**
     * @param string $userId
     * @param string $eventId
     * @param int $createdAt
     * @return News
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function deleteEventNews($userId, $eventId, $createdAt);

    /**
     * @param string $userId
     * @param string $eventId
     * @param int $createdAt
     * @param string $title
     * @param string $content
     * @return News
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventNews($userId, $eventId, $createdAt, $title, $content);

    /**
     * @param string $userId
     * @param string $eventId
     * @param string[] $titleList
     * @return Tag[]
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updateEventTagList($userId, $eventId, array $titleList);

    /**
     * @param string $userId
     * @param string $eventId
     * @param string[] $artistList
     * @return Profile[]
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventArtistList($userId, $eventId, array $artistList);

    /**
     * @param int $since
     * @return Event[]
     */
    public function getEventListStartsAfter($since);

    /**
     * @param string $ip
     * @param int $distance
     * @param int $eventLimit
     * @return EventListPage
     * @throws LocationNotFoundException
     */
    public function getEventListPageByIp($ip, $distance, $eventLimit);

    /**
     * @param string $locationId
     * @param int $distance
     * @param int $eventLimit
     * @return EventListPage
     * @throws LocationNotFoundException
     */
    public function getEventListPageByLocation($locationId, $distance, $eventLimit);

    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $distance
     * @param int $eventLimit
     * @return EventListPage
     * @throws LocationNotFoundException
     */
    public function getEventListPageByPosition($latitude, $longitude, $distance, $eventLimit);

    /**
     * @param string $userId
     * @param string $title
     * @param string $intro
     * @param string $address
     * @param float $latitude
     * @param float $longitude
     * @return Place
     * @throws WriteAccessException
     * @throws InvalidPlaceDataException
     */
    public function createPlace($userId, $title, $intro, $address, $latitude, $longitude);

    /**
     * @param string $userId
     * @param string $placeId
     * @param string $title
     * @param string $intro
     * @param string $address
     * @param float $latitude
     * @param float $longitude
     * @return Place
     * @throws PlaceNotFoundException
     * @throws WriteAccessException
     * @throws InvalidPlaceDataException
     */
    public function updatePlace($userId, $placeId, $title, $intro, $address, $latitude, $longitude);

    /**
     * @param string $userId
     * @param string $placeId
     * @param string[] $linkList
     * @return Link[]
     * @throws PlaceNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updatePlaceLinkList($userId, $placeId, array $linkList);

    /**
     * @param string $search
     * @param int $limit
     * @return Place[]
     * @throws InvalidPlaceDataException
     */
    public function searchPlaceList($search, $limit);

    /**
     * @return Place[]
     */
    public function getPlaceList();

    /**
     * @param string $userId
     * @param string $title
     * @param string $intro
     * @param string $about
     * @param string $hometown
     * @return Profile
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     * @throws ArtistAlreadyExistsException
     */
    public function createArtist($userId, $title, $intro, $about, $hometown);

    /**
     * @param string $userId
     * @param string $artistId
     * @param string $title
     * @param string $intro
     * @param string $about
     * @param string $hometown
     * @return Profile
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     */
    public function updateArtist($userId, $artistId, $title, $intro, $about, $hometown);

    /**
     * @param string $userId
     * @param string $artistId
     * @param string $largeHref
     * @param string $smallHref
     * @return Photo
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     */
    public function updateArtistCover($userId, $artistId, $largeHref, $smallHref);

    /**
     * @param string $userId
     * @param string $artistId
     * @param string[] $hrefList
     * @return Link[]
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updateArtistLinkList($userId, $artistId, array $hrefList);

    /**
     * @param string $userId
     * @param string $artistId
     * @param string $guid
     * @param string $source
     * @param string $type
     * @param string $href
     * @param string $cover
     * @param String $title
     * @return Post
     * @throws ArtistNotFoundException
     */
    public function createArtistPost($userId, $artistId, $guid, $source, $type, $href, $cover, $title);

    /**
     * @param string $search
     * @param int $limit
     * @return Profile[]
     * @throws InvalidArtistDataException
     */
    public function searchArtistList($search, $limit);

    /**
     * @return Profile[]
     */
    public function getArtistList();

}
