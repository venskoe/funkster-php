<?php

namespace Files\Interfaces;

use Files\Exceptions\InvalidDataException;
use Files\Exceptions\WriteAccessException;

interface IService
{
    /**
     * @param string $filename
     * @param string $data
     * @return string
     * @throws InvalidDataException
     * @throws WriteAccessException
     */
    public function uploadBase64File($filename, $data);

}
