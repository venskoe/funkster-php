<?php

namespace Files;

use Files\Exceptions\InvalidDataException;
use Files\Exceptions\WriteAccessException;
use Files\Interfaces\IService;

class Service implements IService
{
    /**
     * @var object
     */
    public $config;

    /**
     * @param string $filename
     * @param string $data
     * @return string
     * @throws InvalidDataException
     * @throws WriteAccessException
     */
    public function uploadBase64File($filename, $data)
    {
        $data = base64_decode($data, true);
        if (false === $data) {
            throw new InvalidDataException();
        }
        $filepath = implode('/', [$this->config->roots->public, $filename]);
        $success = @file_put_contents($filepath, $data, LOCK_EX);
        if (false === $success) {
            throw new WriteAccessException();
        }
        return implode('/', [$this->config->roots->http, $filename]);
    }

}
