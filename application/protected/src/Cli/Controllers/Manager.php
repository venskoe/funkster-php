<?php

namespace Cli\Controllers;

use Menara\Generic\Console\Interfaces\ICron;
use Menara\Generic\Console\Interfaces\IWatchdog;
use Menara\Generic\Interfaces\IRequest;

class Manager
{
    /**
     * @var string
     */
    public $command;

    /**
     * @var IWatchdog
     */
    public $watchdogPrototype;

    /**
     * @var ICron
     */
    public $cron;

    /**
     * @var IWatchdog[]
     */
    private $watchdogMap = array();

    /**
     * @var object[]
     */
    private $cronConfigList = [];

    public function addCronCommand($schedule, $namespace, $controller, $action)
    {
        $this->cronConfigList[] = array(
            'schedule' => $schedule,
            'className' => implode('\\', [$namespace, $controller]),
            'methodName' => $action,
        );
    }

    /**
     * @param string $route
     * @param int $processCount
     */
    public function addWatchdog($route, $processCount = 1)
    {
        $command = implode(' ', [$this->command, $route]);
        $watchdog = clone($this->watchdogPrototype);
        $watchdog->initialize($command, $processCount);
        $this->watchdogMap[$route] = $watchdog;
    }

    /**
     * @Route('cron')
     */
    public function executeCron()
    {
        throw new \RuntimeException('Not implemented yet');
        $this->cron->execute($this->cronConfigList);
    }

    /**
     * @Route('service {route:[\w\.]+} watch')
     */
    public function watch(IRequest $request)
    {
        $this->getWatchdog($request)->watch();
    }

    /**
     * @Route('service {route:[\w\.]+} restart')
     */
    public function restart(IRequest $request)
    {
        $this->getWatchdog($request)->restart();
    }

    /**
     * @Route('service {route:[\w\.]+} stop')
     */
    public function stop(IRequest $request)
    {
        $this->getWatchdog($request)->stop();
    }

    /**
     * @Route('service {route:[\w\.]+} start')
     */
    public function start(IRequest $request)
    {
        $this->getWatchdog($request)->start();
    }

    /**
     * @param IRequest $request
     * @return IWatchdog
     */
    private function getWatchdog(IRequest $request)
    {
        $route = $request->getString('route');
        return $this->watchdogMap[$route];
    }

}
