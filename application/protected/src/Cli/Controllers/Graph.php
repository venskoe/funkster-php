<?php

namespace Cli\Controllers;

use Everyman\Neo4j\Node;
use MetzWeb\Instagram\Instagram;

use Menara\Generic\Interfaces\IRequest;
use Menara\Generic\Operator\ItemGetter;
use Menara\Generic\Storage\Interfaces\IGraphClient;
use Menara\Generic\Tools;

use Graph\Crawler;
use Graph\DataSources\Facebook;
use Graph\DataSources\ResidentAdvisor;
use Graph\Service;

/**
 * @RoutePrefix('graph.')
 */
class Graph
{
    private static $serialId = 0;

    /**
     * @var object
     */
    public $release;

    /**
     * @var object
     */
    public $socials;

    /**
     * @var IGraphClient
     */
    public $graphClient;

    /**
     * @Route('schema.init')
     *
     * @param IRequest $request
     */
    public function initSchema(IRequest $request)
    {
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_tag:Tag) ASSERT _tag.id IS UNIQUE');
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_post:Post) ASSERT _post.id IS UNIQUE');
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_user:User) ASSERT _user.id IS UNIQUE');
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_event:Event) ASSERT _event.id IS UNIQUE');
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_place:Place) ASSERT _place.id IS UNIQUE');
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_artist:Artist) ASSERT _artist.id IS UNIQUE');
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_legacy:Legacy) ASSERT _legacy.id IS UNIQUE');
        $this->recreateConstraintOrIndex('CONSTRAINT ON (_credentials:Credentials) ASSERT _credentials.id IS UNIQUE');
        $this->recreateConstraintOrIndex('INDEX ON :Event(startsAt)');
        $this->recreateConstraintOrIndex('INDEX ON :Event(endsAt)');
        $this->recreateConstraintOrIndex('INDEX ON :Message(createdAt)');
        $this->recreateConstraintOrIndex('INDEX ON :News(createdAt)');
        $this->recreateConstraintOrIndex('INDEX ON :Post(createdAt)');
        $this->rebuildSpatialIndex($request);
    }

    private function recreateConstraintOrIndex($statement)
    {
        $this->graphClient->cypher('DROP ' . $statement);
        $this->graphClient->cypher('CREATE ' . $statement);
    }

    /**
     * @Route('schema.spatial.rebuild')
     *
     * @param IRequest $request
     */
    public function rebuildSpatialIndex(IRequest $request)
    {
        $this->graphClient->deleteSpatialIndex('Place');
        $this->graphClient->createSpatialIndex('Place', array(
            'geometry_type' => 'point',
            'lat' => 'latitude',
            'lon' => 'longitude',
        ));
        $nodeList = $this->graphClient->cypherAll('MATCH (_place:Place) RETURN _place');
        $nodeList = array_map(ItemGetter::create('_place'), iterator_to_array($nodeList));
        foreach ($nodeList as $node) {
            $this->graphClient->upsertSpatialPoint('Place', $node->getId());
        }
    }

    /**
     * @Route('crawl --user-id={userId:\w+}')
     *
     * @param IRequest $request
     */
    public function crawl(IRequest $request)
    {
        libxml_use_internal_errors(true);
        $userId = $request->getString('userId');
        $service = new Service();
        $service->client = $this->graphClient;
        $crawler = new Crawler($userId, $service);
        $crawler->addSource(new ResidentAdvisor());
        $crawler->addSource(new Facebook($this->socials->facebook->client_id, $this->socials->facebook->client_secret));
        $crawler->update(time());
    }

    /**
     * @Route('locations.import --filename={filename:\S+}')
     *
     * @param IRequest $request
     */
    public function mergeLocationList(IRequest $request)
    {
        $filename = $request->getString('filename');
        $handle = fopen($filename, 'r');
        while ($data = fgetcsv($handle)) {
            $this->mergeLocation($data);
        }
        fclose($handle);
    }

    const MERGE_LOCATION = <<<CYPHER
MERGE
    (_city:Location:City {title: {cityTitle}})
WITH
    _city
SET
    _city.id = {cityId},
    _city.latitude = {latitude},
    _city.longitude = {longitude}
MERGE
    (_country:Location:Country {id: {countryId}})
SET
    _country.title = {countryTitle}
WITH
    _city, _country
MERGE
    (_city)-
    [:IS_IN_COUNTRY]->
    (_country)
CYPHER;

    /**
     * @param array $data
     */
    private function mergeLocation(array $data)
    {
        list($_, $cityTitle, $countryCode, $countryTitle, $longitude, $latitude) = $data;
        $this->graphClient->cypher(self::MERGE_LOCATION, array(
            'cityId' => $this->createId($cityTitle),
            'cityTitle' => $cityTitle,
            'countryId' => $countryCode,
            'countryTitle' => $countryTitle,
            'latitude' => $latitude,
            'longitude' => $longitude,
        ));
    }

    /**
     * @Route('users.update-is-admin --user-id={userId:\w+} --value={isAdmin:\d+}')
     *
     * @param IRequest $request
     */
    public function updateUserIsAdmin(IRequest $request)
    {
        $userId = $request->getString('userId');
        $isAdmin = $request->getInteger('isAdmin');
        $this->graphClient->cypher('MATCH (user:User {id: {userId}}) SET user.isAdmin = {isAdmin}', array(
            'userId' => $userId,
            'isAdmin' => $isAdmin,
        ));
    }

    /**
     * @Route('events.create --user-id={userId:\w+} --filename={filename:\S+}')
     *
     * @param IRequest $request
     */
    public function mergeEvent(IRequest $request)
    {
        $userId = $request->getString('userId');
        $filename = $request->getString('filename');
        $user = $this->getUserNodeById($userId);
        $json = file_get_contents($filename);
        $data = json_decode($json);
        $place = $this->getPlaceNodeById($this->createId($data->place));
        $artistList = array_map([$this, 'getArtistNodeById'], array_map([$this, 'createId'], $data->artistList));
        list($startsAt, $tzOffset) = $this->parseUtcTimestampAndTimezone($data->startsAt);
        $this->createEvent(
            $user, 
            $place, 
            $data->title,
            $tzOffset,
            $startsAt,
            $data->duration * 60,
            $data->cover->large, 
            $data->cover->small, 
            $data->tagList,
            $artistList
        );
    }

    /**
     * @param string $datetimeString
     * @return array
     */
    private function parseUtcTimestampAndTimezone($datetimeString)
    {
        $parsed = date_parse($datetimeString);
        return [
            gmmktime($parsed['hour'], $parsed['minute'], 0, $parsed['month'], $parsed['day'], $parsed['year']) + $parsed['zone'] * 60,
            $parsed['zone']
        ];
    }

    const GET_USER_NODE_BY_ID = <<<CYPHER
MATCH
    (_user:User {id: {id}})
RETURN
    _user
CYPHER;

    /**
     * @param string $id
     * @return \ArrayAccess
     */
    private function getUserNodeById($id)
    {
        return $this->graphClient->cypherOne(self::GET_USER_NODE_BY_ID, array('id' => $id))['_user'];
    }

    const GET_ARTIST_NODE_BY_ID = <<<CYPHER
MATCH
    (_artist:Artist {id: {id}})
RETURN
    _artist
CYPHER;

    /**
     * @param string $id
     * @return \ArrayAccess
     */
    private function getArtistNodeById($id)
    {
        return $this->graphClient->cypherOne(self::GET_ARTIST_NODE_BY_ID, array('id' => $id))['_artist'];
    }

    const GET_PLACE_NODE_BY_ID = <<<CYPHER
MATCH
    (_place:Place {id: {id}})
RETURN
    _place
CYPHER;

    /**
     * @param string $id
     * @return \ArrayAccess
     */
    private function getPlaceNodeById($id)
    {
        return $this->graphClient->cypherOne(self::GET_PLACE_NODE_BY_ID, array('id' => $id))['_place'];
    }

    const MIGRATE_POST_TYPE = <<<CYPHER
MATCH
    (_post:Post)
SET
    _post.type = labels(_post)[-1]
WITH
    _post
MATCH
    (_post1:Post)
WHERE
    NOT(HAS(_post1.id)) OR _post1.id = null
SET
    _post1.id = _post.href
CYPHER;

    /**
     * @Route('post_type.migrate')
     *
     * @param IRequest $request
     */
    public function migratePostType(IRequest $request)
    {
        try {
            $this->graphClient->cypher('DROP CONSTRAINT ON (_post:Post) ASSERT _post.id IS UNIQUE');
        }
        catch (\Exception $e) {
            // pass
        }
        $this->graphClient->cypher(self::MIGRATE_POST_TYPE);
        $this->graphClient->cypher('CREATE CONSTRAINT ON (_post:Post) ASSERT _post.id IS UNIQUE');
    }

    /**
     * @Route('event_id.migrate')
     *
     * @param IRequest $request
     */
    public function migrateEventId(IRequest $request)
    {
        $rowList = $this->graphClient->cypherAll('MATCH (event:Event) RETURN event');
        foreach ($rowList as $row) {
            $this->renameEvent($row['event']);
        }
    }

    const RENAME_EVENT = <<<CYPHER
MATCH
    (event:Event {id: {oldId}})
SET
    event.id = {newId}
WITH
    event
MERGE
    (event)-
    [:WAS]->
    (legacy:Legacy {
        id: {oldId}
    })
RETURN
    event
CYPHER;


    private function renameEvent(Node $event)
    {
        $ret = $this->graphClient->cypherOne(self::RENAME_EVENT, array(
            'oldId' => $event->id,
            'newId' => $this->createEventId($event->title, $event->startsAt - $event->tzOffset),
        ));
        return $ret['place'];
    }

    private function createEventId($title, $utcStartsAt)
    {
        return date('Y/m/d/', $utcStartsAt) . Tools::slugify($title);
    }

    /**
     * @Route('places.instagram.config.generate --client-id={clientId:[0-9a-f]+} --distance={distance:\d+} --filename={filename:\S+}')
     *
     * @param IRequest $request
     */
    public function generatePlaceInstagramConfig(IRequest $request)
    {
        $clientId = $request->getString('clientId');
        $distance = $request->getInteger('distance');
        $filename = $request->getString('filename');
        $instagram = new Instagram(['apiKey' => $clientId]);
        $placeList = $this->getPlaceNodeListWithNoInstagram();
        $configList = [];
        foreach ($placeList as $place) {
            $response = $instagram->searchLocation($place->latitude, $place->longitude, $distance);
            if ($response->meta->code != 200) {
                print_r($response);
                continue;
            }
            $config = array(
                'nodeId' => $place->getId(),
                'title' => $place->title,
                'locations' => [],
            );
            foreach ($response->data as $location) {
                $string = '';
                $string .= '[' . $location->id . ']';
                $string .= str_repeat(' ', 16 - strlen($string));
                $string .= $location->name;
                $config['locations'][] = $string;
            }
            $configList[] = $config;
            usleep(300);
        }
        $content = json_encode($configList, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        file_put_contents($filename, $content);
    }

    /**
     * @return array
     */
    private function getPlaceNodeListWithNoInstagram()
    {
        $rowList = $this->graphClient->cypherAll('MATCH (place:Place) WHERE place.instagram IS NULL RETURN place');
        return array_map(ItemGetter::create('place'), iterator_to_array($rowList));
    }

    /**
     * @Route('places.instagram.config.import --filename={filename:\S+}')
     *
     * @param IRequest $request
     */
    public function importPlaceInstagramConfig(IRequest $request)
    {
        $filename = $request->getString('filename');
        $content = file_get_contents($filename);
        $configList = json_decode($content);
        foreach ($configList as $config) {
            if (empty($config->locations)) {
                continue;
            }
            $instagram = implode(':', array_map(function($string) {
                preg_match('/^\[(\d+)\]/', $string, $matches);
                return $matches[1];
            }, $config->locations));
            $this->graphClient->cypher(
                'MATCH (place:Place) WHERE id(place) = {nodeId} SET place.instagram = {instagram}',
                [
                    'nodeId' => $config->nodeId,
                    'instagram' => $instagram,
                ]
            );
        }
    }

    /**
     * @Route('residentadvisor.config.import --filename={filename:\S+}')
     *
     * @param IRequest $request
     */
    public function mergeResidentAdvisorConfig(IRequest $request)
    {
        $filename = $request->getString('filename');
        $content = file_get_contents($filename);
        $config = json_decode($content);
        array_walk($config->places, [$this, 'mergePlaceResidentAdvisorConfig']);
        array_walk($config->artists, [$this, 'mergeArtistResidentAdvisorConfig']);
    }

    const MERGE_PLACE_RESIDENT_ADVISOR_CONFIG = <<<CYPHER
MATCH
    (_place:Place {id: {placeId}})
SET
    _place.tzOffset = {tzOffset}
MERGE
    (_place)-
    [:HAS_LINK]->
    (_link:Link {site: {site}})
ON CREATE SET
    _link.hide = true,
    _link.used = 0
SET
    _link.href = {href}
MERGE
    (_place)-
    [:HAS_COVER]->
    (_cover:Photo)
SET
    _cover.small = {coverSmall},
    _cover.large = {coverLarge}

CYPHER;

    /**
     * @param \StdClass $config
     */
    private function mergePlaceResidentAdvisorConfig(\StdClass $config)
    {
        if (null === $config->ra_link) {
            return;
        }
        $this->graphClient->cypher(self::MERGE_PLACE_RESIDENT_ADVISOR_CONFIG, [
            'placeId' => $config->place_id,
            'tzOffset' => $this->parseTimezone($config->tzOffset),
            'site' => ResidentAdvisor::SITE,
            'href' => $config->ra_link,
            'coverSmall' => $this->removeBoogieCall($config->default_cover_small),
            'coverLarge' => $this->removeBoogieCall($config->default_cover_big),
        ]);
    }

    private function removeBoogieCall($href)
    {
        if (Tools::stringStartsWith($href, 'http://www.boogiecall.com')) {
            $href = substr($href, strlen('http://www.boogiecall.com'));
        }
        return $href;
    }

    /**
     * @param string $string
     * @return int
     */
    private function parseTimezone($string)
    {
        list($h, $m) = array_map('intval', explode(':', $string));
        return $h * 60 + $m;
    }

    const MERGE_ARTIST_RESIDENT_ADVISOR_CONFIG = <<<CYPHER
MATCH
    (_artist:Artist {id: {artistId}})
MERGE
    (_artist)-
    [:HAS_LINK]->
    (_link:Link {site: {site}})
ON CREATE SET
    _link.hide = true,
    _link.used = 0
SET
    _link.href = {href}
MERGE
    (_tag:Tag {id: {tagId}})
ON CREATE SET
    _tag.title = {tagTitle}
MERGE
    (_artist)-
    [:TAGGED_BY]->
    (_tag)

CYPHER;

    /**
     * @param \StdClass $config
     */
    private function mergeArtistResidentAdvisorConfig(\StdClass $config)
    {
        if (null === $config->ra_link) {
            return;
        }
        $this->graphClient->cypher(self::MERGE_ARTIST_RESIDENT_ADVISOR_CONFIG, [
            'artistId' => $config->artist_id,
            'site' => ResidentAdvisor::SITE,
            'href' => $config->ra_link,
            'tagId' => $this->createId($config->tag),
            'tagTitle' => $config->tag,
        ]);
    }


    /**
     * @Route('mocks.init')
     *
     * @param IRequest $request
     * @throws \RuntimeException
     */
    public function initMockGraph(IRequest $request)
    {
        if (!$this->release->mocks) {
            die("Warning!\nUnable to initialize mocks for current release.\n");
        }
        $this->reset();
        $user1 = $this->createUser('user1', 'Chris Doe', '/assets/images/design/people/man-1-300x300.png', '');
        $user2 = $this->createUser('user2', 'John Langan', '/assets/images/design/people/man-2-300x300.png', '');
        $user3 = $this->createUser('user3', 'John Doe', '/assets/images/design/people/man-1-300x300.png', 'CircoLoco is one of the top promoters in Ibiza that started business back in 1923.');
        $user4 = $this->createUser('user4', 'Mary Doe', '/assets/images/design/people/woman-1-300x300.png', '');

        $place = $this->createPlace(
            $user2,
            'DC10 Bla',
            'DC10 in Ibiza is the most famous techno bar on this planet la. You have to visit it la.',
            '/uploads/dc10.jpg',
            'Calle Rey Francisco, 9, Madrid, Spain',
            40.4274021, -3.7159265,
            [
                (object)array(
                    'href' => '#',
                    'site' => 'facebook',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'twitter',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'pinterest'
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'google-plus'
                ),
            ]
        );

        $place = $this->renamePlace($place, 'DC10');

        $artist1 = $this->createArtist(
            $user2,
            'John Langan',
            'Your bones don’t break, mine do. That’s clear. Your cells react to bacteria and viruses differently than mine. You don’t get sick, I do. That’s also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke.',
            '',
            '/uploads/hawtin.jpg',
            'Art Director',
            [
                (object)array(
                    'href' => '#',
                    'site' => 'facebook',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'twitter',
                ),
            ]
        );

        $artist2 = $this->createArtist(
            $user2,
            'Manos Jones',
            'Your bones don’t break, mine do. That’s clear. Your cells react to bacteria and viruses differently than mine. You don’t get sick, I do. That’s also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke.',
            '',
            '/uploads/lucianomainns.jpg',
            'IOS Developer',
            [
                (object)array(
                    'href' => '#',
                    'site' => 'facebook',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'dribbble',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'google-plus'
                ),
            ]
        );
        $artist3 = $this->createArtist(
            $user3,
            'Manos Doe',
            'Your bones don’t break, mine do. That’s clear. Your cells react to bacteria and viruses differently than mine. You don’t get sick, I do. That’s also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke.',
            '',
            '/uploads/LOCO_DICE_12.jpg',
            'IOS Developer',
            [
                (object)array(
                    'href' => '#',
                    'site' => 'facebook',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'dribbble',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'google-plus'
                ),
            ]
        );
        $artist4 = $this->createArtist(
            $user4,
            'Marco Carola',
            'He\'s the exact opposite of the hero. And most times they\'re friends, like you and me! I should\'ve known way back when... You know why, David? Because of the kids. They called me Mr Glass.',
            'Born in 1992, Marco was always a good kid. He started playing vinyl records when CordiS was still in kindergarten.',
            '/uploads/carola.jpg',
            'Creative Director',
            [
                (object)array(
                    'href' => '#',
                    'site' => 'pinterest',
                ),
                (object)array(
                    'href' => '#',
                    'site' => 'instagram',
                ),
            ]
        );

        $post1 = $this->createImagePost($user1, $artist4, '/assets/images/design/vector/img-4-800x600.png', 'This theme is so nice!');
        $post1 = $this->createVideoPost($user2, $artist4, 'http://vimeo.com/20061744', '/assets/images/design/vector/img-2-800x600.png', '');
        $post1 = $this->createImagePost($user3, $artist4, '/assets/images/design/vector/img-6-800x600.png', '');
        $post1 = $this->createAudioPost($user4, $artist4, 'http://theme-background-videos.s3.amazonaws.com/audio/audio.mp3', '', 'Sounds even better!');
        $post1 = $this->createImagePost($user1, $artist4, '/assets/images/design/vector/img-3-800x600.png', 'Pick your Style');
        $post1 = $this->createVideoPost($user2, $artist4, 'http://www.youtube.com/watch?v=cfOa1a8hYP8', '/assets/images/design/vector/img-1-800x600.png', 'Everybody loves it');

        $event1 = $this->createEvent($user3, $place, 'Past event 1', 60, time() - 3600 * 600, 3600 * 6, null, null, [], [$artist4]);
        $event2 = $this->createEvent($user3, $place, 'Past event 2', 60, time() - 3600 * 500, 3600 * 6, null, null, [], [$artist1]);
        $event3 = $this->createEvent($user3, $place, 'Past event 3', 60, time() - 3600 * 400, 3600 * 6, null, null, [], [$artist4]);
        $event4 = $this->createEvent($user3, $place, 'Past event 4', 60, time() - 3600 * 300, 3600 * 6, null, null, [], [$artist4]);
        $event5 = $this->createEvent($user3, $place, 'Past event 5', 60, time() - 3600 * 200, 3600 * 6, null, null, [], [$artist4]);
        $event6 = $this->createEvent($user3, $place, 'Techno rave 2014', 60, time() + 3600 * 200, 3600 * 6, null, null, [], [$artist4]);
        $event7 = $this->createEvent($user3, $place, 'Thursday house', 60, time() + 3600 * 300, 3600 * 6, null, null, [], [$artist4]);
        $event8 = $this->createEvent($user1, $place, 'Sunday brunch', 60, time() + 3600 * 400, 3600 * 6, null, null, [], [$artist4]);
        $event9 = $this->createEvent(
            $user3,
            $place,
            'Soul Content Open-Air',
            60,
            time() + 3600 * 100, 3600 * 6,
            '/assets/images/design/section-bg/bg-2.jpg',
            '/assets/images/design/section-bg/bg-2.jpg',
            ['Open-air', 'Techno', 'House'],
            [$artist1, $artist2, $artist3, $artist4]
        );

        $post1 = $this->createNews(
            $event9,
            '1 Free admission. 10:00 PM - 06:00 AM',
            "<p>After a very long break DC10 is back in business.</p><p>Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that&#x27;s what you see at a toy store. And you must think you&#x27;re in a toy store, because you&#x27;re here shopping for an infant named Jeb.Like you, I used to think the world was this great place where everybody lived by the same standards I did, then some kid with a nail showed me I was living in his world, a world where chaos rules not order, a world where righteousness is not rewarded. That&#x27;s Cesar&#x27;s world, and if you&#x27;re not willing to play by his rules, then you&#x27;re gonna have to pay the price.</p>"
        );
        $post2 = $this->createNews(
            $event9,
            '2 Free admission. 10:00 PM - 06:00 AM',
            "<p>After a very long break DC10 is back in business.</p><p>Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that&#x27;s what you see at a toy store. And you must think you&#x27;re in a toy store, because you&#x27;re here shopping for an infant named Jeb.Like you, I used to think the world was this great place where everybody lived by the same standards I did, then some kid with a nail showed me I was living in his world, a world where chaos rules not order, a world where righteousness is not rewarded. That&#x27;s Cesar&#x27;s world, and if you&#x27;re not willing to play by his rules, then you&#x27;re gonna have to pay the price.</p>"
        );

        $post1 = $this->createImagePost($user2, $event1, '/assets/images/design/vector/img-4-800x600.png');
        $post1 = $this->createImagePost($user2, $event2, '/assets/images/design/vector/img-1-800x600.png');
        $post1 = $this->createImagePost($user2, $event3, '/assets/images/design/vector/img-2-800x600.png');
        $post1 = $this->createVideoPost($user1, $event4, 'http://www.youtube.com/watch?v=cfOa1a8hYP8', '/assets/images/design/vector/img-1-800x600.png', 'Everybody loves it');
        $post1 = $this->createImagePost($user2, $event5, '/assets/images/design/vector/img-3-800x600.png');
        $post1 = $this->createImagePost($user2, $event9, '/assets/images/design/vector/img-4-800x600.png');
        $post1 = $this->createImagePost($user2, $event9, '/assets/images/design/vector/img-1-800x600.png');
        $post1 = $this->createImagePost($user2, $event9, '/assets/images/design/vector/img-2-800x600.png');
        $post1 = $this->createVideoPost($user1, $event9, 'http://www.youtube.com/watch?v=cfOa1a8hYP8', '/assets/images/design/vector/img-1-800x600.png', 'Everybody loves it');
        $post1 = $this->createImagePost($user2, $event9, '/assets/images/design/vector/img-3-800x600.png');

        $comment1 = $this->createComment($event9, $user4, '1 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.');
        $comment2 = $this->createComment($event9, $user3, '2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.');
        $comment3 = $this->createComment($event9, $user2, '3 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.');
        $comment4 = $this->createComment($event9, $user1, '4 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.');
    }

    const RESET = <<<CYPHER
MATCH (n)
OPTIONAL MATCH (n)-[r]-()
DELETE n, r
CYPHER;

    private function reset()
    {
        $this->graphClient->cypher(self::RESET);
    }

    const CREATE_USER = <<<CYPHER
CREATE
    (user:User {id: {id}})-
    [:HAS_PROFILE]->
    (profile:Profile {
        title: {title},
        intro: {intro},
        hometown: null
    })-
    [:HAS_COVER]->
    (photo:Photo {
        large: null,
        small: {smallPhotoHref}
    })
WITH
    user, profile
CREATE
    (profile)-
    [:MANAGED_BY {timestamp: {now}}]->
    (user)
RETURN
    user
CYPHER;


    private function createUser($id, $title, $smallPhotoHref, $intro)
    {
        $ret = $this->graphClient->cypherOne(self::CREATE_USER, array(
            'id' => $id,
            'title' => $title,
            'intro' => $intro,
            'hometown' => null,
            'smallPhotoHref' => $smallPhotoHref,
            'now' => time(),
        ));
        return $ret['user'];
    }

    const CREATE_PLACE = <<<CYPHER
MATCH
    (user:User)
WHERE
    id(user) = {userId}
CREATE
    (user)<-
    [:MANAGED_BY {timestamp: {now}}]-
    (place:Place {
        id: {id},
        title: {title},
        intro: {intro},
        address: {address},
        longitude: {longitude},
        latitude: {latitude}
    })-
    [:HAS_COVER]->
    (photo:Photo {
        large: {largePhotoHref},
        small: NULL
    })
RETURN
    place
CYPHER;


    private function createPlace($user, $title, $intro, $largePhotoHref, $address, $latitude, $longitude, $linkList)
    {
        $ret = $this->graphClient->cypherOne(self::CREATE_PLACE, array(
            'userId' => $user->getId(),
            'id' => $this->createId($title),
            'title' => $title,
            'intro' => $intro,
            'largePhotoHref' => $largePhotoHref,
            'address' => $address,
            'longitude' => $longitude,
            'latitude' => $latitude,
            'now' => time(),
        ));
        $this->graphClient->upsertSpatialPoint('Place', $ret['place']->getId());
        $this->setNodeLinkList($ret['place'], $linkList);
        return $ret['place'];
    }

    const CREATE_ARTIST = <<<CYPHER
MATCH
    (user:User)
WHERE
    id(user) = {userId}
CREATE
    (user)<-
    [:MANAGED_BY {timestamp: {now}}]-
    (artist:Profile:Artist {
        id: {id},
        title: {title},
        intro: {intro},
        about: {about},
        hometown: {hometown}
    })-
    [:HAS_COVER]->
    (photo:Photo {
        large: {largePhotoHref},
        small: NULL
    })
RETURN
    artist
CYPHER;


    private function createArtist($user, $title, $intro, $about, $largePhotoHref, $hometown, $linkList)
    {
        $ret = $this->graphClient->cypherOne(self::CREATE_ARTIST, array(
            'userId' => $user->getId(),
            'id' => $this->createId($title),
            'title' => $title,
            'intro' => $intro,
            'about' => $about,
            'hometown' => $hometown,
            'largePhotoHref' => $largePhotoHref,
            'now' => time(),
        ));
        $this->setNodeLinkList($ret['artist'], $linkList);
        return $ret['artist'];
    }

    const CREATE_LINK = <<<CYPHER
MATCH
    (node)
WHERE
    id(node) = {nodeId}
CREATE
    (node)-
    [:HAS_LINK]->
    (link:Link {
        site: {site},
        href: {href}
    })
RETURN
    link
CYPHER;

    const CREATE_LINK_LIST = <<<CYPHER
MATCH
    (node)
WHERE
    id(node) = {nodeId}
WITH
    node
FOREACH (link in {linkList} |
    CREATE
        (node)-[:HAS_LINK]->(:Link {
            href: link.href,
            site: link.site
        })
)
RETURN
    node

CYPHER;


    private function setNodeLinkList($node, $linkList)
    {
        $ret = $this->graphClient->cypherOne(self::CREATE_LINK_LIST, array(
            'nodeId' => $node->getId(),
            'linkList' => array_map(function($link) { return ['site' => $link->site, 'href' => $link->href]; }, $linkList),
        ));
        return $ret['node'];
    }

    const CREATE_EVENT = <<<CYPHER
MATCH
    (user:User)
WHERE
    id(user) = {userId}
CREATE
    (user)<-
    [:MANAGED_BY {timestamp: {now}}]-
    (event:Event {
        id: {id},
        title: {title},
        tzOffset: {tzOffset},
        startsAt: {startsAt},
        endsAt: {endsAt}
    })-
    [:HAS_COVER]->
    (photo:Photo {
        small: {smallPhotoHref},
        large: {largePhotoHref}
    })
RETURN
    event
CYPHER;


    private function createEvent($user, $place, $title, $tzOffset, $startsAt, $duration, $largePhotoHref, $smallPhotoHref, $tagList, $artistList)
    {
        $ret = $this->graphClient->cypherOne(self::CREATE_EVENT, array(
            'userId' => $user->getId(),
            'id' => $this->createEventId($title, $startsAt - $tzOffset),
            'title' => $title,
            'startsAt' => $startsAt,
            'endsAt' => $startsAt + $duration,
            'largePhotoHref' => $largePhotoHref,
            'smallPhotoHref' => $smallPhotoHref,
            'tzOffset' => $tzOffset,
            'now' => time(),
        ));
        $this->setEventPlace($ret['event'], $place);
        $this->setEventTagList($ret['event'], $tagList);
        $this->setEventArtistList($ret['event'], $artistList);
        return $ret['event'];
    }

    const SET_EVENT_PLACE = <<<CYPHER
MATCH
    (event:Event),
    (place:Place)
WHERE
    id(event) = {eventId} AND
    id(place) = {placeId}
CREATE
    (event)-
    [:HOSTED_AT]->
    (place)
CYPHER;


    private function setEventPlace($event, $place)
    {
        $this->graphClient->cypher(self::SET_EVENT_PLACE, array(
            'eventId' => $event->getId(),
            'placeId' => $place->getId(),
        ));
    }

    const SET_EVENT_TAG = <<<CYPHER
MERGE
    (tag:Tag {id: {id}})
ON CREATE SET
    tag.title = {title}
WITH
    tag
MATCH
    (event:Event)
WHERE
    id(event) = {eventId}
CREATE
    (event)-
    [:TAGGED_BY {index:{index}}]->
    (tag)
CYPHER;


    private function setEventTagList($event, $tagList)
    {
        return array_map(function($title, $index) use ($event) {
            $this->graphClient->cypher(self::SET_EVENT_TAG, array(
                'eventId' => $event->getId(),
                'id' => $this->createId($title),
                'title' => $title,
                'index' => $index,
            ));
        }, $tagList, array_keys($tagList));
    }

    const SET_EVENT_ARTIST = <<<CYPHER
MATCH
    (event:Event),
    (artist:Artist)
WHERE
    id(event) = {eventId} AND
    id(artist) = {artistId}
CREATE
    (artist)-
    [:PERFORMS_AT {index: {index}}]->
    (event)
CYPHER;


    private function setEventArtistList($event, $artistList)
    {
        return array_map(function($artist, $index) use ($event) {
            $this->graphClient->cypher(self::SET_EVENT_ARTIST, array(
                'index' => $index,
                'eventId' => $event->getId(),
                'artistId' => $artist->getId(),
            ));
        }, $artistList, array_keys($artistList));
    }

    const CREATE_NEWS_POST = <<<CYPHER
MATCH
    (node)
WHERE
    id(node) = {nodeId}
CREATE
    (news:News {
        title: {title},
        content: {content},
        createdAt: {createdAt}
    })-
    [:POSTED_AT]->
    (node)
RETURN
    news
CYPHER;


    private function createNews($node, $title, $content)
    {
        return $this->graphClient->cypherOne(self::CREATE_NEWS_POST, array(
            'nodeId' => $node->getId(),
            'title' => $title,
            'content' => $content,
            'createdAt' => time() - rand(10, 100),
        ));
    }

    const CREATE_COMMENT = <<<CYPHER
MATCH
    (user:User),
    (node)
WHERE
    id(user) = {userId} AND
    id(node) = {nodeId}
CREATE
    (user)<-
    [:MANAGED_BY {timestamp: {now}}]-
    (message:Message {
        content: {content},
        createdAt: {createdAt}
    })-
    [:POSTED_AT]->
    (node)
RETURN
    message
CYPHER;


    private function createComment($node, $user, $content)
    {
        return $this->graphClient->cypherOne(self::CREATE_COMMENT, array(
            'nodeId' => $node->getId(),
            'userId' => $user->getId(),
            'content' => $content,
            'createdAt' => time(),
            'now' => time(),
        ));
    }

    const CREATE_POST = <<<CYPHER
MATCH
    (user:User),
    (node)
WHERE
    id(user) = {userId} AND
    id(node) = {nodeId}
WITH
    user, node
CREATE
    (post:Post {
        id: {id},
        type: {type},
        href: {href},
        cover: {cover},
        title: {title},
        source: null,
        createdAt: {createdAt}
    })-
    [:POSTED_AT {timestamp: {createdAt}, userId: user.id}]->
    (node)
RETURN
    post
CYPHER;

    private function createImagePost($user, $node, $href, $title = '')
    {
        return $this->createPost($user, $node, 'Image', $href, $href, $title);
    }

    private function createAudioPost($user, $node, $href, $cover, $title)
    {
        return $this->createPost($user, $node, 'Audio', $href, $cover, $title);
    }

    private function createVideoPost($user, $node, $href, $cover, $title)
    {
        return $this->createPost($user, $node, 'Video', $href, $cover, $title);
    }

    private function createPost($user, $node, $type, $href, $cover, $title)
    {
        return $this->graphClient->cypherOne(self::CREATE_POST, array(
            'nodeId' => $node->getId(),
            'userId' => $user->getId(),
            'id' => ++self::$serialId,
            'type' => $type,
            'href' => $href,
            'cover' => $cover,
            'title' => $title,
            'createdAt' => time(),
            'now' => time(),
        ));
    }

    const RENAME_PLACE = <<<CYPHER
MATCH
    (place:Place {id: {oldId}})
SET
    place.id = {newId},
    place.title = {newTitle}
WITH
    place
CREATE
    (place)-
    [:WAS]->
    (legacy:Legacy {
        id: {oldId}
    })
RETURN
    place
CYPHER;


    private function renamePlace($place, $title)
    {
        $ret = $this->graphClient->cypherOne(self::RENAME_PLACE, array(
            'oldId' => $this->createId($place->title),
            'newId' => $this->createId($title),
            'oldTitle' => $place->title,
            'newTitle' => $title,
        ));
        return $ret['place'];
    }

    /**
     * @param string $title
     * @return string mixed
     */
    private function createId($title)
    {
        return Tools::slugify($title);
    }

}
