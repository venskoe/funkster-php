<?php

namespace Cli\Controllers;

use Menara\Generic\Console\Interfaces\IController;
use Menara\Generic\Console\Interfaces\IRunner;
use Menara\Generic\Interfaces\IRequest;

/**
 * @RoutePrefix('streams.')
 * @Watchdog('streams.listen.zmq')
 * @Watchdog('streams.listen.udp')
 */
class Streams
{
    /**
     * @var IRunner
     */
    public $runner;

    /**
     * @var IController
     */
    public $zmqController;

    /**
     * @var IController
     */
    public $udpController;

    /**
     * @Route('listen.zmq')
     */
    public function listenZmq(IRequest $request = null)
    {
        $this->runner->run($this->zmqController);
    }

    /**
     * @Route('listen.udp')
     */
    public function listenUdp(IRequest $request = null)
    {
        $this->runner->run($this->udpController);
    }

}
