<?php

namespace Cli\Implementations\Phalcon;

use Phalcon\Mvc\Router\Annotations;

use Cli\Controllers\Manager;

class Router extends Annotations
{
    /**
     * @param string $handler
     * @param \Phalcon\Annotations\Annotation $annotation
     */
    public function processControllerAnnotation($handler, $annotation)
    {
        if ($annotation->getName() === 'Watchdog') {
            $this->getManager()->addWatchdog($annotation->getArgument(0));
        }
        else {
            parent::processControllerAnnotation($handler, $annotation);
        }
    }

    /**
     * @param string $module
     * @param string $namespace
     * @param string $controller
     * @param string $action
     * @param \Phalcon\Annotations\Annotation $annotation
     */
    public function processActionAnnotation($module, $namespace, $controller, $action, $annotation)
    {
        if ($annotation->getName() === 'Cron') {
            $this->getManager()->addCronCommand($annotation->getArgument(0), $namespace, $controller, $action);
        }
        else {
            parent::processActionAnnotation($module, $namespace, $controller, $action, $annotation);
        }
    }

    /**
     * @return Manager
     */
    private function getManager()
    {
        return $this->getDI()->getShared('\Cli\Controllers\Manager');
    }

}
