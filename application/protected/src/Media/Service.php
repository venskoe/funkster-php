<?php

namespace Media;

use Media\Beans\Item;
use Media\Exceptions\InvalidHrefException;
use Media\Exceptions\InvalidDataException;
use Media\Interfaces\IService;
use Media\Interfaces\ISource;
use Media\Sources\SoundCloud;
use Media\Sources\Vimeo;
use Media\Sources\YouTube;

class Service implements IService
{
    /**
     * @var ISource[]
     */
    private $sourceList;

    public function __construct()
    {
        $this->sourceList = [
            new Youtube(),
            new Vimeo(),
            new SoundCloud(),
        ];
    }

    /**
     * @param string $href
     * @return Item
     * @throws InvalidDataException
     */
    public function parse($href)
    {
        foreach ($this->sourceList as $source) {
            try {
                return $source->parse($href);
            }
            catch (InvalidHrefException $e) {
                // pass
            }
        }
        throw new InvalidHrefException('invalid_media_href');
    }

}
