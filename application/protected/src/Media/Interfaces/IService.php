<?php

namespace Media\Interfaces;

use Media\Beans\Item;
use Media\Exceptions\InvalidDataException;

interface IService
{
    /**
     * @param string $href
     * @return Item
     * @throws InvalidDataException
     */
    public function parse($href);

}
