<?php

namespace Media\Interfaces;

use Media\Beans\Item;
use Media\Exceptions\InvalidHrefException;

interface ISource
{
    /**
     * @param string $href
     * @return Item
     * @throws InvalidHrefException
     */
    public function parse($href);

}
