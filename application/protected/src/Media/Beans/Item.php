<?php

namespace Media\Beans;

class Item
{
    public static $TYPE_VIDEO = 'Video';
    public static $TYPE_IMAGE = 'Image';
    public static $TYPE_AUDIO = 'Audio';

    /**
     * @var string
     */
    public $guid;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $href;

    /**
     * @var String
     */
    public $cover;

    /**
     * @var String
     */
    public $source;

}
