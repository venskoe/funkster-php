<?php

namespace Media\Sources;

use Media\Beans\Item;
use Media\Exceptions\InvalidHrefException;

class YouTube extends BaseSource
{
    /**
     * @param string $href
     * @return string
     * @throws InvalidHrefException
     */
    protected function parseId($href)
    {
        $data = parse_url($href);

        if (empty($data['host'])) {
            throw new InvalidHrefException();
        }

        $query = array();
        if (isset($data['query'])) {
            parse_str($data['query'], $query);
        }

        if (false !== strpos($data['host'], 'youtube.')
            && in_array($data['path'], array('/watch', '/all_comments'))
            && isset($query['v'])
            && preg_match('#^[\w-]{11}$#', $query['v'])
        ) {
            return $query['v'];
        }
        if (false !== strpos($data['host'], 'youtu.be')
            && preg_match('#^/?[\w-]{11}/?$#', $data['path'])
        ) {
            return trim($data['path'], '/');
        }
        if (false != preg_match('/^www\.youtube(-nocookie)?\.com$/',$data['host'])
            && preg_match('{^/embed/([\w-]{11})}', $data['path'], $matches)
        ) {
            return $matches[1];
        }
        if (false != preg_match('/^www\.youtube(-nocookie)?\.com$/',$data['host'])
            && preg_match('{^/v/([\w-]{11})}', $data['path'], $matches)
        ) {
            return $matches[1];
        }
        if (false != preg_match('/^www\.youtube(-nocookie)?\.com$/',$data['host'])
            && preg_match('{^/p/([\w-]{16})}', $data['path'], $matches)
        ) {
            return $matches[1];
        }
        throw new InvalidHrefException();
    }

    /**
     * @param string $id
     * @return string
     */
    protected function createCover($id)
    {
        return sprintf('https://img.youtube.com/vi/%s/%s.jpg', $id, 'hqdefault');
    }

    /**
     * @return string
     */
    protected function getName()
    {
        return 'youtube';
    }

    /**
     * @return string
     */
    protected function getType()
    {
        return Item::$TYPE_VIDEO;
    }

}
