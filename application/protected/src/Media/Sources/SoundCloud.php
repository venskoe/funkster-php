<?php

namespace Media\Sources;

use Media\Beans\Item;
use Media\Exceptions\InvalidHrefException;

class SoundCloud extends BaseSource
{
    /**
     * @param string $href
     * @return string
     * @throws InvalidHrefException
     */
    protected function parseId($href)
    {
        $data  = parse_url($href);
        if (empty($data['host'])) {
            throw new InvalidHrefException();
        }
        if (false === strpos($data['host'], 'soundcloud.com')) {
            throw new InvalidHrefException();
        }
        if (isset($data['query'])) {
            $queryFields = array();
            parse_str($data['query'], $queryFields);
            if (isset($queryFields['url']) && preg_match('#http://api.soundcloud.com/([^&]*)#', $queryFields['url'], $matches)) {
                return $matches[1];
            }
        }
        if (isset($data['path']) && strlen($data['path']) > 1) {
            return substr($data['path'], 1);
        }
        throw new InvalidHrefException();
    }

    /**
     * @return string
     */
    protected function getName()
    {
        return 'soundcloud';
    }

    /**
     * @return string
     */
    protected function getType()
    {
        return Item::$TYPE_AUDIO;
    }

    /**
     * @param string $id
     * @return string
     */
    protected function createCover($id)
    {
        return null;
    }

}
