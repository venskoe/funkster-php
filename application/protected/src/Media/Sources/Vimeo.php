<?php

namespace Media\Sources;

use Media\Beans\Item;
use Media\Exceptions\InvalidHrefException;

class Vimeo extends BaseSource
{
    /**
     * @param string $href
     * @return string
     * @throws InvalidHrefException
     */
    protected function parseId($href)
    {
        $data = parse_url($href);
        if (empty($data['host'])) {
            throw new InvalidHrefException();
        }
        if (false === strpos($data['host'], 'vimeo.com')) {
            throw new InvalidHrefException();
        }
        if (preg_match('#([0-9]+)$#', $data['path'], $matches)) {
            return $matches[1];
        }
        throw new InvalidHrefException();
    }

    /**
     * @param string $id
     * @return string
     */
    protected function createCover($id)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, sprintf('https://vimeo.com/api/v2/video/%s.json', $id));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($data, true);

        if (false === $data || null === $data || isset($data['error'])) {
            return null;
        }

        return $data[0]['thumbnail_large'];
    }

    /**
     * @return string
     */
    protected function getName()
    {
        return 'vimeo';
    }

    /**
     * @return string
     */
    protected function getType()
    {
        return Item::$TYPE_VIDEO;
    }

}
