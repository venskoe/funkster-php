<?php

namespace Media\Sources;

use Media\Beans\Item;
use Media\Exceptions\InvalidHrefException;
use Media\Interfaces\ISource;

abstract class BaseSource implements ISource
{
    /**
     * @param string $href
     * @return Item
     * @throws InvalidHrefException
     */
    public function parse($href)
    {
        $id = $this->parseId($href);
        $bean = new Item();
        $bean->guid = implode('/', [$this->getName(), $id]);
        $bean->type = $this->getType();
        $bean->href = $href;
        $bean->cover = $this->createCover($id);
        $bean->source = $this->getName();
        return $bean;
    }

    /**
     * @param string $href
     * @return string
     * @throws InvalidHrefException
     */
    abstract protected function parseId($href);

    /**
     * @return string
     */
    abstract protected function getName();

    /**
     * @return string
     */
    abstract protected function getType();

    /**
     * @param string $id
     * @return string
     */
    abstract protected function createCover($id);

}
