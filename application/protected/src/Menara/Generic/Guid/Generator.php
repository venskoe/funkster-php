<?php

namespace Menara\Generic\Guid;

use Menara\Generic\Operator\PartialCaller;

class Generator
{
    const TEMPLATE = ':timestamp:hostname:pid:trail';

    /**
     * @var Generator
     */
    private static $instance = null;

    /**
     * @var int
     */
    private $counter;

    /**
     * @var string
     */
    private $hostname;

    /**
     * @var string
     */
    private $pid;

    /**
     * @var Callable
     */
    private $byteToChar;

    /**
     * @return Generator
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new Generator(self::getRandomNumber());
        }
        return self::$instance;
    }

    /**
     * @return int
     */
    private static function getRandomNumber()
    {
        return rand(0, pow(2, 24));
    }

    /**
     * @param int $counter
     */
    private function __construct($counter)
    {
        $this->counter = $counter;
        $this->hostname = substr(md5(gethostname()), 0, 3);
        $this->pid = pack('n', posix_getpid());
        $this->byteToChar = PartialCaller::create('sprintf', '%02X');
    }

    /**
     * @return string
     */
    public function generate()
    {
        return $this->fromTimestamp(time());
    }

    /**
     * @return string
     */
    public function generateRandom()
    {
        return $this->fromTimestamp(self::getRandomNumber());
    }

    /**
     * @param int $timestamp
     * @param Int $counter
     * @return string
     */
    public function fromTimestamp($timestamp, $counter = null)
    {
        $counter = $this->getCounter($counter);
        $parts = $this->createParts($timestamp, $counter);
        $byteList = str_split(strtr(self::TEMPLATE, $parts));
        $byteList = array_map('ord', $byteList);
        $charList = array_map($this->byteToChar, $byteList);
        return strtolower(implode('', $charList));
    }

    /**
     * @param Int $counter
     * @return Int
     */
    private function getCounter($counter = null)
    {
        if (null !== $counter) {
            return $counter;
        }
        return $this->counter++;
    }

    /**
     * @param int $timestamp
     * @param int $counter
     * @return array
     */
    private function createParts($timestamp, $counter)
    {
        return array(
            ':timestamp' => pack('N', $timestamp),
            ':trail' => substr(pack('N', $counter), 1, 3),
            ':hostname' => $this->hostname,
            ':pid' => $this->pid,
        );
    }

}
