<?php

namespace Menara\Generic\Request\Adapters;

use Menara\Generic\Interfaces\IRequestAdapter;

class CallAdapter implements IRequestAdapter
{
    /**
     * @var IRequestAdapter
     */
    private $delegate;

    /**
     * @var object
     */
    private $impl;

    /**
     * @var \ReflectionObject
     */
    private $reflection;

    /**
     * @param IRequestAdapter $delegate
     * @param object $impl
     */
    public function __construct(IRequestAdapter $delegate, $impl)
    {
        $this->delegate = $delegate;
        $this->impl = $impl;
        $this->reflection = new \ReflectionObject($impl);
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getArgument($name, $default)
    {
        $methodName = 'get' . ucfirst($name);
        if ($this->reflection->hasMethod($methodName)) {
            $method = $this->reflection->getMethod($methodName);
            if ($method->isPublic() || $method->isStatic()) {
                if ($method->getNumberOfParameters() > 0) {
                    return $method->invoke($this->impl, true);
                }
                else {
                    return $method->invoke($this->impl);
                }
            }
        }
        return $this->delegate->getArgument($name, $default);
    }

}
