<?php

namespace Menara\Generic\Request\Adapters;

use Menara\Generic\Interfaces\IRequestAdapter;
use Phalcon\Http\RequestInterface;

class PhalconRequestAdapter implements IRequestAdapter
{
    /**
     * @var RequestInterface
     */
    private $impl;

    /**
     * @var IRequestAdapter
     */
    private $delegate;

    /**
     * @param IRequestAdapter $delegate
     * @param RequestInterface $impl
     */
    public function __construct(IRequestAdapter $delegate, RequestInterface $impl)
    {
        $this->delegate = $delegate;
        $this->impl = $impl;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getArgument($name, $default)
    {
        $value = $this->impl->get($name, null, $default);
        if ($value !== $default) {
            return $value;
        }
        return $this->delegate->getArgument($name, $default);
    }

}
