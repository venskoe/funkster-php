<?php

namespace Menara\Generic\Request\Adapters;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IRequestAdapter;

class StubAdapter implements IRequestAdapter
{
    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     * @throws HttpException
     */
    public function getArgument($name, $default)
    {
        return $default;
    }

}
