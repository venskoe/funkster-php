<?php

namespace Menara\Generic\Request\Adapters;

use Phalcon\Http\Response\CookiesInterface;

use Menara\Generic\Interfaces\IRequestAdapter;

class PhalconCookiesAdapter implements IRequestAdapter
{
    /**
     * @var CookiesInterface
     */
    private $impl;

    /**
     * @var IRequestAdapter
     */
    private $delegate;

    /**
     * @param IRequestAdapter $delegate
     * @param CookiesInterface $impl
     */
    public function __construct(IRequestAdapter $delegate, CookiesInterface $impl)
    {
        $this->delegate = $delegate;
        $this->impl = $impl;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getArgument($name, $default)
    {
        if ($this->impl->has($name)) {
            return $this->impl->get($name)->getValue();
        }
        return $this->delegate->getArgument($name, $default);
    }

}
