<?php

namespace Menara\Generic\Request\Adapters;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IRequestAdapter;

class DictAdapter implements IRequestAdapter
{
    /**
     * @var array
     */
    private $dict;

    /**
     * @var IRequestAdapter
     */
    private $delegate;

    /**
     * @param IRequestAdapter $delegate
     * @param array $dict
     */
    public function __construct(IRequestAdapter $delegate, array $dict)
    {
        $this->delegate = $delegate;
        $this->dict = $dict;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getArgument($name, $default)
    {
        if (isset($this->dict[$name])) {
            return $this->dict[$name];
        }
        return $this->delegate->getArgument($name, $default);
    }

}
