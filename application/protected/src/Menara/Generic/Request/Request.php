<?php

namespace Menara\Generic\Request;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IFormat;
use Menara\Generic\Interfaces\IRequest;
use Menara\Generic\Interfaces\IRequestAdapter;

class Request implements IRequest
{
    /**
     * @var IRequestAdapter
     */
    private $adapter;

    /**
     * @var IFormat
     */
    public $json;

    /**
     * @param IRequestAdapter $adapter
     */
    public function __construct(IRequestAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param string $name
     * @param bool|int $default
     * @return bool
     * @throws HttpException
     */
    public function getBoolean($name, $default = self::MISSED)
    {
        static $falseStringList = ['false'];

        $value = $this->getString($name, $default);
        if (in_array($value, $falseStringList)) {
            return false;
        }
        return (bool)$value;
    }

    /**
     * @param string $name
     * @param string[]|int $default
     * @return array
     * @throws HttpException
     */
    public function getListOfString($name, $default = self::MISSED)
    {
        $valueList = $this->getArgument($name, $default);
        if ($valueList === $default) {
            return $valueList;
        }
        return array_map(function($value) use ($name) {
            return $this->validateString($value, $name . '_contains_empty_string');
        }, $valueList);
    }

    /**
     * @param string $name
     * @param mixed|int $default
     * @return mixed
     * @throws HttpException
     */
    public function getJson($name, $default = self::MISSED)
    {
        $value = $this->getString($name, $default);
        if ($value === $default) {
            return $value;
        }
        return $this->json->decode($value);
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return string
     * @throws HttpException
     */
    public function getString($name, $default = self::MISSED)
    {
        $value = $this->getArgument($name, $default);
        if ($value === $default) {
            return $value;
        }
        return $this->validateString($value, $name . '_is_empty');
    }

    /**
     * @param mixed $value
     * @param string $message
     * @return string
     * @throws HttpException
     */
    private function validateString($value, $message)
    {
        if (strlen(trim($value)) == 0) {
            throw new HttpException(400, $message);
        }
        return $value;
    }

    /**
     * @param integer $name
     * @param integer $default
     * @return integer
     * @throws HttpException
     */
    public function getTimestamp($name, $default = self::MISSED)
    {
        return $this->getInteger($name, $default);
    }

    /**
     * @param string $name
     * @param int $max
     * @return int
     * @throws HttpException
     */
    public function getLimit($name, $max)
    {
        $value = $this->getInteger($name, $max);
        return min($value, $max);
    }

    /**
     * @param string $name
     * @param int[]|int $default
     * @return array
     * @throws HttpException
     */
    public function getListOfInteger($name, $default = self::MISSED)
    {
        $valueList = $this->getArgument($name, $default);
        if ($valueList === $default) {
            return $valueList;
        }
        return array_map(function($value) use ($name, $default) {
            return $this->validateInteger($value, $default, $name . '_contains_not_integer');
        }, $valueList);
    }

    /**
     * @param string $name
     * @param int $default
     * @return int
     * @throws HttpException
     */
    public function getInteger($name, $default = self::MISSED)
    {
        $value = $this->getArgument($name, $default);
        if ($value === $default) {
            return $value;
        }
        return $this->validateInteger($value, $default, $name . '_is_not_integer');
    }

    /**
     * @param mixed $value
     * @param int $default
     * @param string $message
     * @return int
     * @throws HttpException
     */
    private function validateInteger($value, $default, $message)
    {
        if ('0' === $value) {
            return 0;
        }
        $value = intval($value);
        if (0 === $value && $value !== $default) {
            throw new HttpException(400, $message);
        }
        return $value;
    }

    /**
     * @param string $name
     * @param float|int $default
     * @return float
     * @throws HttpException
     */
    public function getFloat($name, $default = self::MISSED)
    {
        $value = $this->getArgument($name, $default);
        if ($value === $default) {
            return $value;
        }
        $value = floatval($value);
        if (0 === $value && $value !== $default) {
            throw new HttpException(400, $name . '_is_not_float');
        }
        return $value;
    }

    /**
     * @param string $name
     * @param mixed|int $default
     * @return mixed
     * @throws HttpException
     */
    private function getArgument($name, $default = self::MISSED)
    {
        $value = $this->adapter->getArgument($name, $default);
        if ($this->isEmpty($value)) {
            if ($default === self::MISSED) {
                throw new HttpException(400, $name . '_is_missed');
            }
            return $default;
        }
        if ($value === self::MISSED) {
            throw new HttpException(400, $name . '_is_missed');
        }
        return $value;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    private function isEmpty($value)
    {
        if (is_string($value)) {
            return strlen($value) == 0;
        }
        return empty($value);
    }

}
