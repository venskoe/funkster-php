<?php

namespace Menara\Generic;

use Symfony\Component\CssSelector\CssSelector;

class Dom
{
    /**
     * @param string $href
     * @param int $attemptCount
     * @param bool $verbose
     * @return \DOMDocument
     * @throws \RuntimeException
     */
    public static function fetch($href, $attemptCount = 1, $verbose = false)
    {
        self::printIfVerbose($verbose, 'Fetching [1] 0 ...', [$href, $attemptCount]);
        $root = new \DOMDocument();
        if (false === $root->loadHTMLFile($href)) {
            self::printIfVerbose($verbose, "Fail\n");
            if ($attemptCount > 1) {
                return self::fetch($href, $attemptCount - 1, $verbose);
            }
            else {
                throw new \RuntimeException('Unable to fetch: ' . $href);
            }
        }
        else {
            self::printIfVerbose($verbose, "OK\n");
            return $root;
        }
    }

    /**
     * @param bool $verbose
     * @param string $message
     * @param array $kwargs
     */
    private static function printIfVerbose($verbose, $message, $kwargs = [])
    {
        if ($verbose) {
            print(strtr($message, $kwargs));
        }
    }

    /**
     * @param \DOMNode $parent
     * @param string $selector
     * @return \DOMNode|null
     */
    public static function selectItem(\DOMNode $parent, $selector)
    {
        $nodeList = self::selectList($parent, $selector);
        if (count($nodeList) == 0) {
            return null;
        }
        return $nodeList[0];
    }

    /**
     * @param $parent
     * @param $selector
     * @return \DOMNode[]
     */
    public static function selectList(\DOMNode $parent, $selector)
    {
        if ($parent instanceof \DOMDocument) {
            $root = $parent;
            $parent = null;
        }
        else {
            $root = $parent->ownerDocument;
        }
        $selector = CssSelector::toXPath($selector);
        $nodeList = (new \DOMXPath($root))->query($selector, $parent);
        return iterator_to_array($nodeList);
    }

    /**
     * @param string $host
     * @param \DOMNode $node
     * @return string
     */
    public static function makeAbsoluteHref($host, \DOMNode $node)
    {
        $href = $node->attributes->getNamedItem('href')->textContent;
        if (null === $href) {
            return null;
        }
        return implode('', [$host, $href]);
    }

}
