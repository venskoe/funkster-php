<?php

namespace Menara\Generic\Exceptions;

class HttpException extends \RuntimeException
{
    /**
     * @param string $statusCode
     * @param string $statusMessage
     * @param array $headers
     */
    public function __construct($statusCode, $statusMessage = '', $headers = array())
    {
        $this->statusCode = $statusCode;
        $this->statusMessage = $statusMessage;
        $this->headers = $headers;
    }

}
