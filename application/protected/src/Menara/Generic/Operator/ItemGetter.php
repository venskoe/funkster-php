<?php

namespace Menara\Generic\Operator;

use InvalidArgumentException;

class ItemGetter
{
    /**
     * @param mixed $itemOrList
     * @param mixed $default
     * @return Callable
     * @throws InvalidArgumentException
     */
    public static function create($itemOrList, $default = null)
    {
        if (!is_array($itemOrList)) {
            $itemOrList = [$itemOrList];
        }
        return new self($itemOrList, $default);
    }

    /**
     * @param string[] $itemOrList
     * @param mixed $default
     */
    private function __construct($itemOrList, $default)
    {
        $this->itemList = $itemOrList;
        $this->default = $default;
    }

    /**
     * @param mixed $object
     * @return mixed
     */
    public function __invoke($object)
    {
        if (is_array($object)) {
            $ret = array_merge(array(), $object);
        }
        else {
            $ret = clone($object);
        }
        foreach ($this->itemList as $item) {
            if (!isset($ret[$item])) {
                return $this->default;
            }
            $ret = $ret[$item];
        }
        return $ret;
    }

}
