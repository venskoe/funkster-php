<?php

namespace Menara\Generic\Operator;

use InvalidArgumentException;

class MethodCaller
{
    /**
     * @param string $methodNameOrList
     * @param array $arguments
     * @return Callable
     * @throws InvalidArgumentException
     */
    public static function create($methodNameOrList, $arguments = array())
    {
        if (!is_array($methodNameOrList)) {
            $methodNameOrList = [$methodNameOrList];
        }
        else if (!empty($arguments)) {
            throw new InvalidArgumentException('arguments');
        }
        return new self($methodNameOrList, $arguments);
    }

    /**
     * @param string[] $methodNameList
     * @param array $arguments
     */
    private function __construct($methodNameList, $arguments)
    {
        $this->methodNameList = $methodNameList;
        $this->arguments = $arguments;
    }

    /**
     * @param mixed $object
     * @return mixed
     */
    public function __invoke($object)
    {
        $ret = clone($object);
        foreach ($this->methodNameList as $methodName) {
            $ret = call_user_func_array([$ret, $methodName], $this->arguments);
        }
        return $ret;
    }

}
