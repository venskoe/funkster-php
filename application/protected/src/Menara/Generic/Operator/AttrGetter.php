<?php

namespace Menara\Generic\Operator;

use InvalidArgumentException;

class AttrGetter
{
    /**
     * @param string $attrOrList
     * @param mixed $default
     * @return Callable
     * @throws InvalidArgumentException
     */
    public static function create($attrOrList, $default = null)
    {
        if (!is_array($attrOrList)) {
            $attrOrList = [$attrOrList];
        }
        return new self($attrOrList, $default);
    }

    /**
     * @param string[] $attrOrList
     * @param mixed $default
     */
    private function __construct($attrOrList, $default)
    {
        $this->attrList = $attrOrList;
        $this->default = $default;
    }

    /**
     * @param mixed $object
     * @return mixed
     */
    public function __invoke($object)
    {
        $ret = clone($object);
        foreach ($this->attrList as $attr) {
            if (!isset($ret->{$attr})) {
                return $this->default;
            }
            $ret = $ret->{$attr};
        }
        return $ret;
    }

}
