<?php

namespace Menara\Generic\Operator;

class PartialCaller
{
    /**
     * @param Callable|array $callable
     * @return Callable
     * @throws \InvalidArgumentException
     */
    public static function create($callable /* $arg1, $arg2, ... */)
    {
        $arguments = func_get_args();
        array_shift($arguments);
        if (!is_callable($callable)) {
            throw new \InvalidArgumentException();
        }
        return new self($callable, $arguments);
    }

    /**
     * @param Callable|array $callable
     * @param array $arguments
     */
    private function __construct($callable, $arguments)
    {
        $this->callable = $callable;
        $this->arguments = $arguments;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $arguments = array_merge($this->arguments, func_get_args());
        return call_user_func_array($this->callable, $arguments);
    }

}
