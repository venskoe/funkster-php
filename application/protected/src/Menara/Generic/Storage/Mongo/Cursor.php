<?php

namespace Menara\Generic\Storage\Mongo;

use Menara\Generic\Storage\Exceptions\ConnectionException;
use Menara\Generic\Storage\Interfaces\IMongoCursor;
use Menara\Generic\Tools;
use Traversable;

class Cursor implements IMongoCursor, \IteratorAggregate
{
    /**
     * @var \MongoCursor
     */
    private $impl;

    /**
     * @param \MongoCursor $impl
     */
    public function __construct(\MongoCursor $impl)
    {
        $this->impl = $impl;
    }

    /**
     * @param int $limit
     * @return IMongoCursor
     */
    public function limit($limit)
    {
        $this->impl = $this->impl->limit($limit);
        return $this;
    }

    /**
     * @param string $field1
     * @param String $field2
     * @return IMongoCursor
     */
    public function order($field1, $field2 = null /*, $field3, ... */)
    {
        $fieldMap = array();
        foreach (func_get_args() as $field) {
            if (Tools::stringStartsWith($field, '-')) {
                $fieldMap[substr($field, 1)] = -1;
            }
            else if (Tools::stringStartsWith($field, '+')) {
                $fieldMap[substr($field, 1)] = +1;
            }
            else {
                $fieldMap[$field]  = +1;
            }
        }
        $this->impl = $this->impl->sort($fieldMap);
        return $this;
    }

    /**
     * @return array
     * @throws ConnectionException
     */
    public function toArray()
    {
        return iterator_to_array($this->impl);
    }

    /**
     * @return Traversable
     */
    public function getIterator()
    {
        return $this->impl;
    }

}
