<?php

namespace Menara\Generic\Storage\Mongo;

use Menara\Generic\Storage\Exceptions\ConnectionException;
use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\StorageException;
use Menara\Generic\Storage\Interfaces\IMongoClient;
use Menara\Generic\Storage\Interfaces\IMongoCursor;

class Client implements IMongoClient
{
    /**
     * @var string
     */
    public $dbPrefix = '';

    /**
     * @var \MongoClient
     */
    private $impl;

    /**
     * @param \MongoClient $impl
     */
    public function __construct(\MongoClient $impl)
    {
        $this->impl = $impl;
    }

    /**
     * @param int $timestamp
     * @return \MongoId
     */
    public function guidFromTimestamp($timestamp)
    {
        return Guid::guidFromTimestamp($timestamp);
    }

    /**
     * @param \MongoId|string $guid
     * @return int
     */
    public function timestampFromGuid($guid)
    {
        return Guid::timestampFromGuid($guid);
    }

    /**
     * If $existed is omitted generates new IMongoId,
     * otherwise it wraps $existed string into IMongoId
     *
     * @param String $existed
     * @return \MongoId
     */
    public function guid($existed = null)
    {
        return Guid::guid($existed);
    }

    /**
     * Inserts single document (writeConcern = default)
     *
     * @param string $collection
     * @param array $document
     * @return array
     * @throws StorageException
     */
    public function insertOne($collection, array $document)
    {
        $this->executeWrite(function() use ($collection, $document) {
            return $this->selectCollection($collection)->insert($document);
        });
        return $document;
    }

    /**
     * Creates or updates single document (upsert = true, multi = false, writeConcern = default)
     *
     * @param string $collection
     * @param array $document
     * @param array $criteria
     * @return array $document
     * @throws StorageException
     */
    public function upsertOne($collection, array $document, array $criteria)
    {
        $this->executeWrite(function() use ($collection, $document, $criteria) {
            return $this->selectCollection($collection)->update($criteria, $document, array(
                'upsert' => true,
                'multiple' => false,
            ));
        });
        return $document;
    }

    /**
     * Updates single document (upsert = false, multi = false, writeConcern = default)
     *
     * @param string $collection
     * @param array $document
     * @param array $criteria
     * @return array $document
     * @throws ItemNotFoundException
     */
    public function updateOne($collection, array $document, array $criteria)
    {
        $this->executeUpdateOne($collection, $document, $criteria);
        return $document;
    }

    /**
     * Partially updates single document (upsert = false, multi = false, writeConcern = default)
     *
     * @param string $collection
     * @param array $modifier
     * @param array $criteria
     * @return void
     * @throws ItemNotFoundException
     */
    public function modifyOne($collection, array $modifier, array $criteria)
    {
        $this->executeUpdateOne($collection, $modifier, $criteria);
    }

    /**
     * @param $collection
     * @param array $query
     * @param array $criteria
     * @return void
     * @throws ItemNotFoundException
     */
    private function executeUpdateOne($collection, array $query, array $criteria)
    {
        $response = $this->executeWrite(function() use ($collection, $query, $criteria) {
            return $this->selectCollection($collection)->update($criteria, $query, array(
                'upsert' => false,
                'multiple' => false,
            ));
        });
        if (1 !== (int)$response['updatedExisting']) {
            throw new ItemNotFoundException();
        }
    }

    /**
     * Finds single document
     *
     * @param string $collection
     * @param array $criteria
     * @param Array $fieldList
     * @return array
     * @throws ItemNotFoundException
     */
    public function findOne($collection, array $criteria, array $fieldList = null)
    {
        $document = $this->executeRead(function() use ($collection, $criteria, $fieldList) {
            return $this->selectCollection($collection)->findOne($criteria, $this->prepareFields($fieldList));
        });
        if (null === $document) {
            throw new ItemNotFoundException();
        }
        return $document;
    }

    /**
     * Finds list of documents by criteria
     *
     * @param string $collection
     * @param array $criteria
     * @param Array $fieldList
     * @return IMongoCursor
     */
    public function findAll($collection, array $criteria, array $fieldList = null)
    {
        $cursor = $this->executeRead(function() use ($collection, $criteria, $fieldList) {
            return $this->selectCollection($collection)->find($criteria, $this->prepareFields($fieldList));
        });
        return $this->decorateCursor($cursor);
    }

    /**
     * @param string $collection
     * @return string
     */
    public function dropCollection($collection)
    {
        $this->selectCollection($collection)->drop();
        return $collection;
    }

    /**
     * @param string $collection
     * @param array $definition
     * @return void
     */
    public function ensureIndexBackground($collection, $definition)
    {
        $this->selectCollection($collection)->ensureIndex($definition, array(
            'background' => true,
        ));
    }

    /**
     * @param string $collection
     * @return \MongoCollection
     * @throws ConnectionException
     */
    private function selectCollection($collection)
    {
        list($db, $collection) = explode('.', $collection);
        try {
            $this->impl->connect();
        }
        catch (\MongoConnectionException $e) {
            throw new ConnectionException($e->getMessage(), $e->getCode());
        }
        return $this->impl->selectCollection($this->dbPrefix . $db, $collection);
    }

    /**
     * @param callable $callable
     * @return mixed
     * @throws ItemAlreadyExistsException
     * @throws StorageException
     */
    private function executeWrite(callable $callable)
    {
        $response = $this->execute($callable);
        if (1 === (int)$response['ok']) {
            return $response;
        }
        $code = (int)$response['code'];
        if (in_array($code, [11000, 11001])) {
            throw new ItemAlreadyExistsException($response['errmsg'], $code);
        }
        throw new StorageException($response['errmsg'], $response['code']);
    }

    /**
     * @param callable $callable
     * @return mixed
     */
    private function executeRead(callable $callable)
    {
        return $this->execute($callable);
    }

    /**
     * @param callable $callable
     * @return mixed
     * @throws StorageException
     * @throws ConnectionException
     */
    private function execute(callable $callable)
    {
        try {
            return $callable();
        }
        catch (\MongoConnectionException $e) {
            throw new ConnectionException($e->getMessage(), $e->getCode());
        }
        catch (\MongoException $e) {
            if (in_array($e->getCode(), [11000, 11001])) {
                throw new ItemAlreadyExistsException($e->getMessage(), $e->getCode());
            }
            throw new StorageException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param array|null $fieldList
     * @return array|null
     */
    private function prepareFields($fieldList)
    {
        if (null === $fieldList) {
            return array();
        }
        return array_fill_keys($fieldList, 1);
    }

    /**
     * @param \MongoCursor $cursor
     * @return IMongoCursor
     */
    private function decorateCursor(\MongoCursor $cursor)
    {
        return new Cursor($cursor);
    }

}
