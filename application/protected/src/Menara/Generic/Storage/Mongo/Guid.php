<?php

namespace Menara\Generic\Storage\Mongo;

class Guid 
{
    /**
     * @param int $timestamp
     * @return \MongoId
     */
    public static function guidFromTimestamp($timestamp)
    {
        return self::guid($guid = sprintf('%08x%016x', $timestamp, 0));
    }

    /**
     * @param \MongoId|string $guid
     * @return int
     */
    public static function timestampFromGuid($guid)
    {
        if (is_string($guid)) {
            $guid = self::guid($guid);
        }
        return $guid->getTimestamp();
    }

    /**
     * If $existed is omitted generates new IMongoId,
     * otherwise it wraps $existed string into IMongoId
     *
     * @param String $existed
     * @return \MongoId
     */
    public static function guid($existed = null)
    {
        if (null !== $existed) {
            $existed = (string)$existed;
        }
        return new \MongoId($existed);
    }

}
