<?php

namespace Menara\Generic\Storage\Interfaces;

use Menara\Generic\Storage\Exceptions\ConnectionException;

interface IMongoCursor
{
    /**
     * @param int $limit
     * @return IMongoCursor
     */
    public function limit($limit);

    /**
     * @param string $field1
     * @param String $field2
     * @return IMongoCursor
     */
    public function order($field1, $field2 = null /*, $field3, ... */);

    /**
     * @return array
     * @throws ConnectionException
     */
    public function toArray();

}
