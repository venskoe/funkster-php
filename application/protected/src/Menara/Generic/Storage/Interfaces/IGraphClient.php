<?php

namespace Menara\Generic\Storage\Interfaces;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;

interface IGraphClient
{
    /**
     * @return \Everyman\Neo4j\Client
     */
    public function getImpl();

    /**
     * @param string $statement
     * @param array $kwargs
     * @return void
     */
    public function cypher($statement, array $kwargs = array());

    /**
     * @param string $statement
     * @param array $kwargs
     * @return \ArrayAccess
     * @throws ItemNotFoundException
     */
    public function cypherJson($statement, array $kwargs = array());

    /**
     * @param string $statement
     * @param array $kwargs
     * @return \ArrayAccess
     */
    public function cypherOne($statement, array $kwargs = array());

    /**
     * @param string $statement
     * @param array $kwargs
     * @return \Iterator|\Countable|\ArrayAccess
     */
    public function cypherAll($statement, array $kwargs = array());

    /**
     * @param string $label
     * @param array $config
     * @return void
     */
    public function createSpatialIndex($label, $config);

    /**
     * @param string $label
     * @return void
     */
    public function deleteSpatialIndex($label);

    /**
     * @param string $label
     * @param int $nodeId
     * @return void
     */
    public function upsertSpatialPoint($label, $nodeId);

}
