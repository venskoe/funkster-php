<?php

namespace Menara\Generic\Storage\Interfaces;

use Menara\Generic\Storage\Exceptions\ConnectionException;
use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Exceptions\StorageException;

interface IMongoClient
{
    /**
     * If $existed is omitted generates new IMongoId,
     * otherwise it wraps $existed string into IMongoId
     *
     * @param String $existed
     * @return \MongoId
     */
    public function guid($existed = null);

    /**
     * @param int $timestamp
     * @return \MongoId
     */
    public function guidFromTimestamp($timestamp);

    /**
     * @param \MongoId|string $guid
     * @return int
     */
    public function timestampFromGuid($guid);

    /**
     * Inserts single document (writeConcern = default)
     *
     * @param string $collection
     * @param array $document
     * @return array
     * @throws StorageException
     */
    public function insertOne($collection, array $document);

    /**
     * Creates or updates single document (upsert = true, multi = false, writeConcern = default)
     *
     * @param string $collection
     * @param array $document
     * @param array $criteria
     * @return array $document
     * @throws StorageException
     */
    public function upsertOne($collection, array $document, array $criteria);

    /**
     * Updates single document (upsert = false, multi = false, writeConcern = default)
     *
     * @param string $collection
     * @param array $document
     * @param array $criteria
     * @return array $query
     * @throws ItemNotFoundException
     * @throws ConnectionException
     */
    public function updateOne($collection, array $document, array $criteria);

    /**
     * Partially updates single document (upsert = false, multi = false, writeConcern = default)
     *
     * @param string $collection
     * @param array $modifier
     * @param array $criteria
     * @return void
     * @throws ItemNotFoundException
     * @throws ConnectionException
     */
    public function modifyOne($collection, array $modifier, array $criteria);

    /**
     * Finds single document
     *
     * @param string $collection
     * @param array $criteria
     * @param Array $fieldList
     * @return array
     * @throws ItemNotFoundException
     * @throws ConnectionException
     */
    public function findOne($collection, array $criteria, array $fieldList = null);

    /**
     * Finds list of documents by criteria
     *
     * @param string $collection
     * @param array $criteria
     * @param Array $fieldList
     * @return IMongoCursor
     * @throws ConnectionException
     */
    public function findAll($collection, array $criteria, array $fieldList = null);

    /**
     * @param string $collection
     * @return string
     */
    public function dropCollection($collection);

    /**
     * @param string $collection
     * @param array $definition
     * @return void
     */
    public function ensureIndexBackground($collection, $definition);

}
