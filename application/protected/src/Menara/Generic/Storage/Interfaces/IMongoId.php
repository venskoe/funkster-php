<?php

namespace Menara\Generic\Storage\Interfaces;

interface IMongoId
{
    /**
     * @return int
     */
    public function getTimestamp();

    /**
     * @return string
     */
    public function __toString();

}
