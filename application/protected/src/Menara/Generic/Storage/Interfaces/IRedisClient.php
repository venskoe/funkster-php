<?php

namespace Menara\Generic\Storage\Interfaces;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;

interface IRedisClient
{
    const MISSED = PHP_INT_MAX;

    /**
     * @param string $arg1
     * @param string $arg2
     * @return string
     */
    public function createKey($arg1, $arg2 = null /* $arg3, $arg4, ... */);

    /**
     * @param string $key1
     * @param string $key2
     * @return void
     */
    public function acquire($key1, $key2 = null /* $key3, $key4, ... */);

    /**
     * @return void
     */
    public function release();

    /**
     * @return void
     */
    public function begin();

    /**
     * @return bool
     */
    public function commit();

    /**
     * @param string $key
     * @return string
     */
    public function delete($key);

    /**
     * @param string $key
     * @param mixed $value
     * @param float $ttl
     * @return mixed
     */
    public function set($key, $value, $ttl = 0.0);

    /**
     * @param string $key
     * @param mixed|int $default
     * @return mixed
     * @throws ItemNotFoundException
     */
    public function get($key, $default = self::MISSED);

    /**
     * @param string[] $keyList
     * @return array
     */
    public function getAll(array $keyList);

    /**
     * @param array $dict
     * @return array
     */
    public function setAll(array $dict);

    /**
     * @param string $key
     * @param array $dict
     * @return array
     */
    public function hSetAll($key, array $dict);

    /**
     * @param string $key
     * @param int|array $default
     * @return array
     * @throws ItemNotFoundException
     */
    public function hGetAll($key, $default = self::MISSED);

    /**
     * @param string $key
     * @param string[] $fieldList
     * @return array
     */
    public function hGetSlice($key, array $fieldList);

    /**
     * @param string $key
     * @param string $field
     * @param mixed $value
     * @return mixed
     */
    public function hSet($key, $field, $value);

    /**
     * @param string $key
     * @param string $field
     * @param mixed|int $default
     * @return mixed
     * @throws ItemNotFoundException
     */
    public function hGet($key, $field, $default = self::MISSED);

    /**
     * @param string $key
     * @param string $field
     * @return string
     */
    public function hDelete($key, $field);

    /**
     * @param string[] $keyList
     * @return array[]
     */
    public function hGetAllByKeyList(array $keyList);

    /**
     * @param string $key
     * @param mixed $min
     * @param mixed $max
     * @return array
     */
    public function zGetRangeByScore($key, $min, $max);

    /**
     * @param string $key
     * @param mixed $min
     * @param mixed $max
     * @return string
     */
    public function zDeleteRangeByScore($key, $min, $max);

    /**
     * @param string $key
     * @param int $start
     * @param int $stop
     * @return string
     */
    public function zDeleteRangeByRank($key, $start, $stop);

    /**
     * @param string $targetKey
     * @param string[] $sourceKeyList
     * @return string
     */
    public function zUnion($targetKey, array $sourceKeyList);

    /**
     * @param string $key
     * @param int $score
     * @param mixed $value
     * @return mixed
     */
    public function zSet($key, $score, $value);

    /**
     * @param string $key
     * @param mixed $value
     * @return string
     */
    public function zDelete($key, $value);

    /**
     * @param string $key
     * @param array $dict ($score => $value)
     * @return array
     */
    public function zSetAll($key, array $dict);

    /**
     * @param string $targetKey
     * @param string[] $sourceKeyList
     * @param int $start
     * @param int $stop
     * @return string
     */
    public function zUnionRevRangeByRank($targetKey, array $sourceKeyList, $start, $stop);

    /**
     * @param string $key
     * @param int $offset
     * @param int $limit
     * @return mixed[]
     */
    public function zGetReversedRangeByMaxScore($key, $offset, $limit);

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function sAdd($key, $value);

    /**
     * @param string $key
     * @param mixed $value
     * @return string
     */
    public function sDelete($key, $value);

    /**
     * @param string $key
     * @return mixed[]
     */
    public function sGetAll($key);

}
