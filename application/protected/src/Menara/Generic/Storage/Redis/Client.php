<?php

namespace Menara\Generic\Storage\Redis;

use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IRedisClient;

class Client implements IRedisClient
{
    const DELIMITER = '.';

    /**
     * @var \Redis
     */
    private $impl;

    /**
     * @param \Redis $impl
     */
    public function __construct(\Redis $impl)
    {
        $this->impl = $impl;
    }

    /**
     * @param string $arg1
     * @param string $arg2
     * @return string
     */
    public function createKey($arg1, $arg2 = null /* $arg3, $arg4, ... */)
    {
        return implode(self::DELIMITER, func_get_args());
    }

    /**
     * @param string $key1
     * @param string $key2
     * @return void
     */
    public function acquire($key1, $key2 = null /* $key3, $key4, ... */)
    {
        $this->impl->watch(func_get_args());
    }

    /**
     * @return void
     */
    public function begin()
    {
        $this->impl->multi();
    }

    /**
     * @return bool
     */
    public function commit()
    {
        return $this->impl->exec();
    }

    /**
     * @return void
     */
    public function release()
    {
        $this->impl->unwatch();
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param float $ttl
     * @return mixed
     */
    public function set($key, $value, $ttl = 0.0)
    {
        $this->impl->set($key, $value, $ttl);
        return $value;
    }

    /**
     * @param string $key
     * @param mixed|int $default
     * @return mixed
     * @throws ItemNotFoundException
     */
    public function get($key, $default = self::MISSED)
    {
        $value = $this->impl->get($key);
        if (false === $value) {
            return $this->getDefault($default);
        }
        return $value;
    }

    /**
     * @param string $key
     * @return string
     */
    public function delete($key)
    {
        $this->impl->del($key);
        return $key;
    }

    /**
     * @param string[] $keyList
     * @return array
     */
    public function getAll(array $keyList)
    {
        return $this->impl->mget($keyList);
    }

    /**
     * @param array $dict
     * @return array
     */
    public function setAll(array $dict)
    {
        $this->impl->mset($dict);
        return $dict;
    }

    /**
     * @param string $key
     * @param array $dict
     * @return array
     */
    public function hSetAll($key, array $dict)
    {
        $this->impl->hMset($key, $dict);
        return $dict;
    }

    /**
     * @param string $key
     * @param int|array $default
     * @return array
     * @throws \Menara\Generic\Storage\Exceptions\ItemNotFoundException
     */
    public function hGetAll($key, $default = self::MISSED)
    {
        $value = $this->impl->hGetAll($key);
        if (empty($value)) {
            return $this->getDefault($default);
        }
        return $value;
    }

    /**
     * @param string $key
     * @param string[] $fieldList
     * @return array
     */
    public function hGetSlice($key, array $fieldList)
    {
        return $this->impl->hMGet($key, $fieldList);
    }

    /**
     * @param string $key
     * @param string $field
     * @param mixed $value
     * @return mixed
     */
    public function hSet($key, $field, $value)
    {
        $this->impl->hSet($key, $field, $value);
        return $value;
    }

    /**
     * @param string $key
     * @param string $field
     * @param mixed|int $default
     * @return mixed
     * @throws ItemNotFoundException
     */
    public function hGet($key, $field, $default = self::MISSED)
    {
        $value = $this->impl->hGet($key, $field);
        if (false === $value) {
            return $this->getDefault($default);
        }
        return $value;
    }

    /**
     * @param string $key
     * @param string $field
     * @return string
     */
    public function hDelete($key, $field)
    {
        $this->impl->hDel($key, $field);
        return $key;
    }

    /**
     * @param string[] $keyList
     * @return array[]
     */
    public function hGetAllByKeyList(array $keyList)
    {
        throw new \RuntimeException('Not implemented');
    }

    /**
     * @param string $key
     * @param mixed $min
     * @param mixed $max
     * @return array
     */
    public function zGetRangeByScore($key, $min, $max)
    {
        return $this->impl->zRangeByScore($key, $min, $max);
    }

    /**
     * @param string $key
     * @param mixed $min
     * @param mixed $max
     * @return string
     */
    public function zDeleteRangeByScore($key, $min, $max)
    {
        $this->impl->zRemRangeByScore($key, $min, $max);
        return $key;
    }

    /**
     * @param string $key
     * @param int $start
     * @param int $stop
     * @return string
     */
    public function zDeleteRangeByRank($key, $start, $stop)
    {
        $this->impl->zRemRangeByRank($key, $start, $stop);
        return $key;
    }

    /**
     * @param string $targetKey
     * @param string[] $sourceKeyList
     * @return string
     */
    public function zUnion($targetKey, array $sourceKeyList)
    {
        $this->impl->zUnion($targetKey, $sourceKeyList);
        return $targetKey;
    }

    /**
     * @param string $key
     * @param int $score
     * @param mixed $value
     * @return mixed
     */
    public function zSet($key, $score, $value)
    {
        $this->impl->zAdd($key, $score, $value);
        return $value;
    }

    /**
     * @param string $key
     * @param array $dict
     * @return array
     */
    public function zSetAll($key, array $dict)
    {
        $argumentList = [$key];
        foreach ($dict as $score => $value) {
            $argumentList[] = $score;
            $argumentList[] = $value;
        }
        call_user_func_array([$this->impl, 'zAdd'], $argumentList);
        return $dict;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return string
     */
    public function zDelete($key, $value)
    {
        $this->impl->zDelete($key, $value);
        return $key;
    }

    const LUA_Z_UNION_STORE_REV_RANGE_BY_RANK = <<<EOF
--  KEYS: destination key [key ...]
--  ARGV: start, stop

redis.call('DEL', KEYS[1])

for i = 2, #KEYS do
    local chunk = redis.call('ZREVRANGE', KEYS[i], ARGV[1], ARGV[2], 'WITHSCORES')
    for j = 1, #chunk, 2 do
        -- Hah, the inversion of ordering for scores and members in ZRANGE
        -- reading vs. writing bites us in the butt.
        chunk[j], chunk[j + 1] = chunk[j + 1], chunk[j]
    end
    redis.call('ZADD', KEYS[1], unpack(chunk))
end

return KEYS[1]
EOF;

    /**
     * @param string $targetKey
     * @param string[] $sourceKeyList
     * @param int $start
     * @param int $stop
     * @return string
     */
    public function zUnionRevRangeByRank($targetKey, array $sourceKeyList, $start, $stop)
    {
        $argumentList = array_merge([$targetKey], $sourceKeyList, [$start, $stop]);
        return $this->impl->eval(self::LUA_Z_UNION_STORE_REV_RANGE_BY_RANK, $argumentList, count($sourceKeyList) + 1);
    }

    /**
     * @param string $key
     * @param int $offset
     * @param int $limit
     * @return mixed[]
     */
    public function zGetReversedRangeByMaxScore($key, $offset, $limit)
    {
        throw new \RuntimeException('Not implemented');
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function sAdd($key, $value)
    {
        $this->impl->sAdd($key, $value);
        return $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return string
     */
    public function sDelete($key, $value)
    {
        $this->impl->sRemove($key, $value);
        return $key;
    }

    /**
     * @param string $key
     * @return mixed[]
     */
    public function sGetAll($key)
    {
        return $this->impl->sMembers($key);
    }

    /**
     * @param mixed|int $default
     * @return mixed
     * @throws ItemNotFoundException
     */
    private function getDefault($default = self::MISSED)
    {
        if (self::MISSED === $default) {
            throw new ItemNotFoundException();
        }
        return $default;
    }

}
