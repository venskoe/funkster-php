<?php

namespace Menara\Generic\Storage\Neo4j;

use Everyman\Neo4j\Cypher\Query as CypherQuery;
use Everyman\Neo4j\Exception;
use Everyman\Neo4j\Index;
use Everyman\Neo4j\Node;

use Menara\Generic\Storage\Exceptions\ItemAlreadyExistsException;
use Menara\Generic\Storage\Exceptions\ItemNotFoundException;
use Menara\Generic\Storage\Interfaces\IGraphClient;

class Client implements IGraphClient
{
    const MONGO_SCOPE_PREFIX = 'graph';

    /**
     * @var \Everyman\Neo4j\Client
     */
    private $client;

    /**
     * @param \Everyman\Neo4j\Client $client
     */
    public function __construct(\Everyman\Neo4j\Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return \Everyman\Neo4j\Client
     */
    public function getImpl()
    {
        return $this->client;
    }

    /**
     * @param string $statement
     * @param array $kwargs
     * @return void
     */
    public function cypher($statement, array $kwargs = array())
    {
        $this->executeCypher($statement, $kwargs);
    }

    /**
     * @param string $statement
     * @param array $kwargs
     * @return array
     * @throws ItemNotFoundException
     */
    public function cypherJson($statement, array $kwargs = array())
    {
        $ret = $this->cypherOne($statement, $kwargs);
        return $ret[0];
    }

    /**
     * @param string $statement
     * @param array $kwargs
     * @return \ArrayAccess
     * @throws ItemNotFoundException
     */
    public function cypherOne($statement, array $kwargs = array())
    {
        $list = $this->cypherAll($statement, $kwargs);
        if (count($list) < 1) {
            throw new ItemNotFoundException();
        }
        return $list[0];
    }

    /**
     * @param string $statement
     * @param array $kwargs
     * @return \Iterator|\Countable|\ArrayAccess
     */
    public function cypherAll($statement, array $kwargs = array())
    {
        return $this->executeCypher($statement, $kwargs);
    }

    /**
     * @param $statement
     * @param array $kwargs
     * @return \Everyman\Neo4j\Query\ResultSet
     * @throws Exception
     * @throws ItemAlreadyExistsException
     */
    private function executeCypher($statement, array $kwargs)
    {
        try {
            return (new CypherQuery($this->client, $statement, $kwargs))->getResultSet();
        }
        catch (Exception $e) {
            throw $this->handleException($e);
        }
    }

    /**
     * @param Exception $e
     * @return ItemAlreadyExistsException
     */
    private function handleException(Exception $e)
    {
        if (stripos($e->getMessage(), 'already exists') !== false) {
            return new ItemAlreadyExistsException();
        }
        return $e;
    }

    /**
     * @param string $label
     * @param array $config
     * @return void
     */
    public function createSpatialIndex($label, $config)
    {
        $this->createSpatialIndexImpl($label, $config)->save();
    }

    /**
     * @param string $label
     * @return void
     */
    public function deleteSpatialIndex($label)
    {
        $this->createSpatialIndexImpl($label)->delete();
    }

    /**
     * @param string $label
     * @param int $nodeId
     * @return void
     */
    public function upsertSpatialPoint($label, $nodeId)
    {
        $node = $this->createNodeImpl($nodeId);
        $index = $this->createSpatialIndexImpl($label);
        $index->remove($node);
        $index->add($this->createNodeImpl($nodeId), 'nodeId', $nodeId);
    }

    /**
     * @param string $label
     * @param array $config
     * @return Index
     */
    private function createSpatialIndexImpl($label, array $config = array())
    {
        return new Index($this->client, Index::TypeNode, $label, array_merge($config, array(
            'provider' => 'spatial',
        )));
    }

    /**
     * @param int $nodeId
     * @return \Everyman\Neo4j\PropertyContainer
     */
    private function createNodeImpl($nodeId)
    {
        return (new Node($this->client))->setId($nodeId);
    }

}
