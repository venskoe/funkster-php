<?php

namespace Menara\Generic\Interfaces;

interface IRequestAdapter
{
    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getArgument($name, $default);

}
