<?php

namespace Menara\Generic\Interfaces;

interface ICipher
{
    /**
     * @param String $data
     * @param object $config
     * @return String
     */
    public function encrypt($data, $config);

    /**
     * @param String $data
     * @param object $config
     * @return String
     */
    public function decrypt($data, $config);

}
