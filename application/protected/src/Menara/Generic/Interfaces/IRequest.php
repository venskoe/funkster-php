<?php

namespace Menara\Generic\Interfaces;

use Menara\Generic\Exceptions\HttpException;

interface IRequest
{
    const MISSED = PHP_INT_MAX;

    /**
     * @param string $name
     * @param bool|int $default
     * @return bool
     * @throws HttpException
     */
    public function getBoolean($name, $default = self::MISSED);

    /**
     * @param string $name
     * @param mixed $default
     * @return string
     * @throws HttpException
     */
    public function getString($name, $default = self::MISSED);

    /**
     * @param string $name
     * @param integer $default
     * @return integer
     * @throws HttpException
     */
    public function getTimestamp($name, $default = self::MISSED);

    /**
     * @param string $name
     * @param int $max
     * @return int
     * @throws HttpException
     */
    public function getLimit($name, $max);

    /**
     * @param string $name
     * @param integer $default
     * @return integer
     * @throws HttpException
     */
    public function getInteger($name, $default = self::MISSED);

    /**
     * @param string $name
     * @param float|int $default
     * @return float
     * @throws HttpException
     */
    public function getFloat($name, $default = self::MISSED);

    /**
     * @param string $name
     * @param int[]|int $default
     * @return array
     * @throws HttpException
     */
    public function getListOfInteger($name, $default = self::MISSED);

    /**
     * @param string $name
     * @param string[]|int $default
     * @return array
     * @throws HttpException
     */
    public function getListOfString($name, $default = self::MISSED);

    /**
     * @param string $name
     * @param mixed|int $default
     * @return mixed
     * @throws HttpException
     */
    public function getJson($name, $default = self::MISSED);

}
