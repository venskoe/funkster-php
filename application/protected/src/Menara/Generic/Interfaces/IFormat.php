<?php

namespace Menara\Generic\Interfaces;

interface IFormat
{
    /**
     * @param mixed $data
     * @return string
     */
    public function encode($data);

    /**
     * @param string $dataString
     * @return mixed
     */
    public function decode($dataString);

}
