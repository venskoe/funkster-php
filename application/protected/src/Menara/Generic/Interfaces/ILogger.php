<?php

namespace Menara\Generic\Interfaces;

interface ILogger 
{
    /**
     * @param \Exception $e
     * @param String $message
     * @param array $params
     */
    public function exception(\Exception $e, $message = null, $params = array());

    /**
     * @param string $message
     * @param array $params
     */
    public function trace($message, $params = array());

    /**
     * @param string $message
     * @param array $params
     */
    public function error($message, $params = array());

    /**
     * @param string $message
     * @param array $params
     */
    public function info($message, $params = array());

}
