<?php

namespace Menara\Generic\Phalcon\Listener;

use Phalcon\Annotations\Annotation;
use Phalcon\Mvc\Dispatcher;

class Injector extends Annotations
{
    /**
     * @var array
     */
    private static $nameToMethodMap = array(
        'InjectAttr' => 'createAttrInjector',
    );

    /**
     * @param mixed $_
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function afterExecuteRoute($_, Dispatcher $dispatcher)
    {
        $response = $dispatcher->getReturnedValue();
        $collection = $this->getMethodAnnotationCollection(
            $dispatcher->getHandlerClass(),
            $dispatcher->getActionName()
        );
        $callableList = $this->createCallableInjectorList($collection);
        foreach ($callableList as $callable) {
            $response = call_user_func_array($callable, [$response, $dispatcher]);
        }
        $dispatcher->setReturnedValue($response);
        return true;
    }

    /**
     * @param Annotation[] $collection
     * @return array
     */
    private function createCallableInjectorList($collection)
    {
        $callableList = [];
        foreach ($collection as $annotation) {
            $name = $annotation->getName();
            if (!isset(self::$nameToMethodMap[$name])) {
                continue;
            }
            $arguments = $annotation->getArguments();
            $callableList[] = call_user_func_array([$this, self::$nameToMethodMap[$name]], $arguments);
        }
        return $callableList;
    }

    /**
     * @param string $attrName
     * @param string $methodName
     * @return callable
     */
    private function createAttrInjector($attrName, $methodName)
    {
        return function($response, Dispatcher $dispatcher) use ($attrName, $methodName) {
            $controller = $dispatcher->getActiveController();
            $params = $dispatcher->getParams();
            $response->{$attrName} = call_user_func_array([$controller, $methodName], $params);
            return $response;
        };
    }

}
