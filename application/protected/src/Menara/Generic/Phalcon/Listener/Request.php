<?php

namespace Menara\Generic\Phalcon\Listener;

use Phalcon\Mvc\Dispatcher;

class Request extends Base
{
    /**
     * @param mixed $_
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeExecuteRoute($_, Dispatcher $dispatcher)
    {
        $request = $this->getDI()->get('\Menara\Generic\Interfaces\IRequest', [
            $dispatcher->getParams(),
            $this->getDI()->get('request'),
        ]);
        $dispatcher->setParams(array('request' => $request));
        return true;
    }

}
