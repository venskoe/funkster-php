<?php

namespace Menara\Generic\Phalcon\Listener;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IFormat;

use Phalcon\Annotations\Annotation;
use Phalcon\Dispatcher;

use Phalcon\Http\ResponseInterface;
use Menara\Web\Interfaces\IProtocol;

class Response extends Annotations
{
    const EXCEPT = 'Except';

    /**
     * @var array
     */
    private static $rendererNameToMethodMap = array(
        'RenderJson' => 'createJsonRenderer',
        'RenderView' => 'createViewRenderer',
    );

    /**
     * @var array
     */
    private static $modifierNameToMethodMap = array(
        'Header' => 'createHeaderModifier',
    );

    /**
     * @var IProtocol
     */
    public $protocol;

    /**
     * @var IFormat
     */
    public $format;

    /**
     * @param $_
     * @param Dispatcher $dispatcher
     * @param \Exception $exception
     * @return bool
     */
    public function beforeException($_, Dispatcher $dispatcher, \Exception $exception)
    {
        $collection = $this->getMethodAnnotationCollection(
            $dispatcher->getHandlerClass(),
            $dispatcher->getActionName()
        );
        foreach ($collection->getAll(self::EXCEPT) as $annotation) {
            if (is_a($exception, $annotation->getArgument(0))) {
                $exception = new HttpException($annotation->getArgument(1), $annotation->getArgument(2));
                break;
            }
        }
        if ($exception instanceof HttpException) {
            if ($collection->has('RenderView')) {
                $response = $this->getDI()->get('view')->render('error', array('error' => $exception));
            }
            elseif ($collection->has('RenderJson')) {
                $response = $this->protocol->jsonifyFailureResponse($exception->statusCode, $exception->statusMessage);
                $response = $this->format->encode($response);
            }
            $response = $this->getDI()->get('response', [$response, $exception->statusCode, $exception->statusMessage]);
            array_walk($exception->headers, function($value, $name) use ($response) {
                $response->setHeader($name, $value);
            });
        }
        else {
            $response = $this->getDI()->get('response', [$exception->getMessage(), 500, $exception->getMessage()]);
        }
        $dispatcher->setReturnedValue($response);
        return true;
    }

    /**
     * @param $_
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function afterExecuteRoute($_, Dispatcher $dispatcher)
    {
        $response = $dispatcher->getReturnedValue();
        $collection = $this->getMethodAnnotationCollection(
            $dispatcher->getHandlerClass(),
            $dispatcher->getActionName()
        );
        $renderer = $this->createCallableRenderer($collection);
        $response = call_user_func_array($renderer, [$response]);
        $response = $this->getDI()->get('response', [$response, 200, 'OK']);
        $modifierList = $this->createModifierCallableList($collection);
        foreach ($modifierList as $modifier) {
            $response = $modifier($response);
        }
        $dispatcher->setReturnedValue($response);
        return true;
    }

    /**
     * @param Annotation[] $collection
     * @return array
     * @throws \RuntimeException
     */
    private function createCallableRenderer($collection)
    {
        foreach ($collection as $annotation) {
            $name = $annotation->getName();
            if (isset(self::$rendererNameToMethodMap[$name])) {
                $arguments = $annotation->getArguments();
                if (null === $arguments) {
                    $arguments = [];
                }
                return call_user_func_array([$this, self::$rendererNameToMethodMap[$name]], $arguments);
            }
        }
        throw new \RuntimeException('Renderer not found. Options: ' . print_r(array_keys(self::$rendererNameToMethodMap)), true);
    }

    /**
     * @return callable
     */
    private function createJsonRenderer()
    {
        return [$this->format, 'encode'];
    }

    /**
     * @param string $template
     * @return callable
     */
    private function createViewRenderer($template)
    {
        return function($response) use ($template) {
            return $this->getDI()->get('view')->render($template, $response);
        };
    }

    /**
     * @param Annotation[] $collection
     * @return callable[]
     */
    private function createModifierCallableList($collection)
    {
        $callableList = [];
        foreach ($collection as $annotation) {
            $name = $annotation->getName();
            if (!isset(self::$modifierNameToMethodMap[$name])) {
                continue;
            }
            $arguments = $annotation->getArguments();
            $callableList[] = call_user_func_array([$this, self::$modifierNameToMethodMap[$name]], $arguments);
        }
        return $callableList;
    }

    /**
     * @param string $name
     * @param string $value
     * @return callable
     */
    private function createHeaderModifier($name, $value)
    {
        return function(ResponseInterface $response) use ($name, $value) {
            $response->setHeader($name, $value);
            return $response;
        };
    }

}
