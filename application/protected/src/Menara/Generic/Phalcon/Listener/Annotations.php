<?php

namespace Menara\Generic\Phalcon\Listener;

abstract class Annotations extends Base
{
    /**
     * @param string $className
     * @param string $methodName
     * @return \Phalcon\Annotations\Collection
     * @throws \RuntimeException
     */
    protected function getMethodAnnotationCollection($className, $methodName)
    {
        return $this->getAnnotations()->getMethod($className, $methodName);
    }

    /**
     * @return \Phalcon\Annotations\AdapterInterface
     */
    private function getAnnotations()
    {
        return $this->getDI()->get('annotations');
    }

}
