<?php

namespace Menara\Generic\Phalcon\Listener;

use Phalcon\Annotations\Annotation;
use Phalcon\Dispatcher;

use Menara\Web\Interfaces\IProtocol;

class Protocol extends Annotations
{
    /**
     * @var array
     */
    private static $nameToMethodMap = array(
        'JsonifyItem' => 'createItemJsonifier',
        'JsonifyList' => 'createListJsonifier',
        'JsonifyDict' => 'createDictJsonifier',
    );

    /**
     * @var IProtocol
     */
    public $protocol;

    /**
     * @param mixed $_
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function afterExecuteRoute($_, Dispatcher $dispatcher)
    {
        $response = $dispatcher->getReturnedValue();
        $collection = $this->getMethodAnnotationCollection(
            $dispatcher->getHandlerClass(),
            $dispatcher->getActionName()
        );
        $callableList = $this->createCallableJsonifierList($collection);
        foreach (array_reverse($callableList) as $callable) {
            $response = call_user_func_array($callable, [$response]);
        }
        $dispatcher->setReturnedValue($response);
        return true;
    }

    /**
     * @param Annotation[] $collection
     * @return array
     */
    private function createCallableJsonifierList($collection)
    {
        $callableList = [];
        foreach ($collection as $annotation) {
            $name = $annotation->getName();
            if (!isset(self::$nameToMethodMap[$name])) {
                continue;
            }
            $arguments = $annotation->getArguments();
            $callableList[] = call_user_func_array([$this, self::$nameToMethodMap[$name]], $arguments);
        }
        return $callableList;
    }

    /**
     * @param string $methodName
     * @return callable
     */
    private function createItemJsonifier($methodName)
    {
        return [$this->getProtocol(), $methodName];
    }

    /**
     * @param string $methodName
     * @return callable
     */
    private function createListJsonifier($methodName)
    {
        return function($itemList) use ($methodName) {
            return array_map([$this->getProtocol(), $methodName], $itemList);
        };
    }

    /**
     * @param string $methodName
     * @return callable
     */
    private function createDictJsonifier($methodName)
    {
        return function($itemList) use ($methodName) {
            $keyList = array_keys($itemList);
            $valueList = array_map([$this->getProtocol(), $methodName], array_values($itemList));
            return array_combine($keyList, $valueList);
        };
    }

    /**
     * @return IProtocol
     */
    private function getProtocol()
    {
        return $this->protocol;
    }

}
