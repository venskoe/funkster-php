<?php

namespace Menara\Generic\Phalcon\Listener;

use Phalcon\DI\InjectionAwareInterface;
use Phalcon\DiInterface;

abstract class Base implements InjectionAwareInterface
{
    /**
     * @var DiInterface
     */
    private $di;

    /**
     * @param DiInterface $di
     */
    public function setDI($di)
    {
        $this->di = $di;
    }

    /**
     * @return DiInterface
     */
    public function getDI()
    {
        return $this->di;
    }

}
