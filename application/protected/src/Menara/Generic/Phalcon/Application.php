<?php

namespace Menara\Generic\Phalcon;

class Application
{
    /**
     * @var \Phalcon\DI
     */
    public $di;

    /**
     * @param \Phalcon\DI $di
     */
    public function __construct(\Phalcon\DI $di)
    {
        $this->di = $di;
    }

    /**
     * @param string $dir1
     * @param string $dir2
     */
    public function initializeClassLoader($dir1, $dir2 = null)
    {
        $loader = new \Phalcon\Loader();
        $loader->registerDirs(func_get_args());
        $loader->register();
    }

    /**
     * @param string $root
     * @param string $environment
     * @param string $target
     */
    public function initializeSettings($root, $environment, $target)
    {
        $config = new \Phalcon\Config(array());
        foreach ($this->getPathList($root, $environment, $target) as $filename) {
            $params = include($filename);
            $config->merge(new \Phalcon\Config($params));
        }
        $this->di->setShared('config', $config);
    }

    /**
     * @param string $root
     * @param string $environment
     * @param string $target
     */
    public function initializeIoc($root, $environment, $target)
    {
        foreach ($this->getPathList($root, $environment, $target) as $filename) {
            $callable = include($filename);
            $this->di = call_user_func_array($callable, [$this->di]);
        }
    }

    /**
     * @param string $root
     * @param string $environment
     * @param string $target
     * @return string[]
     */
    private function getPathList($root, $environment, $target)
    {
        return [
            $root . '/config/' . $target . '.php',
            $root . '/config/' . $environment . '/' . $target . '.php',
        ];
    }

    /**
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @param array $matches
     * @param array $params
     * @return \Phalcon\Mvc\Dispatcher
     */
    public function dispatch(\Phalcon\Mvc\Dispatcher $dispatcher, array $matches, array $params)
    {
        $dispatcher->setControllerSuffix('');
        $dispatcher->setActionSuffix('');
        $dispatcher->setDefaultNamespace($matches['namespace']);
        $dispatcher->setControllerName($matches['controller'], true);
        $dispatcher->setActionName($matches['action']);
        $dispatcher->setParams($params);
        $dispatcher->dispatch();
        return $dispatcher;
    }

}
