<?php

namespace Menara\Generic\Phalcon\Application;

require_once(__DIR__ . '/../Application.php');

use Menara\Generic\Phalcon\Application;

class Web
{
    /**
     * @var \Menara\Generic\Phalcon\Application
     */
    private $application;

    /**
     * @param \Phalcon\DI $di
     */
    public function __construct(\Phalcon\DI $di)
    {
        $this->application = new Application($di);
    }

    /**
     * @param string $root
     * @param string $environment
     * @return $this
     */
    public function initialize($root, $environment)
    {
        $this->initializeExceptionHandler();
        $this->application->initializeClassLoader($root . '/src');
        $this->application->initializeSettings($root, $environment, 'settings');
        $this->application->initializeIoc($root, $environment, 'ioc-web');
        return $this;
    }

    /**
     * @TODO: It's impossible to interrupt dispatch events chain without throwing an exception
     * @TODO: So we properly handle it via ExceptionListener and then catch it here
     */
    private function initializeExceptionHandler()
    {
        set_exception_handler(function(\Exception $exception) {
            $this->sendResponse($this->getDispatcher());

            error_log($exception);
            error_log($exception->getTraceAsString());
        });
    }

    /**
     * @return $this
     * @throws \Menara\Generic\Exceptions\HttpException
     */
    public function handle()
    {
        $router = $this->getRouter();
        $router->handle();
        if (!$router->wasMatched()) {
            $response = $this->application->di->get('view')->render('error', array('error' => (object)[
                'statusCode' => 404,
                'statusMessage' => 'Page not found'
            ]));
            $this->createResponse($response, 404, 'Page not found')->send();
            return $this;
        }
        $matches = $router->getMatchedRoute()->getPaths();

        $dispatcher = $this->getDispatcher();
        $dispatcher = $this->injectDispatcherEventsManager($dispatcher);
        $dispatcher = $this->application->dispatch($dispatcher, $matches, $router->getParams());

        return $this->sendResponse($dispatcher);
    }

    /**
     * @return \Phalcon\Mvc\RouterInterface
     */
    private function getRouter()
    {
        return $this->application->di->get('router');
    }

    /**
     * @return \Phalcon\Mvc\Dispatcher
     */
    private function getDispatcher()
    {
        return $this->application->di->get('dispatcher');
    }

    /**
     * @param \Phalcon\Dispatcher $dispatcher
     * @return $this
     */
    private function sendResponse(\Phalcon\Dispatcher $dispatcher)
    {
        $response = $dispatcher->getReturnedValue();
        if (null === $response) {
            $response = $this->createResponse('Internal Server Error', 500, 'Internal Server Error');
        }
        elseif (!($response instanceof \Phalcon\Http\ResponseInterface)) {
            $response = $this->createResponse('Invalid Response', 500, 'Invalid Response');
        }
        $response->send();
        return $this;
    }

    /**
     * @param string $content
     * @param int $code
     * @param string $message
     * @return \Phalcon\Http\ResponseInterface
     */
    private function createResponse($content, $code, $message)
    {
        return $this->application->di->get('response', [$content, $code, $message]);
    }

    /**
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @return \Phalcon\Mvc\Dispatcher
     */
    private function injectDispatcherEventsManager(\Phalcon\Mvc\Dispatcher $dispatcher)
    {
        $events = new \Phalcon\Events\Manager();
        $events->attach('dispatch', $this->application->di->get('\Menara\Generic\Phalcon\Listener\Request'));
        $events->attach('dispatch', $this->application->di->get('\Menara\Generic\Phalcon\Listener\Injector'));
        $events->attach('dispatch', $this->application->di->get('\Menara\Generic\Phalcon\Listener\Protocol'));
        $events->attach('dispatch', $this->application->di->get('\Menara\Generic\Phalcon\Listener\Response'));
        $dispatcher->setEventsManager($events);
        return $dispatcher;
    }

}
