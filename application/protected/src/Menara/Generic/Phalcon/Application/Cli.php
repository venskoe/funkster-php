<?php

namespace Menara\Generic\Phalcon\Application;

require_once(__DIR__ . '/../Application.php');

use Menara\Generic\Phalcon\Application;

class Cli
{
    /**
     * @var \Menara\Generic\Phalcon\Application
     */
    private $application;

    /**
     * @param \Phalcon\DI $di
     */
    public function __construct(\Phalcon\DI $di)
    {
        $this->application = new Application($di);
    }

    /**
     * @param string $root
     * @param string $environment
     * @return $this
     */
    public function initialize($root, $environment)
    {
        $this->application->initializeClassLoader($root . '/src');
        $this->application->initializeSettings($root, $environment, 'settings');
        $this->application->initializeIoc($root, $environment, 'ioc-cli');
        return $this;
    }

    /**
     * @param string $command
     * @param array $arguments
     * @return $this
     * @throws \RuntimeException
     */
    public function handle($command, $arguments)
    {
        $this->application->di->setShared('argv[0]', $command);
        $command = implode(' ', array_slice($arguments, 1));

        $router = $this->getRouter();
        $router->handle($command);
        if (!$router->wasMatched()) {
            throw new \RuntimeException('Command not found: ' . $command);
        }
        $matches = $router->getMatchedRoute()->getPaths();

        $dispatcher = $this->getDispatcher();
        $dispatcher = $this->injectDispatcherEventsManager($dispatcher);
        $dispatcher = $this->application->dispatch($dispatcher, $matches, $router->getParams());
    }

    /**
     * @return \Phalcon\Mvc\RouterInterface
     */
    private function getRouter()
    {
        return $this->application->di->get('router');
    }

    /**
     * @return \Phalcon\Mvc\Dispatcher
     */
    private function getDispatcher()
    {
        return $this->application->di->get('dispatcher');
    }

    /**
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @return \Phalcon\Mvc\Dispatcher
     */
    private function injectDispatcherEventsManager(\Phalcon\Mvc\Dispatcher $dispatcher)
    {
        $events = new \Phalcon\Events\Manager();
        $events->attach('dispatch', $this->application->di->get('\Menara\Generic\Phalcon\Listener\Request'));
        $dispatcher->setEventsManager($events);
        return $dispatcher;
    }

}
