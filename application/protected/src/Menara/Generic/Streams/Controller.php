<?php

namespace Menara\Generic\Streams;

use Evenement\EventEmitterInterface;

use Menara\Generic\Interfaces\ILogger;
use Menara\Generic\Console\Interfaces\IController;
use Menara\Generic\Streams\Exceptions\StreamsException;
use Menara\Generic\Streams\Interfaces\IListener;
use Menara\Generic\Streams\Interfaces\IObserver;
use Menara\Generic\Streams\Interfaces\IProtocol;

class Controller implements IController
{
    /**
     * @var IListener
     */
    public $listener;

    /**
     * @var IProtocol
     */
    public $protocol;

    /**
     * @var IObserver
     */
    public $observer;

    /**
     * @var ILogger
     */
    public $logger;

    /**
     * @return void
     * @throws StreamsException
     */
    public function setup()
    {
        $this->listener->listen()->then([$this, 'listen'], function(\Exception $e) {
            throw new StreamsException($e->getMessage(), $e->getCode());
        });
    }

    /**
     * @param EventEmitterInterface $stream
     */
    public function listen(EventEmitterInterface $stream)
    {
        $stream->on('message', function($message) {
            $this->logger->trace('Handling message: :message', array(':message' => $message));
            $this->handleMessage($message);
        });
        $stream->on('error', function(\Exception $e) {
            $this->logger->exception($e);
        });
        $stream->on('close', function() {
            $this->logger->info('Connection closed');
        });
        $stream->on('end', function() {
            $this->logger->trace('Something done...');
        });
    }

    /**
     * @param string $message
     */
    private function handleMessage($message)
    {
        try {
            $package = $this->protocol->decode($message);
            $this->logger->trace('Decoded package: :package', array(':package' => $package));
            $this->observer->publish($package);
        }
        catch (\Exception $e) {
            $this->logger->exception($e);
        }
    }

    /**
     * @return void
     */
    public function tick()
    {
        // pass
    }

}
