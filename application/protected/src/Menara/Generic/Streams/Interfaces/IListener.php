<?php

namespace Menara\Generic\Streams\Interfaces;

use React\Promise\PromiseInterface;

interface IListener
{
    /**
     * @return PromiseInterface
     */
    public function listen();

}
