<?php

namespace Menara\Generic\Streams\Interfaces;

interface IObserver
{
    /**
     * @param IPackage $package
     * @return IPackage
     */
    public function publish(IPackage $package);

}
