<?php

namespace Menara\Generic\Streams\Interfaces;

interface IProtocol
{
    /**
     * @param string $message
     * @return IPackage
     */
    public function decode($message);

}
