<?php

namespace Menara\Generic\Streams\Interfaces;

interface IPackage
{
    /**
     * @return array
     */
    public function getHeaders();

    /**
     * @return object
     */
    public function getContent();

}
