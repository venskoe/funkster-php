<?php

namespace Menara\Generic\Streams\Listeners;

use React\EventLoop\LoopInterface;
use React\Promise\PromiseInterface;
use Socket\React\Datagram;

use Menara\Generic\Streams\Interfaces\IListener;

class Udp implements IListener
{
    const ADDRESS_TEMPLATE = '{host}:{port}';

    /**
     * @param string $host
     * @param int $port
     * @param LoopInterface $loop
     */
    public function __construct(LoopInterface $loop, $host, $port)
    {
        $this->loop = $loop;
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * @return PromiseInterface
     */
    public function listen()
    {
        return (new Datagram\Factory($this->loop))->createServer($this->createDsn());
    }

    /**
     * @return string
     */
    private function createDsn()
    {
        return strtr(self::ADDRESS_TEMPLATE, array(
            '{host}' => $this->host,
            '{port}' => $this->port,
        ));
    }

}
