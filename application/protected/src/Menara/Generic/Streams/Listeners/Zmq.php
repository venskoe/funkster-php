<?php

namespace Menara\Generic\Streams\Listeners;

use React\EventLoop\LoopInterface;
use React\Promise\FulfilledPromise;
use React\Promise\PromiseInterface;
use React\ZMQ\Context as ZmqContext;

use Menara\Generic\Streams\Interfaces\IListener;

class Zmq implements IListener
{
    const ADDRESS_TEMPLATE = 'tcp://{host}:{port}';

    /**
     * @param LoopInterface $loop
     * @param string $host
     * @param string $port
     * @param string $channel
     * @param string $identity
     */
    public function __construct(LoopInterface $loop, $host, $port, $channel, $identity)
    {
        $this->loop = $loop;
        $this->host = $host;
        $this->port = $port;
        $this->channel = $channel;
        $this->identity = $identity;
    }

    /**
     * @return PromiseInterface
     */
    public function listen()
    {
        $context = new ZmqContext($this->loop);
        $socket = $context->getSocket(\ZMQ::SOCKET_SUB, $this->identity);
        $socket->setSockOpt(\ZMQ::SOCKOPT_IDENTITY, $this->identity);
        $socket->subscribe($this->channel);
        $socket->connect($this->createDsn());
        return new FulfilledPromise($socket);
    }

    /**
     * @return string
     */
    private function createDsn()
    {
        return strtr(self::ADDRESS_TEMPLATE, array(
            '{host}' => $this->host,
            '{port}' => $this->port,
        ));
    }

}
