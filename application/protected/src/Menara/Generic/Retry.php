<?php

namespace Menara\Generic;

class Retry 
{
    /**
     * @var int
     */
    private $totalAttemptCount;

    /**
     * @var string
     */
    private $retryExceptionClass;

    /**
     * @param int $totalAttemptCount
     * @param string $retryExceptionClass
     */
    public function __construct($totalAttemptCount, $retryExceptionClass)
    {
        $this->totalAttemptCount = $totalAttemptCount;
        $this->retryExceptionClass = $retryExceptionClass;
    }

    /**
     * @param callable $func
     * @return mixed
     */
    public function run(callable $func)
    {
        return $this->runAttempt($this->totalAttemptCount, $func);
    }

    /**
     * @param int $attemptCountLeft
     * @param callable $func
     * @return mixed
     * @throws \Exception
     */
    private function runAttempt($attemptCountLeft, callable $func)
    {
        try {
            return $func();
        }
        catch (\Exception $e) {
            if ($attemptCountLeft === 0) {
                throw $e;
            }
            if (get_class($e) !== $this->retryExceptionClass) {
                throw $e;
            }
            return $this->runAttempt($attemptCountLeft - 1, $func);
        }
    }

}
