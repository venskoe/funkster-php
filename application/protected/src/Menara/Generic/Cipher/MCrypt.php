<?php

namespace Menara\Generic\Cipher;

use Menara\Generic\Interfaces\ICipher;

class MCrypt implements ICipher
{
    /**
     * @param String $data
     * @param object $config
     * @return String
     */
    public function encrypt($data, $config)
    {
        if (null === $data) {
            return null;
        }
        return bin2hex($this->executeCryptMethod('encrypt', $data, $config));
    }

    /**
     * @param String $data
     * @param object $config
     * @return String
     */
    public function decrypt($data, $config)
    {
        if (null === $data) {
            return null;
        }
        return $this->executeCryptMethod('decrypt', hex2bin($data), $config);
    }

    /**
     * @param string $method
     * @param string $data
     * @param object $config
     * @return string
     */
    private function executeCryptMethod($method, $data, $config)
    {
        return call_user_func_array('mcrypt_' . $method, [
            $config->cipher,
            $config->key,
            $data,
            $config->mode,
            base64_decode($config->iv),
        ]);
    }

}
