<?php

namespace Menara\Generic\Implementations;

use Menara\Generic\Streams\Interfaces\IPackage;

class Package implements IPackage
{
    /**
     * @param object $content
     * @param array $headers
     */
    public function __construct($content, $headers = array())
    {
        $this->content = $content;
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return object
     */
    public function getContent()
    {
        return $this->content;
    }

}
