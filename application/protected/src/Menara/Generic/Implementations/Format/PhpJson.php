<?php

namespace Menara\Generic\Implementations\Format;

use Menara\Generic\Interfaces\IFormat;

class PhpJson implements IFormat
{
    /**
     * @param mixed $data
     * @return string
     */
    public function encode($data)
    {
        return json_encode($data);
    }

    /**
     * @param string $dataString
     * @return mixed
     */
    public function decode($dataString)
    {
        return json_decode($dataString);
    }

}
