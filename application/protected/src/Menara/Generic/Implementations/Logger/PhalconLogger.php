<?php

namespace Menara\Generic\Implementations\Logger;

use Phalcon\Logger;

use Menara\Generic\Interfaces\ILogger;

class PhalconLogger implements ILogger
{
    /**
     * @var Logger\AdapterInterface
     */
    private $impl;

    /**
     * @param Logger\AdapterInterface $impl
     */
    public function __construct(Logger\AdapterInterface $impl)
    {
        $this->impl = $impl;
    }

    /**
     * @param \Exception $e
     * @param String $message
     * @param array $params
     */
    public function exception(\Exception $e, $message = null, $params = array())
    {
        if (null !== $message) {
            $this->error($message, $params);
        }
        $this->error($e->getMessage());
        $this->trace($e->getTraceAsString());
    }

    /**
     * @param string $message
     * @param array $params
     */
    public function trace($message, $params = array())
    {
        $this->impl->notice($message, $params);
    }

    /**
     * @param string $message
     * @param array $params
     */
    public function error($message, $params = array())
    {
        $this->impl->error($message, $params);
    }

    /**
     * @param string $message
     * @param array $params
     */
    public function info($message, $params = array())
    {
        $this->impl->info($message, $params);
    }

}
