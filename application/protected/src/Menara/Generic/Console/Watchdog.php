<?php

namespace Menara\Generic\Console;

use Menara\Generic\Console\Exceptions\WatchdogException;
use Menara\Generic\Console\Interfaces\IWatchdog;
use Menara\Generic\Interfaces\ILogger;

class Watchdog implements IWatchdog
{
    const TEMPLATE_GET_PID_LIST = 'process=$(pgrep -lf ":php :command$"|awk \'$2 == ":php" {print $1}\'); echo $process';
    const TEMPLATE_START = 'nohup :php :command >/dev/null & echo $!';
    const TEMPLATE_STOP = 'kill :pid';

    const SLEEP_SECONDS = 1;

    /**
     * @var ILogger
     */
    public $logger;

    /**
     * @var string
     */
    private $command;

    /**
     * @var int
     */
    private $processCount;

    /**
     * @var string
     */
    private $php;

    public function __construct()
    {
        $this->php = exec('which php');
    }

    /**
     * @param string $command
     * @param int $processCount
     * @return IWatchdog
     */
    public function initialize($command, $processCount = 1)
    {
        $this->command = $command;
        $this->processCount = $processCount;
    }

    /**
     * @return bool
     */
    public function watch()
    {
        while (count($this->getOnlineProcessList()) < $this->processCount) {
            $this->start();
        }
    }

    /**
     * @return void
     */
    public function restart()
    {
        $this->stop();
        $this->start();
    }

    /**
     * @return void
     * @throws WatchdogException
     */
    public function stop()
    {
        $pidList = $this->getOnlineProcessList();
        foreach ($pidList as $pid) {
            $this->execute(self::TEMPLATE_STOP, self::SLEEP_SECONDS, array(':pid' => $pid));
        }
        if (count($this->getOnlineProcessList()) > 0) {
            throw new WatchdogException('Not all processes stopped properly');
        }
    }

    /**
     * @return void
     * @throws WatchdogException
     */
    public function start()
    {
        if ($this->processCount <= count($this->getOnlineProcessList())) {
            throw new WatchdogException('Maximum process count exceeded');
        }
        while ($this->processCount > count($this->getOnlineProcessList())) {
            $this->execute(self::TEMPLATE_START, self::SLEEP_SECONDS);
        }
    }

    /**
     * @return int[]
     */
    private function getOnlineProcessList()
    {
        $pidList = $this->execute(self::TEMPLATE_GET_PID_LIST);
        $pidList = explode(' ', $pidList);
        $pidList = array_map('intval', $pidList);
        $pidList = array_filter($pidList);
        return $pidList;
    }

    /**
     * @param string $commandTemplate
     * @param int $sleep
     * @param array $params
     * @return string
     */
    private function execute($commandTemplate, $sleep = 0, $params = array())
    {
        $params = array_merge(array(':php' => $this->php, ':command' => $this->command), $params);
        $command = strtr($commandTemplate, $params);
        $this->logger->info('Execution: {command}', array('command' => $command));
        $ret = exec($command);
        sleep($sleep);
        $this->logger->info('Result: {ret}', array('ret' => $ret));
        return $ret;
    }

}
