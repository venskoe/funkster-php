<?php

namespace Menara\Generic\Console\Interfaces;

interface IWatchdog
{
    /**
     * @param string $command
     * @param int $processCount
     * @return IWatchdog
     */
    public function initialize($command, $processCount = 1);

    /**
     * @return bool
     */
    public function watch();

    /**
     * @return void
     */
    public function restart();

    /**
     * @return void
     */
    public function stop();

    /**
     * @return void
     */
    public function start();

}
