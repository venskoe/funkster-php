<?php

namespace Menara\Generic\Console\Interfaces;

interface ICron
{
    /**
     * @param object[] $configList
     * @return void
     */
    public function execute(array $configList);

}
