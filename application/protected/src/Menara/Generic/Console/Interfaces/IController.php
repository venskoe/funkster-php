<?php

namespace Menara\Generic\Console\Interfaces;

interface IController
{
    /**
     * @return void
     */
    public function setup();

    /**
     * @return void
     */
    public function tick();

}
