<?php

namespace Menara\Generic\Console\Interfaces;

interface IRunner
{
    const DEFAULT_TICK_PERIOD = 10;

    /**
     * @param IController $controller
     * @return void
     */
    public function run(IController $controller);

}
