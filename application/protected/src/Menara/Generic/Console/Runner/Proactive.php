<?php

namespace Menara\Generic\Console\Runner;

use Menara\Generic\Console\Interfaces\IController;
use Menara\Generic\Console\Interfaces\IRunner;

class Proactive implements IRunner
{
    /**
     * @var int
     */
    private $period;

    /**
     * @param int $period
     */
    public function __construct($period = self::DEFAULT_TICK_PERIOD)
    {
        $this->period = $period;
    }

    /**
     * @param IController $controller
     */
    public function run(IController $controller)
    {
        $controller->setup();
        while (true) {
            $controller->tick();
            $this->sleep();
        }
    }

    /**
     * @return void
     */
    private function sleep()
    {
        sleep($this->period);
    }

}
