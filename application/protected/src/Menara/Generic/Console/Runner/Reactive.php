<?php

namespace Menara\Generic\Console\Runner;

use React\EventLoop\LoopInterface;

use Menara\Generic\Console\Interfaces\IController;
use Menara\Generic\Console\Interfaces\IRunner;

class Reactive implements IRunner
{
    /**
     * @var LoopInterface
     */
    public $loop;

    /**
     * @var int
     */
    private $period;

    /**
     * @param int $period
     */
    public function __construct($period = self::DEFAULT_TICK_PERIOD)
    {
        $this->period = $period;
    }

    /**
     * @param IController $controller
     */
    public function run(IController $controller)
    {
        $this->loop->addTimer(0.001, function() use ($controller) {
            $controller->setup();
        });
        $this->loop->addPeriodicTimer($this->period, function() use ($controller) {
            $controller->tick();
        });
        $this->loop->run();
    }

}
