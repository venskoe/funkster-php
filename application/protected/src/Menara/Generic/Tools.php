<?php

namespace Menara\Generic;

class Tools 
{
    /**
     * @param array $map
     * @param array $keyList
     * @return array
     */
    public static function getValueListFromMapSortedByKeyList(array $map, array $keyList)
    {
        $ret = [];
        foreach ($keyList as $key) {
            if (isset($map[$key])) {
                $ret[] = $map[$key];
            }
        }
        return $ret;
    }

    /**
     * @param array $list
     * @param callable $callback
     * @return array
     */
    public static function filter($list, callable $callback = null)
    {
        if (null === $callback) {
            return array_values(array_filter($list));
        }
        else {
            return array_values(array_filter($list, $callback));
        }
    }

    /**
     * @param array $list
     * @param callable $indexGetter
     * @return array
     */
    public static function unique(array $list, callable $indexGetter = null)
    {
        return array_values(self::transformListToDict($list, $indexGetter));
    }

    /**
     * @param array $list
     * @param callable $keyGetter
     * @param callable $valueGetter
     * @return array
     */
    public static function transformListToDict(array $list, callable $keyGetter = null, callable $valueGetter = null)
    {
        if (null === $keyGetter) {
            $keyGetter = self::id();
        }
        if (null === $valueGetter) {
            $valueGetter = self::id();
        }
        $keyList = array_map($keyGetter, $list);
        $valueList = array_map($valueGetter, $list);
        return array_combine($keyList, $valueList);
    }

    /**
     * @return callable
     */
    public static function id()
    {
        return function($anything) {
            return $anything;
        };
    }

    /**
     * @return callable
     */
    public static function bool() {
        return function($anything) {
            return (bool)$anything;
        };
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function stringStartsWith($haystack, $needle)
    {
        return 0 === strpos($haystack, $needle);
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function stringEndsWith($haystack, $needle)
    {
        return strlen($haystack) - strlen($needle) === strrpos($haystack, $needle);
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function stringContains($haystack, $needle)
    {
        return false !== strpos($haystack, $needle);
    }

    /**
     * @param string $text
     * @return string
     * @throws \RuntimeException
     */
    public static function slugify($text)
    {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = transliterator_transliterate('Any-Latin; Latin-ASCII', $text);
        $text = iconv('utf-8', 'ASCII//TRANSLIT//IGNORE', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            throw new \RuntimeException('invalid_data');
        }
        return $text;
    }

}
