<?php

namespace Menara\Auth;

use Menara\Auth\Beans\Session;
use Menara\Auth\Exceptions\InvalidCredentialsException;
use Menara\Auth\Exceptions\InvalidGuidException;
use Menara\Auth\Exceptions\SessionExpiredException;
use Menara\Auth\Exceptions\SessionNotFoundException;
use Menara\Auth\Interfaces\IDao;
use Menara\Auth\Interfaces\ISdb;
use Menara\Auth\Interfaces\IService;

class Service implements IService
{
    /**
     * @var IDao
     */
    public $dao;

    /**
     * @param string $userId
     * @param int $ttl
     * @return Session
     * @throws InvalidCredentialsException
     */
    public function createSession($userId, $ttl)
    {
        $expiredAt = $this->calculateExpireAt($ttl);
        return $this->dao->createSession($userId, $expiredAt);
    }

    /**
     * @param int $ttl
     * @return int
     */
    private function calculateExpireAt($ttl)
    {
        if ($ttl > 0) {
            return time() + $ttl;
        }
        return PHP_INT_MAX;
    }

    /**
     * @param string $token
     * @return Session
     * @throws SessionNotFoundException
     * @throws SessionExpiredException
     */
    public function getSession($token)
    {
        $session = $this->dao->findSessionByToken($token);
        if ($session->getExpiredAt() < time()) {
            throw new SessionExpiredException();
        }
        return $session;
    }

    /**
     * @return Session
     */
    public function getGuestSession()
    {
        $session = new Session();
        $session->token = null;
        $session->userId = null;
        $session->expiredAt = PHP_INT_MAX;
        return $session;
    }

    /**
     * @param Session $session
     * @return int
     * @throws InvalidGuidException
     */
    public function expireSession(Session $session)
    {
        throw new \RuntimeException('NotImplementedError');
    }

}
