<?php

namespace Menara\Auth\Beans;

use Menara\Auth\Interfaces\ISession;

class Session implements ISession
{
    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $userId;

    /**
     * @var int
     */
    public $expiredAt;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

}
