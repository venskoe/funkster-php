<?php

namespace Menara\Auth\Implementations\Dao;

use Menara\Generic\Interfaces\ICipher;

use Menara\Auth\Beans\Session;
use Menara\Auth\Exceptions\SessionNotFoundException;
use Menara\Auth\Interfaces\IDao;

class Encrypted implements IDao
{
    const DELIMITER = ':';

    /**
     * @var ICipher
     */
    public $cipher;

    /**
     * @var object
     */
    public $config;

    /**
     * @param string $token
     * @return Session
     * @throws SessionNotFoundException
     */
    public function findSessionByToken($token)
    {
        if (null === $token) {
            throw new SessionNotFoundException();
        }
        $data = $this->cipher->decrypt($token, $this->config);
        if (null === $data) {
            throw new SessionNotFoundException();
        }
        $data = explode(self::DELIMITER, $data);
        if (count($data) !== 2) {
            throw new SessionNotFoundException();
        }
        list($userId, $expiredAt) = $data;
        $session = $this->createSessionBean($token, $userId, (int)$expiredAt);
        return $session;
    }

    /**
     * @param string $userId
     * @param int $expiredAt
     * @return Session
     */
    public function createSession($userId, $expiredAt)
    {
        $data = implode(self::DELIMITER, [$userId, $expiredAt]);
        $token = $this->cipher->encrypt($data, $this->config);
        return $this->createSessionBean($token, $userId, $expiredAt);
    }

    /**
     * @param string $token
     * @param string $userId
     * @param int $expiredAt
     * @return Session
     */
    private function createSessionBean($token, $userId, $expiredAt)
    {
        $session = new Session();
        $session->token = $token;
        $session->userId = $userId;
        $session->expiredAt = $expiredAt;
        return $session;
    }

}
