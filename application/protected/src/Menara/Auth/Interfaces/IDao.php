<?php

namespace Menara\Auth\Interfaces;

use Menara\Auth\Beans\Session;
use Menara\Auth\Exceptions\SessionNotFoundException;

interface IDao
{
    /**
     * @param string $token
     * @return Session
     * @throws SessionNotFoundException
     */
    public function findSessionByToken($token);

    /**
     * @param string $userId
     * @param int $expiredAt
     * @return Session
     */
    public function createSession($userId, $expiredAt);

}
