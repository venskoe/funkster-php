<?php

namespace Menara\Auth\Interfaces;

use Menara\Auth\Beans\Session;
use Menara\Auth\Exceptions\InvalidCredentialsException;
use Menara\Auth\Exceptions\InvalidGuidException;
use Menara\Auth\Exceptions\SessionExpiredException;
use Menara\Auth\Exceptions\SessionNotFoundException;

interface IService
{
    /**
     * @param string $userId
     * @param int $ttl
     * @return Session
     * @throws InvalidCredentialsException
     */
    public function createSession($userId, $ttl);

    /**
     * @param string $token
     * @return Session
     * @throws SessionNotFoundException
     * @throws SessionExpiredException
     */
    public function getSession($token);

    /**
     * @return Session
     */
    public function getGuestSession();

    /**
     * @param Session $session
     * @return int
     * @throws InvalidGuidException
     */
    public function expireSession(Session $session);

}
