<?php

namespace Menara\Auth\Interfaces;

interface ISession
{
    /**
     * @return string
     */
    public function getToken();

    /**
     * @return string
     */
    public function getUserId();

    /**
     * @return int
     */
    public function getExpiredAt();

}
