<?php

namespace Menara\Web\Interfaces;

interface IProtocol
{
    /**
     * @param int $code
     * @param string $message
     * @return mixed
     */
    public function jsonifyFailureResponse($code, $message);

}
