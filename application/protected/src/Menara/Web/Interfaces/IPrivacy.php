<?php

namespace Menara\Web\Interfaces;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IRequest;

use Menara\Auth\Interfaces\ISession;

interface IPrivacy
{
    /**
     * @param string $userId
     * @return ISession
     */
    public function createSession($userId);

    /**
     * @param IRequest $request
     * @return ISession
     * @throws HttpException
     */
    public function getSession(IRequest $request);

    /**
     * @param IRequest $request
     * @return ISession
     */
    public function getOrCreateSession(IRequest $request);

}
