<?php

namespace Menara\Web;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IRequest;

use Menara\Auth\Interfaces\ISession;
use Menara\Auth\Beans\Session;
use Menara\Auth\Exceptions\InvalidTokenException;

use Menara\Web\Interfaces\IPrivacy;

class Privacy implements IPrivacy
{
    const SESSION_TTL = 604800; // 1 week

    /**
     * @var \Menara\Auth\Interfaces\IService
     */
    public $authService;

    /**
     * @param string $userId
     * @return ISession
     */
    public function createSession($userId)
    {
        return $this->authService->createSession($userId, self::SESSION_TTL);
    }

    /**
     * @param IRequest $request
     * @return Session
     * @throws HttpException
     */
    public function getSession(IRequest $request)
    {
        $token = $request->getString('token');
        try {
            return $this->authService->getSession($token);
        }
        catch (InvalidTokenException $e) {
            throw new HttpException(403, $e->getMessage());
        }
    }

    /**
     * @param IRequest $request
     * @return ISession
     */
    public function getOrCreateSession(IRequest $request)
    {
        $token = $request->getString('token', null);
        if (null === $token) {
            return $this->authService->getGuestSession();
        }
        try {
            return $this->authService->getSession($token);
        }
        catch (InvalidTokenException $e) {
            return $this->authService->getGuestSession();
        }
    }

}
