<?php

namespace Menara\Social;

use Menara\Generic\Tools;
use Menara\Social\Beans\Profile;
use Menara\Social\Exceptions\InvalidFacebookDataException;
use Menara\Social\Interfaces\IService;

class Service implements IService
{
    const FACEBOOK = '//www.facebook.com/';

    /**
     * @var Platforms\Facebook\Client
     */
    public $facebook;

    /**
     * @param string $signedRequest
     * @param string $accessToken
     * @param string $userId
     * @return Profile
     * @throws InvalidFacebookDataException
     */
    public function signinFacebook($signedRequest, $accessToken, $userId)
    {
        return $this->facebook->signin($signedRequest, $accessToken, $userId);
    }

    /**
     * @param string $href
     * @return string
     */
    public function getPermanentHref($href)
    {
        if (Tools::stringContains($href, self::FACEBOOK)) {
            return $this->facebook->getPermanentHref($href);
        }
        return $href;
    }

}
