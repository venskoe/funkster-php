<?php

namespace Menara\Social\Beans;

class Profile
{
    /**
     * @var string
     */
    public $platform;

    /**
     * @var string
     */
    public $userId;

    /**
     * @var string
     */
    public $link;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $smallCoverHref;

    /**
     * @var string
     */
    public $largeCoverHref;

}
