<?php

namespace Menara\Social\Platforms\Facebook;

use Facebook\Entities\SignedRequest;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookSession;
use Facebook\GraphObject;
use Facebook\GraphUser;

use Menara\Social\Beans\Profile;
use Menara\Social\Exceptions\InvalidFacebookDataException;

class Client
{
    /**
     * @var string[]
     */
    private static $userFieldList = ['id', 'link', 'first_name', 'last_name'];

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct($clientId, $clientSecret)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        FacebookSession::setDefaultApplication($this->clientId, $this->clientSecret);
    }

    /**
     * @param string $signedRequest
     * @param string $accessToken
     * @param string $userId
     * @return Profile
     * @throws InvalidFacebookDataException
     */
    public function signin($signedRequest, $accessToken, $userId)
    {
        try {
            $request = new SignedRequest($signedRequest, null, $this->clientSecret);
        }
        catch (FacebookSDKException $e) {
            throw new InvalidFacebookDataException();
        }
        if ($userId !== $request->getUserId()) {
            throw new InvalidFacebookDataException();
        }
        $session = new FacebookSession($accessToken, $request);
        $profile = $this->getMe($session);
        if ($userId !== $profile->getId()) {
            throw new InvalidFacebookDataException();
        }
        return $this->createFacebookProfile($profile);
    }

    /**
     * @param FacebookSession $session
     * @return GraphUser
     */
    private function getMe(FacebookSession $session)
    {
        $request = (new FacebookRequest($session, 'GET', '/me', ['fields' => implode(',', self::$userFieldList)]));
        return $request->execute()->getGraphObject(GraphUser::className());
    }

    /**
     * @param GraphUser $source
     * @return Profile
     */
    private function createFacebookProfile(GraphUser $source)
    {
        $target = new Profile();
        $target->platform = 'facebook';
        $target->userId = (string)$source->getId();
        $target->link = $source->getLink();
        $target->title = implode(' ', [$source->getFirstName(), $source->getLastName()]);
        $target->largeCoverHref = $this->createCoverHref($source->getId(), '400x345');
        $target->smallCoverHref = $this->createCoverHref($source->getId(), '48x48');
        return $target;
    }

    /**
     * @param string $objectId
     * @param String $size
     * @return string
     */
    private function createCoverHref($objectId, $size = null)
    {
        $params = array();
        if ($size !== null) {
            $sizeList = explode('x', $size);
            if (count($sizeList) > 1) {
                $params['width'] = $sizeList[0];
                $params['height'] = $sizeList[1];
            }
            else {
                $params['height'] = $sizeList[0];
            }
        }
        $path = implode('/', [
            FacebookRequest::BASE_GRAPH_URL,
            FacebookRequest::GRAPH_API_VERSION,
            $objectId,
            'picture'
        ]);
        return FacebookRequest::appendParamsToUrl($path, $params);
    }

    /**
     * @param string $href
     * @return string
     */
    public function getPermanentHref($href)
    {
        $href = trim($href, '/');
        $partList = explode('/', $href);
        $objectName = array_pop($partList);
        $session = FacebookSession::newAppSession();
        try {
            $object = $this->getObject($session, $objectName);
        }
        catch (\Exception $e) {
            return $href;
        }
        return implode('/', array_merge($partList, [$object->getProperty('id')]));
    }

    /**
     * @param FacebookSession $session
     * @param string $objectName
     * @return GraphObject
     */
    private function getObject(FacebookSession $session, $objectName)
    {
        $request = (new FacebookRequest($session, 'GET', '/' . $objectName, ['fields' => 'id']));
        return $request->execute()->getGraphObject();
    }

}
