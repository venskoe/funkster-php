<?php

namespace Menara\Social\Interfaces;

use Menara\Social\Beans\Profile;
use Menara\Social\Exceptions\InvalidFacebookDataException;

interface IService
{
    /**
     * @param string $signedRequest
     * @param string $accessToken
     * @param string $userId
     * @return Profile
     * @throws InvalidFacebookDataException
     */
    public function signinFacebook($signedRequest, $accessToken, $userId);

    /**
     * @param string $href
     * @return string
     */
    public function getPermanentHref($href);

}
