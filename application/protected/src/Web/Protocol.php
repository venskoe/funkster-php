<?php

namespace Web;

use Menara\Auth\Interfaces\ISession;

use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\Link;
use Graph\Beans\Entities\Location;
use Graph\Beans\Entities\News;
use Graph\Beans\Entities\Photo;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Post;
use Graph\Beans\Entities\Profile;
use Graph\Beans\Entities\Tag;
use Graph\Beans\EventListPage;

use Menara\Web\Interfaces\IProtocol;

class Protocol implements IProtocol
{
    /**
     * @param object $config
     * @return array
     */
    public function jsonifyPublicConfig($config)
    {
        return array(
            'config' => $config,
        );
    }

    /**
     * @param object $page
     * @return array
     */
    public function jsonifyPage($page)
    {
        return array(
            'page' => $page,
        );
    }

    /**
     * @param EventListPage $page
     * @return array
     */
    public function jsonifyEventListPage(EventListPage $page)
    {
        return array(
            'location' => $this->jsonifyLocation($page->location),
            'eventList' => array_map([$this, 'jsonifyEventDetails'], $page->eventList),
        );
    }

    /**
     * @param Location $location
     * @return array
     */
    public function jsonifyLocation(Location $location)
    {
        return array(
            'id' => $location->id,
            'title' => $location->title,
            'latitude' => $location->latitude,
            'longitude' => $location->longitude,
        );
    }

    /**
     * @param ISession $session
     * @return array
     */
    public function jsonifySession(ISession $session)
    {
        return array(
            'token' => $session->getToken(),
            'userId' => $session->getUserId(),
            'expiredAt' => $session->getExpiredAt(),
            'duration' => max(0, $session->getExpiredAt() - time()),
        );
    }

    /**
     * @param Event $event
     * @return array
     */
    public function jsonifyEventDetails(Event $event)
    {
        return array_merge($this->jsonifyEvent($event), array(
            'cover' => $this->jsonifyPhoto($event->cover),
            'place' => $this->jsonifyPlace($event->place),
            'tagList' => array_map([$this, 'jsonifyTag'], $event->tagList),
            'artistList' => array_map([$this, 'jsonifyProfileDetails'], $event->artistList),
        ));
    }

    /**
     * @param Event $event
     * @return array
     */
    public function jsonifyEvent(Event $event)
    {
        return array(
            'id' => $event->id,
            'title' => $event->title,
            'tzOffset' => $event->tzOffset,
            'startsAt' => $event->startsAt,
            'endsAt' => $event->endsAt,
        );
    }

    /**
     * @param News $news
     * @return array
     */
    public function jsonifyNews(News $news)
    {
        return array(
            'createdAt' => $news->createdAt,
            'title' => $news->title,
            'content' => $news->content,
        );
    }

    /**
     * @param Tag $tag
     * @return array
     */
    public function jsonifyTag(Tag $tag)
    {
        return array(
            'id' => $tag->id,
            'title' => $tag->title,
        );
    }

    /**
     * @param Place $place
     * @return array
     */
    public function jsonifyPlace(Place $place)
    {
        return array(
            'id' => $place->id,
            'title' => $place->title,
            'intro' => $place->intro,
            'address' => $place->address,
            'latitude' => $place->latitude,
            'longitude' => $place->longitude,
            'instagram' => $place->instagram,
        );
    }

    /**
     * @param Post $post
     * @return array
     */
    public function jsonifyPost(Post $post)
    {
        return array(
            'id' => $post->id,
            'type' => $post->type,
            'href' => $post->href,
            'cover' => $post->cover,
            'title' => $post->title,
            'source' => $post->source,
            'postedAt' => $post->postedAt,
            'author' => $this->jsonifyProfileDetails($post->author),
        );
    }

    /**
     * @param Profile $profile
     * @return array
     */
    public function jsonifyProfileDetails(Profile $profile)
    {
        $data = $this->jsonifyProfile($profile);
        return array_merge($data, array(
            'cover' => $this->jsonifyPhoto($profile->cover),
            'linkList' => array_map([$this, 'jsonifyLink'], $profile->linkList),
        ));
    }

    /**
     * @param Profile $profile
     * @return array
     */
    public function jsonifyProfile(Profile $profile)
    {
        return array(
            'id' => $profile->id,
            'title' => $profile->title,
            'intro' => $profile->intro,
            'about' => $profile->about,
            'hometown' => $profile->hometown,
        );
    }

    /**
     * @param Photo $photo
     * @return array
     */
    public function jsonifyPhoto(Photo $photo)
    {
        return array(
            'large' => $photo->large,
            'small' => $photo->small,
        );
    }

    /**
     * @param Link $link
     * @return array
     */
    public function jsonifyLink(Link $link)
    {
        return array(
            'href' => $link->href,
            'site' => $link->site,
            'hide' => $link->hide,
            'used' => $link->used,
        );
    }

    /**
     * @param int $code
     * @param string $message
     * @return mixed
     */
    public function jsonifyFailureResponse($code, $message)
    {
        return array(
            'data' => null,
            'code' => $code,
            'message' => $message,
        );
    }
}
