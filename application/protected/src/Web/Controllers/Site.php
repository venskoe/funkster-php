<?php

namespace Web\Controllers;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IRequest;

use Graph\Beans\ArtistPage;
use Graph\Beans\EventListPage;
use Graph\Beans\EventPage;
use Graph\Beans\GeoIndexPage;
use Graph\Beans\PlacePage;
use Graph\Beans\EmptyPage;
use Graph\Beans\UserPage;
use Graph\Consts;
use Graph\Exceptions\ArtistNotFoundException;
use Graph\Exceptions\EventNotFoundException;
use Graph\Exceptions\LocationNotFoundException;
use Graph\Exceptions\PlaceNotFoundException;
use Graph\Exceptions\UserNotFoundException;

class Site
{
    const EVENT_LIST_MAX_DISTANCE = 50;
    const EVENT_LIST_LIMIT = 100;
    const EVENT_RELATED_EVENT_LIMIT = 3;
    const EVENT_COMMENT_LIMIT = 10;
    const PAST_EVENT_LIMIT = 10;
    const USER_EVENT_LIMIT = 3;
    const USER_PLACE_LIMIT = 3;
    const USER_ARTIST_LIMIT = 3;

    /**
     * @var object
     */
    public $config;

    /**
     * @var Consts
     */
    public $graphConsts;

    /**
     * @var \Menara\Web\Interfaces\IPrivacy
     */
    public $privacy;

    /**
     * @var \Graph\Interfaces\IService
     */
    public $graphService;

    /**
     * @var \Phalcon\Mvc\UrlInterface
     */
    public $url;

    /**
     * @Get('/en/config.js')
     * @Header('Content-Type', 'text/javascript')
     * @JsonifyItem('jsonifyPublicConfig')
     * @RenderView('config.js')
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @Get('/sitemap.xml')
     * @Header('Content-Type', 'application/xml; charset="utf-8"')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('sitemap.xml')
     *
     * @param IRequest $request
     * @return mixed
     */
    public function getSitemapPage(IRequest $request)
    {
        return $this->graphService->getSitemapPage();
    }

    /**
     * @Get('/en/about', name='get-about-page')
     * @InjectAttr('user', 'getSessionUserPageOrNull')
     * @InjectAttr('consts', 'getGraphConsts')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('about')
     *
     * @param IRequest $request
     * @return EmptyPage
     */
    public function getAboutPage(IRequest $request)
    {
        return $this->graphService->getEmptyPage();
    }

    /**
     * @Get('/en', name='get-search-page')
     * @InjectAttr('user', 'getSessionUserPageOrNull')
     * @InjectAttr('consts', 'getGraphConsts')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('events/search')
     *
     * @param IRequest $request
     * @return EmptyPage
     */
    public function getSearchPage(IRequest $request)
    {
        return $this->graphService->getEmptyPage();
    }

    /**
     * @Get('/en/events-in-{locationId:[\w-]+}', name='get-event-list-page-by-location')
     * @Except('Graph\Exceptions\LocationNotFoundException', 404, 'location_not_found')
     * @InjectAttr('user', 'getSessionUserPageOrNull')
     * @InjectAttr('consts', 'getGraphConsts')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('events/list')
     *
     * @param IRequest $request
     * @return EventListPage
     * @throws HttpException
     * @throws LocationNotFoundException
     */
    public function getEventListPageByLocation(IRequest $request)
    {
        $locationId = $request->getString('locationId');
        $distance = $request->getLimit('distance', self::EVENT_LIST_MAX_DISTANCE);
        $limit = $request->getLimit('limit', self::EVENT_LIST_LIMIT);
        return $this->graphService->getEventListPageByLocation($locationId, $distance, $limit);
    }

    /**
     * @Get('/en/events-in-the-world', name='get-geo-index-page')
     * @InjectAttr('user', 'getSessionUserPageOrNull')
     * @InjectAttr('consts', 'getGraphConsts')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('indexes/geo')
     *
     * @param IRequest $request
     * @return GeoIndexPage
     */
    public function getGeoIndexPage(IRequest $request)
    {
        return $this->graphService->getGeoIndexPage();
    }

    /**
     * @Get('/en/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}', name='get-event-page')
     * @Except('Graph\Exceptions\EventNotFoundException', 404, 'event_not_found')
     * @InjectAttr('user', 'getSessionUserPageOrNull')
     * @InjectAttr('consts', 'getGraphConsts')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('events/item')
     *
     * @param IRequest $request
     * @return EventPage
     * @throws HttpException
     * @throws EventNotFoundException
     */
    public function getEventPage(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $userId = $this->privacy->getOrCreateSession($request)->getUserId();
        try {
            return $this->graphService->getEventPage(
                $userId, $eventId, self::EVENT_RELATED_EVENT_LIMIT, self::EVENT_COMMENT_LIMIT
            );
        }
        catch (EventNotFoundException $e) {
            $eventId = $this->graphService->getEventIdByLegacy($eventId);
            throw new HttpException(301, 'event_moved_permanently', array(
                'Location' => $this->url->get(array('for' => 'get-event-page', 'eventId' => $eventId)),
            ));
        }
    }

    /**
     * @Get('/en/places/{placeId:[\w-]+}', name='get-place-page')
     * @Except('Graph\Exceptions\PlaceNotFoundException', 404, 'place_not_found')
     * @InjectAttr('user', 'getSessionUserPageOrNull')
     * @InjectAttr('consts', 'getGraphConsts')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('places/item')
     *
     * @param IRequest $request
     * @return PlacePage
     * @throws HttpException
     * @throws PlaceNotFoundException
     */
    public function getPlacePage(IRequest $request)
    {
        $placeId = $request->getString('placeId');
        $userId = $this->privacy->getOrCreateSession($request)->getUserId();
        try {
            return $this->graphService->getPlacePage($userId, $placeId, self::PAST_EVENT_LIMIT);
        }
        catch (PlaceNotFoundException $e) {
            $placeId = $this->graphService->getPlaceIdByLegacy($placeId);
            throw new HttpException(301, 'place_moved_permanently', array(
                'Location' => $this->url->get(array('for' => 'get-place-page', 'placeId' => $placeId)),
            ));
        }
    }

    /**
     * @Get('/en/artists/{artistId:[\w-]+}', name='get-artist-page')
     * @Except('Graph\Exceptions\ArtistNotFoundException', 404, 'artist_not_found')
     * @InjectAttr('user', 'getSessionUserPageOrNull')
     * @InjectAttr('consts', 'getGraphConsts')
     * @JsonifyItem('jsonifyPage')
     * @RenderView('artists/item')
     *
     * @param IRequest $request
     * @return ArtistPage
     * @throws HttpException
     * @throws ArtistNotFoundException
     */
    public function getArtistPage(IRequest $request)
    {
        $artistId = $request->getString('artistId');
        $userId = $this->privacy->getOrCreateSession($request)->getUserId();
        try {
            return $this->graphService->getArtistPage($userId, $artistId, self::PAST_EVENT_LIMIT);
        }
        catch (ArtistNotFoundException $e) {
            $artistId = $this->graphService->getArtistIdByLegacy($artistId);
            throw new HttpException(301, 'artist_moved_permanently', array(
                'Location' => $this->url->get(array('for' => 'get-artist-page', 'artistId' => $artistId)),
            ));
        }
    }

    /**
     * @param IRequest $request
     * @return UserPage
     */
    public function getSessionUserPageOrNull(IRequest $request)
    {
        $userId = $this->privacy->getOrCreateSession($request)->getUserId();
        try {
            return $this->graphService->getUserPage(
                $userId, self::USER_EVENT_LIMIT, self::USER_PLACE_LIMIT, self::USER_ARTIST_LIMIT
            );
        }
        catch (UserNotFoundException $e) {
            return null;
        }
    }

    /**
     * @param IRequest $request
     * @return Consts
     */
    public function getGraphConsts(IRequest $request)
    {
        return $this->graphConsts;
    }

}
