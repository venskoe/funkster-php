<?php

namespace Web\Controllers;

use Menara\Generic\Exceptions\HttpException;
use Menara\Generic\Interfaces\IRequest;
use Menara\Auth\Interfaces\ISession;

use Graph\Beans\ArtistPage;
use Graph\Beans\Entities\Event;
use Graph\Beans\Entities\News;
use Graph\Beans\Entities\Photo;
use Graph\Beans\Entities\Place;
use Graph\Beans\Entities\Post;
use Graph\Beans\Entities\Profile;
use Graph\Beans\EventListPage;
use Graph\Beans\EventPage;
use Graph\Beans\PlacePage;
use Graph\Exceptions\ArtistAlreadyExistsException;
use Graph\Exceptions\ArtistNotFoundException;
use Graph\Exceptions\EventNotFoundException;
use Graph\Exceptions\InvalidArtistDataException;
use Graph\Exceptions\InvalidDataException;
use Graph\Exceptions\InvalidEventDataException;
use Graph\Exceptions\InvalidPlaceDataException;
use Graph\Exceptions\LocationNotFoundException;
use Graph\Exceptions\PlaceNotFoundException;
use Graph\Exceptions\WriteAccessException;

/**
 * @RoutePrefix('/api/v1')
 */
class Rest 
{
    const ARTIST_SEARCH_LIMIT = 5;
    const PLACE_SEARCH_LIMIT = 5;
    const EVENT_LIST_MAX_DISTANCE = 50;
    const EVENT_LIST_LIMIT = 100;
    const EVENT_RELATED_EVENT_LIMIT = 3;
    const EVENT_COMMENT_LIMIT = 10;
    const PAST_EVENT_LIMIT = 10;
    const USER_EVENT_LIMIT = 3;
    const USER_PLACE_LIMIT = 3;
    const USER_ARTIST_LIMIT = 3;

    /**
     * @var \Menara\Web\Interfaces\IPrivacy
     */
    public $privacy;

    /**
     * @var \Menara\Social\Interfaces\IService
     */
    public $socialService;

    /**
     * @var \Graph\Interfaces\IService
     */
    public $graphService;

    /**
     * @var \Files\Interfaces\IService
     */
    public $filesService;

    /**
     * @var \Media\Interfaces\IService
     */
    private $mediaService;

    /**
     * @Post('/users/signin/facebook')
     * @JsonifyItem('jsonifySession')
     * @RenderJson
     *
     * @param IRequest $request
     * @return ISession
     */
    public function signinFacebook(IRequest $request)
    {
        $signedRequest = $request->getString('signedRequest');
        $accessToken = $request->getString('accessToken');
        $userId = $request->getString('userID');
        $profile = $this->socialService->signinFacebook($signedRequest, $accessToken, $userId);
        $user = $this->graphService->getOrCreateSocialUser($profile);
        return $this->privacy->createSession($user->id);
    }

    /**
     * @Get('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}')
     * @Except('Graph\Exceptions\EventNotFoundException', 404, 'event_not_found')
     * @RenderJson
     *
     * @param IRequest $request
     * @return EventPage
     * @throws HttpException
     * @throws EventNotFoundException
     */
    public function getEventPage(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $userId = $this->privacy->getOrCreateSession($request)->getUserId();
        return $this->graphService->getEventPage(
            $userId, $eventId, self::EVENT_RELATED_EVENT_LIMIT, self::EVENT_COMMENT_LIMIT
        );
    }

    /**
     * @Post('/events/')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidEventDataException', 400, 'invalid_event_data')
     * @JsonifyItem('jsonifyEvent')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Event
     * @throws HttpException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function createEvent(IRequest $request)
    {
        $title = $request->getString('title');
        $title = $this->filterTitle($title);
        $tzOffset = $request->getInteger('tzOffset');
        $startsAt = $request->getTimestamp('startsAt');
        $endsAt = $request->getTimestamp('endsAt');
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->createEvent($userId, $title, $tzOffset, $startsAt, $endsAt);
    }

    /**
     * @Post('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'event_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidEventDataException', 400, 'invalid_event_data')
     * @JsonifyItem('jsonifyEvent')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Event
     * @throws HttpException
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEvent(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $title = $request->getString('title');
        $title = $this->filterTitle($title);
        $tzOffset = $request->getInteger('tzOffset');
        $startsAt = $request->getTimestamp('startsAt');
        $endsAt = $request->getTimestamp('endsAt');
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateEvent($userId, $eventId, $title, $tzOffset, $startsAt, $endsAt);
    }

    /**
     * @Post('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}/cover')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'event_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidEventDataException', 400, 'invalid_event_data')
     * @JsonifyItem('jsonifyPhoto')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Photo
     * @throws HttpException
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventCover(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $large = $request->getString('large');
        $large = $this->filterImage($large);
        $small = $request->getString('small');
        $small = $this->filterImage($small);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateEventCover($userId, $eventId, $large, $small);
    }

    /**
     * @Post('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}/place')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'event_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidEventDataException', 400, 'invalid_event_data')
     * @JsonifyItem('jsonifyPlace')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Place
     * @throws HttpException
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventPlace(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $place = $request->getString('place');
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateEventPlace($userId, $eventId, $place);
    }

    /**
     * @Post('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}/news/{createdAt:[\d]+}/delete')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'event_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidEventDataException', 400, 'invalid_event_data')
     * @JsonifyItem('jsonifyNews')
     * @RenderJson
     *
     * @param IRequest $request
     * @return News
     * @throws HttpException
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function deleteEventNews(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $createdAt = $request->getInteger('createdAt');
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->deleteEventNews($userId, $eventId, $createdAt);
    }

    /**
     * @Post('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}/news')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'event_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidEventDataException', 400, 'invalid_event_data')
     * @JsonifyItem('jsonifyNews')
     * @RenderJson
     *
     * @param IRequest $request
     * @return News
     * @throws HttpException
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventNews(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $createdAt = $request->getInteger('createdAt', time());
        $title = $request->getString('title');
        $title = $this->filterTitle($title);
        $content = $request->getString('content');
        $content = $this->filterAbout($content);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateEventNews($userId, $eventId, $createdAt, $title, $content);
    }

    /**
     * @Post('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}/tags')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'event_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidDataException', 400, 'invalid_event_data')
     * @JsonifyList('jsonifyTag')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Profile
     * @throws HttpException
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updateEventTagList(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $tagList = $request->getListOfString('tagList', []);
        $tagList = array_map([$this, 'filterTitle'], $tagList);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateEventTagList($userId, $eventId, $tagList);
    }

    /**
     * @Post('/events/{eventId:\d{4}/\d{2}/\d{2}/[\w-]+}/artists')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'event_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidEventDataException', 400, 'invalid_event_data')
     * @JsonifyList('jsonifyProfileDetails')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Profile[]
     * @throws HttpException
     * @throws EventNotFoundException
     * @throws WriteAccessException
     * @throws InvalidEventDataException
     */
    public function updateEventArtistList(IRequest $request)
    {
        $eventId = $request->getString('eventId');
        $artistList = $request->getListOfString('artistList');
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateEventArtistList($userId, $eventId, $artistList);
    }

    /**
     * @Get('/events/search/ip')
     * @Except('Graph\Exceptions\LocationNotFoundException', 404, 'location_not_found')
     * @JsonifyItem('jsonifyEventListPage')
     * @RenderJson
     *
     * @param IRequest $request
     * @return EventListPage
     * @throws HttpException
     * @throws LocationNotFoundException
     */
    public function getEventListPageByIp(IRequest $request)
    {
        $ip = $request->getString('clientAddress');
        $ip = $this->filterIp($ip);
        $distance = $request->getLimit('distance', self::EVENT_LIST_MAX_DISTANCE);
        $limit = $request->getLimit('limit', self::EVENT_LIST_LIMIT);
        return $this->graphService->getEventListPageByIp($ip, $distance, $limit);
    }

    /**
     * @Get('/events/search/position')
     * @JsonifyItem('jsonifyEventListPage')
     * @RenderJson
     *
     * @param IRequest $request
     * @return EventListPage
     * @throws HttpException
     */
    public function getEventListByPosition(IRequest $request)
    {
        $latitude = $request->getFloat('latitude');
        $longitude = $request->getFloat('longitude');
        $distance = $request->getLimit('distance', self::EVENT_LIST_MAX_DISTANCE);
        $limit = $request->getLimit('limit', self::EVENT_LIST_LIMIT);
        return $this->graphService->getEventListPageByPosition($latitude, $longitude, $distance, $limit);
    }

    /**
     * @Get('/places/{placeId:[\w-]+}')
     * @Except('Graph\Exceptions\PlaceNotFoundException', 404, 'place_not_found')
     * @RenderJson
     *
     * @param IRequest $request
     * @return PlacePage
     * @throws HttpException
     * @throws PlaceNotFoundException
     */
    public function getPlacePage(IRequest $request)
    {
        $placeId = $request->getString('placeId');
        $userId = $this->privacy->getOrCreateSession($request)->getUserId();
        return $this->graphService->getPlacePage($userId, $placeId, self::PAST_EVENT_LIMIT);
    }

    /**
     * @Post('/places/')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidPlaceDataException', 400, 'invalid_place_data')
     * @JsonifyItem('jsonifyPlace')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Place
     * @throws HttpException
     * @throws WriteAccessException
     * @throws InvalidPlaceDataException
     */
    public function createPlace(IRequest $request)
    {
        $title = $request->getString('title');
        $title = $this->filterTitle($title);
        $intro = $request->getString('intro', '');
        $intro = $this->filterAbout($intro);
        $address = $request->getString('address', '');
        $address = $this->filterIntro($address);
        $latitude = $request->getFloat('latitude');
        $longitude = $request->getFloat('longitude');
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->createPlace($userId, $title, $intro, $address, $latitude, $longitude);
    }

    /**
     * @Post('/places/{placeId:[\w-]+}')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'place_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidPlaceDataException', 400, 'invalid_place_data')
     * @JsonifyItem('jsonifyPlace')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Place
     * @throws HttpException
     * @throws PlaceNotFoundException
     * @throws WriteAccessException
     * @throws InvalidPlaceDataException
     */
    public function updatePlace(IRequest $request)
    {
        $placeId = $request->getString('placeId');
        $title = $request->getString('title');
        $title = $this->filterTitle($title);
        $intro = $request->getString('intro', '');
        $intro = $this->filterAbout($intro);
        $address = $request->getString('address', '');
        $address = $this->filterIntro($address);
        $latitude = $request->getFloat('latitude');
        $longitude = $request->getFloat('longitude');
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updatePlace($userId, $placeId, $title, $intro, $address, $latitude, $longitude);
    }

    /**
     * @Post('/places/{placeId:[\w-]+}/links')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'place_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidDataException', 400, 'invalid_place_data')
     * @JsonifyList('jsonifyLink')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Profile
     * @throws HttpException
     * @throws PlaceNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updatePlaceLinkList(IRequest $request)
    {
        $placeId = $request->getString('placeId');
        $linkList = $request->getListOfString('linkList', []);
        $linkList = $this->filterLinkList($linkList);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updatePlaceLinkList($userId, $placeId, $linkList);
    }

    /**
     * @Get('/places/search/{search:[\w\s-]{1,4}}')
     * @Except('Graph\Exceptions\InvalidPlaceDataException', 400, 'invalid_place_data')
     * @JsonifyList('jsonifyPlace')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Place[]
     * @throws HttpException
     * @throw InvalidPlaceDataException
     */
    public function searchPlaceList(IRequest $request)
    {
        $search = $request->getString('search');
        return $this->graphService->searchPlaceList($search, self::PLACE_SEARCH_LIMIT);
    }

    /**
     * @Get('/artists/{artistId:[\w-]+}', name='get-artist-page')
     * @Except('Graph\Exceptions\ArtistNotFoundException', 404, 'artist_not_found')
     * @RenderJson
     *
     * @param IRequest $request
     * @return ArtistPage
     * @throws HttpException
     * @throws ArtistNotFoundException
     */
    public function getArtistPage(IRequest $request)
    {
        $artistId = $request->getString('artistId');
        $userId = $this->privacy->getOrCreateSession($request)->getUserId();
        return $this->graphService->getArtistPage($userId, $artistId, self::PAST_EVENT_LIMIT);
    }

    /**
     * @Post('/artists/')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\EntityAlreadyExistsException', 400, 'invalid_artist_data')
     * @Except('Graph\Exceptions\InvalidArtistDataException', 400, 'invalid_artist_data')
     * @JsonifyItem('jsonifyProfile')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Profile
     * @throws HttpException
     * @throws WriteAccessException
     * @throws ArtistAlreadyExistsException
     * @throws InvalidArtistDataException
     */
    public function createArtist(IRequest $request)
    {
        $title = $request->getString('title');
        $title = $this->filterTitle($title);
        $intro = $request->getString('intro', '');
        $intro = $this->filterIntro($intro);
        $about = $request->getString('about', '');
        $about = $this->filterAbout($about);
        $hometown = $request->getString('hometown');
        $hometown = $this->filterIntro($hometown);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->createArtist($userId, $title, $intro, $about, $hometown);
    }

    /**
     * @Post('/artists/{artistId:[\w-]+}')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'artist_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidArtistDataException', 400, 'invalid_artist_data')
     * @JsonifyItem('jsonifyProfile')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Profile
     * @throws HttpException
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     */
    public function updateArtist(IRequest $request)
    {
        $artistId = $request->getString('artistId');
        $title = $request->getString('title');
        $title = $this->filterTitle($title);
        $intro = $request->getString('intro');
        $intro = $this->filterIntro($intro);
        $about = $request->getString('about', '');
        $about = $this->filterAbout($about);
        $hometown = $request->getString('hometown');
        $hometown = $this->filterIntro($hometown);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateArtist($userId, $artistId, $title, $intro, $about, $hometown);
    }

    /**
     * @Post('/artists/{artistId:[\w-]+}/cover')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'artist_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidArtistDataException', 400, 'invalid_artist_data')
     * @JsonifyItem('jsonifyPhoto')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Photo
     * @throws HttpException
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidArtistDataException
     */
    public function updateArtistCover(IRequest $request)
    {
        $artistId = $request->getString('artistId');
        $large = $request->getString('large');
        $large = $this->filterImage($large);
        $small = $request->getString('small');
        $small = $this->filterImage($small);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateArtistCover($userId, $artistId, $large, $small);
    }

    /**
     * @Post('/artists/{artistId:[\w-]+}/links')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'artist_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidDataException', 400, 'invalid_artist_data')
     * @JsonifyList('jsonifyLink')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Profile
     * @throws HttpException
     * @throws ArtistNotFoundException
     * @throws WriteAccessException
     * @throws InvalidDataException
     */
    public function updateArtistLinkList(IRequest $request)
    {
        $artistId = $request->getString('artistId');
        $linkList = $request->getListOfString('linkList', []);
        $linkList = $this->filterLinkList($linkList);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->graphService->updateArtistLinkList($userId, $artistId, $linkList);
    }

    /**
     * @Post('/artists/{artistId:[\w-]+}/posts')
     * @Except('Graph\Exceptions\EntityNotFoundException', 404, 'artist_not_found')
     * @Except('Graph\Exceptions\WriteAccessException', 403, 'write_access_required')
     * @Except('Graph\Exceptions\InvalidDataException', 400, 'invalid_post_data')
     * @Except('Media\Exceptions\InvalidDataException', 400, 'invalid_post_data')
     * @JsonifyItem('jsonifyPost')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Post
     * @throws HttpException
     * @throws ArtistNotFoundException
     * @throws InvalidDataException
     */
    public function createArtistPost(IRequest $request)
    {
        $artistId = $request->getString('artistId');
        $href = $request->getString('href');
        $title = $request->getString('title', null);
        $userId = $this->privacy->getSession($request)->getUserId();
        return $this->createPost($userId, $artistId, $href, $title, [$this->graphService, 'createArtistPost']);
    }

    /**
     * @param string $userId
     * @param string $entityId
     * @param string $href
     * @param String $title
     * @param callable $createPostCallable
     * @return Post
     */
    private function createPost($userId, $entityId, $href, $title, $createPostCallable)
    {
        $media = $this->mediaService->parse($href);
        return call_user_func_array($createPostCallable, [
            $userId,
            $entityId,
            $media->guid,
            $media->source,
            $media->type,
            $media->href,
            $media->cover,
            $title
        ]);
    }

    /**
     * @Get('/artists/search/{search:[\w\s-]{1,4}}')
     * @Except('Graph\Exceptions\InvalidArtistDataException', 400, 'invalid_artist_data')
     * @JsonifyList('jsonifyProfileDetails')
     * @RenderJson
     *
     * @param IRequest $request
     * @return Profile[]
     * @throws HttpException
     * @throw InvalidArtistDataException
     */
    public function searchArtistList(IRequest $request)
    {
        $search = $request->getString('search');
        return $this->graphService->searchArtistList($search, self::ARTIST_SEARCH_LIMIT);
    }

    /**
     * @param string $title
     * @return string
     */
    private function filterTitle($title)
    {
        return trim(strip_tags(html_entity_decode($title)));
    }

    /**
     * @param string $intro
     * @return string
     */
    private function filterIntro($intro)
    {
        return trim(strip_tags(html_entity_decode($intro)));
    }

    /**
     * @param string $about
     * @return string
     */
    private function filterAbout($about)
    {
        return trim(strip_tags(html_entity_decode($about), '<div><br>'));
    }

    /**
     * @param string $image
     * @return string
     * @throws HttpException
     */
    private function filterImage($image)
    {
        $matches = array();
        if (preg_match('#^data:image/(\w+);base64,(.*)$#', $image, $matches)) {
            $filename = implode('.', [md5($matches[2]), $matches[1]]);
            return $this->filesService->uploadBase64File($filename, $matches[2]);
        }
        elseif (preg_match('#^http#', $image)) {
            return $image;
        }
        throw new HttpException(400, 'invalid_image_data');
    }

    /**
     * @param string $ip
     * @return string
     * @throws HttpException
     */
    private function filterIp($ip)
    {
        $list = array_filter(array_map('intval' ,explode('.', $ip)), function($i) { return abs($i) < 256; });
        if (count($list) !== 4) {
            throw new HttpException(400, 'invalid_ip_data');
        }
        foreach ($list as $item) {
            if ($item > 255) {
                throw new HttpException(400, 'invalid_ip_data');
            }
        }
        return $ip;
    }

    /**
     * @param string[] $linkList
     * @return string[]
     */
    private function filterLinkList(array $linkList)
    {
        return array_map([$this->socialService, 'getPermanentHref'], $linkList);
    }

}
