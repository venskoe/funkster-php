#!/usr/bin/env php
<?php

require_once(__DIR__ . '/../../vendor/autoload.php');
require_once(__DIR__ . '/src/Menara/Generic/Phalcon/Application/Cli.php');

$di = new Phalcon\DI\FactoryDefault();
$app = new \Menara\Generic\Phalcon\Application\Cli($di);
# @TODO: parse environment from command line
$app->initialize(__DIR__, 'cordis');
$app->handle(__FILE__, $argv);
