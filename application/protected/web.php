<?php

define('REPEATER_ID', 1);

require_once(__DIR__ . '/../../vendor/autoload.php');
require_once(__DIR__ . '/src/Menara/Generic/Phalcon/Application/Web.php');

$di = new Phalcon\DI\FactoryDefault();
$app = new \Menara\Generic\Phalcon\Application\Web($di);
$app->initialize(__DIR__, $_SERVER['ENVIRONMENT']);
$app->handle();
